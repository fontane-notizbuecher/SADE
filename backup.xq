xquery version "3.1";

let $params :=
 <parameters>
   <param name="output" value="backup"/>
   <param name="zip" value="no"/>
   <param name="backup" value="yes"/>
   <param name="incremental" value="yes"/>
 </parameters>
return
    system:trigger-system-task("org.exist.storage.ConsistencyCheckTask", $params)
