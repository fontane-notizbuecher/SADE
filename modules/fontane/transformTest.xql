xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "/db/apps/SADE/modules/fontane/transform.xqm";

let $uri := "2128d"
let $doc := doc( "/db/sade-projects/textgrid/data/xml/data/" || $uri || ".xml" )
let $surface := "1v"

let $tei :=
    <TEI xmlns="http://www.tei-c.org/ns/1.0">
        {$doc//tei:teiHeader}
        <sourceDoc n="{string($doc//tei:sourceDoc/@n)}">
            {$doc//tei:surface[@n = $surface]}
        </sourceDoc>
    </TEI>

return
    fontaneTransfo:magic($tei)
