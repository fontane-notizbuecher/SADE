xquery version "3.1";
(:~
: This module prepares the index html.
: It is used for preparing a cached version and also called when preparing
: the index for a single notebook.
: The corresponding viewer module is `index-viewer.xqm`.
: @author Mathias Göbel
: @version 1.1.3
: @see https://fontane-nb.dariah.eu/register.html
:)
module namespace ixp="http://fontane-nb.dariah.eu/index-processor";
import module namespace code="http://bdn-edition.de/ns/code-view";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc" at "misc.xqm";
import module namespace functx="http://www.functx.com";

declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace gndo="http://d-nb.info/standards/elementset/gnd#";
declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $ixp:metaCollection := '/db/sade-projects/textgrid/data/xml/meta/';
declare variable $ixp:targetStart := "http://textgridrep.org/textgrid:";
(: try/catch here, to pass the tests where no request object is available :)
declare variable $ixp:getEntity := try {request:get-parameter('e', '')} catch * {""};
declare variable $ixp:getIndex := try {request:get-parameter('i', '')} catch * {""};
declare variable $ixp:notebookParam := try {request:get-parameter('nb', '')} catch * {""};
declare variable $ixp:getNotebook := $ixp:targetStart || $ixp:notebookParam;

declare variable $ixp:dataCollection := collection($config:data-root || "/data");
declare variable $ixp:dataPath := '/db/sade-projects/textgrid/data/xml/data/';

(:~
 : Helper function to collect all main nodes from the index files.
:)
declare function ixp:getNodes()
as array(*) {
let $agg := '253st.xml' (: TextGrid Aggregation containing all index files :)
let $uris := (doc('/db/sade-projects/textgrid/data/xml/agg/' || $agg)
          //ore:aggregates/substring-after(@rdf:resource, 'textgrid:')
        )[. != "25547"] (: omit the bibliography here :)
let $docs := $uris ! doc($ixp:dataPath || . || '.xml')

return
    [
    $docs//tei:body/tei:listEvent,
    $docs//tei:body/tei:listOrg,
    $docs//tei:body/tei:listPerson,
    $docs//tei:body/tei:listPlace,
    $docs//tei:body//tei:list[@type="works"],
    $docs//tei:body//tei:list[@type="periodicals"],
    $docs//tei:body//tei:list[@type="Fontane"]
    ]
};

(:~
 : the list type an entity is listed in.
 : This is used when preparing the html resource name.
 : @param $entity On of tei:item, tei:place, tei:person, tei:gersonGrp,
 : tei:event or tei:org
 : @return on of listEvent, listOrg, listPlace, listPerson, list-works,
 : list-Fontane, list-periodicals
 :)
declare function ixp:get-list-by-entity($entity as node()?)
as xs:string {
switch ( $entity/local-name() )
    case "event" return "listEvent"
    case "org" return "listOrg"
    case "person" return
        if($entity/ancestor::tei:item)
        then ixp:get-list-by-entity( $entity/ancestor::tei:item[last()] )
        else "listPerson"
    case "personGrp" return
        if($entity/ancestor::tei:item)
        then ixp:get-list-by-entity( $entity/ancestor::tei:item[last()] )
        else "listPerson"
    case "place" return "listPlace"
    default return
        let $list := ($entity/ancestor::tei:list[@type])[last()]
        return
            $list/local-name() || "-" || string($list/@type)
};

(:~
 : Returns the prefix used in the TEI documents to refere to a index file.
 : @param $entity an element from the index (e.g. tei:person)
 :)
declare function ixp:get-prefix-by-entity($entity as element(*))
as xs:string {
  switch($entity/base-uri())
  case "/db/sade-projects/textgrid/data/xml/data/253t0.xml"
    return "eve"
  case "/db/sade-projects/textgrid/data/xml/data/253t2.xml"
    return "plc"
  case "/db/sade-projects/textgrid/data/xml/data/253t1.xml"
    return "org"
  case "/db/sade-projects/textgrid/data/xml/data/253sx.xml"
    return "psn"
  case "/db/sade-projects/textgrid/data/xml/data/253t3.xml"
    return "wrk"
  default
    return error(QName("FONTANE", "INDEX2"), "entity resource is not considered to belong to the index: " || $entity/base-uri())
};

declare function ixp:get-icon-by-entity($entity as element(*))
as xs:string {
switch ( $entity/local-name() )
    case "event" return "fa-flag"
    case "org" return "fa-university"
    case "person" return "fa-user"
    case "personGrp" return "fa-users"
    case "place" return "fa-map-marker"
    case "item" return "fa-book"
    default return
        "fa-exclamation-triangle"
};

(:~
 : Helper function to use an efficient map data type to store the model.
 :)
declare function ixp:main-map($index as xs:string+)
as map() {
let $nodes := ixp:getNodes()
return
map:merge(
    for $n in 1 to array:size($nodes)
    let $node := $nodes?($n)
    let $localId := string-join(($node/local-name(), $node/@type), "-")
    let $content :=
        for $node in $node return
        switch ($node/local-name())
            case "listEvent" return
                if($index = "listEvent") then ixp:events($node) else ()
            case "listOrg" return
                if($index = "listOrg") then ixp:orgs($node) else ()
            case "listPerson" return
                if($index = "listPerson") then ixp:persons($node) else ()
            case "listPlace" return
                if($index = "listPlace") then ixp:places($node) else ()
            case "list" return
                ixp:works($node)
            default return error(QName("FONTANE", "INDEX1"), "unknown node-name: " || $node/local-name())
    return
        map:entry(
            $localId,
            map:merge(
                map:entry("content", $content)
            )
        )
)
};

declare function ixp:main-map()
as map() {
    ixp:main-map(("listEvent", "listOrg", "listPerson", "listPlace", "list-"))
};

(:~
 : Main function to create a complete index, contains some template specific
 : code.
 :   :)
declare function ixp:register()
as element(xhtml:div)+ {
let $map := ixp:main-map()
return
    $map?*?("content")
};

(:~
 : Main function to create a complete index, contains some template specific
 : code.
 :   :)
declare function ixp:register($index as xs:string?)
as element(xhtml:div)+ {
let $map := ixp:main-map($index)
return
    $map?*?("content")
};

(:~
 : helper function to get the tgmd:title by URI
 : @param $uri The textgrid-URI without prefix
 : @TODO may this function should be placed in a generic module
 :  :)
declare function ixp:resolve-name($uri as xs:string)
as xs:string {
    let $title := string(doc($ixp:metaCollection || $uri || '.xml')//tgmd:title)
    return
        if($title = "")
        then $uri
        else $title
};

(:~
 : tests for a given ID to appear in all @xml:id in a given node
 : @param $id The id to search for
 : @param $node The document or xml fragment
 : :)
declare function ixp:check-active($id as xs:string, $node as element(*)+)
as xs:boolean {
    exists($node/id($id))
};

(:~
 : checks if a target entity is in the current node set
 : @param $node Any node set
 :   :)
declare function ixp:check-entity($node as node()+)
as xs:string? {
    if(ixp:check-active($ixp:getEntity, $node))
    then "active"
    else ()
};

(:~
 : creates a generic xhtml:class attribute mandatory for every entry
 : @param $id ID to check for
 :  :)
declare function ixp:entry-class($id as attribute(xml:id))
as attribute(class)? {
    attribute class {
        "registerEintrag",
        $id/parent::*/local-name()
    }
};

(:~
 : creates a list for label or name
 : @param $variant A tei element containing any secondary label or name variant
 :)
declare function ixp:variant($variant as element()*)
as element(xhtml:li)? {
if(not(exists($variant))) then () else
    element xhtml:li {
        attribute class { "variant" },
        "Im Text: ",
        element xhtml:ul {
            for $var at $pos in $variant
            return
                element xhtml:li {
                    string($var) || (if($pos = count($variant)) then () else ",")
                }
        }
    }
};

(:~
 : transforms tei:idno to xhtml:li
 : @param $idnos TEI element (idno and label) with reference to authority file
 : @TODO Declare element(tei:idno), currently wrong encoding with tei:label
 :  :)
declare function ixp:idno($idnos as element()*)
as element(xhtml:li)* {
for $idno in $idnos
where string-length($idno) lt 1
return
    error(QName("FONTANE", "INDEX3"), "got empty ID in element: " || serialize($idno/parent::tei:* ) ),
for $idno in $idnos
let $str := string($idno)
let $wikipedia :=
  if($idno/@type = "GND") then 
      try {
          ixp:gnd-wikipedia($str)
      } catch * {
          error(QName("FONTANE", "INDEX4"), "error parsing element with gnd-wikipedia#1: " || serialize($idno/parent::tei:* ))
      }
  else
  if($idno/@type = "Wikidata") then ixp:wikidata-wikipedia($str) else ()
let $deutscheBiographie := collection($config:data-root)/id( "gnd"||$str )
return
    element xhtml:li {
        attribute class {"idno"},
        string($idno/@type) || ": " || $str,
        "&#160;",
        element xhtml:a {
            attribute href { string($idno/@xml:base) || $str },
            <xhtml:i title="Link zu externer Ressource" class="fa fa-external-link"></xhtml:i>
        },
        (: Deutsche Biografie :)
        if($idno/parent::tei:person and $deutscheBiographie)
        then
          (",&#160;",
            let $url := string($deutscheBiographie/@db-url)
            return
              element xhtml:a {
                attribute title {
                    if(ends-with($url, "adbcontent"))
                    then "ADB-Eintrag basierend auf GND"
                    else if (ends-with($url, "ndbcontent"))
                    then "NDB-Eintrag basierend auf GND"
                    else "Eintrag in Deutscher Biografie basierend auf GND"
                },
                attribute href {$url},
                if(ends-with($url, "adbcontent"))
                then "ADB"
                else if (ends-with($url, "ndbcontent"))
                then "NDB"
                else "DB"
            })
        else (),
        if(string($wikipedia) = "") then ()
        else (",&#160;",
            element xhtml:a {
                attribute title {"Link zu Wikipedia basierend auf GND"},
                attribute href { $wikipedia },
                <i class="fa fa-wikipedia-w" aria-hidden="true"></i>
            })
    }
};

declare function ixp:gnd-wikipedia($gndid-orig as xs:string)
as xs:string?{
let $gndid := replace($gndid-orig, "[^A-Za-z0-9\-]", "+")
let $gndCollection := "/db/sade-projects/textgrid/data/xml/gnd/"
let $filename := $gndid || ".xml"
let $docname := $gndCollection || $filename
let $url := "https://d-nb.info/gnd/" || $gndid || "/about/lds.rdf"
let $request := <hc:request method="get" href="{$url}" />
let $response :=
    if( doc-available( $docname ))
    then doc( $docname )
    else
        hc:send-request($request)[2]
let $store :=
    if(config:get("sade.develop") = "true")
    then
        if(doc-available($gndCollection || $filename))
        then $gndCollection || $filename
        else
            try {
                xmldb:store($gndCollection, $filename, $response)
            } catch * {
                ()
            }
    else ()
return
    string($response//foaf:page/@rdf:resource)
};

declare function ixp:wikidata-wikipedia($wikidata-id as xs:string)
as xs:string?{
  let $query := escape-uri("SELECT ?s WHERE{?s schema:about wd:" || $wikidata-id || ".}", true())
  let $url := "https://query.wikidata.org/sparql?query=" || $query
  let $request := <hc:request method="get" href="{$url}" />
  let $uris := hc:send-request($request)[2]//*:uri/string()
  return (
    $uris[matches(., "de\.wikipedia")],
    $uris[matches(., "en\.wikipedia")],
    $uris[matches(., "fr\.wikipedia")],
    $uris[matches(., "it\.wikipedia")],
    $uris[1])[1]
};

(:~
 : transformation from tei:note to xhtml:li
 :  :)
declare function ixp:note($note as element(tei:note)*)
as element(xhtml:li)* {
for $n in $note
    return
        element xhtml:li {
            attribute class { "tei-note" },
            $n/text(),
            for $ptr in $n/tei:ptr[@type = "Figurenlexikon"]
            return
                element xhtml:a {
                    attribute href { string($ptr/@target) },
                    attribute class { "figurenlexikon" },
                    string($ptr/@type)
                }
        }
};

(:~
 : creates a list of all creations or points to the creator of a work
 : :)
declare function ixp:creator($creators as element(tei:link)+)
as element(xhtml:li) {

if($creators[1]/parent::*/parent::tei:person)
then
    (: person item that is the creator :)
element xhtml:li {
    attribute class { "creator" },
    "",
    element xhtml:ul {
        attribute class {"workList"},
        for $creator in $creators
        let $target := $creator/@target => substring-before(" ")
        let $targetId := $target => substring-after(":")
        let $targetNode := $ixp:dataCollection/id($targetId)
        let $title := string($targetNode/*[1])
        let $htmlResource :=
(: TODO remove condition_s_ for final production release :)
            if($targetNode)
            then
                if($targetNode[2])
                then "two entities with same ID: " || $targetId
                else ixp:get-list-by-entity( $targetNode )
            else "target-not-available"
        order by $title
        return
            element xhtml:li {
                attribute class {"item"},
                element xhtml:a {
                    attribute href { "register.html?e=" || string-join($targetId) },
                    if($targetNode[2])
                    then "two entities with same ID: " || $targetId
                    else $title
                },
                text{ "&#8195;" },
                element xhtml:span {
                    attribute class { "load-entity" },
                    attribute data-load { $targetId },
                    element xhtml:i {
                        attribute class {"fa fa-plus-square-o"},
                        attribute aria-hidden {"true"}
                    }
                }
            }
    }
}
else
    (: work item with creator :)
element xhtml:li {
    attribute class { "creator" },
    "",
    element xhtml:ul {
        attribute class {"listPerson"},
        for $creator in $creators
        let $target := $creator/@target => substring-after(" ")
        let $targetId := $target => substring-after(":")
        let $targetNode := $ixp:dataCollection/id($targetId)
        let $htmlResource :=
(: TODO remove condition for final production release :)
            if($targetNode)
            then
                if($targetNode[2])
                then "two entities with same ID: " || $targetId
                else ixp:get-list-by-entity( $targetNode )
            else "target-not-available"
        return
            element xhtml:li {
                attribute class {"person"},
                element xhtml:a {
                    attribute href { "register.html?e=" || string-join($targetId) },
                    if($targetNode[2])
                    then "two entities with same ID: " || $targetId
                    else string($targetNode/*[1])
                }
            }
    }
}
};

declare function ixp:has-former-or-current-location($links as element(tei:link)*)
as element(xhtml:li)* {
for $link in $links
let $thisId := string($link/parent::tei:linkGrp/parent::tei:*/@xml:id)
let $id := ($link/@target => replace("\w{3}:|#", "") => tokenize(" "))[. != ""][. != $thisId]
return
    if(count($id) != 1) then
        (update insert comment {"unable to parse target: "  || serialize($link)} following $link,
        update delete $link)
        else
let $targetNode := $ixp:dataCollection/id($id)
return
    if(count($targetNode) gt 1)
    then (update insert comment {"xml:id used multiple times. this occurence is removed. please check and reinsert item when xml:id is unique. "  || serialize($id[2])} following $id[2],
        update delete $id[2]
        )
    else if(count($targetNode) = 0)
    then
        (update insert comment {"ID " || $id || " not found: Please create an appropriate item. "  || serialize($link)} following $link,
        update delete $link
        )
    else
let $list := ixp:get-list-by-entity($targetNode)
return
    element xhtml:li {
        attribute class { "former-or-current-location" },
        element xhtml:a {
        attribute href { "register.html?e=" || $id },
        <xhtml:i class="fa fa-map-marker" aria-hidden="true"></xhtml:i>,
        "&#160;",
        $targetNode/*[1]/string(),
        let $ancestors := $targetNode/ancestor::tei:place
        for $parentPlace at $pos in $ancestors
        let $name := $parentPlace/*[1]/string()
        return
          ((if($pos = 1) then "(" else ", ") ||
          $name ||
          (if($pos = count($ancestors)) then ")" else ()))
        }
    }
};

(:~ renewed version of the linking function
 : generates a XHTML list with all links without using the tei:link[@corresp]
 : from the entity. it queries the database for what is really mentioned.
 : @version 2.0
 :)
declare function ixp:links($entity as element()) {
let $id := string($entity/@xml:id)
let $prefix := ixp:get-prefix-by-entity($entity)
let $reference := $prefix || ":" || $id
let $hits := $ixp:dataCollection//tei:rs
                [contains(@ref, $reference)]
                [not(@prev)]
                [@ref eq $reference]

let $array :=
    array{
        for $hit in $hits
        let $base := $hit/base-uri()
        let $tguri := (substring-before($base, ".xml") => tokenize("/|\."))[last()]
        let $resolve := ixp:resolve-name($tguri)
        return
            map{
                "base": $base,
                "tguri" : $tguri,
                "title" : $resolve,
                "surface": $hit/ancestor::tei:surface[parent::tei:sourceDoc]/string(@n)
            }
    }
let $titles := $array?*?title => distinct-values()
let $countNotebooks := count($titles)
return
element xhtml:li {
    attribute class { "links" },
    if($countNotebooks lt 2) then 'In Notizbuch' else ('In den Notizbüchern ',
    <span style="color:gray">({$countNotebooks} Notizbücher, insgesamt {array:size($array)} Vorkommnisse)</span>),
    element xhtml:ul {
        (: main list :)
        for $title in $titles
        let $titleArray := $array?*[?title = $title]
        let $titleReadable := $title => replace("([A-E])0(\d)", "$1$2")
        let $tguri := $titleArray?tguri[1]
        order by $title
        return
        element xhtml:li {
            $titleReadable || ": ",
            element xhtml:ul {
                attribute class {"link-list-horizontal"},
                for $surface in distinct-values($titleArray?surface)
                let $count := $titleArray[?surface = $surface] => count()
                return
                    element xhtml:li {
                    element xhtml:a {
                        attribute href {
                            "edition.html?id=%2Fxml%2Fdata%2F" || $tguri || ".xml"
                            || "&amp;page=" || $surface
                            || "&amp;target=" || $id
                        },
                        f-misc:n-translate($surface),
                        if($count eq 1) then ()
                        else (" ", <xhtml:span title="Anzahl an Vorkommnissen auf dieser Seite">({$count})</xhtml:span>)
                    }
                }
            }
        }
    }
}
};

(:~
 : prepares a link to the current entry
 : @param $id The xml:id of the currently processed element
:)
declare function ixp:entry-link($id as attribute(xml:id))
as element(xhtml:span) {
    element xhtml:span {
        attribute class {"registerLink"},
        element xhtml:a {
            attribute href {
                "register.html?e=" || $id
                },
            <xhtml:i title="Link zu diesem Eintrag" class="fa fa-link"></xhtml:i>
        }
    }
};

(:~
 : creates a xhtml list item containing serialized code.
 : @param $node A single element
 :   :)
declare function ixp:li-code($node as element(*))
as element(xhtml:li) {
element xhtml:li {
    attribute class {"register-code"},
    element xhtml:span {
        attribute class {"registerCode"},
            <xhtml:i title="Zeige XML" class="fa fa-code"></xhtml:i>
    },
    if(config:get("sade.develop") = "true")
    then
        code:main($node) => functx:change-element-ns-deep("", "")
    else
        code:main($node, false(), "hljs-", true())
            => functx:change-element-ns-deep("", "")
}
};

(:~
 : generic function for entries with tei:note/tei:ptr.
 : @return Link to the corresponding index entry
 :   :)
declare function ixp:pointer($entry as element())
as element(xhtml:li) {
    let $labelMain := $entry/*[1]
    let $target := substring-after(($entry/tei:note/tei:ptr/@target), '#')
    let $targetNode := $ixp:dataCollection/id($target)
    return
        if(not($targetNode)) then <div>target not found</div> else
        element xhtml:li {
            attribute class {
                "registerEintrag",
                if( $entry >> $targetNode )
                then "up"
                else "down"
            },
            element xhtml:span {
                    attribute class { "label-main" },
                    $labelMain/text()
            },
            element xhtml:ul {
                element xhtml:li {
                attribute class { "pointer" },
                "siehe: ",
                    element xhtml:a {
                        attribute href { "?e=" || $target },
                        try {
                             string($targetNode/*[1])
                         } catch * {
                             (: TODO remove try catch when the data is better :)
                             "multiple entities found: " || string-join(($targetNode ! serialize(.)), " AND ")
                         }
                    }
                }
            }
        }
};

(:
 : parses a tei:bibl and mainly prepares a list item with a link to the entity
 :)
declare function ixp:bibl($bibls as element(tei:bibl)*)
as element(xhtml:li)* {
for $bibl in $bibls
    let $biblId := $bibl/tei:ptr/@target => substring-after(":")
    let $targetNode := $ixp:dataCollection/id($biblId)[. instance of element(tei:bibl)]
    return
        element xhtml:li {
            attribute class { "bibl" },
            element xhtml:a {
            attribute href { "literaturvz.html?id=" || $biblId },
            <xhtml:i class="fa fa-book" aria-hidden="true"></xhtml:i>,
            "&#160;",
            $targetNode/tei:choice/tei:abbr/string() ||
            (if($bibl/tei:citedRange)
            then ":&#160;" || $bibl/tei:citedRange
            else ())
            }
        }
};

declare function ixp:get-gnd-data($url as xs:string)
as element(rdf:RDF){
    let $request := <hc:request method="get"/>
    return
    hc:send-request($request, $url || "/about/rdf")[2]//rdf:RDF
};

(: ################################################### :)
(: ################################################### :)
(: ################################################### :)

(:~
 : parser for listEvents/event entities
 : @param $eventDoc The tei:listEvent containing all entries
 :  :)
declare function ixp:events($eventDoc as element(tei:listEvent))
as element(xhtml:div) {
<xhtml:div id="{ string-join(($eventDoc/local-name(), $eventDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $eventList in $eventDoc/tei:listEvent
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title">{$eventList/tei:head/text()}</h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listEvent">
{
if($ixp:getNotebook = $ixp:targetStart)
then
  for $event in $eventList/tei:event
  let $sort := string($event/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-event($event)
else
  for $event in $eventList/tei:event[tei:linkGrp/tei:link
  [starts-with(@target, $ixp:getNotebook)]]
  let $sort := string($event/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-event($event)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};

(:~
 : parser for listEvents/event entities
 : @param $eventDoc The tei:listEvent containing all entries
 :  :)
declare function ixp:events($eventDoc as element(tei:listEvent), $filter as xs:string*)
as element(xhtml:div) {
<xhtml:div id="{ string-join(($eventDoc/local-name(), $eventDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $eventList in $eventDoc/tei:listEvent
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title">{$eventList/tei:head/text()}</h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listEvent">
{
  for $event in $eventList/tei:event[@xml:id = $filter]
  let $sort := string($event/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-event($event)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};

declare function ixp:prepare-event($event as element(tei:event))
as element(xhtml:li) {
(: check if the item points to another entity :)
if( not(exists($event/@xml:id)) )
then
    (: pointer :)
    ixp:pointer($event)
else
    let $id := $event/@xml:id
    let $wheres := tokenize($event/@where, " ") ! substring-after(., ":")
    let $labelMain := ($event/tei:label)[1]
    let $labelAlt := ($event/tei:label[not(@type="variant")])[position() gt 1]
    let $variant := $event/tei:label[@type="variant"]
    let $note := $event/tei:note
    let $bibl := $event/tei:bibl
    let $links := $event/tei:linkGrp/tei:link[@corresp="https://schema.org/mentions"]
return
    element xhtml:li {
        attribute id { string($id) },
        ixp:entry-class($id),
        (: link to this entry :)
        ixp:entry-link($id),
        element xhtml:span {
                attribute class { "label-main" },
                $labelMain/text()
            },
        element xhtml:ul {
            (: variant :)
            ixp:variant($variant),
            (: secondary label :)
            ixp:idno($labelAlt),
            (: note :)
            ixp:note($note),
            (: where :)
            for $where in $wheres
            return
                ixp:data-loader($ixp:dataCollection/id($where)),
            (: bibl :)
            ixp:bibl($bibl),
            (: links :)
            ixp:links($event),
            ixp:li-code($event)
        }
    }
};

(:~
 : parser for tei:listOrg, returns the index of orgnaisations.
 :  :)
declare function ixp:orgs($orgDoc as element(tei:listOrg)+) {
let $result :=
<xhtml:div id="{ string-join(($orgDoc/local-name(), $orgDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $orgList in $orgDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title">{ $orgList/tei:head/text() }</h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listOrg">
{
if($ixp:getNotebook = $ixp:targetStart)
then
  for $org in $orgList/tei:org
  let $sort := string($org/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-org($org)
else
  for $org in $orgList/tei:org[tei:linkGrp/tei:link[starts-with(@target, $ixp:getNotebook)]]
  let $sort := string($org/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-org($org)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
return
    $result
};

(:~
 : parser for tei:listOrg, returns the index of organisations.
 :  :)
declare function ixp:orgs($orgDoc as element(tei:listOrg)+, $filter as xs:string*) {
<xhtml:div id="{ string-join(($orgDoc/local-name(), $orgDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $orgList in $orgDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title">{ $orgList/tei:head/text() }</h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listOrg">
{
  for $org in $orgList/tei:org[@xml:id = $filter]
  let $sort := string($org/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-org($org)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>

};

(:~
 : transforms a single tei:org element to a xhtml:li
 : @since 1.0
 :  :)
declare function ixp:prepare-org($org as element(tei:org))
as element(xhtml:li) {
(: check if the item points to another entity :)
if( not(exists($org/@xml:id)) )
then
    (: pointer :)
    ixp:pointer($org)
else
    let $id := $org/@xml:id
    let $labelMain := ($org/tei:orgName)[1]
    let $idnos := $org/tei:idno
    let $variant := $org/tei:orgName[@type="variant"]
    let $note := $org/tei:note
    let $bibl := $org/tei:bibl
    let $links := $org/tei:linkGrp/tei:link[@corresp="https://schema.org/mentions"]
return
    element xhtml:li {
        attribute id { string($id) },
        ixp:entry-class($id),
        (: link to this entry :)
        ixp:entry-link($id),
        element xhtml:span {
                attribute class { "label-main" },
                $labelMain/text()
            },
        element xhtml:ul {
            (: variant :)
            ixp:variant($variant),
            (: secondary label :)
            ixp:idno($idnos),
            (: note :)
            ixp:note($note),
            (: bibl :)
            ixp:bibl($bibl),
            (: links :)
            ixp:links($org),
            ixp:li-code($org)
        }
    }
};

(:~
 : parser for tei:listPerson, returns the index of persons
 :   :)
declare function ixp:persons($persDoc as element(tei:listPerson))
as element(xhtml:div) {
(: main tab :)
<xhtml:div id="{ string-join(($persDoc/local-name(), $persDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $persList in $persDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">Personen und Werke</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listPerson">
{
if($ixp:getNotebook = $ixp:targetStart)
then
  for $pers in $persList/tei:*
  let $sort := string($pers/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-person($pers)
else
  for $pers in $persList/tei:*[tei:linkGrp/tei:link[starts-with(@target, $ixp:getNotebook)]]
  let $sort := string($pers/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-person($pers)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};


(:~
 : parser for tei:listPerson, returns the index of persons
 :   :)
declare function ixp:persons($persDoc as element(tei:listPerson), $filter as xs:string*)
as element(xhtml:div) {
(: main tab :)
<xhtml:div id="{ string-join(($persDoc/local-name(), $persDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $persList in $persDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">Personen und Werke</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listPerson">
{

  for $pers in $persList/tei:*[@xml:id = $filter]
  let $sort := string($pers/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-person($pers)

}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};

declare function ixp:prepare-person($pers as element(*)+)
as element(xhtml:li) {
(: check if the item points to another entity :)
if( not(exists($pers/@xml:id)) )
then
    (: pointer :)
    ixp:pointer($pers)
else
    (: entity :)
    let $id := $pers/@xml:id
    let $labelMain := $pers/tei:persName[1]
    let $labelAlt := $pers/tei:persName[not(@type="variant")][position() gt 1]
    let $variant := $pers/tei:persName[@type="variant"]
    let $idnos := $pers/tei:idno
    let $birth := $pers/tei:birth
    let $death := $pers/tei:death
    let $occupation := $pers/tei:occupation
    let $state := $pers/tei:state/tei:desc
    let $note := $pers/tei:note
    let $bibl := $pers/tei:bibl
    let $members := $pers/tei:linkGrp/tei:link[@corresp="http://erlangen-crm.org/current/P107_has_current_or_former_member"]
    let $creator := $pers/tei:linkGrp/tei:link[@corresp="http://purl.org/dc/terms/creator"]
    let $links := $pers/tei:linkGrp/tei:link[@corresp="https://schema.org/mentions"]
    return
        element xhtml:li {
            attribute id { string($id) },
            ixp:entry-class($id),
            (: link to this entry :)
            ixp:entry-link($id),
            element xhtml:span {
                    attribute class { "label-main" },
                    $labelMain/text()
                },
                if(not($pers/parent::tei:listPerson/parent::tei:item)) then () else
                element xhtml:span {
                        attribute class { "hiera-ref" },
                        " in: ",
                        element xhtml:a {
                            attribute href { "?e=" || string($pers/parent::tei:listPerson/parent::tei:item/@xml:id) },
                            string($pers/parent::tei:listPerson/parent::tei:item/*[1])
                        }
                    },
            element xhtml:ul {
                (: variant :)
                ixp:variant($variant),
                (: secondary label :)
                for $l in $labelAlt
                return
                    element xhtml:li {
                    attribute class {"label-" || string($l/@type)},
                    $l/text(),
                    if($l/@type = "variant")
                    then ()
                    else (
                        "&#160;",
                        element xhtml:a {
                            attribute href { string($l/@xml:base) || string($l) },
                            <xhtml:i title="Link zu externer Ressource" class="fa fa-external-link"></xhtml:i>
                        }
                    )
                },
                (: idno :)
                ixp:idno($idnos),
                (: birth, death :)
                if( not($birth or $death) ) then () else
                    element xhtml:li {
                        if($birth) then ('&#8727;', $birth/text()) else (),
                        if( $birth and $death ) then " " else (),
                        if($death) then ('✝', $death/text()) else()
                    },
                (: occupation :)
                for $o in $occupation
                return
                    element xhtml:li {
                        attribute class { "occupation" },
                        $o/text()
                    },
                for $s in $state
                return
                    element xhtml:li {
                        attribute class { "state" },
                        $s/text()
                    },
                (: current or former member :)
                if(not(exists($members))) then () else
                element xhtml:li {
                    attribute class { "member" },
                    "Mitglieder:",
                    element xhtml:ul {
                        for $member in $members
                        let $memberId := $members/@target => substring-after(" ") => substring-after("#")
                        return
                            ixp:data-loader($ixp:dataCollection/id($memberId))
                    }
                },
                if (not(exists($creator))) then () else
                ixp:creator($creator),
                (: note :)
                ixp:note($note),
                (: bibl :)
                ixp:bibl($bibl),
                (: links :)
                ixp:links($pers),
                ixp:li-code($pers)
            }
        }
};

(:~
 : prepares the index of geographical references
 :  :)
declare function ixp:places($listPlace as element(tei:listPlace))
as element(xhtml:div) {
<xhtml:div id="{ string-join(($listPlace/local-name(), $listPlace/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">Geographisches Register</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listPlace">
{
if($ixp:getNotebook = $ixp:targetStart)
then
  for $place in $listPlace/tei:place
  let $sort := string($place/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-place($place)

else
  for $place in $listPlace/tei:place
      [tei:linkGrp/tei:link
      [starts-with(@target, $ixp:getNotebook)]]
  let $sort := string($place/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-place($place)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    </xhtml:div>
</xhtml:div>
};

(:~
 : prepares the index of geographical references
 :  :)
declare function ixp:places($listPlace as element(tei:listPlace), $filter as xs:string*)
as element(xhtml:div) {
<xhtml:div id="{ string-join(($listPlace/local-name(), $listPlace/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">Geographisches Register</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="listPlace">
{
  for $place in $listPlace/tei:place[@xml:id = $filter]
  let $sort := string($place/*[1])
                => replace("ä", "ae")
                => replace("ö", "oe")
                => replace("ü", "ue")
  order by $sort
  return ixp:prepare-place($place)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    </xhtml:div>
</xhtml:div>
};

(:~
 : transformation of a single geographical reference item  :)
declare function ixp:prepare-place($place as element(tei:place))
as element(xhtml:ul) {
(: check if the item points to another entity :)
if( not(exists($place/@xml:id)) )
then
    (: pointer :)
    ixp:pointer($place)
else
    (: entity :)
    let $id := $place/@xml:id
    let $labelMain := $place/tei:placeName[1]
    let $labelAlt := $place/tei:placeName[not(@type="variant")][position() gt 1]
    let $variant := $place/tei:placeName[@type="variant"]
    let $idnos := $place/tei:idno
    let $note := $place/tei:note
    let $creator := $place/tei:linkGrp/tei:link[@corresp="http://purl.org/dc/terms/creator"]
    let $bibl := $place/tei:bibl
    let $tookPlaceAt := $place/tei:linkGrp/tei:link[@corresp="http://erlangen-crm.org/current/P7_took_place_at"]
    let $formerCurrentLocation := $place/tei:linkGrp/tei:link[@corresp="http://erlangen-crm.org/current/P53_has_former_or_current_location"]
    let $links := $place/tei:linkGrp/tei:link[@corresp="https://schema.org/mentions"]

    let $itemRefs := $ixp:dataCollection//tei:link
        [@corresp="http://erlangen-crm.org/current/P53_has_former_or_current_location"]
        [ends-with(@target, " plc:" || $id)]/parent::tei:linkGrp/parent::tei:*
    let $itemsRefNodes := for $item in $itemRefs
                            let $node := ixp:data-loader($item)
                            order by string($node)
                            return
                                $node
    return
        element xhtml:li {
            attribute id { string($id) },
            ixp:entry-class($id),
            (: link to this entry :)
            ixp:entry-link($id),
            element xhtml:span {
                    attribute class { "label-main" },
                    $labelMain/text()
                },
            if(not($place/parent::tei:place)) then () else
            element xhtml:span {
                    attribute class { "hiera-ref" },
                    " in: ",
                    element xhtml:a {
                        attribute href { "?e=" || string($place/parent::tei:place/@xml:id) },
                        string($place/parent::tei:place/*[1])
                    }
                },
            if(not($place/parent::tei:listPlace/parent::tei:item)) then () else
            element xhtml:span {
                    attribute class { "hiera-ref" },
                    " in: ",
                    element xhtml:a {
                        attribute href { "?e=" || string($place/parent::tei:item/@xml:id) },
                        string($place/parent::tei:listPlace/parent::tei:item/*[1])
                    }
                },
            element xhtml:ul {
                (: variant :)
                ixp:variant($variant),
                (: secondary label :)
                for $l in $labelAlt
                return
                    element xhtml:li {
                    attribute class {"label-" || string($l/@type)},
                    $l/text(),
                    if($l/@type = "variant")
                    then ()
                    else (
                        "&#160;",
                        element xhtml:a {
                            attribute href { string($l/@xml:base) || string($l) },
                            <xhtml:i title="Link zu externer Ressource" class="fa fa-external-link"></xhtml:i>
                        }
                    )
                },
                (: idno :)
                ixp:idno($idnos),
                (: creator :)
                if (not(exists($creator))) then () else
                ixp:creator($creator),
                (: note :)
                ixp:note($note),
                (: bibl :)
                ixp:bibl($bibl),
                (: events took place :)
                for $l in $tookPlaceAt
                let $id := $l/@target => substring-after(":") => substring-before(" ")
                return
                    ixp:data-loader($ixp:dataCollection/id($id)),
                (: former_or_current_location :)
                ixp:has-former-or-current-location($formerCurrentLocation),
                (: entries referencing this entity :)
                if($itemsRefNodes)
                then <xhtml:ul class="itemsRef"> { $itemsRefNodes } </xhtml:ul>
                else (),
                (: links :)
                ixp:links($place),
                ixp:li-code($place),
                element xhtml:ul {
                  for $plac in $place/tei:place
                  let $sort := string($plac/*[1])
                                => replace("ä", "ae")
                                => replace("ö", "oe")
                                => replace("ü", "ue")
                  order by $sort
                  return ixp:prepare-place($plac)
                }
            }
        }
};

(:~
 : prepares the list of works :)
declare function ixp:works($wrkDoc as element(tei:list))
as element(xhtml:div) {
<xhtml:div id="{ string-join(($wrkDoc/local-name(), $wrkDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $wrkList in $wrkDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$wrkList/tei:head/text()}</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="workList">
{
if($ixp:getNotebook = $ixp:targetStart)
then
for $i in $wrkList//tei:item[not(tei:list)]
order by (lower-case(($i/tei:name[not(@type)]/text())[1]) => replace("ä", "ae") => replace("ö", "oe") => replace("ü", "ue") )
return ixp:prepare-work($i)
else
for $i in $wrkList//tei:item
    [not(tei:list)]
    [tei:linkGrp/tei:link
        [starts-with(@target, $ixp:getNotebook)]]
order by (lower-case(($i/tei:name[not(@type)]/text())[1]) => replace("ä", "ae") => replace("ö", "oe") => replace("ü", "ue") )
return ixp:prepare-work($i)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};

(:~
 : prepares the list of works :)
declare function ixp:works($wrkDoc as element(tei:list), $filter as xs:string*)
as element(xhtml:div) {
<xhtml:div id="{ string-join(($wrkDoc/local-name(), $wrkDoc/@type), "-") }" class="tab-pane">
    <xhtml:div class="panel-group" aria-multiselectable="true" role="tablist">
    {for $wrkList in $wrkDoc
    return
        <xhtml:div class="panel panel-default">
            <xhtml:div class="panel-heading">
                <h4 class="panel-title"><a href="#collapse1-1" data-parent="#accordion" data-toggle="collapse">{$wrkList/tei:head/text()}</a>
                </h4>
            </xhtml:div>
            <xhtml:div class="panel-body">
                <ul class="workList">
{
for $i in $wrkList//tei:item[not(tei:list)][@xml:id = $filter]
order by (lower-case(($i/tei:name[not(@type)]/text())[1])
    => replace("ä", "ae")
    => replace("ö", "oe")
    => replace("ü", "ue") )
return ixp:prepare-work($i)
}
                </ul>
            </xhtml:div>
        </xhtml:div>
    }
    </xhtml:div>
</xhtml:div>
};


(:~
 : prepares a single work item :)
declare function ixp:prepare-work($work as element(tei:item))
as element(xhtml:li) {
(: check if the item points to another entity :)
if( not(exists($work/@xml:id)) )
then
    (: pointer :)
    ixp:pointer($work)
else
    (: entity :)
    let $id := $work/@xml:id
    let $labelMain := $work/tei:name[1]
    let $labelAlt := $work/tei:name[not(@type="variant")][position() gt 1]
    let $variant := $work/tei:name[@type="variant"]
    let $idnos := $work/tei:idno
    let $date := $work/tei:date
    let $note := $work/tei:note
    let $bibl := $work/tei:bibl
    let $creator :=
      ( $work/tei:linkGrp/tei:link[@corresp="http://purl.org/dc/terms/creator"],
        if($work/ancestor::tei:list[@type = "Fontane"]) then
            (: at this point we fake a tei:link to deal with the implicit creator
            relation in case of Theodor Fontane's works. This is necessary to
            achieve a reference to Fontane's index entry when having a look at
            one of his works on register.html. :)
            element tei:link {
                attribute corresp {"http://purl.org/dc/terms/creator"},
                attribute target {"#" || $work/@xml:id/string() || " psn:Fontane_Theodor"}
            }
        else () )
    let $translationOf := $work/tei:linkGrp/tei:link[@corresp="http://purl.org/ontology/bibo/#translationOf"]
    let $formerCurrentLocation := $work/tei:linkGrp/tei:link[@corresp="http://erlangen-crm.org/current/P53_has_former_or_current_location"]
    let $links := $work/tei:linkGrp/tei:link[@corresp="https://schema.org/mentions"]
    return
        element xhtml:li {
            attribute id { string($id) },
            ixp:entry-class($id),
            (: link to this entry :)
            ixp:entry-link($id),
            element xhtml:span {
                    attribute class { "label-main" },
                    $labelMain/text()
                },
            element xhtml:ul {
                (: variant :)
                ixp:variant($variant),
                (: secondary label :)
                for $l in $labelAlt
                return
                    element xhtml:li {
                    attribute class {"label-" || string($l/@type)},
                    $l/text(),
                    if($l/@type = "variant")
                    then ()
                    else (
                        "&#160;",
                        element xhtml:a {
                            attribute href { string($l/@xml:base) || string($l) },
                            <xhtml:i title="Link zu externer Ressource" class="fa fa-external-link"></xhtml:i>
                        }
                    )
                },
                (: idno :)
                ixp:idno($idnos),
                for $d in $date
                return
                    element xhtml:li {
                        attribute class {"date"},
                        $d/text()
                    },
                (: creator :)
                if (not(exists($creator))) then () else
                ixp:creator($creator),
                (: translationOf :)
                for $link in $translationOf
                let $url := $link/@target => substring-after(" ")
                return
                    element xhtml:li {
                        attribute class {"translationOf"},
                        "Übersetzung von ",
                        <a href="{$url}">{
                            ixp:get-gnd-data($url)//gndo:preferredNameForTheWork/string(.)
                        }</a>
                    },
                (: note :)
                ixp:note($note),
                (: bibl :)
                ixp:bibl($bibl),
                (: former_or_current_location :)
                ixp:has-former-or-current-location($formerCurrentLocation),
                (: links :)
                ixp:links($work),
                ixp:li-code($work),
                (: further entities :)
                for $listPerson in $work/tei:listPerson
                return
                    element xhtml:li {
                        element xhtml:ul {
                            attribute class { "work-listPerson" },
                            for $person in $listPerson/tei:person
                            let $sort := string($person/*[1])
                                          => replace("ä", "ae")
                                          => replace("ö", "oe")
                                          => replace("ü", "ue")
                            order by $sort
                            return ixp:prepare-person($person)

                        }
                    },
                for $listPlace in $work/tei:listPlace
                return
                    element xhtml:li {
                        element xhtml:ul {
                            attribute class { "work-listPlace" },
                            for $place in $listPlace/tei:place
                            let $sort := string($place/*[1])
                                          => replace("ä", "ae")
                                          => replace("ö", "oe")
                                          => replace("ü", "ue")
                            order by $sort
                            return ixp:prepare-place($place)
                        }
                    }
            }
        }
};

(:~
 : A generic list item that is enabled for loading the entry it points to
 : via ajax call.
 : @author Mathias Göbel
 : @param $targetNode The TEI element to load in place
 : @return a xhtml:li with the title and an icon to click for loading the entry
:)
declare function ixp:data-loader($targetNode as element())
as element(xhtml:li) {
    let $targetId := string($targetNode/@xml:id)
    let $title := string($targetNode/*[1])
    return
      element xhtml:li {
        attribute class { "registerEintrag", $targetNode/local-name() },
        element xhtml:a {
            attribute href { "register.html?e=" || string-join($targetId) },
            if($targetNode[2])
            then "two entities with same ID: " || $targetId
            else $title
        },
        text{ "&#8195;" },
        element xhtml:span {
            attribute class { "load-entity" },
            attribute data-load { $targetId },
            element xhtml:i {
                attribute class {"fa fa-plus-square-o"},
                attribute aria-hidden {"true"}
            }
        }
    }
};
