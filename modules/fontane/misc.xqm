xquery version "3.1";

module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $f-misc:metadata-collection :=  collection("/db/sade-projects/textgrid/data/xml/meta");

declare
  %test:arg("baseUri","textgrid:16b00") %test:assertEquals("Notizbuch C7")
  %test:arg("baseUri","") %test:assertEmpty
function f-misc:get-title-by-baseUri($baseUri as xs:string)
as xs:string? {
  if($baseUri = "") then () else
  $f-misc:metadata-collection//tgmd:textgridUri[starts-with(string(.), $baseUri)]/ancestor::tgmd:object//tgmd:title/string()
  => replace("([A-E])0(\d)", "$1$2")
};

declare function f-misc:nb-param-to-title($node as node(), $model as map(*), $nb as xs:string?)
as xs:string? {
  if(string($nb) = "")
  then ""
  else f-misc:get-title-by-baseUri( "textgrid:" || $nb )
};

(:~
 : Translates tei:surface/@n to German
 : @param $n A valid @tei:n value
 : @return the German version, e.g. outer_front_cover translates to "vordere Einbanddecke außen"
 : :)
declare function f-misc:n-translate($n as xs:string)
as xs:string{
if(contains($n, 'endpaper'))
then
    let $seq := tokenize($n, '_')
    return
        ((switch($seq[1])
            case "front" return 'vorderes'
            case "back" return 'hinteres'
            default return 'ungültiger Wert in @n')
        || ' Vorsatzblatt ' || $seq[last()])
else
    switch ($n)
       case "outer_front_cover" return "vordere Einbanddecke außen"
       case "inner_front_cover" return "vordere Einbanddecke innen"
       case "inner_back_cover" return "hintere Einbanddecke innen"
       case "outer_back_cover" return "hintere Einbanddecke außen"
       case "spine" return "Buchrücken"
       default return $n
};

declare function f-misc:cite($node as node(), $model as map(*)) {
let $url := request:get-url(),
    $res := tokenize($url, '/')[last()],
    $id := request:get-parameter('id', ''),
    $page := request:get-parameter('page', 'outer_front_cover'),
    $page := if($page = '') then 'outer_front_cover' else $page,
    $page := switch ($page)
               case "outer_front_cover" return "vordere Einbanddecke außen"
               case "inner_front_cover" return "vordere Einbanddecke innen"
               case "inner_back_cover" return "hintere Einbanddecke außen"
               case "outer_back_cover" return "hintere Einbanddecke innen"
               case "spine" return "Buchrücken"
               default return 'Blatt '||$page,
    $doc := doc( config:get("data-dir")||$id),
    $title := $doc//tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/string(),
    $version := string( ($doc//tei:TEI/tei:teiHeader/tei:revisionDesc/tei:change[last()]/text())[1] ),
    $date := current-date(),
    $date := day-from-date($date)||'.'||month-from-date($date)||'.'||year-from-date($date),
    $url := replace($url, 'localhost:8080/exist/apps/SADE/textgrid', 'fontane-nb.dariah.eu') ||(if (request:get-query-string()) then '?' || request:get-query-string() else '')

return
    switch ($res)
    case 'edition.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: {$title}. Hrsg. von Gabriele Radecke.
                        In: Theodor Fontane: Notizbücher. Digitale genetisch-kritische und kommentierte Edition.
                        Hrsg. von Gabriele Radecke,
                        Blatt {$page}.
                        {$url}.
                        {$version},
                        abgerufen am {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    case 'inhalt.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: {$title} (Inhaltsverzeichnis). Hrsg. von Gabriele Radecke.
                        {$url},
                        abgerufen am: {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    case 'doku.html'
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Gabriele Radecke unter Mitarbeit von Martin de la Iglesia und Mathias Göbel: Editorische Gesamtdokumentation.
                        Editorische Assistenz: Rahel Rami und Judith Michaelis. In: Theodor Fontane: Notizbücher. Digitale
                        genetisch-kritische und kommentierte Edition. Hrsg. von Gabriele Radecke.
                        {$url}, abgerufen am {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
    default
        return
            <span>
                <a href="#" data-toggle="modal" data-target="#citation">Zitationsempfehlung</a>
                <!-- Modal window: -->
                <div class="modal fade" id="citation" tabindex="-1" role="dialog" aria-labelledby="citationrecommendation" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <h4 class="modal-title">Zitationsempfehlung</h4>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-body">
                        Theodor Fontane: Notizbücher. Hrsg. von Gabriele Radecke.<br/>
                        {$url}<br/>
                        abgerufen am: {$date}
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End of modal window -->
            </span>
};

declare function f-misc:kaesten($node as node(), $model as map(*)){
    let $n := request:get-parameter('n', 'a0')
    let $num := if(matches($n, "\d+$")) then number(substring($n, 2)) else 0
    let $k := lower-case(substring($n, 1, 1))
    let $q := $k || $num
    let $data-dir := config:get("data-dir")
return

    (<ul class="nav nav-tabs">
        {for $kasten in (('a', 'b', 'c', 'd', 'e'))
        return
            if ($k = $kasten)
            then
                <li class="active">
                    <a href="#{$kasten}" data-toggle="tab">Kasten {upper-case($kasten)}</a>
                </li>
            else
                <li>
                    <a href="#{$kasten}" data-toggle="tab">Kasten {upper-case($kasten)}</a>
                </li>
        }
    </ul>,
    <div class="tab-content">
    {
        for $kasten in (('a', 'b', 'c', 'd', 'e'))
            return
                if ($k = $kasten)
            then
        <div class="tab-pane active" id="{$kasten}">
            <div class="panel-group" id="accordion{upper-case($kasten)}" role="tablist" aria-multiselectable="true">
                    {f-misc:list( $data-dir || '/xml/meta', upper-case($kasten), $num)}
            </div>
        </div>
        else
        <div class="tab-pane" id="{$kasten}">
            <div class="panel-group" id="accordion{upper-case($kasten)}" role="tablist" aria-multiselectable="true">
                {f-misc:list($data-dir || '/xml/meta', upper-case($kasten), $num)}
            </div>
        </div>
    }
    </div>)
};

declare function f-misc:list($datadir, $param, $num as xs:integer)
as element(xhtml:div)+ {

    let $tgnav := doc("/db/sade-projects/textgrid/data/xml/data/3qnsx.xml")
    let $tgnavObjects := $tgnav//object[starts-with(./@title, 'Notizbuch ' || $param)]/string(@title)
                => distinct-values()

    for $item at $pos in $tgnavObjects
    let $uri:= $tgnav//object[string(@title) = $item]/substring-after(@uri, 'textgrid:')
    let $teiDoc := doc("/db/sade-projects/textgrid/data/xml/data/" || $uri || ".xml")
    let $title := if (matches($item, '0\d')) then $item => replace('0', '') else $item
    let $beta := if(contains(($teiDoc//tei:revisionDesc/tei:change)[last()]/text(), 'Version 0.')) then <xhtml:sup>beta</xhtml:sup> else ()
    let $thisIn := (request:get-parameter('n', '') => replace("0(\d)", "$1") => upper-case()) = $param||$pos
    return
        <div class="panel panel-dark">
          <div class="panel-heading" role="tab" id="heading{$param||$pos}">
          <h3 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion{$param}" href="#collapse{$param||$pos}" aria-expanded="false" aria-controls="collapse{$param||$pos}">
          {$title}
          {$beta}
            </a>
          </h3>
        </div>
        <div id="collapse{$param||$pos}" class="panel-collapse collapse{if ($thisIn) then ' in' else ()}" role="tabpanel" aria-labelledby="heading{$param||$pos}">
        <div class="panel-body">
        <div class="row">
            <div class="col-md-9">
                <a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page=">Synoptische Ansicht</a>
                <span style="margin-left:20px;"/>
                <form class="form-inline" style="display:inline;" action="edition.html" metod="get">
                    <input type="hidden" name="id" value="/xml/data/{$uri[last()]}.xml"/>
                    <div class="form-group">

                    <input name="page" type="text" style="border-radius:0;border-style:solid;width:40px;" placeholder="1r" data-toggle="tooltip" data-placement="bottom" title="Blattnummer"/>
                    <button type="submit" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>

                    </div>
                </form>
              <br/>
                <ul>
                    <li><a href="mirador.html?n={$param||$pos}">Digitalisate</a></li>
                    <li><a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page="
                    onclick="goldenState('trans')">Transkriptionsansicht</a></li>
                    <li><a href="edierter-text.html?id=/xml/data/{$uri[last()]}.xml&amp;page=">Edierter Text</a>/Textkritischer Apparat</li>
                    <li>TEI/XML-Ansicht: <a href="edition.html?id=/xml/data/{$uri[last()]}.xml&amp;page="
                    onclick="goldenState('code')"><i title="seitenweise" class="fa fa-file-o"></i></a> | <a href="xml.html?id=/xml/data/{$uri[last()]}.xml"><i title="gesamtes XML auf der Webseite betrachten" class="fa fa-file-code-o"></i></a> |<a target="_blank" href="/rest/data/{$uri[last()]}.xml">REST</a></li>
                    <li>Kommentare und Register
                        <ul>
                            <li><a href="ueberblickskommentar.html?id=/xml/data/{$uri[last()]}.xml">Überblickskommentar</a></li>
                            <li>Stellenkommentar</li>
                            <li>Register
                                <ul>
                                    <li><a href="register-listPerson.html?nb={$uri[last()]}">Register der Personen und Werke</a></li>
                                    <li><a href="register-list-works.html?nb={$uri[last()]}">Register der Werke</a></li>
                                    <li><a href="register-list-Fontane.html?nb={$uri[last()]}">Register der Werke Theodor Fontanes</a></li>
                                    <li><a href="register-list-periodicals.html?nb={$uri[last()]}">Register der Periodika</a></li>
                                    <li><a href="register-listPlace.html?nb={$uri[last()]}">Geographisches Register</a></li>
                                    <li><a href="register-listEvent.html?nb={$uri[last()]}">Register der Ereignisse</a></li>
                                    <li><a href="register-listOrg.html?nb={$uri[last()]}">Register der Institutionen und Körperschaften</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="inhalt.html?id=/xml/data/{$uri[last()]}.xml">Inhaltsverzeichnis</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <div id="thumb">{
                    let $img := "digilib/"||substring-after($item, ' ')||"_001.jpg?dh=350&amp;mo=jpg"
                    return
                    element xhtml:img {
                        attribute class {"imgLazy"},
                        attribute src {
                            if ($thisIn) then $img else "/public/img/loader.svg"
                        },
                        if(not($thisIn)) then
                            attribute data-original {$img}
                        else ()
                        }
                    }
                </div>
            </div>
        </div>
        </div>
        </div>
      </div>
};

declare function f-misc:content($node as node(), $model as map(*), $id, $page){
<div id="nb-title" class="col-xs-3 col-md-2" title="reset layout">
    <h1 class="animated slideInLeft">
        <span>{f-misc:nbTitle(request:get-parameter('id', ''), $model)}
{if(matches($page, '\d+(r|v)')) then ': '||replace($page, "-alt", "") else ()}</span>
    </h1>
</div>,
<div id="facsBtn" class="col-xs-1 col-md-2{ if(contains(request:get-cookie-value('facs'), 'inactive')) then ' inactive' else ()}">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-image-o"></i></span>
        <span class="hidden-sm hidden-xs">Faksimile</span>
    </h1>
</div>,
<div id="transBtn" class="col-xs-1 col-md-2 col-md-offset-1 { if(contains(request:get-cookie-value('trans'), 'inactive')) then ' inactive' else ()} ">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-text-o"></i></span>
        <span class="hidden-sm hidden-xs">Transkription</span>
    </h1>
</div>,
<div id="xmlBtn" class="col-xs-1 col-md-2 col-md-offset-1 {if(contains(request:get-cookie-value('xml'), 'inactive')) then ' inactive' else ()}">
    <h1 class="animated slideInLeft">
        <span class="hidden-md hidden-lg"><i class="fa fa-file-code-o"></i></span>
        <span class="hidden-sm hidden-xs">XML</span>
    </h1>
</div>
};


declare function f-misc:editedTextContent($node as node(), $model as map(*), $id) {
    let $xml := request:get-parameter-names()
    let $xml := request:get-parameter("page", "")

    return
        <div id="nb-title" class="col-xs-2 col-md-2 col-md-offset-1" title="reset layout">
            <h1 class="animated slideInLeft">
                <span>{f-misc:nbTitle(request:get-parameter('id', ''), $model)}</span>
            </h1>
        </div>,
        <div id="optionsBtn" class="col-xs-2 col-md-3 col-md-offset-1 dropdown">
            <h1 class="animated slideInLeft edited-text-nav-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                <span class="hidden-md hidden-lg"><i class="fa fa-cogs"></i></span>
                <span class="hidden-sm hidden-xs">Registereinträge <i class="fa fa-chevron-down"></i></span>
            </h1>
            <ul class="dropdown-menu et-dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation"><a class="options-item" id="persons">Zeige Personen</a></li>
                <li role="presentation"><a class="options-item" id="works">Zeige Werke</a></li>
                <li role="presentation"><a class="options-item" id="place">Zeige Orte</a></li>
                <li role="presentation"><a class="options-item" id="orgs">Zeige Institutionen und Körperschaften</a></li>
                <li role="presentation"><a class="options-item" id="event">Zeige Ereignisse</a></li>
                <li role="presentation" class="divider"></li>
                <li role="presentation"><a class="clear-item">Blende alle Registereinträge aus</a></li>
            </ul>
        </div>,
        <div id="synBtn" class="col-xs-2 col-md-3 col-md-offset-1">
            <a href="edition.html?id={$id}&amp;page=">
                <h1 class="animated slideInLeft">
                    <span class="hidden-md hidden-lg"><i class="fa fa-file-text-o"></i></span>
                    <span class="hidden-sm hidden-xs">Synoptische Ansicht</span>
                </h1>
            </a>
        </div>,
        <div class="col-xs-5 col-md-1">
            <h1 id="infoViewBtn" class="animated slideInRight inactive">
                <span>
                    <i class="fa fa-info-circle"></i>
                </span>
            </h1>
        </div>,
        <div class="section-header animated slideInTop" id="infoView" style="display:none; position:fixed; z-index:100; width: 100%;margin-top: 37px;">
            <div class="container">
                 Informationen zu Schreiberhänden/Schreibmedien und anderen Details erhalten Sie durch Bewegen der Maus über den Text.
            </div>
        </div>,
        <div id="helperUnderSectionHeader" style="height: 75px;"/>
};


declare function f-misc:buttons($node as node(), $model as map(*)){
let $cookies := request:get-cookie-names()
let $pagenum := request:get-parameter('page', 'outer_front_cover')
let $pagenum := if ($pagenum = '') then 'outer_front_cover' else $pagenum
let $page := doc( config:get("data-dir") || request:get-parameter('id', '/xml/data/16b00.xml'))//tei:surface[@n = $pagenum]
let $entities := distinct-values($page//tei:rs/substring-before(@ref, ':'))
let $rslistItems := for $e in $entities
                    order by $e
                    return
                        switch($e)
                            case 'psn' return <li class="psn rsHigh">Personen</li>
                            case 'plc' return <li class="plc rsHigh">Orte</li>
                            case 'wrk' return <li class="wrk rsHigh">Werke</li>
                            case 'eve' return <li class="eve rsHigh">Ereignisse</li>
                            case 'org' return <li class="org rsHigh">Institutionen</li>
                            default return <li class="none">Keine</li>
let $datelistItems := if($page//tei:date) then for $item in distinct-values( $page//tei:date/string(@when-iso) ) return <li>{$item}</li> else <li class="none">Keine</li>
let $reflistItems := if($page//tei:ref[not(parent::tei:figDesc)]) then for $item in distinct-values( $page//tei:ref[not(parent::tei:figDesc)]/string-join(., ' ') ) return <li>{$item}</li> else <li class="none">Keine</li>

return
switch ( config:get('sade.develop') )
    case "true" return
		<div class="row">
			<div id="rsBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'rs') or contains(request:get-cookie-value('rs'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Entitäten</span>
				</h1>
				<div id="rsBtnRefine">
				    <ul>
				        {$rslistItems}
				    </ul>
				</div>
			</div>
			<div id="dateBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'date') or contains(request:get-cookie-value('date'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Datierung</span>
				</h1>
				<div id="dateBtnRefine">
				    <ul>
				        {$datelistItems}
				    </ul>
				</div>
			</div>
			<div id="refBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'ref') or contains(request:get-cookie-value('ref'), 'none')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Referenzierungen</span>
				</h1>
				<div id="refBtnRefine">
				    <ul>
				        {$reflistItems}
				    </ul>
				</div>
			</div>
		</div>
		default return
		<div class="row">
			<div id="rsBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'rs') or contains(request:get-cookie-value('rs'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Entitäten</span>
				</h1>
				<div id="rsBtnRefine">
				    <ul>
				        {$rslistItems}
				    </ul>
				</div>
			</div>
			<div id="dateBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'date') or contains(request:get-cookie-value('date'), 'inactive')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Datierung</span>
				</h1>
				<div id="dateBtnRefine">
				    <ul>
				        {$datelistItems}
				    </ul>
				</div>
			</div>
			<div id="refBtn" class="col-xs-4{ if(not(request:get-cookie-names() = 'ref') or contains(request:get-cookie-value('ref'), 'none')) then ' inactive' else ()}">
				<h1 class="animated slideInLeft">
				    <span>Referenzierungen</span>
				</h1>
				<div id="refBtnRefine">
				    <ul>
				        {$reflistItems}
				    </ul>
				</div>
			</div>
		</div>
};

declare function f-misc:mirador($node as node(), $model as map(*), $n){
let $sandbox := doc( "/db/sade-projects/textgrid/data/xml" || "/sandbox-iiif.xml" )
let $url := "https://textgridlab.org/1.0/iiif/mirador/?uri="
let $book := if(string-length($n) = 2) then substring($n, 1, 1) || "0" || substring($n, 2, 1)  else $n
let $uri := string($sandbox//tgmd:title[matches(., "Notizbuch_" || $book || "-IIIF")]/ancestor::tgmd:generic//tgmd:textgridUri)
return
    <iframe src="{$url || $uri}" height="100%" width="100%"/>
};

declare function f-misc:debug( $node as node(), $model as map(*) ){
    <meta name="debug" content="{ request:get-url() }"/>
};

declare function f-misc:ToolbarExtended($node as node(), $model as map(*)) {
let $develop := config:get('sade.develop')
return
switch ($develop)
    case "true" return
        <div id="ToolbarExtended">
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'hi') or contains(request:get-cookie-value('hi'), 'inactive')) then ' inactive' else ()}" id="hiBtn">hi</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'del') or contains(request:get-cookie-value('del'), 'inactive')) then ' inactive' else ()}" id="delBtn">del</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'add') or contains(request:get-cookie-value('add'), 'inactive')) then ' inactive' else ()}" id="addBtn">add</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'mod') or contains(request:get-cookie-value('mod'), 'inactive')) then ' inactive' else ()}" id="modBtn">mod</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'seq') or contains(request:get-cookie-value('seq'), 'inactive')) then ' inactive' else ()}" id="seqBtn">seq</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'latn') or contains(request:get-cookie-value('latn'), 'inactive')) then ' inactive' else ()}" id="latnBtn">Latn</button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'script') or contains(request:get-cookie-value('script'), 'inactive')) then ' inactive' else ()}" id="scriptBtn">script
                <div class="BtnExt">
                    <ul>
                        <li class="clean">clean</li>
                        <li class="hasty">hasty</li>
                        <li class="angular">angular</li>
                    </ul>
                </div>
            </button>
            <button class="btn btn-default{ if(not(request:get-cookie-names() = 'med') or contains(request:get-cookie-value('med'), 'inactive')) then ' inactive' else ()} disabled" id="medBtn">medium</button>
        </div>
    default return <!-- develop feature -->
};

declare function f-misc:textgridStatus($node as node(), $model as map(*)) {
  let $col := "/db/sade-projects/textgrid"
  let $res := "tgstatus.xml"
  let $url := "https://dariah-de.github.io/status/textgrid/repstatus.html"
  let $lastMod := xmldb:last-modified($col, $res)
  let $path := $col || "/" || $res
  let $doc := doc( $path )
  let $active := not( exists( $doc//ok ) )
  let $needUpdate := not(doc-available($path)) or $lastMod < (current-dateTime() - xs:dayTimeDuration("PT6H"))
  let $getStatus :=
      if( $needUpdate )
      then
        let $status := hc:send-request(<hc:request method="get" href="{$url}"/>)[2]//xhtml:div[contains(@class, 'repstatus')][not( contains(@class, 'ok') )]
        let $status := if( exists( $status ) ) then $status else <ok/>
        return
            (xmldb:login($col, config:get("sade.user"), config:get("sade.password")),
            xmldb:store($col, $res, $status))
    else if($active and $lastMod < (current-dateTime() - xs:dayTimeDuration("PT2H")))
        then
            let $status := hc:send-request(<hc:request method="get" href="{$url}"/>)[2]//xhtml:div[contains(@class, 'repstatus')][not( contains(@class, 'ok') )]
            let $status := if( exists( $status ) ) then $status else <ok/>
            return
              (xmldb:login($col, config:get("sade.user"), config:get("sade.password")), xmldb:store($col, $res, $status))
    else ()
let $doc := doc( $col || "/" || $res )
return
    let $status := $doc/xhtml:div[@class]/tokenize(@class, '\s+')[last()]
    return
    if( $doc//ok ) then () else
        <div class="alert alert-{if($status = "error") then "danger" else $status}">
            <h4>TextGrid-Statusmeldung</h4>
            <p>Es liegt eine aktuelle Meldung zum TextGrid Repository vor. Einige
            Funktionen, z.Bsp die Darstellung der Faksimiles, sind vom TextGrid
            Repository abhängig und daher evtl. davon betroffen. Hier folgt die
            Meldung von TextGrid.</p>
            <h5>Status: {$status}</h5>
            {for $i at $pos in $doc/xhtml:div//xhtml:p[@lang="de"] return (if($pos gt 1) then <hr/> else (),$i)}
        </div>
};

declare function f-misc:serverStatus($node as node(), $model as map(*)) {
  let $col := "/db/sade-projects/textgrid"
  let $res := "serverstatus.xml"
  let $url := "https://dariah-de.github.io/status/index.html"
  let $lastMod := xmldb:last-modified($col, $res)
  let $path := $col || "/" || $res
  let $doc := doc( $path )
  let $active := not( exists( $doc//ok ) )
  let $needUpdate := not(doc-available($path)) or $lastMod < (current-dateTime() - xs:dayTimeDuration("PT6H"))
  let $getStatus :=
      if( $needUpdate )
      then
        let $status := hc:send-request(<hc:request method="get" href="{$url}"/>)[2]//*:li[contains(., "Fontane")]/ancestor::*:div[contains(@class, "alert")]
        let $status := if( exists( $status ) ) then $status else <ok/>
        return
            (xmldb:login($col, config:get("sade.user"), config:get("sade.password")),
            xmldb:store($col, $res, $status))
    else if($active and $lastMod < (current-dateTime() - xs:dayTimeDuration("PT2H")))
        then
            let $status := hc:send-request(<hc:request method="get" href="{$url}"/>)[2]//*:li[contains(., "Fontane")]/ancestor::*:div[contains(@class, "alert")]
            let $status := if( exists( $status ) ) then $status else <ok/>
            return
              (xmldb:login($col, config:get("sade.user"), config:get("sade.password")), xmldb:store($col, $res, $status))
    else ()
let $doc := doc( $col || "/" || $res )
return
    let $status := $doc/*:div[@class]/tokenize(@class, '\s+')[last()]
    return
    if( $doc//ok ) then () else
        <div class="alert {$status}">
            <h4>Statusmeldung</h4>
            <p>Es liegt eine aktuelle Meldung zum Betrieb dieses Servers vor.</p>
            <h5>Status: {substring-after($status, "-")}<br/>
                Datum: {string-join( ($doc//*:time/string(@datetime)), ";")}</h5>
              <!-- at DARIAH-DE status page no p[@lang='de'] available -->
            <p>Die vollständige Statusmeldung finden Sie unter <a href="https://dariah-de.github.io/status/">dariah-de.github.io/status/</a></p>
        </div>
};

declare function f-misc:nbTitle($id as xs:string, $model as map(*)){
let $doc := doc( config:get("data-dir") || $id)
let $title := $doc//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/string(.)
let $beta := if(contains(($doc//tei:revisionDesc/tei:change)[last()]/text(), 'Version 0.')) then <xhtml:sup>beta</xhtml:sup> else ()
return (
(:    <xhtml:span class="hidden-sm hidden-xs">{substring-before($title, ' ')}</xhtml:span>,:)
    <xhtml:span>{' '||substring-after($title, ' ')}</xhtml:span>,
    $beta
    )
};

declare function f-misc:pageNav($node as node(), $model as map(*), $id as xs:string, $page as xs:string)
as element()* {
if(request:get-parameter-names() = 'page')
    then
        let $uri := $id => substring-after("/xml/data") => substring-before(".xml")
        let $page := if($page = '') then 'outer_front_cover' else $page
        let $indexFile := doc('/db/sade-projects/textgrid/data/xml/xhtml/' || $uri || "/toc.xml" )
        let $pageList2 := $indexFile//@data-page/string()
        let $index2 := (index-of($pageList2, $page))[1]
        let $return :=
        (
            (
            if ( $index2 gt 1 )
            then
            <a id="navPrev" href="edition.html?id={$id}&amp;page={$pageList2[$index2 - 1]}">
			<!-- slash needed in LIVE?  -->
                <i class="fa fa-chevron-left"></i>
                <span id="navPrevInfo"> vorige Seite </span>
            </a> else ()
            )
            ,
            (
            if ( $index2 lt count($pageList2) )
            then
            <a id="navNext" href="edition.html?id={$id}&amp;page={$pageList2[$index2 + 1]}">
			<!-- slash needed in LIVE?  -->
                <span id="navNextInfo"> nächste Seite </span>
                <i class="fa fa-chevron-right"></i>
            </a>
            else ()
            ),
            element script {
                '$(document).keyup(function(e){
                    var next = jQuery.isEmptyObject( $("#navNext").attr("href") );
                    var prev = jQuery.isEmptyObject( $("#navPrev").attr("href") );
                    if ( (e.keyCode == 37) &amp;&amp; prev == false )
                        {location.href = $( "#navPrev" ).attr("href"); }
                    else if (e.keyCode == 39 &amp;&amp; next == false )
                     {location.href = $( "#navNext" ).attr("href"); }
                });'
            }
        )
        return $return
else ()
};


declare function f-misc:notebookNav($node as node(), $model as map(*)) {
    let $showcases :=
        (
            "3qtcz.xml", (: case C :)
            "3qtqv.xml", (: case A :)
            "3qtqw.xml", (: case B :)
            "3qtqx.xml", (: case D :)
            "3qtqz.xml"  (: case E :)
        )
    let $showcases :=
        for $case in $showcases return
            doc("/db/sade-projects/textgrid/data/xml/data/" || $case)
    let $current-nb-uri :=
        request:get-parameter("id", "")
        => substring-after("data/")
        => substring-before(".xml")
    let $prev-nb := $showcases//*[@href = $current-nb-uri]/preceding-sibling::*[1]/@href
    let $next-nb := $showcases//*[@href = $current-nb-uri]/following-sibling::*[1]/@href
    let $prev-nb-title := $showcases//*[@href = $current-nb-uri]/preceding-sibling::*[1]/@type/string()
    let $next-nb-title := $showcases//*[@href = $current-nb-uri]/following-sibling::*[1]/@type/string()


    return
        (if($prev-nb) then
            <a id="navPrev" href="edierter-text.html?id=/xml/data/{$prev-nb}.xml&amp;page=">
        	<!-- slash needed in LIVE?  -->
                <i class="fa fa-chevron-left"></i>
                <span id="navPrevInfo" class="etNavPrevInfo">gehe zu {$prev-nb-title}</span>
            </a>
        else
            (),
        if($next-nb) then
            <a id="navNext" href="edierter-text.html?id=/xml/data/{$next-nb}.xml&amp;page=">
        	<!-- slash needed in LIVE?  -->
                <span id="navNextInfo" class="etNavNextInfo">gehe zu {$next-nb-title}</span>
                <i class="fa fa-chevron-right"></i>
            </a>
        else
            ())
};


declare function f-misc:tocHeader($node as node(), $model as map(*)){
<div id="nb-title" class="col-xs-4 col-md-2">
    <h1 class="animated slideInLeft">
        <span>{f-misc:nbTitle(request:get-parameter('id', ''), $model)}</span>
    </h1>
</div>
};
