xquery version "3.1";
(:~
 : This is the main test script for fontane specific modules.
 : it is supposed to run during post installation stage.
 :
 : @version 1.0.0
 : @since 2.5.6
 : @author Michelle Weidling
 : @author Mathias Göbel
 :)

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

(: stellenkommentar :)
test:suite(inspect:module-functions(xs:anyURI("stellenkommentar.xqm"))),

(: misc module :)
test:suite(inspect:module-functions(xs:anyURI("misc.xqm"))),

(: index api  :)
test:suite(inspect:module-functions(xs:anyURI("index-api.xqm"))),

(: index viewer :)
test:suite(inspect:module-functions(xs:anyURI("index-viewer.xqm"))),

(: transform :)
test:suite(inspect:module-functions(xs:anyURI("transform-tests.xqm")))

(: all things simplifying the TEI encoding :)
(: test:suite(inspect:module-functions(xs:anyURI("edited-text/tests/teisimple-test-runner.xq"))), :)
(: test:suite(inspect:module-functions(xs:anyURI("edited-text/tests/tidysimple-test-runner.xq"))), :)
(: test:suite(inspect:module-functions(xs:anyURI("edited-text/tests/index-info-test-runner.xq"))) :)
