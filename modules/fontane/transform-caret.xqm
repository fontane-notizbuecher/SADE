xquery version "3.1";
(:~
 : This module provides function corresponding to attribute::tei:rend describing carets.
 :
 : Types currently used are:
 : [x] "caret:bow"
 : [x] "caret:curved_V"
 : [x] "caret:slash"
 : [x] "caret:backslash https://fontane-nb.dariah.eu/test/edition.html?id=%2Fxml%2Fdata%2F22jtq.xml&page=39v"
 : [x] "caret:half-bow"
 : [x] "caret:V"
 : [x] "caret:semicircle [bottom: style via transform]"
 : [x] "caret:quartercircle https://fontane-nb.dariah.eu/test/edition.html?id=%2Fxml%2Fdata%2F22jtp.xml&page=4v"
 : [o] "caret:looped_arc"
 : [>] "caret:funnel"
 : [x] "caret:3/4-circle [style via transform]"
 : [o] "caret:retraced_half-bow | eine Verwendung A4 42v https://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/22jtv.xml&page=42v"
 :
 : x erledigt
 : o nicht unterstützt
 : > in Arbeit
 :
 :)

module namespace caret="http://fontane-nb.dariah.eu/Transfo/caret";

declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function caret:slash() {
<svg xmlns="http://www.w3.org/2000/svg"
    class="teicaret slash"
    style="right: -8px;"
    height="20"
    width="20">
    <line y2="0" x2="20" y1="20" x1="5"/>
</svg>
};

declare function caret:v($node) {
let
    $parameter := substring-before( substring-after( $node/string(@rend), '('), ')'),
    $parameter := tokenize(replace($parameter, 'cm', ''), ','),
    $l := number($parameter[1]) * 100,
    $r := number($parameter[2]) * 100,
    $height := '100',
    $viewbox := '0 0 ' || string($l+ $r) || ' ' || $height,
    $style := string-join(($node/tokenize(@style)[not(contains(., "margin-left"))], 'margin-left:-' || $parameter[1] || 'cm'), ";")

return
    <svg xmlns="http://www.w3.org/2000/svg"
        class="caretV"
        style="{$style}"
        width="{string(($l + $r) div 100)}cm"
        height="0.6cm"
        viewBox="{$viewbox}"
        preserveAspectRatio="none">
        <polyline
            points="0,0 {string($l)},{$height} {string($l + $r)},0"
            fill="none"
            stroke-width="3"/>
    </svg>
};

declare function caret:half-bow($node, $lineHeight) {
let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
let $width := $params[1]
let $pos := $params[2]

let $seq := tokenize($node/string(@style), ':|\s')
let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
    $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
let $viewW := number( substring-before($width, 'cm') ) * 100

return
    switch($pos)
        case 'pos-right' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="caret_half-bow_pos-right"
            width="{$width}"
            height="{$lineHeight - 0.2}cm"
            viewBox="0 0 {$viewW} 100"
            preserveAspectRatio="none"
            style="left:{string(if($mlnum lt -0.19) then -0.2 else $mlnum)}cm;">
            <path
                d="M30,95 C30,95 5,100 5,60 M 5,60 C 5,20 {$viewW div 2},10 {$viewW - 5},5"
                fill="none"
                stroke-width="2"/>
        </svg>
        case 'pos-left' return
          <svg xmlns="http://www.w3.org/2000/svg"
            class="caret_half-bow_pos-left"
            width="{$width}" height="{$lineHeight - 0.2}cm"
            viewBox="0 0 100 100"
            preserveAspectRatio="none"
            style="left:{$mlnum}cm;">
            <path
                fill="none"
                stroke-width="2"
                d="M 78.36,37.00 C 96.91,33.55 108.45,20.36 86.00,10.45 86.18,10.55 72.27,2.73 4.55,9.09" />
          </svg>
    default return
        error(QName("https://sade.textgrid.de/ns/error/fontane", "CARET-HALF-BOW"), "unsupported position: " || $pos)
};

declare function caret:retraced-half-bow($node, $lineHeight) {
let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
let $width := $params[1]
let $pos := $params[2]

let $seq := tokenize($node/string(@style), ':|\s')
let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
    $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
let $viewW := number( substring-before($width, 'cm') ) * 100

return
    switch($pos)
        case 'pos-right' return
        <svg xmlns="http://www.w3.org/2000/svg"
            width="{$width}"
            height="{$lineHeight}cm"
            viewBox="0 0 {$viewW} 100"
            preserveAspectRatio="none"
            style="left:{string($mlnum - 0.3)}cm;top: 0;">
            <path
                d="M30,95 C30,95 5,100 5,60 M 5,60 C 5,20 {$viewW div 2},10 {$viewW - 5},5"
                fill="none"
                stroke-width="2"/>
            <path
                d="M35,95 C35,95 10,100 10,60 M 10,60 C 10,20 {$viewW div 2},10 {$viewW - 5},10"
                fill="none"
                stroke-width="2"/>
        </svg>
    default return
        <xhtml:div>{$pos} no example in Gesamtdoku</xhtml:div>
};

declare function caret:bow($node as element(*), $lineHeight) {
let $params:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ',')
let $width := $params[1]
let $pos := $params[2]

let $seq := tokenize($node/string(@style), ':|\s')
let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
    $mlnum := number( substring-before( $seq[$mlnum], 'cm') )

let $viewW := number( substring-before($width, 'cm') ) * 100,
    $p := $viewW - ($viewW * 0.05),
    $q := $viewW * (3 div 8)

return

switch($pos)
    case 'pos-right' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="caret_bow_pos-right"
            width="{$width}"
            height="{$lineHeight}cm"
            viewBox="0 0 {$viewW} 80"
            preserveAspectRatio="none"
            style="left:-0.1cm;">
            <path
                d="M{$p}, 5 C{$p},  5 {$viewW}, 10 {$p}, 18 M{$p}, 18 C{$p}, 18   0, 10   5, 60 M  5, 60 C {$viewW div 10},100 {$viewW div 3}, 60  {$q - 15}, 70"
                fill="none"
                stroke-width="2"/>
        </svg>

    case 'pos-left' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="caret_bow_pos-left"
            width="{$width}"
            height="{$lineHeight}cm"
            viewBox="0 0 {$viewW} 80"
            preserveAspectRatio="none"
            style="left:{string($mlnum)}cm;">
            <path
                d="M10, 5 C10,5 0,15 10,20 M10,20 C{$viewW div 3},35 {$viewW},30 {$viewW - 5},65 M{$viewW - 5},65 C{$viewW - 5},80 {($viewW div 2) + ($viewW * 0.25)},75 {($viewW div 2) + ($viewW * 0.25)},75"
                fill="none"
                stroke-width="2"/>
        </svg>

    default return
        error(QName("https://sade.textgrid.de/ns/error/fontane", "CARET-BOW"), "unsupported position: " || $pos)
};

declare function caret:backslash($node as element()) {
    <svg xmlns="http://www.w3.org/2000/svg"
        class="backslash"
        height="20"
        width="20">
        <line x1="0" y1="0" x2="15" y2="20"/>
    </svg>
};

declare function caret:semicircle($node, $lineHeight) {
()
(: styled via border, see transform.xqm, for svg see older revisions here :)
};

declare function caret:quartercircle($node, $lineHeight) {
let $pos := substring-before(substring-after($node/@rend, '('), ')')
let $class := (substring-after($node/@rend, "caret-medium:") => replace(";", ""))
let $seq := tokenize($node/string(@style), ':|\s')
let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
    $mlnum := number( substring-before( $seq[$mlnum], 'cm') )
let $width := if($mlnum lt 0) then $mlnum * -1 else $mlnum
let $width := if($width = 0) then 0.7 else $width
let $width := if(string($width) = "NaN" ) then 1.2 else $width
return
    switch($pos)
    case 'upper-right' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="quartercircle-upper-right {$class}"
            width="{$width}cm"
            height="{$lineHeight - 0.2}cm"
            viewBox="0 0 100 50"
            preserveAspectRatio="none">
            <path d="M0,10 C 0,10 95,-10 95,45" fill="none" stroke-width="2"/>
        </svg>
    case 'upper-left' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="quartercircle-upper-left"
            width="1cm"
            height="{$lineHeight - 0.2}cm"
            viewBox="0 0 100 50"
            preserveAspectRatio="none">
            <path d="M3,95 C 3,95 0,35 60,20 M60,20 C60,20 90,15 100,20" fill="none" stroke-width="2"/>
        </svg>
    case 'lower-left' return
        <svg xmlns="http://www.w3.org/2000/svg"
            class="quartercircle-lower-left"
            width="{$width * 3}cm"
            height="{$lineHeight - 0.2}cm"
            viewBox="0 0 100 55"
            preserveAspectRatio="none">
            <path
                id="quartercircle_lower-left"
                fill="none"
                stroke="black"
                stroke-width="1"
                d="M 3.30,48.00 C 3.30,48.00 35.09,65.78 63.43,4.74" />
        </svg>
    default return <xhtml:div>unknown parameter in function 'caret'</xhtml:div>
};

declare function caret:funnel($node) {
let $params as xs:decimal+ := tokenize(substring-before(substring-after($node/replace(@rend, "cm", ""), '('), ')'), ',') ! xs:decimal(.)
return
<svg xmlns="http://www.w3.org/2000/svg"
    style="margin-left: -1cm;margin-bottom: 0.2cm;"
    width="{$params[1] + $params[2]}cm"
    viewBox="0 0 350 231"
    height="0.5cm">
    <path
        id="right"
        fill="none"
        stroke="darkslategrey"
        d="M 222.50,210.75 C 222.50,210.75 345.00,11.25 345.00,11.25"
        stroke-width="5"></path>
    <path
        id="left"
        fill="none"
        stroke="darkslategrey"
        d="M 33.00,12.50 C 33.00,12.50 172.25,205.75 172.25,205.75 .75,167.50 -68.00,46.25"
        stroke-width="5"></path>
  </svg>
};

declare function caret:curved-v($node, $lineHeight) {
let $seq := tokenize($node/string(@style), ':|\s')
let $class := ("curved_V", (substring-after($node/@rend, "caret-medium:") => replace(";", "")))
let $mlnum := (for $i at $pos in $seq where contains($i, 'margin-left') return $pos)+1,
    $mlnum := string(number( substring-before( $seq[$mlnum], 'cm') ) - 0.1) || 'cm'
let $width:= tokenize(substring-before(substring-after($node/@rend, '('), ')'), ','),
    $w1:=number(substring-before($width[1], 'cm')),
    $w2:=number(substring-before($width[2], 'cm')),
    $x:=($w1 * 100) div ($w1 + $w2),
    $width:= $w1 + $w2,
    $viewBoxHeight := 100,
    $viewBoxWidth := 300,
    $turn1 := round( (($viewBoxWidth div 2) * ($w1 div $w2)) ),
    $turn := ($viewBoxWidth div $width * $w1) + 10

return
<svg xmlns="http://www.w3.org/2000/svg"
    class="{$class}"
    height="100%"
    width="{$width}cm"
    viewBox="0 0 {$viewBoxWidth} {$viewBoxHeight}"
    preserveAspectRatio="none"
    style="left:{ 0 - $width div $viewBoxWidth * $turn }cm;">
    <path
        d="M  10,40 C   0, 45   0, 55  10,60 M  10,60 C 10, 60 {$turn + 5}, 60 {$turn + 5},80 M {$turn + 5},80 C {$turn + 5}, 80 {$turn + 2}, 87 {$turn},80 M {$turn},80 C {$turn + 2}, 50 {290 - (290 - $turn) div 2}, 60 290,60 M 290,60 C 300, 55 300, 45 290,40"
        stroke-width="4"
        fill="none" />
</svg>
};
