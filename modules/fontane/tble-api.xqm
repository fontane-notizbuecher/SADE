xquery version "3.1";
(:~
 : This module provides an API to retreive images based on a TextGrid URI and a
 : corresponding xml:id used in compliance with the Text-Image-Link-Editor.
 : It queries the database for the latest link based on the xml:id and returns
 : the image.
 :
 : @author Mathias Göbel
 : @version 1.0
 : @since 2.5.1
 : :)

module namespace tbleapi="https://fontane-nb.dariah.eu/tble";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace xlink="http://www.w3.org/1999/xlink";

import module namespace rest="http://exquery.org/ns/restxq";
import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "transform.xqm";

(:~
 : retreives parameters via REST and returns the corresponding
 : image. Resolution is always 1000px in height.
 :
 : @param $uri A valid TextGrid Base-URI without prefix
 : @param $xmlid Any xml:id used inside the document specified by $uri
 : @param $format The return format (IIIF: http://iiif.io/api/image/2.0/#format)
 : @return response object with the binary data
 :  :)
declare
  %rest:GET
  %rest:path("/api/tble/{$uri}-{$xmlid}.{$format}")
  %output:method("binary")

  %test:arg("uri", "16b00")
  %test:arg("xmlid", "a1")
  %test:arg("format", "jpg")
function tbleapi:stream-image($uri as xs:string, $xmlid as xs:string, $format as xs:string)
{
let $url := tbleapi:get-url($uri, $xmlid, $format)
let $request := <hc:request method="get" href="{ $url }" />
let $response := hc:send-request($request)
let $data := $response[2]
let $binary-data :=
  try { $data => xs:base64Binary() }
  catch * {
    error(
      QName("err", "load1"),
      string-join(("&#10;Could not load data from TextGrid: ", $data, $err:code, $err:description), "&#10;"))
  }
let $mime := string($response[1]/hc:body/@media-type)

return
  $binary-data
};

(:~
 : retrieves the URL to a clipping generated with Text-Image-Link-Editor without increasing contrast
 :
 : @param $uri A valid TextGrid Base-URI without prefix
 : @param $xmlid Any xml:id used inside the document specified by $uri
 : @param $format The return format (IIIF: http://iiif.io/api/image/2.0/#format)
 : @return the URL to the image as xs:string
 :)
declare function tbleapi:get-url($uri as xs:string, $xmlid as xs:string, $format as xs:string)
as xs:string {
    tbleapi:get-url($uri, $xmlid, $format, false())
};

(:~
 : retrieves the URL to a clipping generated with Text-Image-Link-Editor, increasing the contrast
 :
 : @param $uri – A valid TextGrid Base-URI without prefix
 : @param $xmlid – Any xml:id used inside the document specified by $uri
 : @param $format – the return format (IIIF: http://iiif.io/api/image/2.0/#format)
 : @param $contrast – indicates whether contrast should be increased
 : @return the URL to the image as xs:string
 :)
declare function tbleapi:get-url($uri as xs:string, $xmlid as xs:string, $format as xs:string, $contrast as xs:boolean) as xs:string {
    let $link := fontaneTransfo:newestTBLELink($uri, $xmlid) (: TODO: move this function here :)
    let $shape := $link/@targets => substring-after('#') => substring-before(' ')
    let $svgg := $link/root()//svg:g[@id = $link/parent::tei:linkGrp/substring-after(@facs, '#')]
    let $image := substring-before($svgg//svg:image/@xlink:href, ".")
    let $rect := $svgg//svg:rect[@id = $shape]
    let $x := $rect/@x => substring-before('%')
    let $y := $rect/@y => substring-before('%')
    let $w := $rect/@width => substring-before('%')
    let $h := $rect/@height => substring-before('%')

    let $rotation := 0
    let $contrast :=
        if ($contrast) then
            "cont=2"
        else
            ()

    let $url := "https://textgridlab.org/1.0/digilib/rest/IIIF/"
      || $image || '/pct:' || $x || ',' || $y || ',' || $w || ',' || $h
      || "/,1000/" || $rotation || "/default." || $format
    return
      $url
};
