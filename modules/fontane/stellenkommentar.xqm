xquery version "3.1";
(:~
 : This module provides an API to retrieve textual commentary entries as a sequence for a
 : given notebook or page.
 :
 : @author Simon Sendler
 : @author Mathias Göbel
 : @version 1.2
 : @since 3.2.2
 : :)

module namespace stk="https://fontane-nb.dariah.eu/stk-api";

declare namespace output ="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace functx="http://www.functx.com";
import module namespace ixp="http://fontane-nb.dariah.eu/index-processor" at "index-processor.xqm";
import module namespace rest="http://exquery.org/ns/restxq";

declare variable $stk:dataPath := "/db/sade-projects/textgrid/data/xml/data";
declare variable $stk:dataCollection := collection($stk:dataPath);

(:~
 : wrapper function to provide a versionized REST-API
 : @see stk:entities-per-page
 :)
declare
  %private
  %rest:GET
  %rest:path("/api/v1/stk/{$uri}/{$surface}")
  %output:method("html5")
function stk:v1-entities-per-page($uri as xs:string, $surface as xs:string)
as element(xhtml:div) {
    stk:entities-per-page($uri, $surface)
};

(:~
 : lists all textual commentary on a given page
 :
 : @param $uri A valid TextGrid-Base-URI without prefix
 : @param $surface A string to match a tei:surface/attribute::n in the document (use “-” as delimiter)
 : @return a html5 list with all entries
 : @see http://localhost:8080/exist/restxq/api/stk/22jt7/1r
 : @see https://fontane-nb.dariah.eu/api/stk/22jt7/1r
 :  :)
declare
  %rest:GET
  %rest:path("/api/stk/{$uri}/{$surface}")
  %output:method("html5")

  %test:arg("uri", "22jt7") %test:arg("surface", "1r") %test:assertExists
  %test:arg("uri", "22jt7") %test:arg("surface", "1r") %test:assertXPath("count($result/xhtml:ul/xhtml:li) eq 3")
  %test:arg("uri", "22jt7") %test:arg("surface", "1r") %test:assertXPath("$result/xhtml:ul/xhtml:li/@data-ref")
  %test:arg("uri", "22jt7") %test:arg("surface", "1r") %test:assertXPath("$result/xhtml:ul/xhtml:li[1]/xhtml:span eq 'Kiel Nordiſches Muſeum.]'")
  %test:arg("uri", "22jt7") %test:arg("surface", "2r") %test:assertExists
  %test:arg("uri", "22jt7") %test:arg("surface", "2r") %test:assertXPath("$result/xhtml:ul")
  %test:arg("uri", "22jt7") %test:arg("surface", "2r") %test:assertXPath("not($result/xhtml:ul/xhtml:li)")
function stk:entities-per-page($uri as xs:string, $surface as xs:string)
as element(xhtml:div)
{
    let $docpath := $stk:dataPath || "/" || $uri || ".xml"
    let $doc := doc($docpath)
    let $surface := tokenize($surface, "-")
    let $page := $doc//tei:surface[@n = $surface]

    let $notes := $doc//tei:note[@type="editorial"]
    let $noteTargets := ($notes/@target ! replace(., "#", ""))
                        ! tokenize(., " ")[contains(., "_"||$surface[1]||"_") or contains(., "_"||$surface[2]||"_")]

    return
        if (not(doc-available($docpath))) then error(QName("https://sade.textgrid.de/ns/error/fontane", "STK01"), "Document not in database.") else
        if (not($surface = $doc//tei:surface/@n)) then error(QName("https://sade.textgrid.de/ns/error/fontane", "STK02"), "Surface «" || $surface || "» not available.") else
        element xhtml:div {
            attribute class { "notes" },
            element xhtml:ul {
                let $preparedNotes :=
                    for $note in $page/(.//tei:*[@xml:id=$noteTargets] | .//tei:figure[@xml:id][tei:figDesc])
                    let $note := if($note/local-name() != "figure")
                                then $doc//tei:note[contains(@target, "#" || $note/@xml:id)]
                                else $note
                    return
                        element xhtml:li {
                            attribute class { 'editorialNote' },
                            stk:noteParser($note)
                        }
                for $pN at $pos in $preparedNotes
                where functx:index-of-deep-equal-node($preparedNotes, $pN)[1] eq $pos
                return
                    $pN
            }
        }
};

(:~
 : lists all textual commentary in a given notebook
 :
 : @param $uri A valid TextGrid-Base-URI without prefix
 : @return A html5 list with all entries
 : @see http://localhost:8080/exist/restxq/api/stk/22jt7
 : @see https://fontane-nb.dariah.eu/api/stk/22jt7
 :  :)
declare
  %rest:GET
  %rest:path("/api/stk/{$uri}")
  %output:method("html5")

  %test:arg("uri", "22jt7") %test:assertExists
  %test:arg("uri", "22jt7") %test:assertXPath("count($result/xhtml:ul/xhtml:li) ge 30")
  %test:arg("uri", "22jt7") %test:assertXPath("$result/xhtml:ul/xhtml:li/@data-ref")
function stk:entities-per-book($uri as xs:string)
as element(xhtml:div)
{
    let $docpath := $stk:dataPath || "/" || $uri || ".xml"
    let $doc := doc($docpath)
    let $reference := $doc//tei:surface//@xml:id
    return
        if (not(doc-available($docpath))) then error(QName("https://sade.textgrid.de/ns/error/fontane", "STK01"), "Document not in database.") else
        element xhtml:div {
            attribute class { "notes" },
            element xhtml:ul {
                for $note in $doc//tei:note[@type="editorial"]
                where $note/@target => replace("#", "") => tokenize (" ") = $reference
                return
                    element xhtml:li {
                        attribute class { "editorialNote" },
                        stk:noteParser($note)
                    }
            }
        }
};

(:~
 : parses a tei:note and the corresponding target to a html representation form
 : Uses recursion.
 : @param $note – a tei:note to initate
 :)
declare function stk:noteParser($note as node()*)
{
    for $node in $note return
        typeswitch ($node)
        case element (tei:note) return
            let $xmlids := tokenize(replace($node/@target, "#", ""), " ")
            return (
                attribute data-ref { $xmlids },
                element xhtml:span {
                    attribute class { "target" },
                    (if (count($xmlids) = 1) then
                        stk:lemmafinder($node/preceding-sibling::tei:surface[1]//*[@xml:id = $xmlids])
                    else
                        stk:lemmafinder($node/preceding-sibling::tei:surface[1]//*[@xml:id = $xmlids[1]])
                        || " <...> " ||
                        stk:lemmafinder($node/preceding-sibling::tei:surface[1]//*[@xml:id = $xmlids[2]])
                    )
                    => replace("- |⸗ ", "")
                    || "]"

                },
                element xhtml:ul {
                    element xhtml:li {
                        stk:noteParser($node/node())
                    }
                })

        case element (tei:bibl) return
            stk:noteParser($node/node())

        case element (tei:citedRange) return (
            ", ",
            if ( starts-with($node, "http") )
                then <a title="citedRange" href="{string($node)}"><i class="fa fa-external-link"></i></a>
            else $node/text())

        case element (tei:ptr) return (
            element xhtml:a {
                let $targetId := substring-after($node/@target, ":")
                return
                (attribute href {"literaturvz.html?id=" || $targetId},
                $stk:dataCollection/id($targetId)/tei:choice/tei:abbr/text())
            },
            stk:noteParser($node/node())
            )

        case element (tei:ref) return
            let $TGuri := string($node/root()/tei:teiHeader//tei:idno[@type="TextGrid"])
            let $uri := $TGuri => substring-after(":")
            let $pattern := "^#.+_.+_.+$"
            return
                if(starts-with($node/@target, "http:")) then $node//text() else
                if (matches($node/@target, $pattern)) then
                    let $target := replace($node/@target, "#", "")
                    return
                    element xhtml:a {
                        attribute href { "edition.html?id=%2Fxml%2Fdata%2F"
                            || $uri || ".xml&amp;page=" || $node
                            || "&amp;target=" || $target },
                        stk:noteParser($node/node())
                    }
                else if (starts-with($node/@target, "#xpath(//surface"))
                    then
                    let $page := $node/@target => substring-after("'") => substring-before("'")
                    return
                        element xhtml:a {
                        attribute href { "edition.html?id=%2Fxml%2Fdata%2F"
                            || $uri || ".xml&amp;page=" || $page },
                            stk:noteParser($node/node())
                    }
                else
                    <xhtml:span style="color:red; font-weight:bold">error:
                    tei:ref/@target does not match pattern »{$pattern}« </xhtml:span>

        case element (tei:rs) return
            element xhtml:a {
                attribute href { stk:teiref2href($node/string(@ref)) },
                stk:noteParser($node/node())
            }

        case element (tei:seg) return
            element xhtml:span {
                $node/@style,
                stk:noteParser($node/node())
            }
        case element (tei:figure) return
                element xhtml:span {
                    attribute class { "target" },
                    if ($node/tei:figDesc/(@copyOf or @xml:id)) then "<Teil einer Skizze>]" else "<Skizze>]",
                    element xhtml:ul {
                        element xhtml:li {
                          if ($node/tei:figDesc/(@copyOf))
                          then stk:noteParser($node/ancestor::tei:sourceDoc//*[@xml:id eq substring-after($node/tei:figDesc/@copyOf, "#")])
                          else stk:noteParser($node/node())
                        }
                    }
                }
        case element (tei:figDesc) return
            stk:noteParser($node/node())
        case text() return $node

        default return
            stk:noteParser($node/node())
};

(:~
 : prepares a URL for a referenced item form the index
 : @param $ref An attribute::tei:ref value with prefix
 :)
declare function stk:teiref2href($ref as xs:string)
as xs:string
{
    let $prefix := substring-before($ref, ":")
    let $id := substring-after($ref, ":")
    return
        switch($prefix)
            case "lit" return
                "literaturvz.html?id=" || $id
            default return
                "register.html?e=" || $id
};

(:~
 : parses a tei-element to a string to be used as lemma
 : Uses recursion.
 : @param $target A tei-element referenced in a tei:note[@target]
 :)
declare function stk:lemmafinder($target as node()*)
as xs:string
{
    if ($target/@next) then
        let $nextid := replace($target/@next, "#", "")
        let $nextpart := $target/ancestor::tei:surface//*[@xml:id = $nextid]
        return
        string-join(
            (string-join($target//text()[not(parent::tei:expan)], ""),
            stk:lemmafinder($nextpart)), " ")
    else
        string-join($target//text()[not(parent::tei:expan)], "")
};
