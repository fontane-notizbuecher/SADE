xquery version "3.1";
(:~
 : This module provides the main transformations. It is called during
 : publication.
 : @author Mathias Göbel
 : @todo
  <xhtml:ul>
    <xhtml:li>bootstrap classes defined in here.</xhtml:li>
    <xhtml:li>at least two methods for rendering borders, one directly in local:magic()</xhtml:li>
    <xhtml:li>pre Text with link is disabled</xhtml:li>
    <xhtml:li></xhtml:li>
  </xhtml:ul>
 :)

module namespace transfo="http://fontane-nb.dariah.eu/Transfo";

declare boundary-space preserve;
declare default element namespace "http://www.tei-c.org/ns/1.0";

declare namespace digilib="digilib:digilib";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace xlink="http://www.w3.org/1999/xlink";

import module namespace caret="http://fontane-nb.dariah.eu/Transfo/caret" at "transform-caret.xqm";
import module namespace code="http://bdn-edition.de/ns/code-view";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace index="https://fontane-nb.dariah.eu/indexapi" at "index-api.xqm";
import module namespace f-misc="http://fontane-nb.dariah.eu/ns/SADE/misc" at "misc.xqm";

declare variable $transfo:tooltipReplacementPattern := '“$|\s$|,$|\.|\($|\)$|;|&#x2003;|\[\d+\]$|^Wohn⸗|^ſonder-|^\(|Landſchaften$|^Oelbil|^Schwedens|Schwedens$';

declare variable $transfo:dataCollection := collection("/db/sade-projects/textgrid/data/xml/data");

declare variable $transfo:base-url :=
    if (config:get("sade.develop") = "true")
    then "https://fontane-nb.dariah.eu/test/"
    else "https://fontane-nb.dariah.eu/";

declare variable $transfo:numberPattern := ".#####";

declare function transfo:magic($nodes as node()*) {
    transfo:magic(($nodes), "undefined")
};

(:~
 : Main function in this module, called in recursion. Used with a tei:TEI node to create a complete
 : transformation of a document.
 : @param $node A tei:TEI
 : @param $inital The inital tei:surface is wrapped in a specific xhtml:div
 : @return is specific to $nodes, so nothing declared
 :)
declare function transfo:magic($nodes as node()*, $inital as xs:string?) {
for $node in $nodes return
typeswitch($node)
  case element(tei:TEI)
    return
      element xhtml:div {
        attribute class {'TEI', 'clearfix' (: damn bootstrap in this code? damn it! :) },
        transfo:magic($node/node())
      }

    case element(tei:teiHeader)
      return ()
    case element(tei:sourceDoc)
      return
        transfo:sourceDoc($node)
    case element(tei:surface)
      return
        transfo:surfaceWrapper($node, $inital)
    case element(tei:zone)
      return
        transfo:zoneWrapper($node)
    case element(tei:figure)
      return
        transfo:figure($node)
    case element(tei:line)
      return
        transfo:line($node)
    case element(tei:seg)
      return
        transfo:seg($node)
    case element(tei:fw)
      return
        transfo:fw($node)
    case element(tei:mod)
      return
        transfo:mod($node)
    case element(tei:del)
      return
        transfo:del($node)
    case element(tei:hi)
      return
        transfo:hi($node)
    case element(tei:add)
      return
        transfo:add($node)
    case element(tei:retrace)
      return (
        transfo:retrace($node),
        transfo:magic($node/node()))

    case element(tei:restore)
      return
        transfo:restore($node)
    case element(tei:choice)
      return
        transfo:choice($node)
    case element(tei:abbr)
      return
        transfo:abbr($node)
    case element(tei:rs)
      return
        transfo:rs($node)
    case element(tei:date)
      return
        transfo:date($node)
    case element(tei:stamp)
      return
        transfo:stamp($node)
    case element(tei:metamark)
      return
        transfo:metamark($node)
    case element(tei:g)
      return
        (: horizontal bars are rendered as fractions within a tei:seg :)
        if($node[@ref="#hb"]) then () else
        transfo:glyph($node)
    case element(tei:unclear)
      return
        transfo:unclear($node)
    case element(tei:gap)
      return
        transfo:gap($node)
    case element(tei:milestone)
      return
        transfo:milestone($node)
    case element(tei:ref)
      return
        transfo:ref($node)
    case element(tei:space)
      return
        transfo:space($node)
    case element(tei:said)
      return
        transfo:said($node)
    case element(tei:note)
      return
        transfo:note($node)
    case element(tei:anchor)
      return
        transfo:anchor($node)

    case element(tei:expan) return ()
    case element(tei:addSpan) return ()
    case element(tei:handShift) return ()
    case element(tei:lb) return ()

    case text()
      return
          transfo:text($node)

    default return transfo:magic($node/node())
};

(:~
 : transformation from tei:sourceDoc to xhtml:div
 :)
declare function transfo:sourceDoc($node as element(tei:sourceDoc))
as element(xhtml:div) {
  element xhtml:div {
    attribute id {substring-after($node/@n, 'er_')},
    attribute class {'sourceDoc'},
    transfo:magic($node/node())
  }
};


(:~
 : transforms tei:surface to xhtml:div
 :)
declare function transfo:surfaceWrapper($node as element(tei:surface), $inital as xs:string)
as item()* {(
    if($node/parent::tei:sourceDoc or string($node/@n) = $inital)
    then
            element xhtml:div {
            attribute class {'rowWrapper', 'clearfix', 'butthead'},
(:                    attribute id { $node/string(@n) },:)
            transfo:facs($node),
(:                    if($node/string(@type) = ("clipping", "additional")):)
            element xhtml:div {
                if($node/@subtype="Kalenderblatt")
                (: cause of rotation handling, an additional div is needed for
                mostly rotated Kalenderblätter:)
                then
                    element xhtml:div {
                        transfo:surface($node, $inital),
                        transfo:magic($node/node())
                    }
                else(
                transfo:surface($node, $inital),
                transfo:magic($node/node()))
            },
            element xhtml:div {
                attribute class {'teixml'},
                element xhtml:pre {
                    element xhtml:b {
                        <xhtml:h3 class="xmlTitle">Auszug aus dem TEI/XML-Dokument</xhtml:h3>,
                        <xhtml:div>
                            <xhtml:a href="xml.html?id=/xml/data/{$node/preceding::tei:teiHeader//tei:idno[@type="TextGrid"]/substring-after(., 'textgrid:')}.xml">Gesamtes TEI/XML-Dokument</xhtml:a>
                            |
                            <xhtml:a target="_blank" href="rest/data/{$node/preceding::tei:teiHeader//tei:idno[@type="TextGrid"]/substring-after(., 'textgrid:')}.xml">(via REST)</xhtml:a>
                        </xhtml:div>
                    },
                    if( config:get("sade.develop") = "true" )
                    then code:main($node)
                    else code:main($node, false(), "hljs-", true()),
                    (: for normal tei:surfaces :)
                    for $n in $node/following-sibling::tei:note
                        where $n >> $node and $n << ($node/following-sibling::tei:surface)[1]
                        return  code:main($n),
                    (: for tei:surfaces that are part of a "Doppelseitenansicht".
                    in this case an associated tei:note is encoded after the first/left
                    tei:surface but should also be displayed on the second/right one. :)
                    if($node/@prev and $node/preceding-sibling::*[1][self::tei:note]) then
                        for $n in $node/preceding-sibling::tei:note
                            where $n >> $node/preceding-sibling::tei:surface[1]
                            return
                                code:main($n)
                    else
                        ()
                }
              }
            }
    else
    (
    element xhtml:div {
        transfo:surface($node, $inital),
        if($node/@type = ('clipping', 'additional'))
        then
            transfo:magic($node/node())
        else transfo:magic($node/node())
    }),
    let $ids := $node//@xml:id,
        $notes := $node/ancestor::tei:TEI//tei:note[@type="editorial"]/tokenize(replace(@target, "#", ""), " "),
        $test := (($ids = $notes) and string($node/@n) = $inital) or $node//tei:figDesc[exists(./parent::tei:figure/@xml:id)]
    return
        if($test) then
            element xhtml:div {
                attribute class {"notes"},
                element xhtml:ul {
                    for $i in $node/ancestor::tei:TEI//tei:note[@type="editorial"][tokenize(replace(@target, "#", ""), " ") = $ids]
                    return
                        element xhtml:li {
                            attribute class {'editorialNote'},
                            transfo:noteParser($i)
                        },
                        for $figDesc in $node//tei:figDesc[exists(./parent::tei:figure/@xml:id)]
                        return
                          element xhtml:li {
                              attribute class {'editorialNote'},
                              element xhtml:span {
                                attribute class {"target kursiv"},
                                text {"<Skizze>]"},
                                element xhtml:ul {
                                  element xhtml:li {
                                    string($figDesc)
                                  }
                                }
                              }
                          }
                }

            }
        else ()

)};

(:~
 : prepares the xhtml:div with attributes and nodes()
 :)
declare function transfo:surface($n, $inital) {
attribute class {
    'surface',
    (if($n/@type) then (' tei' || $n/@type) else ()),
    (if($n/@subtype) then string($n/@subtype) else ()),
    if( string($n/@subtype)="Kalenderblatt") then if(ends-with($n/@n, 'v')) then 'verso' else 'recto' else (),
    if( $n/parent::tei:surface ) then "nested" else (),
    if( not($n//tei:line[ancestor::tei:zone[@rotate = 180]]) or string($n/@subtype)="Kalenderblatt") then () else "rotate",
    $n/@rend ! string(.)
},
(: dont call it a label (bootstrap) :)
(if($n/@n) then attribute id {"s" || $n/@n} else()),
attribute style {
  let $values := distinct-values((
    if ($n/(@ulx or @uly)) then ('position:absolute;', 'top:' || $n/@uly || 'cm;', 'left:' || $n/@ulx || 'cm;') else (),
    if ($n/(@ulx and @lrx)) then ('position:absolute;' ,'left:' || $n/@ulx || 'cm;' , 'width:' || $n/@lrx - $n/@ulx || 'cm;') else (),
    if ($n/(@uly and @lry)) then ('position:absolute;', 'top:' || $n/@uly || 'cm;' , 'height:' || $n/@lry - $n/@uly || 'cm;') else (),
    if ($n/@subtype = 'Kalenderblatt')
    then (
            'top:' || $n/@uly || 'cm;',
            'left:' || $n/@ulx || 'cm;',
            'width:' || $n/@lrx - $n/@ulx || 'cm;',
            'height:' || $n/@lry - $n/@uly || 'cm;',
            "position:relative;"
        )
    else ()
    )) (: distinct values :)
  return
    (: omit top, left and position in case of a nested surface :)
      if( string($n/@n) = $inital )
      then $values[not( contains(., "absolute") ) and not( starts-with(., "top:") ) and not( starts-with(., "left:") )]
      else $values
}
};

(:~
 : transforms tei:zone to xhtml:div.
 : wrapper places some parts outside the actual div for the zone
 :)
declare function transfo:zoneWrapper($node as element(tei:zone))
as element(xhtml:div) {
element xhtml:div {
  try {
    transfo:zone($node)
  }
  catch * {
    error(QName("https://sade.textgrid.de/ns/error/fontane", "ZONE01"), "can not deal with this zone. error: " || string-join(($err:code, $err:description, $err:value), "; "))
  },
  if(
    $node/contains(@style, "border-left-style:double")
    and $node/contains(@rend,"border-left-style:wavy") )
  then
    element xhtml:div {
      attribute class {"border-left border-left-style wavy"},
      try {
        transfo:magic($node/node())
      }
      catch * {
        error(QName("https://sade.textgrid.de/ns/error/fontane", "ZONE02"), "can not deal with this zone. error: " || string-join(($err:code, $err:description, $err:value), "; "))
      }
    }
  else
    try {
      transfo:magic($node/node())
    }
    catch * {
      error(QName("https://sade.textgrid.de/ns/error/fontane", "ZONE03"), "can not deal with this zone. error: " || string-join(($err:code, $err:description, $err:value), "; "))
    }
}
};


(:~
 : transforms tei:zone to xhtml:div
 :)
declare function transfo:zone($n) {
$n/@xml:id ! attribute id { string(.) },
attribute class {
  'zone',
  string($n/@type),
  string($n/@subtype),
  if (exists($n/preceding-sibling::tei:addSpan) and $n/preceding-sibling::tei:addSpan/substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id ) then 'addSpan' else (),
  (: mod sequence #1 :)
  let $mod := ($n/preceding::tei:mod[@seq][parent::tei:zone][substring-after(@spanTo, '#') = $n/following::tei:anchor/string(@xml:id)], $n/tei:mod[@seq])
  return
    if ($mod)
    then 'mod-seq-' || $mod/@seq
  else (),
  if ($n/@type="illustration") then 'figure' else(),
  transfo:segStyle($n),
  if( $n/@type="illustration" and $n/parent::tei:line) then "verticalMiddel" else (),
  if(not( $n//tei:line ) and $n//tei:figure[string(.//tei:ref) = "Diagonale Streichung"]) then "deletion-diagonal" else (),
  let $id := string($n/@xml:id)
  let $metamark := $n/ancestor::tei:surface[1]//tei:metamark[contains(string(@corresp), $id)]
  return
      if( ($id != "") and ( $metamark//tei:ref/ends-with(., 'gestrichen; erledigt') = true()) ) then 'hover' else ()
},
attribute style {
(: place it! :)
    if (($n/@ulx or $n/@uly or $n/@lrx or $n/@lry) and not($n/ancestor::tei:zone/@rotate))
        then
            if ($n/ancestor::tei:zone[last()]//tei:figure)
                then
                ('position:absolute;',
                'top:' || $n/@uly - sum($n/ancestor::tei:zone/@uly) || 'cm;',
                'left:' || $n/@ulx -sum($n/ancestor::tei:zone/@ulx) || 'cm;',
(:  'width:' || (if($n/@lry) then $n/@lrx - (if($n/@ulx) then $n/@ulx else 0) - sum($n/ancestor::tei:zone/@lrx) || 'cm;'  else '' ) || :)
                    'width:' || (if($n/@lry) then $n/@lrx - (if($n/@ulx) then $n/@ulx else 0) - (if ($n/parent::tei:line) then 0 else sum($n/ancestor::tei:zone/@lrx)) || 'cm;'  else '' ),
(:  'height:' || $n/@lry - (if($n/@uly) then $n/@uly else 0)  - sum($n/ancestor::tei:zone/@lry) || 'cm;':)
                   'height:' || $n/@lry - (if($n/@uly) then $n/@uly else 0) || 'cm;'
                ) else
            if (not($n/@ulx|$n/@uly|$n/@lrx) and $n//tei:fw)
                    then 'width:100%;'
(:                then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;height:0;':)
            else
                'position:absolute;' ||
                'top:' || $n/@uly || 'cm;' ||
                'left:' || $n/@ulx || 'cm;' ||
                'width:' || $n/@lrx - (if($n/@ulx) then $n/@ulx else 0) || 'cm;' ||
                'height:' || $n/@lry - (if($n/@uly) then $n/@uly else 0)  || 'cm;'
    else(),
    if( $n/not(@ulx|@lrx|@rotate) and $n//tei:fw)
(:        then 'width:100%;':)
        then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;'
(: edited: 2016-12-07
 : cause: email  "Probleme mit Seitenzahlen; dringend"
 : was: then if( $n//text()[matches(., '\w')][not(./ancestor::tei:fw)]) then 'width:100%;' else 'width:100%;height:0;'
 :)
    else (),
    if($n/ancestor::tei:zone/@rotate) then
        let $rotation := sum( $n/ancestor::tei:zone/@rotate )
        let $value:=
            if($rotation gt 89 and $rotation lt 180) then 'position:absolute;' || 'top:-' || $n/@ulx || 'cm;left:' || $n/@uly || 'cm;'
            else ()
        return if(contains($value, '-cm') or contains($value, ':cm')) then () else $value
    else(),
(:    if( preceding-sibling::tei:*[1]/local-name="line" ) then 'position:absolute;' else(),:)
(: rotate it! :)
(:    if ($n/@rotate and not( $n/parent::tei:surface/count(tei:zone) = 1)) :)
    if ($n/@rotate)
        then
            'transform:rotate(' || $n/@rotate || 'deg);' || '-webkit-transform:rotate(' || $n/@rotate || 'deg);'
    else (),
    transfo:lineHeight($n),
(: look 4 TILE objects :)
    if ($n/tei:seg/tei:rs/tei:figure[@xml:id] or $n/tei:figure[@xml:id] or $n/tei:del/tei:figure[@xml:id]) then
        let $id := (: get the TILE xml:id and try to resolve deleted sketches as well :)
          string($n/tei:figure/@xml:id) || string($n/tei:del/tei:figure/@xml:id)
        let $uri := string($n/root()//tei:idno[@type="TextGrid"])

        let $link := transfo:newestTBLELink($uri, $id)
        let $shape := $link/substring-before(substring-after(@targets, '#'), ' ')
(:        let $image := $tei//svg:g[@id = $link/parent::tei:linkGrp/substring-after(@facs, '#')]/svg:image/@xlink:href:)
(:        let $image := ($n/ancestor::tei:surface)[1]/substring-after(@facs, "textgridrep.org/") || ".1":)

        let $svgg := $link/ancestor::tei:TEI//svg:g[@id = $link/parent::tei:linkGrp/substring-after(@facs, '#')]
        let $image := substring-before($svgg//svg:image/@xlink:href, ".") || ".1"
        let $x := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@x, '%')) => format-number($transfo:numberPattern)
        let $y := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@y, '%')) => format-number($transfo:numberPattern)
        let $w := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@width, '%')) => format-number($transfo:numberPattern)
        let $h := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@height, '%')) => format-number($transfo:numberPattern)

        let $rotation := sum( ($n/ancestor-or-self::tei:*/@rotate) )
        let $rotation := if($rotation ne 0) then 360 - $rotation else 0
        let $furtherAttributes :=
                if($n/ancestor::tei:line)
                then "width:" || $n/@lrx - $n/@ulx || "cm; height:" || $n/@lry - $n/@uly || "cm;"
                else ""
        return ("background-image: url('https://textgridlab.org/1.0/digilib/rest/IIIF/" || $image || '/pct:' || $x || ',' || $y || ',' || $w || ',' || $h || "/,1000/" || $rotation || "/default.jpg'); background-repeat: no-repeat; background-size: 100% 100%;" || $furtherAttributes)
(:        digilib/" || $image || '?dh=500&amp;dw=500&amp;wx=' || $x || '&amp;wy=' || $y || '&amp;ww=' || $w || '&amp;wh=' || $h || "&amp;mo=png&amp;rot=" || $rotation || "'); background-repeat: no-repeat; background-size: 100% auto;"):)
    else (),

    (: like on C07 15r, a zone can contain a single (nonword) character that belongs to nearby lines. in this cases, we increase the font size by rule.
    this stays against our practise to encode a font-size if semantic matters. we should discuss this.
    :)
    if (sum($n/tei:line//text()/string-length(.)) = 1 and $n/tei:line//text()/matches(., '\W' ) ) then 'font-size:' || $n/@lry - $n/@uly || 'cm;' else (),
    if (sum($n/tei:line//text()/string-length(.)) = 1 and $n/tei:line//text()/matches(., '\W' ) and ($n/@lry - $n/@uly gt 1)) then 'transform:scaleX(0.5);' else (),

    (: override height statements before, if we want to prepare an inline zone :)
    if($n/parent::tei:line)
        then
            (
                'display:inline-block; position:relative; left:0; top:0;'
            )
        else ()
},
if (exists($n/preceding-sibling::tei:addSpan) and $n/preceding-sibling::tei:addSpan/substring-after(@spanTo, '#') = $n/following-sibling::tei:anchor/@xml:id )
  then
    element xhtml:div {attribute class {'addSpanHover italic'},
      if($n//tei:note/@subtype = "footnote")
      then 'Fußnote'
      else
        if(exists($n//tei:metamark[@function="caret"]))
        then 'Hinzufügung mit Einweisungszeichen'
        else 'Hinzufügung'
   }
  else(),
let $id := string($n/@xml:id)
let $metamark := $n/ancestor::tei:surface[1]//tei:metamark[contains(string(@corresp), $id)]
return
  if( ($id != "") and ( $metamark//tei:ref/ends-with(., 'gestrichen; erledigt') = true()) )
  then
      element xhtml:div {attribute class {'zoneHover'}, 'Erledigungsstreichung'}
  else (),

if($n/tei:metamark[@function="authorial_note"])
then element xhtml:div {attribute class {'zoneHover'}, 'Klammer zusammenfassende Funktion'}
else (),

if($n/tei:metamark/tei:figure/tei:figDesc/tei:ref/text())
then
  let $string := string($n/tei:metamark/tei:figure/tei:figDesc/tei:ref/text())
  return
      element xhtml:div {
        attribute class {'zoneHover'},
        switch($string)
          case "gestrichen; erledigt" return "Erledigungsstreichung"
          default return $string
      }
else ()
};

declare function transfo:line($node as element(tei:line)) {
  try {
  element xhtml:div  {
    $node/@xml:id ! attribute id { string(.) },
    attribute class {
      'line',
      transfo:segStyle($node),
      if($node/@type) then string($node/@type) else (),
      if($node/tei:seg[@type="col"][@n]) then "column" else (),
      if($node/@rendition) then replace($node/@rendition, "#", "") else ()
    },
    attribute style {
      string-join((
        ($node/@style),
        (if($node//text()[not(parent::tei:fw)]) then () else ('height:0.6cm;')),
        ( (: line-height given in div.zone[style] has no effect wheren large rendering is needed :)
        if($node//*[contains(string(@style), 'large')] or $node//ancestor::tei:zone[contains(string(@style), 'large')])
        then replace( transfo:lineHeight($node/ancestor::tei:zone[1]), 'line-height', 'max-height')
        else ())
    ), ';')
    },
    try {transfo:magic($node/node())} catch * { ($err:code , $err:description, $err:value) }
  }}
  catch * { ("line: ", $err:code, $err:description, $err:value, $err:module, $err:line-number)  }
};



(:~ transformation of any tei:seg
 :)
declare function transfo:seg($node as element(tei:seg)){
  transfo:seg($node, 0)
};
declare function transfo:seg($node as element(tei:seg), $i as xs:integer) {
  (: before main() :)
  if ($node/tei:g[@ref="#hb"]) then transfo:fraction($node)
  else if (($i = 0) and contains($node/tei:seg/@rend, "underline-style:zigzag_retrace"))
    then
      element xhtml:span {
        attribute class {'zigzag_retrace'},
        transfo:seg($node, 1)
      }
  else
  element xhtml:span {
    $node/@xml:id ! attribute id { string(.) },
    attribute class {
      'seg',
      if ($node/@style or $node/@rend or $node/@type) then transfo:segStyle($node) else ()
    },
    if(contains($node/@style, 'letter-spacing')) then
      attribute style { 'letter-spacing:' || substring-before(substring-after($node/@style, 'letter-spacing:'), 'cm') || 'cm;'}
      else (),

    (: recursion :)
    transfo:magic($node/node()),

    (: after main() :)
    if($node/@type = "abort")
    then
      element xhtml:div {
        attribute class {'segHover italic'},
        'Schreibabbruch'}
    else (),
    if($node/@type = "multiphrase" and not($node/parent::tei:add/substring-after(@corresp, '#') = $node/ancestor::tei:surface[1]//tei:anchor/@xml:id))
    then
      element xhtml:div {
        attribute class {'segHover italic'},
        'Mehrfachformulierung'}
    else (),
    if($node/@copyOf)
    then
      element xhtml:div {
        attribute class {'segHover italic'},
        'Unterführung'}
    else (),
    if(contains($node/@rend, "underline-cancel"))
    then
      element xhtml:div {
        attribute class {'segHover italic'},
        'Unterstreichung zurückgenommen'}
    else ()
  }
};

(:~ returns a sequence of class names according to the given styles
 : not only for tei:seg, but also for tei:zone
 : @param $node A single element with tei:style attribute
 : or @rend or to calculate line-height.
:)
declare function transfo:segStyle($node) {
let $style := $node/@style || (if(not($node/@style) and $node/@rend and $node/parent::*/@style) then (" " || $node/parent::*/@style) else ())
let $rend :=  $node/@rend
let $type:= if(($node/local-name() = ("seg", "zone")) and ($node/@type = "cancel"))
        then
            if(not($node/@prev) and not($node/@next))
              then "cancelLeft cancelRight"
            else if($node/@next and not($node/@prev))
              then "cancelLeft"
            else if($node/@prev and not($node/@next))
              then
                if($node/ancestor::tei:zone//tei:metamark[@function="deletion"][contains(@corresp, $node/@xml:id)]//tei:ref != "Diagonale Streichung, begrenzt durch eine senkrechte Abgrenzung am Anfang")
                then ()
                else "cancelRight"
            else () (: must be an existing @next and @prev :)
        else if($node/@type = "initials") then "initials" else ()

let $noLineThroughInCaseOfDeletionViaMetamark :=  if(
    contains($node/@style, 'text-decoration:line-through') and
    ($node/ancestor-or-self::tei:*/concat('#', @xml:id) = tokenize( $node/ancestor::tei:TEI//tei:metamark/@corresp, ' ')) )
    then " noLineThrough"
    else ()

let $rend := replace($rend, '\s', '')
let $seq :=
    for $s in tokenize(replace(replace($style, 'radius:', 'radius'), '\s|\.|%|-style', ''), ';')
    where not(matches($s, "^margin\-left:\d{1,2}\.{0,1}\d{0,1}cm$"))
    return
        tokenize($s, ':') !
            (
                (if (matches(., "^\d")) then ("d" || .) else .)
             ! (if(matches(., "^\-")) then ("minus" || .) else .)
            )
let $rendSeq :=
    for $r in tokenize(replace($rend, '\s', ''), ';')
    return
        replace(replace(replace($r, "medium:", "medium_"), ':', ' '), '[\(\)]', '')

let $preMedium := transfo:getMedium($node)
let $medium := if (tokenize($preMedium, " ") = ('blue_pencil', 'violet_pencil', 'black_ink', 'blue_ink', 'brown_ink'))
  then $preMedium
  else ()

let $test:= if($seq = ()) then ('TODO', $medium) else ($seq, $rendSeq, $medium, $type, $noLineThroughInCaseOfDeletionViaMetamark)

return
    $test ! replace(replace(replace(., '/', '_'), '3_4', 'd3_4'), '\s(\d|\-)', ' d$1')
};

declare function transfo:mod($node as element(tei:mod))
as element(xhtml:span) {
  element xhtml:span {
    $node/@xml:id ! attribute id { string(.) },
    $node/@seq ! attribute data-seq { string(.) },
    attribute class {
      'mod',
      $node/@type,
      if($node/@rend) then "mod" || replace($node/@rend, ";", "") => replace(":", "_") => replace(" ", " mod") else (),
      if(local:modTest($node)) then 'addSesquipedalian' else ()
    },
    if($node/@style and not($node/@seq)) then $node/@style else (),
    if($node//*/@corresp = "#C01_4r_c")
      then element xhtml:div { attribute class { 'modHover italic'},
          element xhtml:span {'d'},
          '<',
          element xhtml:span {'in'},
          ' überschrieben ',
          element xhtml:span {'urch'},
          '>'
        }
    else if($node/tei:del[@rend=('overwritten', 'erased; overwritten')]) then
      element xhtml:div {
        attribute class {
          'modHover italic',
          if($node/ancestor::tei:*/@rotate or $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
        if(ends-with($node/preceding::text()[1][parent::tei:line = $node/parent::tei:line], ' ') or ends-with($node/preceding::text()[1][parent::tei:line = $node/parent::tei:line], ' '))
          then ()
          else
            (: do not add preText when mod starts with punctuation :)
            if(matches(($node//text())[1], "^\.|^\?|^,|^!")) then element xhtml:span {}
          else
            element xhtml:span {local:preText($node)},
        '<',
        element xhtml:span {
          if( $node//tei:g[@ref="#hb"] )
            then transfo:magic($node/tei:del/node())
            else transfo:magic($node/tei:del/node())//text()[. != "unsichere Lesart"][not(contains(string-join(ancestor::*/@class), "expan"))],
          (if ($node/tei:del/tei:gap) then transfo:magic($node/tei:del/tei:gap) else ())
        },
        if($node/tei:del/tei:gap[@unit='mm'][@reason='illegible']) then 'unentziffert' else (),
        if($node//del[@rend="erased; overwritten"]) then ' radiert ' else ' überschrieben ',
        element xhtml:span {
          (: todo: remove decendant::tei:add since it results from a coding error (mod without direkt add) :)
          string-join(($node//tei:add//text()[not(parent::tei:expan)][not(parent::tei:del)]))
        },
        '>', if(matches(string-join($node//tei:add/text()), '^\.$|^,$|^:$|^;$')) then () else element xhtml:span { transfo:postText($node) }
      }
    else if(
      $node/tei:del[not(@rend=('overwritten', 'erased; overwritten'))]
      or ($node/@prev and $node/root()//tei:mod[@xml:id = substring-after($node/@prev, "#")]//tei:del[not(@rend=('overwritten', 'erased; overwritten'))])
  ) then
      element xhtml:div {
        attribute class {
          'modHover italic',
          if($node/ancestor::tei:*/@rotate or $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
        ('Ersetzung',
            if( contains(string-join($node//@rend), 'caret') )
            then 'mit Einweisungszeichen'
            else
            if($node/@next and contains(string-join($node/root()//tei:mod[@xml:id = substring-after($node/@next, "#")]//@rend), 'caret') )
            then 'mit Einweisungszeichen'
            else
            if( not(exists($node//tei:add[not(tei:del)])) )
            then 'durchgestrichen'
            else ()
        )
      }
    else if($node//tei:add and not($node//tei:del))
    then
      element xhtml:div {
        attribute class {
          'modHover italic',
          if($node/ancestor::tei:*/@rotate or $node/ancestor::tei:zone/preceding-sibling::tei:addSpan) then () else 'hoverTop'},
        ('Hinzufügung',
            if( contains(string-join($node//@rend), 'caret') )
            then 'mit Einweisungszeichen'
            else
            if($node/@next and $node/@next and contains(string-join($node/root()//tei:mod[@xml:id = substring-after($node/@next, "#")]//@rend), 'caret') )
            then 'mit Einweisungszeichen'
            else ()
        )
      }
    else (),
    if($node/tei:del[@rend = ('overwritten', 'erased; overwritten')])
    then
          (
            transfo:magic($node/node()[. << $node/tei:add[1]][. << $node/tei:del[1]]),
            transfo:magic($node/tei:add),
            transfo:magic($node/tei:del),
            transfo:magic($node/node()[. >> $node/tei:add[last()]][. >> $node/tei:del[last()]])
          )
    else transfo:magic($node/node())
  }
};

(:~
 : transforms a tei:del to xhtml:span
 :)
declare function transfo:del($del as element(tei:del))
as element(xhtml:span) {
(: show line through on figures… :)
if ($del/parent::tei:zone and $del/tei:figure)
then
    element xhtml:div {
        transfo:zone($del/parent::tei:zone)[1],
        attribute style { "width:100%; height:100%" },
        element xhtml:span {
            attribute class {'del'}
        }
    }
else
  element xhtml:span {
    attribute class {
      'del',
      replace($del/@rend, '#', 'hash'),
      if ($del/@instant) then 'instant' else()
    (:  if($del/parent::tei:mod ! local:modTest(.)) then 'addSesquipedalian' else () :)
    },
    $del/@xml:id ! attribute id { string(.) },
    transfo:magic($del/node()),
    if($del/@instant) then
      element xhtml:div {
        attribute class {'instantHover italic'},
        'Sofortkorrektur'}
    else ()
  }
};

(:~
 : transforms a tei:hi to xhtml:span
 :)
declare function transfo:hi($hi as element(tei:hi))
as element(xhtml:span) {
  element xhtml:span {
    $hi/@xml:id ! attribute id { string(.) },
    attribute class { 'hi' },
    transfo:magic($hi/node())
  }
};

(:~
 : transforms a tei:add
 :)
declare function transfo:add($add as element(tei:add)) {
    transfo:add($add, false())
};

(:~
 : transforms a tei:add to XHTML with the option to force the rendering. This is
 : useful when a previously rendered element (with @corresp and an already
 : processed tei:anchor) is added by the typeswitch loop, we will omit it, as it
 : comes with the flag from the wrapper function local:add#1. The function on
 : tei:anchor will always trigger the rendering with a true().
 :)
declare function transfo:add($add as element(tei:add), $force as xs:boolean) {
  if(
      $add/@corresp and not($force)
      (: when a node is placed with the help of a tei:anchor, we force the
        rendering by a superior call to this function. :)
      and not($add/@type eq "multiphrase")
    )
  then ()
  else
(: preapre the tei:add :)
  element xhtml:span {
    $add/@xml:id ! attribute id { string(.) },
    attribute class {'add'},
    transfo:add-worker($add)
  }
};

declare %private function transfo:add-worker($node) {
let $style := (
          (:  if(contains($node/@rend, 'caret:V') and contains($node/@style, 'margin-left'))
            then 'left:' || number(analyze-string($node/@style, '-\d+\.\d')/fn:match/text()) + 0.2 || 'cm;'
            else :)
                if(contains($node/@style, 'margin-left'))
                then replace($node/@style, 'margin-left', 'left')
            else () (: ,
            if(number(substring-before(tokenize(transfo:lineHeight($node/ancestor::tei:zone[1]), ':')[2], 'cm')) lt 0.7) then ';top:-0.3cm;' else ()
  :)          )
return
if ($node/@place = ('above', 'below')) then (
        element xhtml:span {
            attribute class {
                $node/@place,
                if(contains($node/@rend, '3/4-circle')) then 'threequatercircle' else (),
                if(contains($node/@rend, 'semicircle(bottom)')) then 'semicircle bottom' else (),
                if(contains($node/@rend, 'semicircle(left)')) then 'semicircle left' else (),
                if(contains($node/@rend, 'semicircle(right)')) then 'semicircle right' else (),
                if(contains($node/@rend, 'caret:looped_arc')) then 'looped_arc' else ()
            },
            if($style = () ) then () else attribute style {$style},
        if($node/@rend="caret:looped_arc(pos-left)")
        then <xhtml:img src="/public/img/caret:looped-arc-pos-left.svg" width="140px" height="auto" style="left: -0.7cm; position: absolute;"/>
        else (),
        transfo:magic($node/node()),
        if(
                not($node/ancestor::tei:mod)
            and not( $node/@copyOf )
            and not( $node/tei:seg[@type="multiphrase"] or $node/ancestor::tei:seg[@type="multiphrase"] ) )
        then
            element xhtml:div {
                attribute class {'addHover italic'},
                'Hinzufügung',
                if($node/tei:del) then "durchgestrichen" else (),
                if(contains($node/@rend, 'caret')) then 'mit Einweisungszeichen' else ()
            }
        else (),
        if($node/@copyOf)
        then
            element xhtml:div {
                attribute class {'addHover italic'},
                'Wiederholung zur Verdeutlichung'
            }
        else ()
        },
        (if(starts-with(string($node/@rend), 'caret:'))then(transfo:caret($node))else())
)
else if($node/@rend = "|")
then
  element xhtml:span {
    $node/@xml:id ! attribute id { string(.) },
    attribute class {'rend-pipe'},
    text {"|"},
    element xhtml:div {
      attribute class {"addHover italic"},
      "Fontanes Korrekturzeichen: Trennstrich"
    }
  }
else
  element xhtml:span {
    attribute class {
      $node/@place,
      if($node/@place = "superimposed" and ($node/@next or $node/@prev))
        then 'linked'
        else()
      (:,if($node/parent::tei:mod ! local:modTest(.))
        then "addSesquipedalian"
        else ():)

    },
    if($node/@place = "left")
    then attribute style { "position:absolute;",$style }
    else (),
    transfo:magic($node/node()),
    if($node/@place != ('above', 'below') and contains($node/@rend, 'caret')) then transfo:caret($node) else (),
    if(not($node/ancestor::tei:mod or $node/tei:seg[@type="multiphrase"])) then
      element xhtml:div {
        attribute class {'addHover italic'},
        'Hinzufügung', if(contains($node/@rend, 'caret')) then 'mit Einweisungszeichen' else ()
      }
    else ()
  }
};

(:~
 : transforms a tei:restore to xhtml:span
 :)
declare function transfo:restore($restore as element(tei:restore))
as element(xhtml:span) {
  let $what := if($restore/tei:mod[@type="subst"]/tei:del[@rend="overwritten"]) then "Überschreibung" else "Durchstreichung"
  return
    element xhtml:span {
      attribute class {'restore'},
      element xhtml:div {
        attribute class {'restoreHover italic'},
        if($restore/parent::tei:seg/parent::tei:del)
        then $what || " gilt"
        else $what || " zurückgenommen"
      },
      transfo:magic($restore/node())
    }
};

(:~
 : transforms a tei:unclear to xhtml:span
 :)
declare function transfo:unclear($unclear as element(tei:unclear))
as element(xhtml:span) {
  element xhtml:span {
    attribute class { 'unclear' },
    $unclear/@xml:id ! attribute id { string(.) },
    transfo:magic($unclear/node()),
    transfo:tooltip($unclear)
  }
};

(:~
 : transforms a tei:rs to xhtml:span
 :)
declare function transfo:rs($rs as element(tei:rs))
as element(xhtml:span) {
  element xhtml:span {
    attribute class {
      'rs',
      if($rs/@type) then $rs/@type else (),
      if($rs/@ref) then substring-before($rs/@ref, ':') else ()
    },
    attribute data-ref { replace($rs/@ref, ":", "") },
    $rs/@xml:id ! attribute id { string(.) },
    transfo:magic($rs/node())
  }
};

(:~
 : transforms a tei:date to xhtml:span
 :)
declare function transfo:date($date as element(tei:date))
as element(xhtml:span) {
  element xhtml:span {
    $date/@xml:id ! attribute id { string(.) },
    attribute class {'date'},
    transfo:magic($date/node())
  }
};

(:~
 : transforms a tei:fw to xhtml:div or - in case of Fontanes hand - to
 : xhtml:span.
 :)
declare function transfo:fw($fw as element(tei:fw))
as item()* {
  if($fw/tei:figure//tei:ref/contains(., 'Seitenzahlabgrenzungslinie'))
    then <xhtml:hr class="Seitenzahlabgrenzungslinie"/>
  else if (($fw/preceding::tei:handShift[@new][1]/@new != '#Fontane')
      or not( exists( $fw/preceding::tei:handShift[@new] ) ))
  then
    element xhtml:div {
      attribute class {'fwWrapper'},
      element xhtml:span {
        $fw/@xml:id ! attribute id { string(.) },
        attribute class { 'fw' },
        transfo:magic($fw/node())}
    }
  else
    element xhtml:span {
      $fw/@xml:id ! attribute id { string(.) },
      attribute class {'fw-fontane'},
      transfo:magic($fw/node())
    }
};

(:~ transformation of tei:g to xhtml:span
 :)
declare function transfo:glyph($node as element(tei:g))
as element(xhtml:span) {
  if ($node/@ref='#mgem') then
    element xhtml:span {
      attribute class {'g mgem'},
      transfo:magic($node/node())
    }
  else if ($node/@ref='#ngem') then
    element xhtml:span {
      attribute class {'g ngem'},
      transfo:magic($node/node())
    }
  else if ($node/@ref="#rth") then
    element xhtml:span {
      attribute class {'g rth'},
      <xhtml:div class="hover">Reichstaler</xhtml:div>,
      <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 82 107">
        {element path {
          attribute fill {"none"},
          attribute stroke {"darkslategrey"},
          attribute stroke-width {"5"},
          attribute d {
            "M 24.73,35.82" ||
            " C 24.73,35.82 30.36,30.73 32.00,26.55" ||
            " 46.73,45.27 80.55,37.45 70.00,38.18" ||
            " 16.73,50.73 -2.55,99.09 19.09,101.82" ||
            " 52.00,103.45 57.09,80.00 42.91,80.91" ||
            " 11.82,83.64 35.45,105.45 52.00,99.64" }}}
      </svg>
    }
  else if ($node/@ref='#vds') then
    element xhtml:span {
      attribute class {'g vds'},
      text {"&#840;"}
    }
  else ()
};

(:~
 : transforms a tei:space to xhtml:span
 :)
declare function transfo:space($space as element(tei:space))
as element(xhtml:space) {
  element xhtml:span {
    attribute class {
      "space",
      string($space/@type),
      string($space/@unit) || string($space/@quantity)
    },
    if($space/@type = "pause")
    then
      element xhtml:div {
        attribute class {'spaceHover'},
        'Gedankenpause'
      }
    else (),
    if($space/@type = "placeholder")
    then
      element xhtml:div {
        attribute class {'spaceHover'},
        'Platzhalter'
      }
    else ()
  }
};

(:~
 : transforms a tei:choice to xhtml:span
 :)
declare function transfo:choice($choice as element(tei:choice))
as element(xhtml:span) {
  element xhtml:span {
    attribute class {'choice'},
    if ($choice/tei:expan) then
      element xhtml:div {
        attribute class {'expan italic'},
        let $count := count($choice//tei:expan)
        return
          if($count = 1) then $choice//tei:expan/text()
          else
            for $expan at $pos in $choice//tei:expan
              return
                if($pos = ($count - 1)) then $expan || '&#x2003;oder&#x2003;'
                else if($pos = $count) then $expan/text()
                else $expan/text() || ',&#x2003;'
      }
    else (),
    transfo:magic($choice/node())
  }
};

(:~
 : transforms a tei:abbr to xhtml:span
 :)
declare function transfo:abbr($abbr as element(tei:abbr))
as element(xhtml:span) {
  element xhtml:span {
    $abbr/@xml:id ! attribute id { string(.) },
    attribute class {'abbr'},
    transfo:magic($abbr/node())
  }
};

(:~
 : transforms a tei:said to xhtml:span
 :)
declare function transfo:said($said as element(tei:said))
as element(xhtml:span) {
  element xhtml:span {
    attribute class {"said"},
    $said/@xml:id ! attribute id { string(.) },
    transfo:magic($said/node())
  }
};

(:~
 : transforms a tei:note (authorial) to xhtml:span. For editorial notes see
 : local:noteParser()
 :)
declare function transfo:note($note as element(tei:note))
as element(xhtml:span)? {
  if(not($note/@type="editorial"))
  then
    element xhtml:span {
      $note/@xml:id ! attribute id { string(.) },
      attribute class {
        'teinote',
        string($note/@type), string($note/@subtype)
      },
      if($note/@type = "authorial" and $note/@subtype = "text")
      then
        element xhtml:div {
          attribute class { 'noteHover' },
          'Autoranmerkung: Text'
        }
      else if($note/@type = "authorial" and $note/@subtype = "revision")
      then
        element xhtml:div {
          attribute class {'noteHover'},
          'Autoranmerkung: Revision'
        }
      else if($note/@type = "authorial" and $note/@subtype = "notebook")
      then
        element xhtml:div {
          attribute class {'noteHover'},
          'Autoranmerkung: Notizbuch'
        }
      else (),
      transfo:magic($note/node())
    }
  else transfo:magic($note/node())
};

(:~
 : transforms a tei:retrace. doubles the retraced content.
 :)
declare function transfo:retrace($retrace as element(tei:retrace))
as element(xhtml:span){
  element xhtml:span {
    attribute class {'retrace'},
    $retrace/@xml:id ! attribute id { string(.) },
    element xhtml:span {
      attribute class {'retraced'},
      transfo:magic($retrace/text()),
      element xhtml:div {
        attribute class {
          'retraceHover',
          if( $retrace/ancestor::tei:*/@rotate
              and $retrace/ancestor::tei:zone/preceding-sibling::tei:addSpan)
          then ()
          else 'hoverTop'
        },
        let $pre := local:preText($retrace)
        return if($pre = $retrace//text()) then () else $pre,

        element xhtml:span {
            attribute class {'italic'},
            '<'},
        $retrace//text(),
        element xhtml:span {
            attribute class {'italic'},
            ' nachgezogen '},
            $retrace//text(),
            element xhtml:span {
            attribute class {'italic'},
            '>'},
        transfo:postText($retrace)
      }
    }
  }
};

(:~
 : creates an xhtml:a from a tei:ref
 :)
declare function transfo:ref($ref as element(tei:ref))
as element(xhtml:span)? {
  if($ref/parent::tei:figDesc) then () else
  element xhtml:span {
    $ref/@xml:id ! attribute id { string(.) },
    attribute class { 'ref' },
    attribute data-ref { replace($ref/@target, '#', '') },
    transfo:magic($ref/node()),
    (: link :)
    if( starts-with($ref/@target, '#xpath(//surface')
        or not(contains($ref/@target, $ref/ancestor::tei:surface[last()]/@n)))
    then
      element xhtml:span {
        attribute class { 'href' },
        element xhtml:a {
          if( not( contains($ref/@target, 'xpath') ))
          then (
            attribute href {
              'edition.html?id=/xml/data/'
              || transfo:get-base($ref)
              || '.xml&amp;page='
              || substring-before(substring-after($ref/@target, "_"), "_")
              || '&amp;target='
              || substring-after($ref/@target, "#")
            },
            <i class="fa fa-link"/>)
          else (
            attribute href {
              'edition.html?id=/xml/data/'
              || transfo:get-base($ref)
              || '.xml&amp;page='
              || substring-before(substring-after($ref/@target, "[@n='"), "']")
            },
            <i class="fa fa-link"/>
              )
        }
      }
    else ()
  }
};

(:~
 : transforms a tei:milestone to an empty xhtml:span, initially invisible
 :)
declare function transfo:milestone($milestone as element(tei:milestone))
as element(xhtml:span) {
  element xhtml:span {
    attribute class {
      'milestone',
      string($milestone/@type),
      string($milestone/@unit)
    },
    $milestone/@xml:id ! attribute id { string(.) },
    attribute style {'display:none;'}
  }
};

(:~
 : sets an anchored element to the current place
 :)
declare function transfo:anchor($anchor as element(tei:anchor))
as element(xhtml:span)? {
  let $id := string($anchor/@xml:id)
  let $correspElement := $anchor/root()//tei:add[@corresp = '#' || $id]
  return
    if( $correspElement )
    then
      element xhtml:span {
        attribute class { 'anchored' },
        transfo:add($correspElement, true())
      }
      else ()
};




(:~
 : Prepares a string containing the last word in the text
 :)
declare function local:preText($n) {
let $directText :=
  $n/preceding::text()
    [ancestor::tei:line = $n/ancestor::tei:line]
    [not(parent::tei:add[@place = ('below', 'above')])]
    [not(parent::tei:expan)]
    [not(ancestor::tei:fw)]
    [not(parent::tei:del)]

let $prvLineText :=
  if(($n/preceding::text()
    [not(./parent::tei:expan)]
    [not(ancestor::tei:fw)]
    [not(parent::tei:del)]
    [./ancestor::tei:line = $n/ancestor::tei:line]
    )[1]/parent::*/@prev)
(: TODO: mechanism for pre text from previous line is disabled. :)
      then ($n/preceding::text()[not(parent::tei:expan)][not(ancestor::tei:fw)][1][ancestor::tei:*/substring-after(@next, "#") = $n/ancestor::*[@xml:id]/@xml:id])
      else ()

let $prvLineText :=
  if(
      (string-join($prvLineText)="")
      and ( not(matches(string-join($n/preceding::text()[not(parent::tei:expan)][not(ancestor::tei:fw)][ancestor::tei:line = $n/ancestor::tei:line]), '\s$')))
      and (
             ends-with($n/ancestor::tei:line/preceding::tei:line[1], '-')
          or ends-with($n/ancestor::tei:line/preceding::tei:line[1], '⸗')
          )
      )
  then tokenize( string(string-join(($n/ancestor::tei:line/preceding::tei:line[1])//text()[not(parent::tei:expan)], "")), ' ' )[last()]
  else $prvLineText
return
    replace(tokenize(string-join( ($prvLineText, $directText) ), '\s|\.|,|;|&#x2003;' )[last()], $transfo:tooltipReplacementPattern, '' )
    => replace("lang fahren <ſ überschrieben S>weitSſieda?", "lang fahren Sie da?") (: https://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/2128s.xml&page=53r :)
};

declare function transfo:postText($n) {
(:  former mod hover::)
let $text :=
  if( $n/@next )
  (: linking mechanism to find following text :)
  then
    string($n/root()//*[@xml:id= substring-after($n/@next, '#')])
  else
    if(matches($n/following::text()[1][ancestor::tei:line = $n/ancestor::tei:line], '^\s|^ '))
    then ()
    else
        if( matches(string-join($n/following::text()[not(parent::tei:expan)][not(ancestor::tei:del)][not(ancestor::tei:fw)][ancestor::tei:line = $n/ancestor::tei:line]), '\s' ))
        then tokenize(string-join($n/following::text()[not(parent::tei:expan)][not(ancestor::tei:del)][not(ancestor::tei:fw)][ancestor::tei:line = $n/ancestor::tei:line]), '\s' )[1]
        else if(matches($n/ancestor::tei:line, '-$|⸗$' ))
        then
            tokenize(
              string-join(
                ($n/following::text()[not(parent::tei:expan)][not(ancestor::tei:del)][not(ancestor::tei:fw)][ancestor::tei:line][not(matches(., "^\s+$"))])
                )
              , ' ')[1]
        else tokenize(string-join($n/following::text()[not(parent::tei:expan)][not(ancestor::tei:del)][not(ancestor::tei:fw)][ancestor::tei:line = $n/ancestor::tei:line]), ' |\.|,')[1]

return
  (if($text = "(") then $text else replace( $text , $transfo:tooltipReplacementPattern, '' ))
  => replace("Sweit", "weit")
  => replace("crodiDanteKleiner", "crodi")
};

declare function transfo:stamp($node as element(tei:stamp))
as element(xhtml:div) {
  element xhtml:div {
    attribute class {'stamp'},
    switch (string($node))
      case 'FONTANE.'
        return
          <svg xmlns="http://www.w3.org/2000/svg" width="23mm" height="4mm" data-info="Stempel: FONTANE.">
              <g alignment-baseline="baseline">
                  <text x="0mm" y="3.5mm" style="fill: purple;stroke: none;font-size:4mm; font-family:Ubuntu-Light, Verdana, sans-serif;">FONTANE.</text>
              </g>
          </svg>

    case 'STAATSBIBLIOTHEK •BERLIN•'
      return
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="24.2mm" height="15.2mm" data-info="Stempel: STAATS- BIBLIOTHEK BERLIN">
            <defs>
                <filter id="unschaerfe" color-interpolation-filters="sRGB">
                    <feGaussianBlur stdDeviation="0.0"/>
                </filter>
            </defs>
            <g alignment-baseline="baseline">
                <g filter="url(#unschaerfe)">
                    <rect x="0.5mm" y="0.5mm" width="23mm" height="14mm" rx="1mm" fill="none" stroke="black" stroke-width="1mm"/>
                    <text
                        style="stroke:none; font-family:FreeSerif, serif; font-size: 3.4mm; font-weight: bold;">
                        <tspan id="zeile1" x="5mm" y="4mm">STAATS-</tspan>
                        <tspan id="zeile2" x="1mm" y="8.5mm">BIBLIOTHEK</tspan>
                        <tspan id="zeile3" x="3mm" y="13mm">∙ BERLIN ∙</tspan>
                    </text>
                </g>
            </g>
        </svg>

    case 'DSB Font.-Arch. Potsdam'
      return
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="15.2mm" height="15.2mm" data-info="Stempel: DSB Font.-Arch. Potsdam">
            <defs>
                <filter id="unschaerfe" color-interpolation-filters="sRGB">
                    <feGaussianBlur stdDeviation="0.3"/>
                </filter>
            </defs>
            <g alignment-baseline="baseline">
                <circle cx="7.5mm" cy="7.5mm" r="7.25mm" fill="none" stroke="darkblue" stroke-width="0.5mm"/>
                <text style="font-size:65%; font-family:FreeSans;" fill="darkblue">
                    <tspan id="zeile1" x="4.5mm" y="4mm">DSB</tspan>
                    <tspan id="zeile2" x="0.9mm" y="7.5mm">Font.-Arch.</tspan>
                    <tspan id="zeile3" x="2mm" y="11mm">Potsdam</tspan>
                </text>
            </g>
        </svg>

    case "BERLIN W.West"
      return
        <img src="/public/img/stempel-berlin-w.png" width="100%" height="100%"/>

    case "Königl.Königliches Polizei⸗Praesidium zu Berlin"
      return
        <img class="polizei-berlin" src="public/img/stamp-polizeipraesidium-berlin.svg" width="100%" height="100%"/>

    case "DEUTSCHE STAATSBIBLIOTHEK Theodor-Fontane-Archiv"
      return
        <img class="dtstaatsbiliothek-tfa" src="/public/img/stamp-DtStaatsbibliothekTFA.svg" width="100%" height="100%"/>

    default return
      <svg xmlns="http://www.w3.org/2000/svg" width="23mm" height="3.5mm">
          <g alignment-baseline="baseline">
              <text x="0mm" y="3.5mm" style="font-size:4mm; font-family:Ubuntu-Light, Verdana, sans-serif;" fill="purple">Stempel nicht unterstützt oder Fehler im Code.</text>
          </g>
      </svg>
  }
};

declare function transfo:text($n as text())
as item()* {
  let $followingAnchorWithModBeforeThisNode :=  $n/following::tei:anchor[@xml:id = $n/preceding::tei:mod[@spanTo]/substring-after(@spanTo, '#')]/('#' || @xml:id)
  let $allModBefore := $n/preceding::tei:mod[@spanTo = $followingAnchorWithModBeforeThisNode]
  let $modClasses :=
      let $styleSource :=
        for $mod in $allModBefore
        return
          ("modSeq" || string($mod/@seq), transfo:segStyle($mod))
      let $styleToken := string-join($styleSource, " ") => tokenize()
      let $style := if(count($styleToken[. eq "solid"]) gt 1) then (distinct-values($styleToken)[. ne "solid"], "double") else $styleToken
      return
        $style
  let $handShiftNewRef := $n/preceding::tei:handShift[@new][1]/substring-after(@new, '#')

  return
  if ($n/ancestor::tei:line) then
  element xhtml:span {
    attribute class {
        $modClasses,
        if(not(
            matches($handShiftNewRef, 'Archivar|Druck')
            ))
        then (
          (: medium :)
            transfo:getMedium($n),
          (: hand :)
            $handShiftNewRef,
            if ($handShiftNewRef = '') then 'Fontane' else
          (: rendition :)
            $n/preceding::tei:handShift[@rendition][1]/@rendition,
          (: script :)
            transfo:script($n)
          )
        else (
            $handShiftNewRef
            )
    },
    attribute data-info {
        let $hand := $handShiftNewRef
        let $hand := switch($hand)
                        case '' return 'Theodor Fontane'
                        case 'Fontane' return 'Theodor Fontane'
                        case 'Friedrich_Fontane' return 'Friedrich Fontane'
                        default return $hand
        let $medium := tokenize(transfo:getMedium($n), ' ')
        let $medium := for $m in $medium
                        return
                            switch($m)
                                case 'pencil' return 'Bleistift'
                                case 'thick_pencil' return 'dicker Bleistift'
                                case 'blue_pencil' return 'Blaustift'
                                case 'violet_pencil' return 'Violettstift'
                                case 'black_ink' return 'schwarze Tinte'
                                case 'blue_ink' return 'blaue Tinte'
                                case 'light_blue_ink' return 'hellblaue Tinte'
                                case 'brown_ink' return 'braune Tinte'
                                case 'thin_pen' return 'feine Feder'
                                case 'thick_pen' return 'breite Feder'
                                case 'medium_pen' return 'mittlere Feder (Standard)'
                                case 'blue_ballpoint' return 'blauer Kugelschreiber'
                                default return $m
        let $medium := string-join($medium, ', ')
        let $style := $n/preceding::tei:handShift[@script][tokenize(@script, ' ') = ('standard', 'clean', 'hasty', 'angular')][1]/tokenize(@script, ' ')[. =('standard', 'clean', 'hasty', 'angular')]
        let $style := for $s in $style
                        return
                            switch($s)
                            case 'standard' return 'Standard'
                            case '' return  'Standard'
                            case 'clean' return 'Reinschriftlich'
                            case 'hasty' return 'Unruhig'
                            case 'angular' return 'Gezackt'
                            default return $s
        let $scripthS := $n/preceding::tei:handShift[@script][tokenize(@script, ' ') = ('Latn', 'Latf')][1]
        let $segLang := string($n/ancestor::tei:seg[@xml:lang][1]/@xml:lang)
        let $segLang := if(contains($segLang, "-")) then $segLang else $segLang || "-"
        let $script := if( $segLang != "" )
                    then tokenize($segLang, "-")[last()]
                    else tokenize($scripthS/@script, ' ')[. = ('Latn', 'Latf')]

        let $script :=
          if(starts-with($hand, "Archivar"))
          then "Latein"
          else
            for $s in $script
            return
                switch($s)
                case 'Latn' return 'Latein'
                case 'Latf' return 'Kurrent'
                default return 'Kurrent'
        let $lang := switch (substring-before($segLang, "-"))
                        case "en" return "Englisch"
                        case "la" return "Latein"
                        case "fr" return "Französisch"
                        case "it" return "Italienisch"
                        case "da" return "Dänisch"
                        case "sv" return "Schwedisch"
                        case "grc" return "(Alt)-Griechisch"
                        case "nds" return "Berlinerisch/Märkisch-Plattdeutsch/Plattdeutsch"
                        case "ru" return "Russisch "
                        case "szl" return "Schlesisch"
                        case "sxu" return "Sächsisch"
                        case "pfl" return "Pfälzisch"
                        default return "Deutsch"
        return
        if(contains($hand, "Druck"))
            then
                ('Schreiberhand:', replace($hand, '_', ' '),
                '|',
                'Sprache: ', $lang)
            else
                ('Schreiberhand:', replace($hand, '_', ' '),
                '|',
                'Medium:', $medium,
                '|',
                'Schrift:', $script,
                '|',
                'Duktus:', $style,
                '|',
                'Sprache: ', $lang)
    },
    if ($n/matches(., '.[\w\[\d]') and
        string($n/preceding::tei:handShift[@new][1]/@new) != string($n/preceding::text()[1]/preceding::tei:handShift[@new][1]/@new) )
    then
        (attribute id { 'new' || $n/preceding::tei:handShift[@new][1]/substring-after(@new, '#') },
        attribute data-handtoggle { $n/preceding::tei:handShift[@new][1]/substring-after(@new, '#') })
    else (),

    (: manipulate the string in case of Gemination :)
    if($n/parent::tei:g) then (
        switch ($n/parent::tei:g/@ref)
            case '#mgem' return '&#xe095;'
            case '#ngem' return '&#xe096;'
            default return string($n/parent::tei:g/@ref)
        )
    else
        let $stringSeq := tokenize( $n/string() , '&#x20b0;|&#x2114;|&#x2032;|&#x271d;|&#x3003;|&#x204a;c\.')
        return if(count($stringSeq) gt 1) then
            if(matches( $n,'&#x20b0;'))
            then (substring-before($n, '&#x20b0;'), <xhtml:span class="sonderzeichen">&#x20b0;<xhtml:div class="hover">Pfennig</xhtml:div></xhtml:span> ,substring-after($n, '&#x20b0;'))
            else if(matches( $n,'&#x2114;'))
            then (
                let $seq := tokenize($n, '&#x2114;')
                return
                    switch( count($seq) )
                    case 2 return
                        ( substring-before($n, '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,substring-after($n, '&#x2114;'))
                    case 3 return
                        ( substring-before($n, '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,
                            substring-before(substring-after($n, '&#x2114;'), '&#x2114;'), <xhtml:span class="sonderzeichen">&#x2114;<xhtml:div class="hover">Pfund</xhtml:div></xhtml:span> ,
                            substring-after(substring-after($n, '&#x2114;'), '&#x2114;') )
                    default return 'TODO: add item to sequence'
                )
            else if(matches( $n,'&#x2032;'))
            then (substring-before($n, '&#x2032;'), <xhtml:span class="sonderzeichen">&#x2032;<xhtml:div class="hover">Fuß</xhtml:div></xhtml:span> ,substring-after($n, '&#x2032;'))
            else if(matches( $n,'&#x271d;'))
            then (substring-before($n, '&#x271d;'), <xhtml:span class="sonderzeichen">&#x271d;<xhtml:div class="hover">Kirche</xhtml:div></xhtml:span> ,substring-after($n, '&#x271d;'))
            else if(matches( $n,'&#x3003;'))
            then (substring-before($n, '&#x3003;'), <xhtml:span class="sonderzeichen">&#x3003;<xhtml:div class="hover">Unterführung</xhtml:div></xhtml:span> ,substring-after($n, '&#x3003;'))
            else if(matches( $n,'&#x204a;c\.'))
            then (substring-before($n, '&#x204a;c.'), <xhtml:span class="sonderzeichen">&#x204a;c.<xhtml:div class="hover">etc.</xhtml:div></xhtml:span> ,substring-after($n, '&#x204a;c.'))
            else ()
        else $stringSeq
    }
else ()
};

declare function transfo:facs($node) {
    element xhtml:div {
        attribute class {'facs'},
        if($node/parent::tei:sourceDoc and not($node/tei:graphic[1]/@xml:id)) then () else
            attribute style {
                "width:"  || xs:decimal($node/@lrx) - xs:decimal($node/@ulx) || "cm;",
                "height:" || xs:decimal($node/@lry) - xs:decimal($node/@uly) || "cm;"
            },
        element xhtml:a {
            attribute href { 'digilib/' || ( string($node/tei:graphic[1]/@n) || '.jpg') || '?m2' },
            attribute target {'_blank'},
            if($node/@subtype="Kalenderblatt" and ends-with($node/@n, "v"))
            then
                element xhtml:div {
                    attribute class {
                        "Kalenderblatt",
                        "verso"
(:                        if(not( $node//tei:line[ancestor::tei:zone[@rotate = 180]] )) then "rotate" else ():)
                    },
                    element xhtml:img {
                        attribute class {('facs', string( ($node/@subtype)[1] ))},
                        attribute src {transfo:digilib($node)},
                        attribute data-tile {
                            let $id := ($node/tei:graphic/string(@xml:id))[1]
                            return
                              if($id = "")
                              then error(QName("https://sade.textgrid.de/ns/error/fontane", "TILE00"), "@subtype='Kalenderblatt' requires @xml:id (Text-Image-Link) in ./tei:graphic!")
                              else
                            let $uri := $node/ancestor::tei:TEI//tei:publicationStmt//tei:idno[@type="TextGrid"]/string(.)
                            return transfo:newestTBLELink($uri, $id)/base-uri()
                        }
                    }
                }
            else
            (element xhtml:img {
                attribute class {('facs', string( ($node/@subtype)[1] ))},
                attribute src {transfo:digilib($node)}
            },
              for $graphic in $node/tei:graphic[position() gt 1]
              let $tgUri :=  $graphic/substring-after(@url, "textgridrep.org/")
              let $src :=
                if($graphic/@xml:id)
                then
                    transfo:image-tile($graphic) => replace("/,500/", "/,1500/")
                else
                    transfo:image($graphic) => replace("textgrid:.[a-z0-9]{4,5}", $tgUri)
              return
                element xhtml:div {
                  attribute class {'alt-facs'},
                  element xhtml:a {
                    attribute data-src { $src },
                    attribute data-href {"digilib/" || string($graphic/@n) || ".jpg?m2"},
                    attribute href {"#"},
                    text { "Alternativer Scan verfügbar" }
                  }
                }
            )
        }
    }
};

(:~
 : prepares URLs to textgrid digilib for given tei:surface
 : TODO: should work with tei:graphic
 :
 : :)

declare function transfo:digilib($node){

(: TILE object available :)
if($node/tei:graphic[1]/@xml:id)
  then
      transfo:image-tile($node/tei:graphic[1])
else
(: typical cover :)
    transfo:image($node/tei:graphic[1])
};

(:~
 :
 : prepares a URL to digilib (IIIF) for a given tei element
 :
 : @param $graphic a tei element like graphic or figure with a xml:id
 : @return a URL to digilib
 :   :)
declare function transfo:image-tile($graphic as element(*))
as xs:string {
  let $id := $graphic/string(@xml:id)
  let $uri := $graphic/root()//tei:publicationStmt//tei:idno[@type="TextGrid"]/string(.)

  let $link := transfo:newestTBLELink($uri, $id)
  let $facs := $link/parent::tei:linkGrp/substring-after(@facs, '#')
  let $tei := $link/ancestor::tei:TEI
  let $shape := $link/parent::tei:linkGrp/tei:link[contains(@targets, 'shape')][ends-with(@targets, $id)]/substring-before(substring-after(@targets, '#'), ' ')

  let $svgg := $tei//svg:g[@id = $facs]
  let $image := string($svgg/svg:image/@xlink:href)
  let $image := if ($image = "") then $graphic/substring-after(@url, "http://textgridrep.org/") else $image

  let $x := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@x, '%')) => format-number($transfo:numberPattern)
  let $y := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@y, '%')) => format-number($transfo:numberPattern)
  let $w := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@width, '%')) => format-number($transfo:numberPattern)
  let $h := xs:decimal($svgg//svg:rect[@id = $shape]/substring-before(@height, '%')) => format-number($transfo:numberPattern)

return
    'https://textgridlab.org/1.0/digilib/rest/IIIF/'
    || $image
    || '/pct:'
    || string-join(($x, $y, $w, $h), ",")
    || '/,500/0/default.jpg'
};

(:~
 : prepares a URL to digilib
 :
 : @param $graphic A tei element like graphic and figure
 : @return a URL like https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164g9.1/pct:7.747474581830108,5.289308105687416,84.44747294194818,89.91823779668606/,1000/0/default.jpg
 :   :)
declare function transfo:image($graphic as element(*))
as xs:string{
let
  $currentNotebook := substring-before( string($graphic/@n), '_'),
  $parent := $graphic/parent::tei:surface,
  $n := $parent/@n,
  $type := local-name($graphic[1]),
  $facs := $graphic/@n || '.jpg',
  $facs := $graphic/substring-after(@url, "http://textgridrep.org/"), (: complete base uri :)
  $extent := $graphic/root()//tei:extent[1],
  $surfaceWidth := xs:decimal($extent/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity),
  $surfaceHeight := xs:decimal($extent/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity),
  $bindingWidth := xs:decimal($extent/tei:dimensions[@type = 'binding']/tei:width[1]/@quantity),
  $bindingHeight := xs:decimal($extent/tei:dimensions[@type = 'binding']/tei:height[1]/@quantity),
  $dpcm := xs:decimal(236.2205), (:  600 dpi = 236.2205 dpcm  :)
  $exif := doc('/db/sade-projects/textgrid/data/xml/data/217qs.xml'),
  $covertble := doc('/db/sade-projects/textgrid/data/xml/tile/218r2.xml'),
  $image-width := xs:decimal($exif//digilib:image[@uri = $facs]/@width),
  $image-height := xs:decimal($exif//digilib:image[@uri = $facs]/@height),
  $plusX := xs:decimal($exif//digilib:offset[@notebook = $currentNotebook][@x]/@x),
  $plusX := if ($plusX = ()) then 1 else $plusX,
  $plusY := $exif//digilib:offset[@notebook = $currentNotebook][@y]/xs:decimal(@y),
  $plusY := if ($plusY = ()) then 1 else $plusY,
  $shape := if($type = 'cover' and $exif//digilib:image[@name = $facs]/@xml:id) then (substring-after(substring-before($covertble//tei:link[ends-with(@targets, $exif//digilib:image[@name = $facs]/@xml:id)][1]/@targets, ' '), '#')) else 0,
  $scaler :=  'https://textgridlab.org/1.0/digilib/rest/IIIF',

  $ulx := xs:decimal($parent/@ulx),
  $uly := xs:decimal($parent/@uly),
  $lrx := xs:decimal($parent/@lrx),
  $lry := xs:decimal($parent/@lry),

(: lets start building the url :)
  $image := $facs,
  $resolution :=
    switch ($type)
      case 'thumb' return ("", 150)
      case 'thumb-link' return ("", 1500)
      case 'surface-empty' return ("", 300)
      case 'figure' return ("", 500)
      default return ("",1000),

(: wx , wy :)
  $offset :=
    switch($type)
      case 'figure'
        return
            if (matches($n, '\d[a-z]*r|Ir$')) then (
                (($plusX + $ulx + $surfaceWidth div 10) * $dpcm div $image-width) * 100,
                (($plusY + $uly) * $dpcm div $image-height) * 100 )
            else if(matches($n, '\d[a-z]*v|Iv$')) then (
                (($plusX + $ulx) * $dpcm div $image-width) * 100,
                (($plusY + $uly) * $dpcm div $image-height) * 100 )
            else ()
      case 'cover'
        return (
            xs:decimal(substring-before($covertble//svg:rect[@id = $shape][1]/@x, '%')),
            xs:decimal(substring-before($covertble//svg:rect[@id = $shape][1]/@y, '%')))
            default
              return
                if(contains($n, 'outer')) then (
                    $dpcm div $image-width * 100,
                    $dpcm div $image-height * 100 )
                else if($n = 'inner_back_cover') then (
                    (xs:decimal($plusX * $dpcm div $image-width + ($surfaceWidth div 10) * ($dpcm div $image-width)) * 100),
                    (xs:decimal($plusY * $dpcm div $image-height) * 100))
                else if($n = 'inner_front_cover') then (
                    ($dpcm div $image-width) * 100,
                    ($dpcm div $image-height) * 100 )
                else if (matches($n, '\d[a-z]*r|Ir$')  and not(matches($n, "\dv[a-z]r"))) then (
                    (xs:decimal($plusX * ($dpcm div $image-width) + ($surfaceWidth div 10) * ($dpcm div $image-width)) * 100),
                    (($plusY * $dpcm div $image-height) * 100))
                else if(matches($n, '\d[a-z]*v|Iv$') or matches($n, "\dv[a-z]r")) then (
                    ($plusX * $dpcm div $image-width) * 100,
                    ($plusY * $dpcm div $image-height) * 100 )
                else (0, 0),

(: ww , wh :)
$range :=
    if ($type = 'figure') then ( (($lrx - $ulx) * $dpcm div $image-width) * 100,  (($lry - $uly) * $dpcm div $image-height ) * 100 )
        else if ($type = 'cover' and $exif//digilib:coveroffset[@notebook = $currentNotebook]) then (
                    xs:decimal(substring-before($covertble//svg:rect[@id = $shape][1]/@width, '%')),
                    xs:decimal(substring-before($covertble//svg:rect[@id = $shape][1]/@height, '%')))
        else if (contains($n, 'outer')) then (

            xs:decimal(($bindingWidth div 10) * $dpcm div $image-width) * 100,
            xs:decimal(($bindingHeight div 10) * $dpcm div $image-height) * 100 )
        else (
            xs:decimal((($surfaceWidth div 10) + 0.5) * $dpcm div $image-width) * 100,
            xs:decimal((($surfaceHeight div 10) + 0.5) * $dpcm div $image-height) * 100 )

return if($type = 'total') then concat($scaler, $image, $resolution, '&amp;mo=png')
else
    string-join(
        ($scaler,
        $image,
        "pct:" ||
            string-join(
                (format-number($offset[1], $transfo:numberPattern),
                format-number($offset[2], $transfo:numberPattern),
                format-number($range[1], $transfo:numberPattern),
                format-number($range[2], $transfo:numberPattern)), ","),
        string-join($resolution, ","),
        0,
        "default.jpg" ),
    "/")
};

declare function transfo:fraction($node) {
element xhtml:span {
    $node/@xml:id ! attribute id { string(.) },
    attribute class {'fraction', transfo:magic( $node/tei:seg[@style='vertical-align:super']/text() )/@class},
    attribute style {$node/@style},
    element xhtml:span { attribute class {'top'},
        transfo:magic( $node/tei:seg[@style='vertical-align:super']/text() )
    },
    element xhtml:span { attribute class {'bottom'},
        if(exists($node/tei:seg[@style='vertical-align:sub']/text()))
        then
          transfo:magic( $node/tei:seg[@style='vertical-align:sub']/text() )
        else
          transfo:magic( $node/tei:seg[@style='vertical-align:sub']//text()[not(parent::tei:expan)] )
    }
}

};

declare function transfo:caret($node)
as item()* {
let $lineHeight := transfo:lineHeight( ($node/ancestor::tei:zone[1]) ),
    $lineHeight := replace($lineHeight, '[^\d+.?\d*]', ''),
    $lineHeight as xs:decimal := if($lineHeight) then xs:decimal($lineHeight) else 1

let $caretType:=    if($node/local-name() != "metamark")
                    then tokenize( $node/string(@rend), '\(' )[starts-with(., 'caret')]
                    else "caret:" || tokenize($node/string(@rend), "\(")[1]

return
    switch($caretType)
        case 'caret:V' return
            caret:v($node)
        case 'caret:slash' return
                    (: also used when place=superimposed, see function add :)
            caret:slash()
        case 'caret:backslash' return
            caret:backslash($node)
        case 'caret:curved_V' return
            caret:curved-v($node, $lineHeight)
        case 'caret:bow' return
            caret:bow($node, $lineHeight)
        case 'caret:retraced_half-bow' return
            caret:retraced-half-bow($node, $lineHeight)
        case 'caret:half-bow' return
            caret:half-bow($node, $lineHeight)
        case 'caret:semicircle' return
            caret:semicircle($node, $lineHeight)
        case 'caret:quartercircle' return
            caret:quartercircle($node, $lineHeight)
        case 'caret:3/4-circle' return '' (: class will be added in transfo:add :)
        case 'caret:funnel' return
            caret:funnel($node)
        case 'caret:looped_arc' return ''
        default return ()
};

declare function transfo:lineHeight($node as node())
as xs:string? {
    if ($node/ancestor::tei:zone[last()]/@type = "illustration")
    then
        (: dont start calcualting in case of a figure :)
        'line-height:100%;'
    else
        transfo:lineHeight-calculator($node) ! ('line-height:' || format-number(., $transfo:numberPattern) || 'cm;')
};

declare function transfo:lineHeight-calculator($n as node())
as xs:decimal? {
(: coordinates :)
let
    $ulx := $n/@ulx,
    $uly := $n/@uly,
    $lrx := $n/@lrx,
    $lry := $n/@lry,
    $rotate := $n/@rotate

let $cntline :=
          count($n//tei:line)
        - count($n//tei:zone[@uly or @lry]//tei:line)
let $surfaceHeight :=
        if ($n/parent::tei:surface/parent::tei:sourceDoc)
        then xs:decimal($n/root()//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:height[1]/@quantity) div 10
        else xs:decimal($n/parent::tei:surface/@lry) - xs:decimal($n/parent::tei:surface/@uly)
let $surfaceHeight :=
        if($n/parent::tei:surface[@type = ("clipping", "additional")][parent::tei:sourceDoc])
        then $n/parent::tei:surface/@lry - $n/parent::tei:surface/@uly
        else $surfaceHeight
return
if($cntline gt 1 and count(($n/ancestor::tei:surface)[1]//tei:zone) gt 0) then
    if ($lry and not($rotate)) then
        let $height := xs:decimal($lry - (if($uly) then $uly else 0))
        return
            if (not($n/ancestor-or-self::*/@type = 'illustration') and $cntline != 0)
                then
                    $height div $cntline
            else ()
    else
        (: only one zone and no @lry :)
        if (count($n/parent::tei:surface[parent::tei:sourceDoc]/tei:zone) = 1 and not($lry) and $cntline gt 6 and not($rotate))
        then xs:decimal($surfaceHeight - (if($uly) then xs:decimal($uly) else 0)) div $cntline
    else
        if ($uly and not($lry) and not($rotate))
        then ($surfaceHeight - xs:decimal($uly)) div $cntline
    else
        if ($n/@rotate = '90')
        then
            (xs:decimal($lry) - xs:decimal($uly))  div $cntline
    else
        if ($rotate = '270')
        then
            if(not($uly and $lry)) then
                (xs:decimal($n/preceding::tei:teiHeader//tei:extent[1]/tei:dimensions[@type = 'leaf']/tei:width[1]/@quantity) div 10 - $uly) div $cntline
            else (: usual rotation with coordinates :)
               (xs:decimal($lry) - xs:decimal($uly)) div $cntline
    else if((xs:decimal($n/@rotate) gt 270) and (xs:decimal($rotate) lt 360)) then
        if ($uly and $lry)
        then (xs:decimal($lry) - xs:decimal($uly)) div $cntline
        else ()
    else
        if($n/parent::tei:surface[not(tei:zone[@rotate|@lrx|@lrx|@ulx|@uly])])
        (: https://projects.gwdg.de/issues/17466 :)
        then
            let $cntline := count($n/parent::tei:surface//tei:line)
            return
                $surfaceHeight div $cntline
    else
        if($n[not(@rotate)][not(@lrx)][not(@lrx)][not(@ulx)][not(@uly)] and ($surfaceHeight div $cntline lt 1) )
        then
            $surfaceHeight div $cntline
    else
        if($rotate and $uly and $lry) then
            (xs:decimal($lry) - xs:decimal($uly)) div $cntline
    else ()
else
(:  if($cntline = 1 and count($n/parent::tei:surface//tei:zone) gt 1) :)
(:  https://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/1zzdp.xml&page=inner_front_cover :)
    if($cntline = 1 and count($n/parent::tei:surface/tei:zone) gt 1)
    then
        if($lry and $uly)
        then
            $lry - $uly
        else if($n/parent::tei:surface//tei:zone[not(@rotate)][not(@lrx)][not(@lry)][not(@ulx)][not(@uly)])
        (: https://projects.gwdg.de/issues/17466 , we need to test for more then one zone ! :)
        then
            let $cntline := count($n/parent::tei:surface//tei:line)
            return
                if($rotate and $uly and $lry)
                then
                    (xs:decimal($lry) - xs:decimal($uly)) div $cntline
                else
                    $surfaceHeight div $cntline
        else ()
    else ()
};

(:~
 : prepares a tei:gap for XHTML output
 :)
declare function transfo:gap($node as element(tei:gap)) {
element xhtml:span {
            attribute class {'gap', $node/@reason, ($node/@unit || $node/@quantity (: classes must begin with a letter :)),
            if(($node/@reason = 'damage') and $node/@unit = 'mm') then attribute style {'width:' || $node/@quantity || $node/@unit} else(),
            (let $preMedium := transfo:getMedium($node)
            return
                if(tokenize($preMedium, ' ') = ('blue_pencil', 'violet_pencil', 'black_ink', 'blue_ink', 'brown_ink')) then $preMedium else ())
            }
        }
};

declare function transfo:metamark($node) {(
element xhtml:span {
    attribute class {
        'metamark',
        if(contains($node/@rend, 'bracket_left') and $node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
            then 'cm' || floor($node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly)
            else (),
        string($node/@function) => replace('caret', 'teicaret'),
        if($node/@function = "deletion" and $node = "|") then "noBackground" else (),
        if($node/tei:figure[not(./ancestor::tei:zone/@points)]) then "innerFigure" else (),
        if($node/@function = "deletion" and $node/parent::tei:zone/@points)
            then "no-border"
            else(),
        if($node/text() = "╒" and $node/ancestor::tei:seg[contains(@style, "vertical-align:super")]) then ("f") else (),
        $node//descendant-or-self::tei:*[@rend]/replace(string(@rend),":"," "),
        $node/@rend => replace(":", " "),
        transfo:getMedium($node)
    },
    transfo:magic($node/node()),
    (: if($node/@rend = "slash") then "/" else (), :)
    if(contains($node/@rend, 'bracket_left'))
        then
            let $num :=  $node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly
            return
        (
            if($node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
                then
                        attribute style { 'font-size:' || $num || 'cm;' }
                else (),
              <xhtml:span class="bracket_left" style="transform: scaleX({1 div ceiling($num)});">{
                if($node/@rend = "bracket_left")
                then "&#123;"
                else "&#91;"}</xhtml:span>
        )
        else (),
    if(contains($node/@rend, 'bracket_right'))
        then
            <xhtml:span class="bracket_right">{
                if($node/parent::tei:zone/*[1] = $node/parent::tei:zone/*[last()])
                then
                    let $num :=  $node/parent::tei:zone/@lry - $node/parent::tei:zone/@uly
                    return (
                        attribute style {
                            'font-size:' || $num || 'cm;',
                            if($num gt 3) then 'display:block; transform:scaleX(' || 1 div $num || ');' else ()
                        }
                    )
                else ()
            }{
              if($node/@rend = "bracket_right")
              then "&#125;"
              else "&#93;"}</xhtml:span>
        else (),
    if(matches($node, '#|╒|/|\*') and not($node/@function = ("footnote-mark", "verse_marker"))) then
        <xhtml:div class="metamarkHover"> Einweisungszeichen </xhtml:div>
    else (),
    if ($node/@function="footnote-mark") then
        <xhtml:div class="metamarkHover"> Fußnotenzeichen </xhtml:div>
    else (),
    if ($node/@function="paragraph") then
        <xhtml:div class="metamarkHover"> Korrekturzeichen Absatz </xhtml:div>
    else (),
    if ($node/@function="etc.") then
        <xhtml:div class="metamarkHover"> und so weiter </xhtml:div>
    else (),
    if ($node/@function="verse_marker") then
        <xhtml:div class="metamarkHover"> Versmarkierung </xhtml:div>
    else (),
    if ($node/@function="integrate") then
        <xhtml:div class="metamarkHover"> Klammer zusammenfassende Funktion </xhtml:div>
    else (),
    (if($node/@function = "caret" and $node/@rend ) then
        (<xhtml:span class="smallspace"/>,
        transfo:caret($node),
        <xhtml:span class="smallspace"/>)
        else()
      ),
      if ($node/@function="placeholder") then
          <xhtml:div class="metamarkHover"> Platzhalter </xhtml:div>
      else ()
},
if(($node/@function = ("deletion", "connect", "transposition")) and $node/parent::tei:zone/@points) then
  <svg xmlns="http://www.w3.org/2000/svg" class="{string($node/@function)}-points {replace($node//tei:figure/@rend, ":", " ")}" width="100%" height="100%">
      <g transform="scale(37)">
        <polyline fill="none" stroke="darkslategrey" stroke-width="0.04px" points="{$node/parent::tei:zone/@points}"/>
      </g>
    </svg>
    else ()
)};

(:~
 : Provides information if there is no contemporary writing on a surface.
 : It excludes all hands we find at tei:handNotes
 : @param $surface A single tei:surface
 : @return a boolean true if surface is vakat, else false
 :)
declare function transfo:vakat($surface as element(tei:surface)) as xs:boolean {
let $initalHand :=
    let $i := string($surface/preceding::tei:handShift[@new][1]/@new)
    return
        (: fallback :)
        if($i != "") then $i else "#Fontane"
let $allText := string-join($surface//text())
return
(: completly empty :)
    if( replace( $allText, "\s", "" ) = "" and not($surface//tei:figure))
    then true()
    else
        let $hands :=   ($surface/ancestor::tei:TEI/tei:teiHeader//tei:handNote[@script="contemporary"]/concat("#",@xml:id)
                        , "")
        return
(: test for sketches :)
            if( $surface//tei:figure[preceding::tei:handShift[@new][1]/@new=$hands] )
            then false()
            else
(: test for text by contemporary hands :)
                if( exists(
                        $surface
                        //text()
                            [preceding::tei:handShift[@new][1]/@new=$hands]
                            [ancestor::tei:line]
                    )
                )
                then false()
                else true()
};

(: prerendered toc :)
declare function transfo:toc($doc) {
let $responsilbeHands as xs:string+ :=
    ($doc//tei:profileDesc/tei:handNotes[string(@n) = ("5", "4")]/tei:handNote/("#" || @xml:id),"#Druck_p")
let $uri := $doc/*//tei:idno[@type="TextGrid"]/text() => substring-after('textgrid:')
return
element xhtml:div {
    attribute id {'toc'},
    $doc/tei:fileDesc/tei:titleStmt/tei:title/string(.),
    <xhtml:ol>
        {for $surface in $doc/tei:sourceDoc/tei:surface
        let $n := string($surface/@n)
        let $url := "?id=/xml/data/"
                    || $uri
                    || ".xml&amp;page="
                    || $n
        let $vakat := if(transfo:vakat($surface)) then "vakat" else (),
            $fragment :=    if($surface/@type="fragment")
                            then ('Fragment',
                                    if($surface/@attachment)
                                    then
                                        switch($surface/@attachment)
                                        case 'cut' return 'Wegschnitt'
                                        case 'torn' return 'Wegriss'
                                        case 'torn_and_cut' return 'Wegriss und Wegschnitt'
                                        default return $surface/@attachment
                                    else ())
                            else (),
            $info := string-join(($fragment, $vakat), ', '),
            $childs := for $child in $surface//tei:surface[@type= ('clipping', 'additional', 'pocket')][@n]
                        let $vakat-child := if(transfo:vakat($child)) then "vakat" else ()
                        let $n-child := replace($child/string(@n), "_", " ")
                        let $subtype-child := replace($child/string(@subtype), "_", " ")
                        let $info := "(" || string-join(($subtype-child, $vakat-child), ", ") || ")"
                        return
                            <xhtml:li data-page="{ $child/string(@n) }">
                                <xhtml:a href="?id=/xml/data/{ $uri }.xml&amp;page={ $child/@n }">
                                    { $n-child } { if ($info != "()") then $info else () }
                                </xhtml:a>
                            </xhtml:li>

        return
        ( (: double pages at first :)
        if( exists($surface/@next) )
        then
          let $correspondingPage := substring-after($surface/@next, "_")
          return
          <xhtml:li data-page="{$n}-{$correspondingPage}">
              <xhtml:a href="{$url}-{$correspondingPage}">
                      {$n}/{$correspondingPage} (Doppelseitenansicht)
                  </xhtml:a>
          </xhtml:li>
        else (),

        (: normal single surface :)
                <xhtml:li data-page="{$n}">
                <xhtml:a href="{$url}">
                  {f-misc:n-translate($n)}
                  {if($info = '') then () else ' (' || $info || ')'}
                </xhtml:a>
                    {if($childs = ()) then () else <xhtml:ul> {$childs} </xhtml:ul>}
            </xhtml:li>,

        (: surface without additionals :)
          if(($surface/tei:surface/starts-with(@n, "Beilage_") = true())
          and (($surface/tei:surface[@type="clipping"]/starts-with(@attachment, "partially_glued") = true())
                or
              (exists($surface/tei:surface[@type="pocket"]/tei:surface))
          ))
          then
            <xhtml:li data-page="{$surface/string(@n)}-alt">
                <xhtml:a href="{$url}-alt">
                    {f-misc:n-translate($surface/string(@n))} (ohne Beilage, ohne Anklebung)
                </xhtml:a>
            </xhtml:li>
          else
            if ($surface/tei:surface/starts-with(@n, "Beilage_") = true())
            then
                <xhtml:li data-page="{$surface/string(@n)}-alt">
                    <xhtml:a href="{$url}-alt">
                        {f-misc:n-translate($surface/string(@n))} (ohne Beilage)
                    </xhtml:a>
                </xhtml:li>
        (: surface without additionals :)
           else if ($surface/tei:surface[@type="clipping"]/starts-with(@attachment, "partially_glued") = true())
            then
                <xhtml:li data-page="{$surface/string(@n)}-alt">
                    <xhtml:a href="{$url}-alt">
                        {f-misc:n-translate($surface/string(@n))} (ohne Anklebung)
                    </xhtml:a>
                </xhtml:li>
            else if (exists($surface/tei:surface[@type="pocket"]/tei:surface))
            then
                <xhtml:li data-page="{$surface/string(@n)}-alt">
                    <xhtml:a href="{$url}-alt">
                        {f-misc:n-translate($surface/string(@n))} (ohne Anklebung)
                    </xhtml:a>
                </xhtml:li>
            else ()
          )
        }
    </xhtml:ol>
}
};

(:~
 : Test whether tei:add or tei:del needs more space.
 : @param $mod A tei:mod containing tei:add and tei:del
 : @return true if tei:add is sesquipedalian
 ::)
declare function local:modTest($mod as element(tei:mod))
as xs:boolean? {
let $delStringLength:=
    if($mod/tei:del//tei:gap)
        then
            sum((
                ($mod/tei:del//tei:gap[@unit="lc_chars"]/@quantity),
                ($mod/tei:del//tei:gap[@unit="uc_chars"]/@quantity),
                ($mod/tei:del//tei:gap[@unit="cap_words"]/(xs:decimal(@quantity) * 5)),
                ($mod/tei:del//tei:gap[@unit="uncap_words"]/(xs:decimal(@quantity) * 5))
            )) + string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )
        else string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )

let $addStringLength :=
    if($mod/tei:add//tei:gap)
        then
            sum((
                ($mod/tei:add//tei:gap[@unit="lc_chars"]/@quantity),
                ($mod/tei:add//tei:gap[@unit="uc_chars"]/@quantity),
                ($mod/tei:add//tei:gap[@unit="cap_words"]/(xs:decimal(@quantity) * 5)),
                ($mod/tei:add//tei:gap[@unit="uncap_words"]/(xs:decimal(@quantity) * 5))
            )) + string-length(replace(string-join($mod/tei:del//text() ), '\s', '' ) )
        else string-length( replace(string-join($mod//tei:add[not(@place = 'above' or @place="below")]//text()), '\s', '') )
let $pattern := 'ſ|i|j|l|t|I|,'
return
(
    $delStringLength
    lt
    $addStringLength
)
or
(
    (
        string-length(string-join($mod/tei:add//text() ))
        =
        string-length(string-join($mod/tei:del/text() ))
    )
    and
        count(tokenize(string-join($mod/tei:add//text()), $pattern))
        lt
        count(tokenize(string-join( $mod/tei:del/text() ), $pattern))
)
};

declare function transfo:newestTBLELink($uri as xs:string, $id as xs:string) as node() {
let $links := collection('/db/sade-projects/textgrid/data/xml/tile')
            //tei:link
            [matches(@targets, "#shape.*" || $uri || "\.\d+#" || $id || "$")]

let $maxBase :=  max( $links ! ./base-uri() )

return
    if( count( $links ) = 1 )
    then
        $links
    else
        let $links := $links[base-uri() = $maxBase]
        return
            if(count($links) = 0)
            then
                error(QName("https://sade.textgrid.de/ns/error/fontane", "TILE01"), "no link found for " || $uri || "#" || $id || ".")
            else
                if(count($links) = 1)
                then $links
                else $links[last()]
                (: if there are more than one links to a single xml:id in a single TILE object, take the last one :)
};

declare function transfo:getMedium($node)
as xs:string {
  string($node/preceding::tei:handShift[@medium][1]/@medium)
};

declare function transfo:noteParser($note as node()*) {
    for $node in $note return
        typeswitch ( $node )
        case element (tei:note) return
            let $xmlids := tokenize(replace($node/@target, "#", ""), " ")
            return (
            attribute data-ref {string-join($xmlids, " ")},
            element xhtml:span {
                attribute class {"target"},
                string-join(
                    (for $xmlid in $xmlids
                    return
                        transfo:magic($node/root()//*[@xml:id=$xmlid]))
                        //text()
                        [not(exists(parent::*[contains(@class, "expan")]))]
                        [not(exists(ancestor::xhtml:div/contains(lower-case(@class), "hover")))]
                        [not(ancestor::xhtml:span/@class = "retrace")]
                    , " ") || "]" => replace("- ", "") => replace("  ", "")
                },

            element xhtml:ul {
              element xhtml:li {
                transfo:noteParser($node/node())
              }
            })
        case element( tei:bibl ) return
            transfo:noteParser($node/node())
        case element( tei:ptr ) return (
            element xhtml:a {
                let $targetId := $node/@target => substring-after(":")
                return
                    (attribute href {"literaturvz.html?id=" || $targetId},
                    $transfo:dataCollection/id($targetId)/tei:choice/tei:abbr/text())
            },
            transfo:noteParser($node/node())
            )
        case element( tei:citedRange ) return (
            ", ",
            if ( starts-with($node, "http") )
            then <a title="citedRange" href="{string($node)}"><i class="fa fa-external-link"></i></a>
            else $node/text())
        case element( tei:ref ) return
            let $pattern := "^#.+_.+_.+$"
            let $targetParts := tokenize($node/@target, "_")
            let $signature := $targetParts[1]
                                    => substring-after("#")
            let $TGuri := local:getUriBySignature($signature)
            let $TGuri := if($TGuri = "") then string($node/ancestor::tei:TEI/tei:teiHeader//tei:idno[@type="TextGrid"]) || ".0" else $TGuri
            let $uri := $TGuri
                                    => substring-after(":")
                                    => substring-before(".")
            return
                if( matches($node/@target, $pattern))
                then
                    element xhtml:a {
                        attribute href {$transfo:base-url || "edition.html?id=%2Fxml%2Fdata%2F" || $uri || ".xml&amp;page=" || $targetParts[2] || "&amp;target=" || substring-after($node/@target, "#"),
                        transfo:noteParser($node/node()),
                        $node/node()
                        }
                    }
                else <xhtml:span style="color:red; font-weight:bold"> tei:ref/@target does not macht »{$pattern}« </xhtml:span>
        case element( tei:seg ) return
            element xhtml:span {
                $node/@style,
                transfo:noteParser($node/node())
            }
        case element( tei:rs ) return
            element xhtml:a {
                attribute href {local:teiref2href($node/string(@ref))},
                transfo:noteParser($node/node())
            }
        case text() return $node
        default return
            transfo:noteParser($node/node())
};

declare function local:getUriBySignature($signature as xs:string) as xs:string* {
    collection("/db/sade-projects/textgrid/data/xml/meta")//tgmd:object
        [tgmd:generic/tgmd:provided/tgmd:title[starts-with(., "Notizbuch")][ends-with(., $signature)]]
        /tgmd:generic/tgmd:generated/tgmd:textgridUri/string()
};

declare function local:teiref2href($ref as xs:string) as xs:string {
    let $prefix := substring-before($ref, ":")
    let $id := substring-after($ref, ":")
    return
        switch ($prefix)
            case "lit" return $transfo:base-url || "literaturvz.html?id=" || $id
            default return
                let $listName := index:get-list-by-id($id)
                return $transfo:base-url || "pocketregister.html?e=" || $id
};

declare function transfo:figure($node as node()*) {
if( $node/parent::tei:zone/@points )
then
    let $coordinates := tokenize( $node/ancestor::tei:zone[1]/@points, ' ' )
    return
        (<svg class="{tokenize($node//tei:figDesc/tei:ref/text(), ' ')[2]}"
        width="{$node/ancestor::tei:TEI/tei:teiHeader//tei:dimensions[@type="leaf"]/tei:width/string(@quantity)}mm"
        height="{$node/ancestor::tei:TEI/tei:teiHeader//tei:dimensions[@type="leaf"]/tei:height/string(@quantity)}mm"
        style="position:absolute; top:0;">
            {$node/@xml:id ! attribute id { string(.) },
            for $pair in 1 to (count($coordinates) - 1)
            let $x1 := tokenize($coordinates[$pair], ',')[1]
            let $y1 := tokenize($coordinates[$pair], ',')[2]
            let $x2 := tokenize($coordinates[$pair + 1], ',')[1]
            let $y2 := tokenize($coordinates[$pair + 1], ',')[2]
                return
                    <line style="stroke-width:1" x1="{$x1}cm" y1="{$y1}cm" x2="{$x2}cm" y2="{$y2}cm" />
            }
        </svg>,
        element xhtml:div {
            attribute class {'hrHover'},
            tokenize($node//tei:figDesc/tei:ref/text(), ' ')[2] => replace(";", "")
            }
        )
else
  if($node/tei:figDesc/tei:ref eq "horizontale einfache Fußnotenlinie")
  then
    <xhtml:hr class="fußnotenlinie"/>
  else  if( contains($node//tei:figDesc/tei:ref/text(), 'Seitenzahlabgrenzungslinie') )
        then element xhtml:hr { attribute class {'Seitenzahlabgrenzungslinie'}}
    else if(contains($node//tei:figDesc/tei:ref/text(), 'Absatzlinie') and contains($node//tei:figDesc/tei:ref/text(), 'nachgezogen')) then
    (<svg xmlns="http://www.w3.org/2000/svg" class="absatzlinie--nachgezogen" width="100%" height="auto" viewBox="0 0 300 20">
    {$node/@xml:id ! attribute id { string(.) }}
        <path d="M130 4 C 130 4, 110 2, 90 6" fill="transparent"/>
        <path d="M90 6 C 90 4, 100 10, 295 3" fill="transparent"/>
        <path d="M295 3 C 300 -2, 300 8, 5 20" fill="transparent"/>
        <path d="M5 20 C 4 17, 8 22, 295 14" fill="transparent"/>
    </svg>,
    element xhtml:div {
    attribute class {'hrHover'},
    if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
    local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), "einfache Absatzlinie (hin und her)"))
    then
        element svg {
            attribute viewBox {"0 0 451 46"},
            attribute class {"absatzlinie--hin-und-her"},
            $node/@xml:id ! attribute id { string(.) },
            element path {
               attribute id {"ahihe"},
               attribute fill {"none"},
               attribute stroke {"darkslategrey"},
               attribute stroke-width {"1"},
               attribute d {"M 440.67,14.67 " ||
                "C 440.67,14.67 25.33,2.00 7.33,21.33 " ||
                 "11.33,28.00 422.00,11.33 414.67,20.00 " ||
                 "392.67,8.67 32.00,36.67 32.00,36.67" }
            }
        }
    else
    if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschleife von links nach rechts unten"))
    then (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 114 24">
    {$node/@xml:id ! attribute id { string(.) }}
    <path id="Unnamed" fill="none" stroke="darkslategrey" stroke-width="1"
            d="M 2.75,20.00 C 12.88,23.38 91.88,-1.50 106.75,4.12  113.62,6.88 112.00,12.12 110.50,13.25  107.25,15.00 105.00,11.12 105.00,11.12" />
    </svg>,
    element xhtml:div {
        attribute class {'hrHover'},
        if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
        local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschleife von links oben nach rechts"))
    then
    (<svg xmlns="http://www.w3.org/2000/svg" style="bottom:0; position:absolute;" viewBox="0 0 743 76" height="100%" width="100%" preserveAspectRatio="none">
        {$node/@xml:id ! attribute id { string(.) }}
        <path fill="none" stroke="darkslategrey" stroke-width="3"
            d="M 181.33,35.33 C 181.33,35.33 257.33,20.67 232.67,10.67  222.67,-3.33 4.67,27.33 6.67,42.00  4.67,73.33 380.00,68.00 730.00,52.67" />
    </svg>,
    element xhtml:div {
        attribute class {'hrHover'},
        if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
        local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(),"horizontale Schleife von links oben nach rechts unten"))
    then (
    (: there are rendering issues when using the correct svg namespace. :)
    element xhtml:svg {
    $node/@xml:id ! attribute id { string(.) },
    attribute width {"100%"},
    attribute height {"auto"},
    attribute viewBox {"0 0 220 29"},
    element xhtml:path {
          attribute id {"sl2r1"},
          attribute fill {"none"},
          attribute stroke {"darkslategrey"},
          attribute stroke-width {"1.5"},
          attribute d {"M 14.00,18.18 " ||
             " C 4.55,19.82 -2.73,27.09 5.27,26.55 " ||
               " 65.64,20.91 197.64,-20.00 218.18,16.36 " ||
               " 213.64,19.82 203.45,13.64 203.45,13.64" }}
    },
    element xhtml:div {
    attribute class {'hrHover'},
    if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
    local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(),"horizontale Halbschleife von links unten nach rechts"))
    then (
    <svg xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:cc="http://creativecommons.org/ns#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns="http://www.w3.org/2000/svg"
    version="1.1"
    viewBox="0 0 59 16"
    height="100%"
    width="100%"
    style="min-height: 3mm;"
    preserveAspectRatio="none">
    {$node/@xml:id ! attribute id { string(.) }}
    <metadata>
      <rdf:rdf>
        <cc:work rdf:about="">
          <dc:format>image/svg+xml</dc:format>
          <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"></dc:type>
          <dc:title></dc:title>
        </cc:work>
      </rdf:rdf>
    </metadata>
    <path d="M 20.55,10.82 C 20.55,10.82 -8.18,5.55 6.91,5.09  35.18,4.00 57.00,3.91 57.00,3.91" stroke-width="1" stroke="black" fill="none" id="hsl2r"></path>
    </svg>,
    element xhtml:div {
        attribute class {'hrHover'},
        if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
        local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), 'Absatzlinie')) then
    (element xhtml:hr {
        $node/@xml:id ! attribute id { string(.) },
        attribute class {
          'Absatzlinie',
          transfo:getMedium($node),
          if(contains($node//tei:figDesc/tei:ref/text(), 'leicht gewellt'))
          then "underline slightly_wavy"
          else (),
          if(contains($node//tei:figDesc/tei:ref/text(), '(gewellt)'))
          then "underline wavy"
          else ()
        }
    },
    element xhtml:div {
    attribute class {'hrHover'},
    if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
    local:get-line-type($node//tei:figDesc)
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), 'Summenstrich')) then
    (element xhtml:hr {
        $node/@xml:id ! attribute id { string(.) },
        attribute class {'Summenstrich'}
    },
    element xhtml:div {
    attribute class {'hrHover'},
    'Summenstrich'
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), 'Differenzstrich')) then
    (element xhtml:hr {
        $node/@xml:id ! attribute id { string(.) },
        attribute class {'Differenzstrich'}
    },
    element xhtml:div {
    attribute class {'hrHover'},
    'Differenzstrich'
    })
    else
    if($node//tei:figDesc/tei:ref = 'horizontale einfache Abgrenzungslinie (geschweifte Klammer)') then
      (element xhtml:hr {
      $node/@xml:id ! attribute id { string(.) },
      attribute class {
          'Abgrenzungslinie curvy_bracket',
          (let $medium := transfo:getMedium($node)
          return if($medium != ("pencil", "")) then $medium else ())
      }
      },
      element xhtml:div {
      attribute class {'hrHover'},
      'Abgrenzungslinie'
      })
    else
    if($node//tei:figDesc/tei:ref = 'horizontale einfache Abgrenzungslinie (gewellt)') then
      (element xhtml:hr {
      $node/@xml:id ! attribute id { string(.) },
      attribute class {
          'Abgrenzungslinie wavy',
          (let $medium := transfo:getMedium($node)
          return if($medium != ("pencil", "")) then $medium else ())
      }
      },
      element xhtml:div {
      attribute class {'hrHover'},
      'Abgrenzungslinie'
      })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), 'Abgrenzungslinie')) then
    (element xhtml:hr {
    $node/@xml:id ! attribute id { string(.) },
    attribute class {
        'Abgrenzungslinie',
        (let $medium := transfo:getMedium($node)
        return if($medium != ("pencil", "")) then $medium else ()),
        if(contains($node//tei:figDesc/tei:ref/text(), 'Halbkreis'))
        then "Halbkreis"
        else ()
    }
    },
    element xhtml:div {
    attribute class {'hrHover'},
    if (starts-with($node//tei:figDesc/tei:ref/text(), "ehemalige")) then "ehemalige" else (),
    'Abgrenzungslinie'
    })
    else
    if(contains($node//tei:figDesc/tei:ref/text(), 'Schlusslinie')) then
    (
    if(contains($node//tei:figDesc/tei:ref/text(), "horizontale Halbschliefe von links oben nach rechts"))
    then
          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="auto" viewBox="0 0 418 40">
            {$node/@xml:id ! attribute id { string(.) },
            element svg:path {
            attribute fill {"none"},
            attribute stroke {"darkslategrey"},
            attribute stroke-width {"1"},
            attribute d {"M 408.00,13.50" ||
                " C -6.50,51.50 -4.50,22.50 17.00,16.00" ||
                " 17.00,16.00 35.50,12.50 35.50,12.50"}
              }
            }
          </svg>
    else if($node//tei:figDesc/tei:ref/text() = "horizontale einfache Schlusslinie (leicht gewellt)")
    then
      (element xhtml:hr {
        $node/@xml:id ! attribute id { string(.) },
        attribute class {
            'Schlusslinie slightly_wavy',
            (let $medium := transfo:getMedium($node)
            return if($medium != ("pencil", "")) then $medium else ())
        }
        },
        element xhtml:div {
        attribute class {'hrHover'},
        'Schlusslinie'
      })
    else
        element xhtml:hr {
            attribute class {'Schlusslinie'}
        },
    element xhtml:div {
    attribute class {'hrHover'},
    if(contains($node//tei:figDesc/tei:ref/text(), 'ehemalige')) then 'ehemalige' else (),
    'Schlusslinie'
    })
    else ()
};

declare function local:get-line-type($node as element(tei:figDesc))
as xs:string {
  if (contains($node, 'Absatzlinie')) then "Absatzlinie" else
  if (contains($node, 'Abgrenzungslinie')) then "Abgrenzungslinie" else
  if (contains($node, 'Schlusslinie')) then "Schlusslinie" else
  "TODO: specify type in transform.xql"
};

(:~
 : returns the current script value based on preceding tei:handShift and/or
 : ancestor::*[@xml:lang]
 :
 : @param $n A text() node in a context
 : @return script type (a combination of Latn/Latf and standard/angular/clean/hasty)
 :)
declare function transfo:script($text as text())
as xs:string {
let $handShifts :=
  if (contains($text/preceding::tei:handShift[@script][1]/@script, ' '))
  then string($text/preceding::tei:handShift[@script][1]/@script)
  else
      if($text/preceding::tei:handShift[@script][1]/@script = ('standard', 'clean', 'hasty', 'angular'))
      then $text/preceding::tei:handShift[contains(@script, 'Latn') or contains(@script, 'Latf')][1]/tokenize(@script, ' ')[. = ('Latn', 'Latf')] || ' ' || $text/preceding::tei:handShift[@script][1]/@script
      (: ^^^ no test for an additional whitespace together with Latn or Latf, characteristics will be overwritten by the current @script :)
  else
      if ($text/preceding::tei:handShift[@script][1]/@script = ('Latn', 'Latf'))
      then $text/preceding::tei:handShift[@script][1]/@script || ' ' || $text/preceding::tei:handShift[contains(@script, 'standard') or contains(@script, 'clean') or contains(@script, 'hasty') or contains(@script, 'angular')][1]/tokenize(@script, ' ')[. = ('standard', 'clean', 'hasty', 'angular')]
  else 'Latf standard'

let $segLang := $text/ancestor::tei:seg[@xml:lang]

return
    if($segLang)
    then
        if($segLang >> $text/preceding::tei:handShift[@script = ('Latn', 'Latf')][1])
        then substring-after($segLang/@xml:lang, "-") || " " || substring-after($handShifts, " ")
        else $handShifts
    else $handShifts
};

(:~
 : get the TextGrid URI without prefix and without revision number
 : (base URI base)
 :)
declare function transfo:get-base($node as node())
as xs:string {
  string($node/root()
  //tei:teiHeader//tei:idno[@type="TextGrid"])
  => substring-after(':')
};

(:~
 : prepare a tooltip for a given element
 :)
declare function transfo:tooltip($node as element())
as element()? {
  let $localName := $node/local-name()
  return
    element xhtml:div {
      attribute class { 'f-tooltip for-' || $localName },
      switch($localName)
        case "unclear" return "unsichere Lesart"
        default return false()
    }
};
