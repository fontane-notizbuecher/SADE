xquery version "3.1";
(:~
 : Provides a REST-API for miscellaneous purposes.
 :
 : @author Mathias Göbel
 : @version 0.1
 : @since 3.2.3
 : :)

module namespace misc="https://fontane-nb.dariah.eu/miscapi";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace http = "http://expath.org/ns/http-client";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace rest="http://exquery.org/ns/restxq";

declare variable $misc:dataPath := "/db/sade-projects/textgrid/data/xml/data/";
declare variable $misc:xhtmlPath := "/db/sade-projects/textgrid/data/xml/xhtml/";

(:~ Prepares a list of published notebooks :)
declare
  %rest:GET
  %rest:path("/api/app/published")
  %rest:produces("application/json")
  %output:method("json")

  %test:name("published notebooks")

function misc:public-books()
{
    let $publishedItems := collection($misc:dataPath)//tei:title[starts-with(., "Notizbuch ")]/ancestor::tei:TEI[.//tei:change/contains(., "Version ") = true()]
    let $result := for $TEI in $publishedItems
    let $fulltitle := string(($TEI//tei:title)[1])
    let $title := $fulltitle => substring-after("Notizbuch ")
    let $order1 := substring($title, 1, 1)
    let $order2 := substring($title, 2) => format-number("00")
    let $order := $order1 || $order2

    let $thumb := "https://textgridlab.org/1.0/digilib/rest/IIIF/"
                    || $TEI/tei:sourceDoc/tei:surface[1]/tei:graphic/@url => substring-after(".org/")
                    ||"/pct:7.7424,5.2634,83.6179,88.4248/,1000/0/default.jpg"

    let $date := min( ($TEI//tei:creation//tei:date/number(./@when-iso))[string(.) != "NaN"] ) => string()
    let $date := if($date) then $date else "no date available."

    let $abstract := string-join($TEI//tei:abstract//text(), " ") => normalize-space()
    let $abstract := if($abstract) then $abstract else "no abstract available."

    let $uri := ($TEI/base-uri() => tokenize("/"))[last()] => substring-before(".xml")
    order by $order
    return

        map{
            "notebook": map{
                "title": $fulltitle,
                "shorttitle": $title,
                "thumb": $thumb, (: TODO use the prerendered images to get better croped images :)
                "abstract": $abstract,
                "date": $date,
                "uri": $uri
            }
        }
return
    (<rest:response>
        <http:response status="200">
            <http:header name="Access-Control-Allow-Origin" value="*"/>
        </http:response>
    </rest:response>,
    map{ "notebooks": $result})
};

(:~ Prepares a list of pages in a single notebooks  :)
declare
  %rest:GET
  %rest:path("/api/app/listpages/{$uri}")
  %rest:produces("application/json")
  %output:method("json")

  %test:name("list pages per notebook notebooks")
  %test:arg("uri", "16b00")
  %test:assertTrue
function misc:listpages($uri as xs:string)
{
    let $toc := ($misc:xhtmlPath || $uri || "/toc.xml") => doc()
    let $sequence :=
        for $a in $toc/xhtml:div/xhtml:ol/xhtml:li/xhtml:a[not(contains(., "vakat"))]
        let $n := string($a/parent::xhtml:li/@data-page)
        let $xhtml := doc($misc:xhtmlPath || $uri || "/" || $n || ".xml")
        return
            map{
                "page": map{
                    "title": string($a) => normalize-space(),
                    "n": $n,
                    "thumb": ($xhtml//xhtml:img/@src)[1] => replace("/,1000/0/default.jpg", "/,150/0/default.jpg")
                }
            }
return
    (<rest:response>
        <http:response status="200">
            <http:header name="Access-Control-Allow-Origin" value="*"/>
        </http:response>
    </rest:response>,
    $sequence)
};

(:~
 : get a single surface transcription (HTML) and facsimile (URL) put into JSON
 : @param $uri a valid TextGrid URI without prefix and revision number
 : @return a JSON resource with a URL to the facsimile and the transcription in HTML
 :)
declare
  %rest:GET
  %rest:path("/api/app/page/{$uri}/{$surface}")
  %rest:produces("application/json")
  %output:method("json")

  %test:name("get transcription")
  %test:arg("uri", "16b00")
  %test:arg("surface", "1r")
  %test:assertTrue

function misc:listpages($uri as xs:string, $surface as xs:string)
{
    let $rendering := ($misc:xhtmlPath || $uri || "/" || $surface || ".xml") => doc()
    let $result :=
        map{
            "page": map{
                "facs": $rendering//xhtml:div[@class="facs"]//xhtml:img[1]/@src => string(),
                "transcription": $rendering//xhtml:div[@class="surface"] => serialize() => replace("xhtml:", "")
            }
        }
return
    (<rest:response>
        <http:response status="200">
            <http:header name="Access-Control-Allow-Origin" value="*"/>
        </http:response>
    </rest:response>,
    $result)
};

declare
  %rest:GET
  %rest:path("/api/installation/version")
  %rest:produces("application/json")
  %output:method("json")

  %test:name("backend version")

function misc:version(){
  (<rest:response>
      <http:response status="200">
          <http:header name="Access-Control-Allow-Origin" value="*"/>
      </http:response>
  </rest:response>,
  <packages>
    <sade>
      <name>{string(doc('/db/apps/SADE/expath-pkg.xml')//*:package/@name)}</name>
      <version>{string(doc('/db/apps/SADE/expath-pkg.xml')//*:package/@version)}</version>
      <deployed>{string(doc('/db/apps/SADE/repo.xml')//*:meta/*:deployed)}</deployed>
    </sade>
    <fontane>
      <name>{string(doc('/db/apps/fontane/expath-pkg.xml')//*:package/@name)}</name>
      <version>{string(doc('/db/apps/fontane/expath-pkg.xml')//*:package/@version)}</version>
      <deployed>{string(doc('/db/apps/fontane/repo.xml')//*:meta/*:deployed)}</deployed>
    </fontane>
    <tgconnect>
      <name>{string(doc('/db/apps/textgrid-connect/expath-pkg.xml')//*:package/@name)}</name>
      <version>{string(doc('/db/apps/textgrid-connect/expath-pkg.xml')//*:package/@version)}</version>
      <deployed>{string(doc('/db/apps/textgrid-connect/repo.xml')//*:meta/*:deployed)}</deployed>
    </tgconnect>
  </packages>
  )
};

(:~
 : JSON serialization of a TEI file
 : @param $uri A valid TextGrid URI (without prefix) of an item available in the database
 : @return
 :)
declare
  %rest:GET
  %rest:path("/api/data/json/{$uri}")
  %rest:produces("application/json")
  %output:method("json")

  %test:arg("uri", "16b00")
  %test:assertTrue
function misc:json($uri as xs:string){
  (<rest:response>
    <http:response status="200">
      <http:header name="Access-Control-Allow-Origin" value="*"/>
    </http:response>
  </rest:response>,
  doc("/db/sade-projects/textgrid/data/xml/data/" || $uri || ".xml")/*
  )
};

declare
  %rest:GET
  %rest:path("/api/doc")
  %rest:produces("application/xml")
  %output:method("xml")
function misc:doc(){
  (<rest:response>
    <http:response status="200">
      <http:header name="Access-Control-Allow-Origin" value="*"/>
    </http:response>
  </rest:response>,
  <rest:resources>{
  for $uri at $pos in (collection("/db/apps/SADE")/base-uri())[ends-with(., ".xqm")]
    let $content := $uri => util:binary-doc() => util:base64-decode()
    let $isRest := if(contains($content, "%rest:")) then true() else false()
    where $isRest
    return
        exrest:register-module(xs:anyURI($uri))
  }</rest:resources>)
};

declare
  %rest:GET
  %rest:path("/api/res/{$notebook}/{$page}")
function misc:resolver($notebook as xs:string, $page as xs:string?){
  let $id := (collection("/db/sade-projects/textgrid/data/xml/meta")//tgmd:provided/tgmd:title[lower-case(.) = "notizbuch " || lower-case($notebook)]/base-uri()
            => tokenize("/"))[last()]
  let $url := if(config:get("sade.develop") = "true")
                then "http://fontane-nb.dariah.eu/test/edition.html?id=/xml/data/" || $id || "&amp;page=" || $page
                else "http://fontane-nb.dariah.eu/edition.html?id=&amp;page="
  return
  (<rest:response>
    <http:response status="301">
      <http:header name="Access-Control-Allow-Origin" value="*"/>
      <http:header name="Location" value="{$url}"/>
    </http:response>
  </rest:response>,
  <rest:resources/>)
};
