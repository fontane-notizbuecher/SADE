xquery version "3.1";

(:~
 : This modules serves for setting a tei:ptr right before each element that
 : is referenced by an editorial commentary (tei:note[@type = 'editorial']).
 :
 : @author Michelle Weidling
 : @version 1.1
 :)

module namespace prepCom="http://fontane-nb.dariah.eu/prepCom";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";


declare variable $prepCom:literature :=
    map:merge(for $entry in doc("/db/sade-projects/textgrid/data/xml/data/25547.xml")//tei:bibl
    return
        map:entry(string($entry/@xml:id), string($entry/tei:choice/tei:abbr[1])));


(:~
 : The main function.
 :
 : @author Michelle Weidling
 : @param $tei The current notebook in-memory
 : @param $id The notebook's ID, e.g. "16b00"
 : @return The current notebook with prepared editorial commentaries
 :)
declare function prepCom:main($tei as node()*, $id as xs:string)
as element(tei:TEI){
    let $add-ptr := prepCom:recursion($tei)
    let $prepared := prepCom:find-literature($add-ptr)
    let $store := xmldb:store($config:data-root || "/print/xml/", $id || "-prepcom.xml", $prepared)

    return
        $prepared
};


(:~
 : This function handles the tei:ptr as well as the label creation for an
 : editorial commentary. All other elements are simply copied and passed.
 :
 : @author Michelle Weidling
 : @param $node The current node we're looking at
 : @return The processed node
 :)
declare function prepCom:recursion($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            $node

        case comment() return
            if(matches($node/string(), "rotate")) then
                $node
            else
                ()

        (: in case of figures the text in the tei:figDesc serves as editorial
        commentary - there's no tei:note referring to it. thus we set a
        different tei:ptr which helps us to distinguish this case from the
        regular one. :)
        case element(tei:figDesc) return
            (if(contains($node, "Absatzlinie")
            or contains($node, "Abgrenzungslinie")
            or contains($node, "Schlusslinie")) then
                ()
            else
                element tei:ptr {
                    attribute type {"editorial-commentary"},
                    attribute subtype {"figure"}
                },
            element {QName("http://www.tei-c.org/ns/1.0", name($node))} {
                $node/@*,
                prepCom:recursion($node/node())
            })

        default return
            if($node/@xml:id) then
                (: some notes refer to several elements because the lemma is split
                into several parts. in this case we only set the ptr when we have
                the first lemma part :)
                (if(prepCom:check-if-first-target-of-commentary($node)) then
                    (element tei:ptr {
                        attribute reference {$node/@xml:id},
                        attribute type {"editorial-commentary"}
                    },
                    prepCom:make-complete-label($node))
                else
                    (),
                element {QName("http://www.tei-c.org/ns/1.0", name($node))} {
                    $node/@*,
                    prepCom:recursion($node/node())
                })
            else
                element {QName("http://www.tei-c.org/ns/1.0", name($node))} {
                    $node/@*,
                    prepCom:recursion($node/node())
                }
};


(:~
 : In Fontane we have lots of elements that have an @xml:id. This function checks
 : if this ID is the first one referred to by an editorial note. We do not
 : consider other IDs in order to avoid entry duplications.
 :
 : @author Michelle Weidling
 : @param $node the current element
 : @return true() if the current element's ID is targeted by an editorial note
 :)
declare function prepCom:check-if-first-target-of-commentary($node as element(*))
as xs:boolean {
    let $note := prepCom:get-editorial-note($node)
    let $corresp-ids := tokenize($note/@target, " ")
    return
        if(matches($corresp-ids[1], $node/@xml:id)) then
            true()
        else
            false()
};


declare function prepCom:get-editorial-note($node as element(*))
as element(tei:note)? {
    $node/root()//tei:note[@type = "editorial"][@target = "#" || $node/@xml:id]
};


(:~
 : Oftentimes the label of an editorial note consists of more than one element
 : which are encoded in the @target range of a tei:note. This function assembles
 : all relevant labels and summarizes them in one tei:seg[@type = 'editorial-label'].
 :
 : @author Michelle Weidling
 : @param $node The node which is the first one reference by a tei:note[@type = 'editorial'']/@target
 : @return a tei:seg[@type = 'editorial-label'] with the complete label
 :
 :   :)
declare function prepCom:make-complete-label($node as element(*))
as element(tei:seg) {
    let $note := prepCom:get-editorial-note($node)
    let $corresp-ids := tokenize($note/@target, " ")

    return
        element tei:seg {
            attribute type {"editorial-label"},
            let $no-of-elements := count($corresp-ids)
            let $label-elements :=
                for $id in $corresp-ids
                    let $id := substring-after($id, "#")
                    return $node/root()//*[@xml:id = $id]
            return
                if($no-of-elements = 1) then
                    prepCom:create-label-text($label-elements)
                else if($no-of-elements = 2) then
                    (prepCom:create-label-text($label-elements[1]),
                    element tei:seg {
                        attribute type {"label-separator"}
                    },
                    prepCom:create-label-text($label-elements[2]))
                else if($no-of-elements gt 2) then
                    "MEHR ALS ZWEI TARGETS"
                else
                    "KEIN TARGET GEFUNDEN"
        }

};


(:~
 : Nodes that are referenced by tei:note[@type = 'editorial']/@target can have
 : lots of child nodes of which only some are text(). Since we only want the
 : text itself for the label in the commentary, these text() are summarized and
 : regularized.
 :
 : @author Michelle Weidling
 : @param $node A node referred to by tei:note[@type = 'editorial']/@target
 : @return The complete label text
 :)
declare function prepCom:create-label-text($node as node()) as xs:string {
    string-join($node/descendant::text()[not(parent::tei:expan)], "")
    => replace("- |⸗ ", "")
};


(:~
 : Finds and resolves references to (modern) scholarly literature mentioned in
 : the editorial commentaries.
 :
 : @author Michelle Weidling
 : @param The current node of a notebook
 : @return The copy of a node if it's not a tei:ptr in an editorial node, else the resolved reference
 :
 :)
declare function prepCom:find-literature($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            $node
        case comment() return
            if(matches($node/string(), "rotate")) then
                $node
            else
                ()

        case element(tei:ptr) return
            if($node[ancestor::tei:note[@type = "editorial"]]) then
                let $corresp-id := substring-after($node/@target, ":")
                return
                    text {
                        map:get($prepCom:literature, $corresp-id),
                        ", "
                    }
            else
                element {QName("http://www.tei-c.org/ns/1.0", name($node))} {
                    $node/@*,
                    prepCom:find-literature($node/node())
                }

        default return
            element {QName("http://www.tei-c.org/ns/1.0", name($node))} {
                $node/@*,
                prepCom:find-literature($node/node())
            }
};
