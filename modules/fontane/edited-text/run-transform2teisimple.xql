xquery version "3.1";

(:~
 : This modules runs the whole process that transforms the Fontane
 : TEI subset into a "simpler" TEI encoding.
 :
 : This is used for development and debugging purposes only.
 :
 : @author Michelle Weidling
 : @version 1.0
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace etTransfo="http://fontane-nb.dariah.eu/etTransfo" at "etTransfo.xqm";

declare option exist:serialize "method=xml media-type=text/xml expand-xincludes=no";

etTransfo:complete()
