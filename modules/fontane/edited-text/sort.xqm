xquery version "3.1";

(:~
 : This module is a refactored version of the old presort module which lead to
 : unidentifiable errors while transforming the original encoding to another TEI.
 :
 : It mainly deals with bringing everything into the right order, i.e. in a fully
 : legible flow of text that has the right chronological flow.
 :
 :)

module namespace fsort="http://fontane-nb.dariah.eu/sort";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";
import module namespace functx="http://www.functx.com";

(:~
 : The main function. Sorts each notebook and saves a sorted version at
 : /db/sade-projects/textgrid/data/xml/print/xml/notebook_uri-sorted.xml.
 :
 : @author Michelle Weidling
 :)
 declare function fsort:main($tei as node()*, $log as xs:string) as element(tei:TEI) {
     let $prepared := fsort:prepare($tei)
     let $prepared := fsort:enhance-handshifts($prepared, $log)
     let $prepared := fsort:sort-certain-anchors($prepared, $log)
     let $fully-sorted := fsort:sort($prepared, $log)
     let $integrations := fsort:sort-integrations($fully-sorted, $log)
     let $transpositions := fsort:sort-transpositions($integrations, $log)
     let $multiphrases := fsort:sort-multiphrases($transpositions, $log)
     let $marked := fsort:mark-referenced-elements($multiphrases, $log)
     let $id := $tei/@id

     let $store := xmldb:store($config:data-root || "/print/xml/", $id || "-sorted.xml", $marked)
     return $marked
 };


(:~
 : The main sorting routine. Everything apart from tei:hi and tei:rs follows the
 : same rules that are specified in fsort:default-return().
 :
 :)
declare function fsort:sort($nodes as node()*, $log as xs:string) {
    for $node in $nodes return
        typeswitch ( $node )
        case text() return
            $node

        case comment() return
            if(matches($node, "transform")) then
                $node
            else
                ()

        case element(tei:zone) return
            fsort:default-return($node, $log)

        case element(tei:line) return
            fsort:default-return($node, $log)

        case element(tei:seg) return
            fsort:default-return($node, $log)

        case element(tei:rs) return
            fsort:default-return($node, $log)

        case element(tei:abbr) return
            fsort:default-return($node, $log)

        case element(tei:anchor) return
            fsort:default-return($node, $log)

        default return
            fsort:copy-node($node, "sort", $log)
};

(:~
 : Handles what happens with a node. We distinguish between nodes that are at the
 : start of a virtual aggregation, in its middle, its end, and interlinear
 : additions.
 :
 : @author Michelle Weidling
 : @param $node the current TEI element
 : @return a copy of the current element, a fully sorted virtual aggregation or an empty sequence
 :)
declare function fsort:default-return($node as node(), $log as xs:string) as node()* {
    (: nodes with @next and @prev are in the middle of a virtual aggregation and
    handled in fsort:apply-all-nexts, thus we only look at the first element of
    the aggregation.
    furthermore, when nodes that indicate an underlined section of text (@style = "text-decoration: underline"
    or similar), their @prev/@next doesn't indicate the chronology but only the
    continuation of the highlighting. they should thus be omitted.:)
    if(matches($node/@style, "underline")) then
        fsort:copy-node($node, "sort", $log)

    else if($node[@next and not(@prev)]) then
        fsort:apply-all-nexts($node, $log)

    (: since all parts of the virtual aggregation are handled by fsort:apply-all-nexts
    we can ignore the ones that have a @prev.
    this doesn't hold for the ones with a @anchor, since they are the first real
    element of a virtual aggregation that has been started by an anchor when we
    have an addition in a page's margin (3.21.13.3) :)
    else if($node[@prev]
    and not($node/@anchor = "true")) then
        if($node/descendant::*[last()][self::tei:handShift]) then
            fsort:copy-node($node/descendant::*[last()][self::tei:handShift], "sort", $log)
        else
            ()

    (: we distinguish this case and the one below to improve performance :)
    else if($node[descendant::*[@next or @prev]]) then
        fsort:copy-node($node, "sort", $log)

    (: nodes with this attribute are part of an interlinear addition; they have
    been marked by fsort:prepare. since interlinear additions are handled as a
    whole and put into the right place, their elements can be safely dumped here :)
    else if($node[@type = "interlinear"]) then
        ()

    else
        $node
};


(:
 : This functions establishes the correct chronology of elements which is
 : indicated by a virtual aggregation (@prev/@next). It's called by the first
 : node that has a @next (but not a @prev) and recursively finds the next
 : node in the aggregation.
 :
 : @author Michelle Weidling
 : @param $node the current node with a @next and/or @prev attribute
 : @return a sequence of all nodes belonging to the virtual aggregation in the
 : right order
 :  :)
declare function fsort:apply-all-nexts($node as node(), $log as xs:string) as node()* {
    (: in case an addSpan is part of a virtual aggregation :)
    if($node[self::tei:addSpan]) then
        let $spanTo := substring-after($node/@spanTo, "#")
        let $anchor := $node/following::tei:anchor[@xml:id = $spanTo]
        let $nodes-inbetween := $node/following-sibling::*[. << $anchor]

        return
            for $node-inbetween in $nodes-inbetween return
                fsort:copy-node($node-inbetween, "sort", $log)

    else
        (: first element of a virtual aggregation: entry point :)
        if($node/@next and not($node/@prev)
        or ($node/@next and $node/ancestor::tei:seg[@type = "integration"])) then
            let $next-node := fsort:find-corresp-node($node, "next")
            return
                if(count($next-node) = 1) then
                    (fsort:copy-node($node, "sort", $log),
                    (: check if the parts are in different lines :)
                    if($node/ancestor::tei:line = $next-node/ancestor::tei:line) then
                        ()
                    else
                        element tei:milestone {
                            attribute unit {"line"}
                        },
                    if($next-node/ancestor::tei:seg/@xml:lang) then
                        element tei:milestone {
                            attribute unit {"handshift"},
                            attribute script {"Latn"}
                        }
                    else
                        (),
                    fsort:apply-all-nexts($next-node, $log),
                    if($next-node/ancestor::tei:seg/@xml:lang) then
                        element tei:milestone {
                            attribute unit {"handshift"},
                            attribute script {"Latf"}
                        }
                    else
                        ()
                    )
                else if(not($next-node)) then
                    (
                        fsort:copy-node($node, "sort", $log),
                        fsort:add-log-entry($log, "No next node found for " || $node/@next)
                    )
                else
                    fsort:add-log-entry($log, "Several next nodes found for " || $node/@next)

        (: last of a virtual aggregation: exit point :)
        else if(not($node/@next)) then
            ($node/preceding::*[self::tei:handShift or self::tei:milestone[@unit = "handshift"]][1],
            fsort:copy-node($node, "sort", $log))

        (: element in the middle of a virtual aggregation:)
        else
            let $next-node := fsort:find-corresp-node($node, "next")
            let $prev-handshift := $node/preceding::tei:handShift[1]
            return
                if(count($next-node) = 1) then
                    ($prev-handshift,
                    fsort:copy-node($node, "sort", $log),
                    (: check if the parts are in different lines :)
                    if($node/ancestor::tei:line = $next-node/ancestor::tei:line) then
                        ()
                    else
                        element tei:milestone {
                            attribute unit {"line"}
                        },
                    fsort:apply-all-nexts($next-node, $log))
                else if(not($next-node)) then
                    fsort:add-log-entry($log, "No next node found for " || $node/@next)
                else
                    fsort:add-log-entry($log, "Several next nodes found for " || $node/@next)
};


(:~
 : In cases where a node has a descendant one with a @prev or @next attribute
 : the current node has to be kept, but its descendants have to be sorted.
 :
 : In cases where a node DOESN'T have a descendant with said attributes, the
 : whole node is kept in fsort:default-return.
 :
 : This procedure should improve perfomance when dealing with all notebooks
 : (which takes some time).
 :
 : @author Michelle Weidling
 : @param $node the current node
 : @return a copy of the current node with sorted descendants
 :)
 declare function fsort:copy-node($node as node(), $flag as xs:string, $log as xs:string) as node() {
    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
        $node/@*,
        if($flag = "sort") then
            fsort:sort($node/node(), $log)
        else if($flag = "handshift") then
            fsort:enhance-handshifts($node/node(), $log)
        else if($flag = "sort-integrations") then
            fsort:sort-integrations($node/node(), $log)
        else if($flag = "sort-transpositions") then
            fsort:sort-transpositions($node/node(), $log)
        else if($flag = "sort-multiphrases") then
            fsort:sort-multiphrases($node/node(), $log)
        else if($flag = "sort-certain-anchors") then
            fsort:sort-certain-anchors($node/node(), $log)
        else
            error(QName("FONTANE", "fsort2"), "Invalid flag: " || $flag || "." )
     }
 };



declare function fsort:find-corresp-node($node as node(), $flag as xs:string) {
    let $target-id :=
        if($flag = "next") then
            fsort:get-id($node/@next)
        else if($flag = "prev") then
            fsort:get-id($node/@prev)
        else
            error(QName("FONTANE", "fsort1"), "Invalid flag: " || $flag || "." )
    return
        $node/ancestor::*[last()]//*[@xml:id = $target-id]
};


declare function fsort:get-id($target as xs:string) as xs:string {
    $target => replace("#", "")
};



declare function fsort:add-log-entry($log-file as xs:string,
$message as xs:string) as empty-sequence() {
  let $entry := <LogEntry timestamp="{util:system-time()}">{$message}</LogEntry>
  return update insert $entry into doc($log-file)/*
};



(:~
 : Adds the information back to tei:handShifts that have been omitted in order to
 : avoid redundancy.
 :
 :)
declare function fsort:enhance-handshifts($nodes as node()*, $log as xs:string)
as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            if(matches($node, "transform")) then
                $node
            else
                ()

        case element(tei:handShift) return
            element tei:handShift {
                if($node/@new) then
                    $node/@new
                else
                    attribute new {$node/preceding::tei:handShift[@new][1]/@new},

                if($node/@medium) then
                    $node/@medium
                else
                    attribute medium {$node/preceding::tei:handShift[@medium][1]/@medium},

                if($node/@script
                and (matches($node/@script, "Lat") or matches($node/@script, "Druck"))) then
                    $node/@script
                else if($node/@script) then
                    let $prev-hs := $node/preceding::tei:handShift[1]/@script
                    return
                        attribute script {string-join(($node/@script, $prev-hs), " ")}
                else
                    attribute script {$node/preceding::tei:handShift[@script][1]/@script}
            }

        default return
            fsort:copy-node($node, "handshift", $log)
};


(:~
 : Marks all nodes that are part of an interlinear addition.
 :
 : @author Michelle Weidling
 : @param $node The current node
 : @return The processed node
 :)
declare function fsort:prepare($nodes as node()*) as node()* {
    for $node in $nodes
      return
        typeswitch ($node)
        case text() return
            $node

        case comment() return
            if(matches($node/string(), "rotate")) then
                $node
            else
                ()

        default return
            if($node/preceding-sibling::*[self::tei:addSpan][1][@place = 'interlinear'][@prev or @next]
            and not($node[self::tei:anchor])) then
                fsort:mark-interlinear-node-to-be-moved($node)
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    fsort:prepare($node/node())
                }

};


(:~
 : Interlinear additions can be part of a virtual aggregation. Since their
 : start is marked by tei:addSpan and their end by tei:anchor, all nodes inbetween
 : have to be moved when the right chronology is established, too. To make this
 : easier, the respective nodes are marked with an attribute.
 :
 : All other nodes are simply copied.
 :
 : @author Michelle Weidling
 : @param $node The current node
 : @return The processed node
 :)
declare function fsort:mark-interlinear-node-to-be-moved($node as element())
as element() {
    let $addSpan := $node/preceding-sibling::*[self::tei:addSpan][1][@place = 'interlinear'][@prev or @next]
    let $spanTo := substring-after($addSpan/@spanTo, "#")
    let $anchor := $addSpan/following::tei:anchor[@xml:id = $spanTo]
    let $nodes-inbetween := $addSpan/following-sibling::*[. << $anchor]

    return
        element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
            $node/@*,
            if(functx:is-node-in-sequence-deep-equal($node, $nodes-inbetween)) then
                attribute type {"interlinear"}
            else
                (),
            $node/node()
        }
};


(:~
 : Creates a new encoding structure for tei:metamark[@function = "integrate"]
 : that makes further processing of them easier. All nodes relevant for the
 : integration brackets are summarized in a tei:seg[@type = 'integration'].
 :
 : @author Michelle Weidling
 : @param $node The current node
 : @return The processed node
 :)
declare function fsort:sort-integrations($nodes as node()*, $log as xs:string) as node()* {
    for $node in $nodes return
    if($node/@xml:id) then
        let $id := $node/@xml:id
        let $metamarks := $node/root()//tei:metamark[@function = ("integrate", "authorial_note")]
        let $linking-node-corresp := $metamarks[substring-after(@corresp, "#") = $id]
        let $linking-node-target := $metamarks[substring-after(@target, "#") = $id]

        return
            if($linking-node-corresp and not($linking-node-target)) then
                let $integration-target-id := replace($linking-node-corresp/@target, "#", "")
                let $integration-comment := $linking-node-corresp/ancestor::*[last()]//*[@xml:id = $integration-target-id]
                return
                    element tei:seg {
                        attribute type {"integration"},
                        $integration-comment,
                        $linking-node-corresp,
                        fsort:copy-node($node, "sort-integrations", $log)
                    }
            else if($linking-node-target) then
                ()
            else
                fsort:copy-node($node, "sort-integrations", $log)

    else if($node[self::tei:metamark[@function = "integrate"]]) then
        ()
    else if($node[self::text() or self::comment()]) then
        $node
    else
        fsort:copy-node($node, "sort-integrations", $log)
};


(:~ In some cases we have text that is transposed, i.e. two or more areas of
 : text that are switched. The correct order of the texts is denoted in a tei:listTranspose.
 : This function serves restore the intended order by setting the sequence of texts
 : of tei:listTranspose when the first transposed element is met.
 :
 : @author Michelle Weidling
 : @param $nodes the nodes to be processed
 : @param path to log file
 : @return the processed nodes. transpositions are put in the right order, the rest is kept as is :)
declare function fsort:sort-transpositions($nodes as node()*, $log as xs:string) as node()* {
    for $node in $nodes return

    if(fsort:is-transposed($node)
    and fsort:is-first-transposed($node)) then
        let $transposed-order-ids :=
            for $ptr in $node/root()//tei:transpose/tei:ptr return
                replace($ptr/@target, "#", "") => tokenize(" ")


        return
            element tei:seg {
                attribute type {"transposed"},
                for $id in $transposed-order-ids return
                    let $corresp-node := $node/root()//*[@xml:id = $id]
                    return fsort:copy-node($corresp-node, "sort-transpositions", $log)
            }

    else if(fsort:is-transposed($node)
        or $node[self::tei:listTranspose]) then
        ()

    else if($node[self::text() or self::comment()]) then
        $node

    else
        fsort:copy-node($node, "sort-transpositions", $log)
};


(:~ Checks if a node with an xml:id is transposed or not.
 :
 : @author Michelle Weidling
 : @param $node A node with an xml:id
 : @return true() when the node's xml:id is mentioned in tei:listTranspose, false() otherwise
 :)
declare function fsort:is-transposed($node as node()*) as xs:boolean {
    let $id := $node/@xml:id
    let $transpositions := $node/root()//tei:transpose/tei:ptr
    let $transpo-ids := for $ptr in $transpositions return
            replace($ptr/@target, "#", "") => tokenize(" ")

    return $id = $transpo-ids
};


(:~ Checks if a node with an xml:id is the first transposed element in a
 : tei:listTranspose.
 :
 : @author Michelle Weidling
 : @param $node A node with an xml:id
 : @return true() when the node's xml:id is the first transposed element, false() otherwise
 :)
declare function fsort:is-first-transposed($node as node()*) as xs:boolean {
    let $first-transposed-id := replace($node/root()//tei:transpose/tei:ptr[1]/@target, "#", "") => tokenize(" ")
    return $node/@xml:id = $first-transposed-id[1]
};


(:~ In some cases we have text two or more longer variations of a text passage,
 : e.g. in a draft for a piece of literature. These passages should be marked as
 : different layers of text and have to be put into the right order.
 :
 : @author Michelle Weidling
 : @param $nodes the nodes to be processed
 : @param path to log file
 : @return the processed nodes. transpositions are put in the right order, the rest is kept as is :)
declare function fsort:sort-multiphrases($nodes as node()*, $log as xs:string) as node()* {
    for $node in $nodes return
    if($node[self::tei:addSpan[@type = "multiphrase" and @subtype = "extensive"]]) then
        let $corresp-id := replace($node/@spanTo, "#", "")
        let $corresp-node := $node/root()//*[@xml:id = $corresp-id]

        return
            element tei:div {
                attribute type {"multiphrase"},
                for $inbetween in $node/following-sibling::*[. << $corresp-node] return
                    fsort:copy-node($inbetween, "sort-multiphrases", $log),
                    $corresp-node
            }

    else if($node[self::text() or self::comment()]) then
        $node

    else
        let $addSpan := $node/preceding-sibling::tei:addSpan[@type = "multiphrase" and @subtype = "extensive"]
        let $anchor := $addSpan/root()//*[@xml:id = replace($addSpan/@spanTo, "#", "")]

        return
            if($addSpan and $node/following::*[$anchor]) then
                ()
            else
                fsort:copy-node($node, "sort-multiphrases", $log)
};

declare function fsort:sort-certain-anchors($nodes as node()*, $log as xs:string) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case element(tei:anchor) return
            if($node/@next) then
                let $corresp-node := fsort:find-corresp-node($node, "next")
                return if($corresp-node/preceding-sibling::*[1][self::tei:addSpan[@place = "margin"]]) then
                    fsort:apply-only-next-node($corresp-node, $log)
                else
                    $node

            else
                $node

        default return
            if($node[self::text() or self::comment()]) then
                $node

            else
                fsort:copy-node($node, "sort-certain-anchors", $log)
};

declare function fsort:apply-only-next-node($node as node()*, $log as xs:string) as node()* {
    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
        $node/(@* except @prev),
        attribute anchor {"true"},
        fsort:sort-certain-anchors($node/node(), $log)
    }
};


(:~ adds an extra flag for the elements that are referenced in the editorial
 : TOC. :)
declare function fsort:mark-referenced-elements($nodes as node()*, $log as xs:string)
as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            $node

        default return
            element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                $node/@*,
                let $refs := $node/root()//tei:list[@type = "editorial"]//tei:ref[@type]
                let $referenced-ids :=
                    for $ref in $refs return
                        let $tokens := tokenize($ref/@target, " ")
                        let $tokens := $tokens[2]
                        for $token in $tokens return
                            substring-after($token, "@xml:id='")
                            => substring-before("']")
                return
                    if($node/@xml:id = $referenced-ids) then
                        attribute subtype {"referenced"}
                    else
                        (),
                fsort:mark-referenced-elements($node/node(), $log)
            }
};
