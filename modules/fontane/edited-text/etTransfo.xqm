xquery version "3.1";

(:~
 : This module is responsible for pulling the whole transformation/serialization
 : of the edited text together.
 :
 : It creates all the single HTML files for each notebook as well as a complete
 : intermediate format TEI/XML that is used for the print output.
 :
 : This module is used in production and is called by tgconnect.
 :
 : @author Michelle Weidling
 : @version 1.0
 :)

module namespace etTransfo="http://fontane-nb.dariah.eu/etTransfo";

declare namespace err="http://www.w3.org/2005/xqt-errors";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xi="http://www.w3.org/2001/XInclude";

import module namespace abbrev-index="http://fontane-nb.dariah.eu/abbrev-index" at "abbrev-index.xqm";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";
import module namespace fontaneSimple="http://fontane-nb.dariah.eu/teisimple" at "tei2teisimple.xqm";
import module namespace fsort="http://fontane-nb.dariah.eu/sort" at "sort.xqm";
import module namespace functx = "http://www.functx.com";
import module namespace prepCom="http://fontane-nb.dariah.eu/prepCom" at "prepcom.xqm";
import module namespace simple2xhtml="http://fontane-nb.dariah.eu/simple2xhtml" at "simple2xhtml.xqm";
import module namespace tidy ="http://fontane-nb.dariah.eu/tidy" at "tidy.xqm";

declare variable $etTransfo:cases :=
    (
    "3qtcz.xml", (: case C :)
    "3qtqv.xml", (: case A :)
    "3qtqw.xml", (: case B :)
    "3qtqx.xml", (: case D :)
    "3qtqz.xml" (: case E :)
    );

declare variable $etTransfo:coll := "/db/apps/SADE/modules/fontane/edited-text/";


(:~
 : The main function. It is called by tgconnect.
 :
 : @author Michelle Weidling
 : @return An XHTML page for each notebook in /db/sade-projects/textgrid/data/print/xhtml
 : @return An intermediate TEI/XML with all notebooks in it in /db/sade-projects/textgrid/data/print/xml/fontane-full.xml
 :)
declare function etTransfo:complete() {
    etTransfo:assure-coll-available("print"),
    etTransfo:assure-coll-available("log"),
    for $case in $etTransfo:cases return
        (etTransfo:create-case($case),
        etTransfo:create-htmls($case)),

    try {
        etTransfo:create-print-tei()
    } catch * {
        error(QName("error", "ETTRANSFO02"), "An error occurred while creating the whole TEI base for print.")
    },
    etTransfo:tidy-logs(),
    try {
        etTransfo:report-errors()
    } catch * {
        error(QName("error", "ETTRANSFO15"), "An error occurred while reporting errors.")
    }
};


(:~
 : A function to trigger the creation of the edited text for a single notebook.
 : Mainly used for parallel creation of the notebooks but can also be used for
 : debugging.
 :
 : @param $uri The notebook's URI, e.g. "16b00"
 :)
declare function etTransfo:transform-single-nb($uri as xs:string) as xs:string* {
    let $assure-dir-available := etTransfo:assure-coll-available("print")
    let $assure-dir-available := etTransfo:assure-coll-available("log")

    let $log := util:log-system-out("Start creating edited text for " || $uri)
    let $log := etTransfo:create-log($uri)

    let $tei := doc($config:data-root || "/data/" || $uri || ".xml")//tei:TEI
    let $updated-notebook :=
        element tei:TEI {
            attribute id {$uri},
            $tei/@*,
            $tei/*
        }

    return
        (
          try {
              etTransfo:transform-tei($updated-notebook, $log)
          } catch * {
              etTransfo:add-log-entry($log, "ETTRANSFO08: Couldn't transform TEI.")
          },
          etTransfo:tidy-logs()
        )
};


(:~
 : Resolves all XIncludes in a given case file and copies all notebooks belonging
 : to a case into one XML file.
 :
 : This is mainly used to be able to serialize one case at a time which is
 : a project requirement.
 :
 : @author Michelle Weidling
 : @param $showcase The filename of a showcase, e.g. "12345.xml"
 : @return The full path to the created showcase with all notebooks in it.
 :)
declare function etTransfo:create-case($showcase as xs:string) as xs:string {
    let $doc :=
        try {
            doc("/db/sade-projects/textgrid/data/xml/data/" || $showcase)
        } catch * {
            error(QName("error", "ETTRANSFO03"), "An error occured while opening " || $showcase || ".")
        }

    let $log := etTransfo:create-log($doc//tei:title[1] => replace(" ", "-"))

    let $log := util:log-system-out("Start creating case " || $showcase || " (" || $doc//tei:title[1] || ").")
    let $new-filename := $doc//tei:title[1] => replace(" ", "-") || ".xml"
    let $assure-dir-available := etTransfo:assure-coll-available("print")

    let $create-new-file := xmldb:store($config:data-root || "/print/xml/", $new-filename, $doc/*)

    let $xis := doc($create-new-file)//xi:include
    let $update :=
        for $xi in $xis
            let $uri := $xi/@href/string()
            let $tei-referred-to :=
            try {
                doc($config:data-root || "/data/" || $uri || ".xml")/tei:TEI
            } catch * {
                etTransfo:add-log-entry($log, "ETTRANSFO04: Couldn't find the node tei:TEI of  " || $uri || ".")
            }
            let $updated-notebook :=
                element tei:TEI {
                  attribute id {$uri},
                  $tei-referred-to/@*,
                  $tei-referred-to/*
                }

            return update replace $xi with $updated-notebook
    return
        $create-new-file
};


(:~
 : Returns all notebooks of a showcase
 :
 : @author Michelle Weidling
 : @param $showcase The filename of a showcase, e.g. "12345.xml"
 : @return All notebooks of a showcase in-memory, i.e. a sequence of tei:TEI
 :)
declare function etTransfo:get-teis($showcase as xs:string) as element(tei:TEI)* {
    let $doc := doc($config:data-root || "/data/" || $showcase)
    let $summary-file-name := $doc//tei:title[1] => replace(" ", "-") || ".xml"
    let $log := doc($etTransfo:coll || "logs/" || $doc//tei:title[1] => replace(" ", "-") || "-log.xml")
    return
        try {
            doc($config:data-root || "/print/xml/" || $summary-file-name)//tei:TEI
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO05: Summary file for case " || $showcase || "couldn't be opened.")
        }
};


(:~
 : Creates the intermediate format of a given case and transforms each notebook of a showcase to a XHTML page.
 :
 : @author Michelle Weidling
 : @param $showcase The filename of a showcase, e.g. "12345.xml"
 : @return One or more strings indicating the location where the XHTML(s) have been stored to.
 :)
declare function etTransfo:create-htmls($showcase as xs:string) as xs:string* {
    let $teis := etTransfo:get-teis($showcase)

    for $tei in $teis return
        let $log := util:log-system-out("Start creating edited text for " || $tei//tei:idno[@type = "TextGrid"])
        let $log := etTransfo:create-log(substring-after($tei//tei:idno[@type = "TextGrid"], "textgrid:"))
        return
            try {
                etTransfo:transform-tei($tei, $log)
            } catch * {
                etTransfo:add-log-entry($log, "ETTRANSFO08: Couldn't transform TEI. ")
            }
};


(:~
 : Handles the transformation to the intermediate format and the XHTML
 : representation.
 :
 : The whole transformation is a five step process in order to keep different
 : steps apart semantically and logically.
 :
 : @param $tei The current notebook as element(tei:TEI)
 : @param $log The absolute path of the log file belonging to the current notebook
 :)
declare function etTransfo:transform-tei($tei as element(tei:TEI), $log as xs:string) {
    let $sorted :=
        try {
            fsort:main($tei, $log)
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO09: Error while sorting this notebook. Reason: &#13;" ||
            concat("[", $err:line-number, ": ", $err:column-number, "] Error ", $err:code, ": ", $err:description))
        }

    let $prepare-comment :=
        try {
            prepCom:main($sorted, $tei/@id)
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO10: Error while preparing commentary for this notebook. Reason: &#13;" ||
            concat("[", $err:line-number, ": ", $err:column-number, "] Error ", $err:code, ": ", $err:description))
        }

    let $transform-to-interform :=
        try {
            fontaneSimple:main($prepare-comment, $tei/@id, $log)
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO11: Error while transforming this notebook to an intermediate format.                Reason: &#13;" ||
            concat("[", $err:line-number, ": ", $err:column-number, "] Error ", $err:code, ": ", $err:description))
        }

    let $tidy-interform :=
        try {
            tidy:main($transform-to-interform, $tei/@id)
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO12: Error while tidying up the intermediate format for this notebook.                Reason: &#13;" ||
            concat("[", $err:line-number, ": ", $err:column-number, "] Error ", $err:code, ": ", $err:description))
        }

    return
        try {
            simple2xhtml:main($tidy-interform, $tei/@id)
        } catch * {
            etTransfo:add-log-entry($log, "ETTRANSFO13: Error while creating XHTML for this notebook. Reason: &#13;" ||
            concat("[", $err:line-number, ": ", $err:column-number, "] Error ", $err:code, ": ", $err:description))
        }
};


(:~
 : Assembles the intermediate format version of each notebook and copies it into one file.
 : Furthermore, additional data needed for the print is appended.
 :
 : The resulting file is mainly needed for serializing the print.
 : Attention: In case you change the file name, update the PERL script in the print repo as well.
 :
 : @author Michelle Weidling
 : @return A string indicating the location where the full TEI/XML of the edition was stored to, i.e. "/db/sade-projects/textgrid/data/xml/print/xml/fontane-full.xml"
 :)
declare function etTransfo:create-print-tei() as xs:string {
    let $xmls := etTransfo:get-all-xmls()
    let $log := etTransfo:create-log("general-print-creation")
    let $complete-print-file :=
        element {QName("http://www.tei-c.org/ns/1.0", "teiCorpus")} {
            $xmls,
            try {
                etTransfo:get-literature()
            } catch * {
                etTransfo:add-log-entry($log, "ETTRANSFO06: An error occurred while creating the bibliography.")
            },

            try {
                abbrev-index:main()
            } catch * {
                etTransfo:add-log-entry($log, "ETTRANSFO07: An error occurred while creating the index of abbreviations.")
            },

            try {
                etTransfo:create-overall-toc()
            } catch * {
                etTransfo:add-log-entry($log, "ETTRANSFO10: An error occurred while creating the overall TOC..")
            }
        }
    return
        xmldb:store($config:data-root || "/print/xml/", "fontane-full.xml", $complete-print-file)
};


(:~
 : An auxiliary function. Returns all final intermediate format TEI/XML files.
 :
 : @author Michelle Weidling
 : @return All available final intermediate format TEI/XML files as document nodes
 :)
declare function etTransfo:get-all-xmls() as node()+ {
    for $res in collection($config:data-root || "/print/xml/")
        [contains(base-uri(.), ".xml")
        and not(contains(substring-after(base-uri(.), "/print/xml/"), "-"))]
    order by $res/tei:TEI/@key1, number($res/tei:TEI/@key2)
    return
       $res
};


(:~
 : Creates a summary tei:div with all information needed for the print
 : bibliography.
 :
 : @author Michelle Weidling
 : @return A tei:div containing all bibliographic sections of the print
 :)
declare function etTransfo:get-literature() as element(tei:div) {
    element {QName("http://www.tei-c.org/ns/1.0", "text")} {
        attribute type {"bibliography"},

        element tei:div {
            attribute n {"Fontanes_Werke"},
            local:make-bib-div("Erstausgaben"),
            local:make-bib-div("Werkausgaben"),
            local:make-bib-div("Briefausgaben"),
            local:make-bib-div("Postume_Einzelausgaben"),
            local:make-bib-div("Populaere_postume_Einzelausgaben")
        },
        local:make-bib-div("Fontanes_Quellen"),
        local:make-bib-div("Allgemeine_Nachschlagewerke"),
        local:make-bib-div("Nachschlagewerke_der_Fontane-Forschung"),
        local:make-bib-div("Forschungsliteratur")
    }
};


(:~
 : An auxiliary function to create the tei:div elements/sections in the bibliography.
 :
 : @author Michelle Weidling
 : @param $identifier An identifier for a subtype of scholarly literature, e.g. "Briefausgaben"
 : @return A tei:div containing all bibliographic information of the literature's subtype
 :)
declare function local:make-bib-div($identifier as xs:string) as element(tei:div) {
    element {QName("http://www.tei-c.org/ns/1.0", "div")} {
        attribute n {$identifier},
        etTransfo:make-bib-entries($identifier)
    }
};



(:~
 : Creates a tei:div containing all the scholarly literature ("Forschungsliteratur")
 : of a kind that's listed in the respective index. Examples for this are
 : "Allgemeine Nachschlagewerke" and "Werkausgaben"
 :
 : @author Michelle Weidling
 : @param $type The type of a tei:listBibl, e.g. "Forschungsliteratur"
 : @return A tei:div containing the scribal abbreviation as well as the full
 : bibliographic reference
 :)
declare function etTransfo:make-bib-entries($type as xs:string) as element(tei:div)* {
    (: 25547.xml is the file that contains all bibliographical information :)
    for $entry in doc($config:data-root || "/data/25547.xml")//tei:listBibl[@type = $type]//tei:bibl
        let $abbr :=
            if($entry/tei:abbr) then
                $entry/tei:abbr
            else
                $entry/tei:choice/tei:abbr
        let $full-ref := $entry/tei:choice/tei:expan
        let $bib-no := count($entry/ancestor::tei:bibl)

        return
            element {QName("http://www.tei-c.org/ns/1.0", "div")} {
                attribute type {"entry"},
                element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                    attribute type {"abbr"},
                    $abbr
                },
                if($full-ref) then
                    element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                        attribute type {"full"},
                        $full-ref
                    }

                else if($entry/tei:ptr[@target]) then
                    let $target-id := replace($entry/tei:ptr/@target, "#", "")
                    let $referenced-entry := $entry/ancestor::tei:body//tei:bibl[@xml:id = $target-id]
                    let $same-as := $referenced-entry/tei:choice/tei:abbr/string()
                    let $same-as-loc := $referenced-entry/ancestor::tei:listBibl[1]/@type/string()

                    return
                        (
                          element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                              attribute type {"same-as"},
                              $same-as
                          },
                          element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                              attribute type {"same-as-loc"},
                              $same-as-loc
                          }
                        )

                else
                    (),
                if($bib-no gt 0) then
                    element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                        attribute type {"sub-entry"},
                        $bib-no
                    }
                else
                    ()
            }
};


(:~
 : Creates a log file for a given notebook.
 :
 : @author Michelle Weidling
 : @param $uri The notebook's URI, e.g. "12345.xml"
 : @return The location of the stored log-file, e.g. "/db/apps/sade/modules/fontane/edited-text/logs/12345-log.xml"
 :)
declare function etTransfo:create-log($uri as xs:string) as xs:string {
  let $assure-dir-available := etTransfo:assure-coll-available("logs")
  let $log-name := $uri || "-log.xml"
  return xmldb:store($etTransfo:coll || "logs", $log-name, <log/>)
};


(:~
 : Makes sure a requested collection is available.
 :
 : @author Michelle Weidling
 : @param $flag A string indicating the target collection
 : @return The location of the log collection, "/db/apps/sade/modules/fontane/edited-text/logs/"
 :)
declare function etTransfo:assure-coll-available($flag as xs:string) {
    switch ($flag)
        case "logs" return
            if(xmldb:collection-available($etTransfo:coll || "logs")) then
                $etTransfo:coll || "logs"
            else
                xmldb:create-collection($etTransfo:coll, "logs")

        case "print" return
            if(xmldb:collection-available($config:data-root || "/print/")) then
                ()
            else
                (xmldb:create-collection($config:data-root, "/print"),
                xmldb:create-collection($config:data-root || "/print/", "xml"),
                xmldb:create-collection($config:data-root || "/print/", "xhtml"))
        default return ()
};


(:~
 : Adds a log entry to a given log file.
 :
 : @author Michelle Weidling
 : @param $log-file The path to the log file, e.g. "/db/apps/sade/modules/fontane/edited-text/logs/12345-log.xml"
 : @param $message The message of the log entry
 : @return The location of the log collection, "/db/apps/sade/modules/fontane/edited-text/logs/"
 :)
declare function etTransfo:add-log-entry($log-file as xs:string,
$message as xs:string) as empty-sequence() {
  let $entry := <LogEntry timestamp="{util:system-time()}">{$message}</LogEntry>
  return
    update insert $entry into doc($log-file)/*
};


(:~
 : Removes all log files that don't contain any information.
 :
 : @author Michelle Weidling
 :)
declare function etTransfo:tidy-logs() as item()* {
    for $log in collection($etTransfo:coll || "logs/") return
        if($log//LogEntry) then
            ()
        else
            let $uri := substring-after(base-uri($log), "logs/")
            return
                xmldb:remove($etTransfo:coll || "logs/", $uri)
};

(:~
 : Prints all errors found during the transformation to stdout.
 :
 : @author Michelle Weidling
 :)
declare function etTransfo:report-errors() as item()* {
    let $log-path := $etTransfo:coll || "logs/"
    return
        if(normalize-space(string-join(xmldb:get-child-resources($log-path), "")) = "") then
            util:log-system-out("No errors found while creating the edited text! Yay!")
        else
            (
                util:log-system-out("Errors detected in the following notebooks:"),
                for $log in collection($log-path) return
                    (
                        util:log-system-out("*&#8195;" ||
                        substring-after(base-uri($log), "logs/")
                        => substring-before("-log")),
                        util:log-system-out($log)
                    ),
                util:log-system-out("Total: " || count(xmldb:get-child-resources($log-path)) || " notebook(s).")
            )
};


(:~
 : Creates the overall TOC by collecting information from all "Überblickskommentare".
 : This info is needed for a separate kind of index in the book.
 :
 : This feature is still in development and has to be extended to work with all
 : notebooks, not just E1.
 :
 : @author Michelle Weidling
 :)
declare function etTransfo:create-overall-toc() as element(tei:div) {
    let $nbs := collection($config:data-root || "/data")
    let $items := $nbs//tei:TEI[descendant::tei:title = "Notizbuch E1"]/tei:teiHeader//tei:msContents/tei:ab/tei:list[@type="editorial"]/tei:item
    let $sortPattern := "^\W+"

    return element {QName("http://www.tei-c.org/ns/1.0", "div")} {
        attribute type {"overall-toc"},
        let $segs :=
          for $doc in $nbs
          return
              let $nb-name := $doc//tei:fileDesc/tei:titleStmt/tei:title[1]/substring-after(., " ")
              return
                  for $item in $doc//tei:msContents/tei:ab/tei:list[@type="editorial"]//tei:item return
                      let $first-ref := $item//tei:ref[@type = "first"]/@target/string()
                      let $last-ref := $item//tei:ref[@type = "last"]/@target/string()
                      let $refs :=
                          for $ref in ($first-ref, $last-ref) return
                              let $tokens := tokenize($ref, " ")
                              return $tokens[2]
                      return
                            element {QName("http://www.tei-c.org/ns/1.0", "seg")} {
                              attribute type {$nb-name},
                              attribute source {
                                  let $string := for $ref in $refs return
                                      substring-after($ref, "@xml:id='")
                                      => substring-before("']")
                                  return
                                      string-join($string, " ")
                              },
                              normalize-space(functx:substring-before-last($item, ")") || ")")
                          }
        return for $seg in $segs
            order by replace($seg, $sortPattern, "") => substring(1, 1) => upper-case()
            return $seg
    }
};
