xquery version "3.1";

(:~
 : This module is responsible for retrieving all relevant information from the
 : different indices in order to create a tei:index in the simpler TEI.
 :
 : @author Michelle Weidling
 : @version 0.1
 : @since v4.0.
 :)

module namespace index-info="http://fontane-nb.dariah.eu/index-info";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";

declare namespace map = "http://www.w3.org/2005/xpath-functions/map";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $index-info:event-map :=
    map:merge(for $entry in doc($config:data-root || "/data/253t0.xml")//tei:event
    return
        map:entry(string($entry/@xml:id),
            map {
                "regular-name"  : string-join($entry/tei:label[not(@type)], ", "),
                "category"      : string($entry/preceding::tei:head[1]),
                "occs-in-nbs"   : ""
            }
        ));

declare variable $index-info:org-map :=
    map:merge(for $entry in doc($config:data-root || "/data/253t1.xml")//tei:org
    return
        map:entry(string($entry/@xml:id),
            map {
                "regular-name"  : string-join($entry/tei:orgName[not(@type)], ", "),
                "occs-in-nbs"   : ""
            }
        ));

declare variable $index-info:place-map :=
    map:merge(for $entry in doc($config:data-root || "/data/253t2.xml")//tei:place
    return
        map:entry(string($entry/@xml:id),
            map {
                "regular-name"  : string-join($entry/tei:placeName, ", "),
                "subref-of"     : string-join($entry/parent::tei:place/tei:placeName, ", ")
            }
        ));

declare variable $index-info:psn-map :=
    map:merge(for $entry in doc($config:data-root ||"/data/253sx.xml")//*[self::tei:person or self::tei:personGrp]
        let $same-as := $entry/ancestor::tei:listPerson//tei:person[not(@xml:id)][descendant::tei:ptr[@target = concat("#", $entry/@xml:id)]]/tei:persName

        return
        map:entry(string($entry/@xml:id),
            map {
                "regular-name"          : string-join($entry/tei:persName[not(@type)], ", "),
                "birth"                 : string($entry/tei:birth),
                "death"                 : string($entry/tei:death),
                "occupation"            : string($entry/tei:occupation),
                "relation-to-fontane"   : string($entry/tei:state/tei:desc),
                "same-as"               : string-join($same-as, "+")
            }
        ));

declare variable $index-info:wrk-map :=
    map:merge(
        (for $entry in doc($config:data-root ||"/data/253t3.xml")//tei:item[ancestor::tei:list[@type]]
        return
            let $creators := $entry/tei:linkGrp/tei:link[@corresp = "http://purl.org/dc/terms/creator"]
            let $creator-ids := $creators/@target/string()
            let $created-by-fontane :=
                if($entry/ancestor::tei:list[@type = "Fontane"]) then
                    true()
                else
                    false()
            let $periodical :=
                if($entry/ancestor::tei:list[@type = "periodicals"]) then
                    true()
                else
                    false()
            let $is-periodical :=
                if($periodical) then
                    "true"
                else
                    "false"

            let $same-as :=
                if($periodical) then
                    $entry/ancestor::tei:list[@type = "periodicals"]//tei:item[not(@xml:id)][descendant::tei:ptr[@target = concat("#", $entry/@xml:id)]]/tei:name
                else
                    ()
            let $same-as :=
                if(count($same-as) gt 1) then
                    string-join($same-as/string(), ", ")
                else if(count($same-as) = 1) then
                    $same-as/string()
                else
                    ()

            let $general-map :=
                    map {
                        "regular-name"  : string-join($entry/tei:name[not(@type)], ", "),
                        "periodical"    : $is-periodical,
                        "type"          : "work"
                    }

            return
                if($creators and $periodical
                or $creators) then
                    let $updated-map := map:put($general-map, "creator-ids", string-join($creator-ids, " "))

                    return
                        map:entry(string($entry/@xml:id),
                            if($same-as) then
                                map:put($updated-map, "same-as", $same-as)
                            else
                                $updated-map
                        )

                else if($created-by-fontane) then
                    map:entry(string($entry/@xml:id),
                        map:put($general-map, "creator", "Fontane")
                    )

                else if($periodical) then
                    map:entry(string($entry/@xml:id),
                        if($same-as) then
                            map:put($general-map, "same-as", $same-as)
                        else
                            $general-map
                    )
                else
                    map:entry(string($entry/@xml:id),
                        map:put($general-map, "creator", "Anonym/nicht ermittelt")
                    ),
        (: (fictional) characters mentioned/depicted in works :)
        for $person in doc($config:data-root || "/data/253t3.xml")//tei:person
            return
                map:entry(string($person/@xml:id),
                    map {
                        "regular-name"  : string-join($person/tei:persName[not(@type)], ", "),
                        "part-of-id"    : concat("wrk:", string($person/ancestor::tei:item/@xml:id)),
                        "note"          : string($person/tei:note[not(child::tei:ptr)]),
                        "type"          : "person"
                    }
                ))
    );

declare variable $index-info:lit-map :=
    map:merge(for $entry in doc($config:data-root || "/data/25547.xml")//tei:bibl
    return
        map:entry(string($entry/@xml:id),
            map {
                "abbrev"        : string-join($entry//tei:abbr, ", "),
                "full-bibl"     : string-join($entry//tei:expan)
            }
        ));

(:~
 : Gets information about a given term.
 :
 : @author Michelle Weidling
 : @param $index-type the index type ("psn", "eve", ...)
 : @param $term the current term, e.g. plc:Lueneburg
 : @param $info the specific information to be retrieved, e.g. "regular-name"
 : @return The information asked
 :)
declare function index-info:get-info-about($index-type as xs:string,
$term as xs:string, $info as xs:string) as xs:string* {
    local:get-main-entry($term, $index-type)
    => map:get($info)
};


(:~
 : Selects the right entry on the first level of the map.
 :
 : @author Michelle Weidling
 : @param $ref the current term, e.g. plc:Lueneburg
 : @param $index-type the index type ("psn", "eve", ...)
 : @return a map with all available information about the given term
 :)
declare function local:get-main-entry($ref as xs:string,
$index-type as xs:string) as map()* {
    let $key := substring-after($ref, ":")
    return
        switch ($index-type)
            case "eve" return
                map:get($index-info:event-map, $key)
            case "org" return
                map:get($index-info:org-map, $key)
            case "plc" return
                map:get($index-info:place-map, $key)
            case "psn" return
                map:get($index-info:psn-map, $key)
            case "wrk" return
                map:get($index-info:wrk-map, $key)
            case "lit" return
                map:get($index-info:lit-map, $key)
            default return ()
};
