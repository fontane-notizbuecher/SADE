xquery version "3.1";

(: This library module contains XQSuite tests for the all modules module stored 
 : in tidysimple.xqm :)

module namespace tidysimple-test = "http://fontane-nb.dariah.eu/tidysimple-test";

import module namespace tidySimple ="http://fontane-nb.dariah.eu/tidysimple" at "../tidysimple.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";

(: handshifts :)
declare 
    %test:name("handShift - Sort out invalid ones")
    %test:args("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Stempel2""/>")
    %test:assertEmpty   
    
    %test:args("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf) medium()""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf) medium()""/>")
    
    function tidysimple-test:handShift-invalid($node as element(*)) {
        tidySimple:sort-out-invalid-hands($node)
};

declare 
    %test:name("handShift - Sort out invalid nodes after an invalid hand")
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn clean) medium(black_ink)""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small"">Dieses Buch hat 52 Blatt.</seg></body>")
    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/></body>")  
    
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn clean) medium(black_ink)""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small"">Dieses Buch hat 52 Blatt.</seg><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium(black_ink)""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small"">Text von Fontane</seg><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/></body>")
    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium(black_ink)""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small"">Text von Fontane</seg><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/></body>") 
    
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" xml:id=""a4""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#fremde_Hand3"" rend=""script(Latn clean) medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""align(center)""/>✓ </div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" xml:id=""a4""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""align(center)""/></div>")
    
    function tidysimple-test:handShift-invalid-nodes($node as element(*)) {
        tidySimple:sort-out-invalid-hands($node)
};

declare 
    %test:name("handShift - Sort out surplus ones")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf clean) medium()""/>Some text<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/></div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf clean) medium()""/>Some text<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/></div>")    
    
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/>Some text<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/></div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/>Some text</div>")   
    
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf standard) medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""section"" type=""Text_1"" spanTo=""#C07_3r_s""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/><head xmlns=""http://www.tei-c.org/ns/1.0"" type=""x-large"" rend=""align(center)""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Luther</hi></rs></head></body>")
    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""section"" type=""Text_1"" spanTo=""#C07_3r_s""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/><head xmlns=""http://www.tei-c.org/ns/1.0"" type=""x-large"" rend=""align(center)""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Luther</hi></rs></head></body>")
    
    function tidysimple-test:handShift-surplus($node as element(*)) {
        tidySimple:sort-out-surplus-elements($node)
};


(: Sections :)

declare 
    %test:name("Sections")
(:    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""section"" type=""Text_2"" spanTo=""#C07_4r_s""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/><head xmlns=""http://www.tei-c.org/ns/1.0"" type=""x-large"" rend=""align(center)""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:x-large; letter-spacing:0.2cm""><rs type=""direct"" ref=""psn:Cranach""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Lucas Cranach</hi></rs>.</seg></head><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf clean) medium()""/><anchor xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_4r_s""/></body>"):)
(:    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0"" type=""section""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/><head xmlns=""http://www.tei-c.org/ns/1.0"" type=""x-large"" rend=""align(center)""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:x-large; letter-spacing:0.2cm""><rs type=""direct"" ref=""psn:Cranach""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Lucas Cranach</hi></rs>.</seg></head><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf clean) medium()""/></div></body>")    :)
    
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Amalberga""><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_7r_d"" prev=""#C07_7r_c"">Amalberga</hi></rs></body>")
    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Amalberga""><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_7r_d"" prev=""#C07_7r_c"">Amalberga</hi></rs></body>")
    
    %test:args("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""indent""/>Dies dauerte <date xmlns=""http://www.tei-c.org/ns/1.0"" from=""1125"" to=""1425"" type=""asynchronous"">300 Jahr</date>.<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/></body>")
    %test:assertEquals("<body xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""indent""/>Dies dauerte <date xmlns=""http://www.tei-c.org/ns/1.0"" from=""1125"" to=""1425"" type=""asynchronous"">300 Jahr</date>.<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/></body>")    
    
    function tidysimple-test:sections($node as element(*)) {
        tidySimple:make-structure($node)
};


(: tidying :)
declare 
    %test:name("Tidying up surplus elements")
    %test:args("<text xmlns:tei=""http://www.tei-c.org/ns/1.0""><pb xmlns:tei=""http://www.tei-c.org/ns/1.0"" n=""58r""/><milestone xmlns:tei=""http://www.tei-c.org/ns/1.0"" unit=""line""/><pb xmlns:tei=""http://www.tei-c.org/ns/1.0"" n=""58v""/></text>")
    %test:assertEquals("<text xmlns:tei=""http://www.tei-c.org/ns/1.0""><pb xmlns:tei=""http://www.tei-c.org/ns/1.0"" n=""58r""/><pb xmlns:tei=""http://www.tei-c.org/ns/1.0"" n=""58v""/></text>")
    
    function tidysimple-test:tidying($node as element(*)) {
        tidySimple:sort-out-surplus-elements($node)
};


(: TOC by Friedrich Fontane :)
declare
    %test:name("TOC by Friedrich Fontane")
    %test:args("<div xmlns:tei=""http://www.tei-c.org/ns/1.0"" type=""toc"" subtype=""Friedrich_Fontane""><milestone unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/><milestone unit=""line""/><milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script() medium(blue_ink)""/><milestone unit=""line""/>Material zu<milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn) medium()""/><item>Ellernklipp</item><milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn) medium(black_ink)""/><item><ref target=""#xpath(//surface[@n='21r'])"">Wanderungen (Spreeland)</ref></item><milestone unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf) medium(pencil)""/></div>")
    %test:assertEquals("<div xmlns:tei=""http://www.tei-c.org/ns/1.0"" type=""toc"" subtype=""Friedrich_Fontane""><milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script() medium(blue_ink)""/>Material zu<milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn) medium()""/><item>Ellernklipp</item><milestone unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn) medium(black_ink)""/><item><ref target=""#xpath(//surface[@n='21r'])"">Wanderungen (Spreeland)</ref></item><milestone unit=""handshift"" subtype=""#Fontane"" rend=""script(Latf) medium(pencil)""/></div>")

    function tidysimple-test:toc-by-friedrich($node as element(*)) {
        tidySimple:sort-out-invalid-hands($node) 
        => tidySimple:sort-out-surplus-elements()
        => tidySimple:whitespaces()
        => tidySimple:split-headings()
};