xquery version "3.1";

(: This main module starts the tests stored in index-info-test.xql. :)

import module namespace index-info-test = "http://fontane-nb.dariah.eu/index-info-test" at "index-info-test.xq";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

test:suite(
  util:list-functions("http://fontane-nb.dariah.eu/index-info-test")
)
