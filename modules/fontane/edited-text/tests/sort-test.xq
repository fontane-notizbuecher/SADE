xquery version "3.1";

(: This library module contains XQSuite tests for the presorting module stored
 : in ../presort.xqm :)

module namespace fsort-test = "http://fontane-nb.dariah.eu/sort-test";

import module namespace fsort="http://fontane-nb.dariah.eu/sort" at "../sort.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";


(: Text :)
declare
    %test:name("Text nodes")
    %test:args("Just some text")
    %test:assertEquals("Just some text")

    %test:args("Text with Ümläöuts")
    %test:assertEquals("Text with Ümläöuts")

    %test:args("Hiſtoriſches ✓")
    %test:assertEquals("Hiſtoriſches ✓")
    function fsort-test:text($node as text()) {
        fsort:sort($node)
};

declare
    %test:name("Comments")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0""><!-- a comment --></div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0""/>")
    function fsort-test:comment($node as element()) {
        fsort:sort($node)
};

(: DEFAULT BEHAVIOUR :)
declare
    %test:name("Default behaviour")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</div>")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"" prev=""#some-id"">Some text inside</div>")
    %test:assertEmpty

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""first-div"" next=""#second-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""inbetween-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""second-div"" next=""#third-div"" prev=""#first-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""inbetween-div2""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""third-div"" prev=""#second-div""/></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""first-div"" next=""#second-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""second-div"" next=""#third-div"" prev=""#first-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""third-div"" prev=""#second-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""inbetween-div""/><div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""inbetween-div2""/></wrapper>")
    function fsort-test:default($node as element()) {
        fsort:sort($node)
};

(: TEI:HI :)
declare
    %test:name("tei:hi")
    %test:args("<hi xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</hi>")
    %test:assertEquals("<hi xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</hi>")

    %test:args("<hi xmlns=""http://www.tei-c.org/ns/1.0"" prev=""#some-ig"">Some text inside</hi>")
    %test:assertEmpty

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""first-hi"" next=""#second-hi"">Some text inside</hi><div xmlns=""http://www.tei-c.org/ns/1.0"">Something inbetween.</div><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""second-hi"" prev=""#first-hi"">Some text inside</hi></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""first-hi"" next=""#second-hi"">Some text inside</hi><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""second-hi"" prev=""#first-hi"">Some text inside</hi><div xmlns=""http://www.tei-c.org/ns/1.0"">Something inbetween.</div></wrapper>")

        %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""first-hi"" next=""#second-hi"">Some text inside</hi><div>Something inbetween.</div><rs><hi xml:id=""second-hi"" prev=""#first-hi"">Some text inside</hi></rs></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""first-hi"" next=""#second-hi"">Some text inside</hi><rs><hi xml:id=""second-hi"" prev=""#first-hi"">Some text inside</hi></rs><div >Something inbetween.</div></wrapper>")
    function fsort-test:hi($node as element()) {
        fsort:sort($node)
};


(: TEI:RS :)
declare
    %test:name("tei:rs defaults")
    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</rs>")
    %test:assertEquals("<rs xmlns=""http://www.tei-c.org/ns/1.0"">Some text inside</rs>")

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0"" prev=""#some-ig"">Some text inside</rs>")
    %test:assertEmpty

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some <handShift/>text inside</rs><div>Something inbetween.</div><rs xml:id=""second-hi"" prev=""#first-hi"">Some <handShift/>text inside</rs></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some <handShift/>text inside</rs><rs xml:id=""second-hi"" prev=""#first-hi"">Some <handShift/>text inside</rs><div >Something inbetween.</div></wrapper>")

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><div>Something inbetween.</div><rs xml:id=""second-hi"" prev=""#first-hi"">Some text inside</rs></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><rs xml:id=""second-hi"" prev=""#first-hi"">Some text inside</rs><div >Something inbetween.</div></wrapper>")

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><div>Something inbetween.</div><rs xml:id=""second-hi"" prev=""#first-hi"">Some text <handShift/>inside</rs></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><rs xml:id=""second-hi"" prev=""#first-hi"">Some text <handShift/>inside</rs><div >Something inbetween.</div></wrapper>")

    %test:args("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><div>Something inbetween.</div><rs xml:id=""third-hi"" next=""#fourth-hi"" prev=""#second-hi"">Some text inside</rs><div>Something inbetween.</div><rs xml:id=""second-hi"" prev=""#first-hi"" next=""#third-hi"">Some text <handShift/>inside</rs><rs xml:id=""fourth-hi"" prev=""#third-hi"">Some text <handShift/>inside</rs></wrapper>")
    %test:assertEquals("<wrapper xmlns=""http://www.tei-c.org/ns/1.0""><rs xml:id=""first-hi"" next=""#second-hi"">Some text inside</rs><rs xml:id=""second-hi"" prev=""#first-hi"" next=""#third-hi"">Some text <handShift/>inside</rs><rs xml:id=""third-hi"" next=""#fourth-hi"" prev=""#second-hi"">Some text inside</rs><rs xml:id=""fourth-hi"" prev=""#third-hi"">Some text <handShift/>inside</rs><div>Something inbetween.</div><div>Something inbetween.</div></wrapper>")
    function fsort-test:rs-default($node as element()) {
        fsort:sort($node)
};


(: KEEP NODE :)
declare
    %test:name("Keeping node")
    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""some-id"">Some text inside</div>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""some-id"">Some text inside</div>")
    function fsort-test:keep-node($node as element()) {
        fsort:keep-node($node)
};


(: FIND CORRESPONDING NODE :)
declare
    %test:name("Find corresponding node")

    %test:args("<div xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""first-div"" next=""#second-div""/>")
    %test:assertEmpty
    function fsort-test:find-corresp($node as element()) {
        fsort:find-corresp-node($node, "next")
};


(: GET ID :)
declare
    %test:name("Get ID")

    %test:args("#some-id")
    %test:assertEquals("some-id")
    function fsort-test:get-id($node as xs:string) {
        fsort:get-id($node)
};

(: HAS TEI:RS ONLY COPIED CHILDREN :)
declare
    %test:name("Copied children")

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0"">Test</rs>")
    %test:assertFalse

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""first-hi"" next=""#some-id""/><hi xml:id=""second-hi"" prev=""#first-id""/></rs>")
    %test:assertFalse

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0"">Some Text here<hi xml:id=""first-hi"" next=""#some-id""/><hi xml:id=""second-hi"" prev=""#first-id""/></rs>")
    %test:assertFalse

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""second-hi"" prev=""#first-id""/></rs>")
    %test:assertTrue

    %test:args("<rs xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""second-hi"" prev=""#first-id""/>Some text</rs>")
    %test:assertFalse
    function fsort-test:copied-children($node as element()) {
        fsort:has-only-copied-children($node)
};
