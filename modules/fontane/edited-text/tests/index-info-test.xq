xquery version "3.1";

(: This library module contains XQSuite tests for the all modules module stored
 : in index-info.xqm :)

module namespace index-info-test = "http://fontane-nb.dariah.eu/index-info-test";

import module namespace index-info ="http://fontane-nb.dariah.eu/index-info" at "../index-info.xqm";
import module namespace fontaneSimple="http://fontane-nb.dariah.eu/teisimple" at "../tei2teisimple.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";

declare
    %test:name("Test the test")
    %test:assertFalse

    function index-info-test:a-test-the-test() {
        false()
};

declare
    %test:name("Events: Full references")
    %test:args("eve:Thesenanschlag", "eve", "regular-name")
    %test:assertEquals("Thesenanschlag")

    function index-info-test:places-full-reference($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

(: EVENTS :)
declare
    %test:name("Events: Places")
    %test:args("eve:Thesenanschlag", "eve", "place")
    %test:assertEquals("plc:Kirche_Wittenberg")

    function index-info-test:events-places($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };


(: PLACES :)
declare
    %test:name("Places: Full references")
    %test:args("plc:Thueringen", "plc", "regular-name")
    %test:assertEquals("Thüringen")

    (: entry with several variant names :)
    %test:args("plc:Albaner_Berge", "plc", "regular-name")
    %test:assertEquals("Albaner Berge (ital.: Colli Albani), Albanergebirge")

    function index-info-test:places-full-reference($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

declare
    %test:name("Places: Subreferences")
    %test:args("plc:Sacro_Convento_Assisi", "plc", "subref-of")
    %test:assertEquals("Assisi")

    %test:args("plc:Thueringen", "plc", "subref-of")
    %test:assertEquals("")

    function index-info-test:places-sub-reference($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

(: ORGANIZATIONS :)
declare
    %test:name("Organizations: Regular names")
    %test:args("org:Garde_Grenadier_Regiment_IV", "org", "regular-name")
    %test:assertEquals("Garde-Grenadier-Regiment Nr. 4")

    function index-info-test:org-full-reference($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };


(: PERSONS :)
declare
    %test:name("Persons: Regular names")
    %test:args("psn:Papke", "psn", "regular-name")
    %test:assertEquals("Papke, Herr")

    function index-info-test:persons-full-reference($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

declare
    %test:name("Persons: Birth date")
    %test:args("psn:Pape", "psn", "birth")
    %test:assertEquals("1813")

    function index-info-test:persons-birth($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

declare
    %test:name("Persons: Death date")
    %test:args("psn:Pape", "psn", "death")
    %test:assertEquals("1895")

    function index-info-test:persons-death($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

declare
    %test:name("Persons: Occupation")
    %test:args("psn:Pape", "psn", "occupation")
    %test:assertEquals("preuß. General")

    function index-info-test:persons-occupation($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };

declare
    %test:name("Persons: Transformation result")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther"">Luther</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Luther, Martin</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1483</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1546</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">dt. Reformator</term></index>Luther</rs>")

    function index-info-test:events-full-reference-xml($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
    };

(: WORKS :)
declare
    %test:name("Works: Regular names")
    %test:args("wrk:Blechen_Gitarre", "wrk", "regular-name")
    %test:assertEquals("Guitarrespielender Italiener")

    function index-info-test:wrk-regular($term as xs:string, $index-type as xs:string, $info as xs:string) {
        index-info:get-info-about($index-type, $term, $info)
    };
