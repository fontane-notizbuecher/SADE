xquery version "3.1";

(: This library module contains XQSuite tests for the iu:analyze-date-string()
 : module storedd transform2teisimple.xqm :)

module namespace teisimple-test = "http://fontane-nb.dariah.eu/teisimple-test";

import module namespace fontaneSimple="http://fontane-nb.dariah.eu/teisimple" at "../tei2teisimple.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";


(: milestones :)
declare
    %test:name("Milestone")
    %test:args("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/>")
    function teisimple-test:analyze-milestones($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: segs :)
declare
    %test:name("Segs")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.3cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Friedrich_Fontane"" medium=""black_ink"" script=""Latn clean""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:small"">Dieses Buch hat 52 Blatt.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script(Latn clean) medium(black_ink)""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small"">Dieses Buch hat 52 Blatt.</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""letter-spacing:0.2cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dieses Buch hat 52 Blatt.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""letter-spacing:0.2cm""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dieses Buch hat 52 Blatt.</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-transform:uppercase""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dieses Buch hat 52 Blatt.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""text-transform:uppercase""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dieses Buch hat 52 Blatt.</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:small""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dieses Buch hat 52 Blatt.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dieses Buch hat 52 Blatt.</seg>")
    function teisimple-test:analyze-seg($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.2 Intervening in texts :)

declare
    %test:name("Intervening in texts")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""10v""><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C03_10v_1"">mar</seg></line>-</zone><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><add xmlns=""http://www.tei-c.org/ns/1.0"" copyOf=""#C03_10v_1"" cause=""catchword"">mar</add>mor</line></zone></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""10v""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "mar", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""reduplication""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>mar<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>", "mor")    
    
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line>ei<mod type=""subst""><del rend=""overwritten"">l</del><add place=""superimposed"">n</add></mod>la<seg xml:id=""C02_32r_1"">den</seg>-</line><line><add copyOf=""#C02_32r_1"">den</add>den</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "ei", "n", "la", "den", "@P", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""reduplication""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>den<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>", "den")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg  xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D08_7v_1"">und</seg><add  xmlns=""http://www.tei-c.org/ns/1.0"" place=""above"" copyOf=""#D08_7v_1"" cause=""unclear"">und</add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "und")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><add place=""above"" style=""margin-left:-0.4cm"" xmlns=""http://www.tei-c.org/ns/1.0"" rend=""caret:bow(1.5cm,pos-right)"">ringt <seg xmlns=""http://www.tei-c.org/ns/1.0"" copyOf=""#B05_5r_z"">und</seg></add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "ringt ", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""reduplication""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>und<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><add xmlns=""http://www.tei-c.org/ns/1.0"">oder Ende des 17<seg xmlns=""http://www.tei-c.org/ns/1.0"" copyOf=""#D08_30r_a"">.</seg></add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "oder Ende des 17", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""reduplication""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>.<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0"">Sei</line><lb xmlns=""http://www.tei-c.org/ns/1.0"" break=""no""/><line xmlns=""http://www.tei-c.org/ns/1.0"">ten</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Se", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""missing-hyphen""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>i<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>t<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "en")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><choice xmlns=""http://www.tei-c.org/ns/1.0""><sic xmlns=""http://www.tei-c.org/ns/1.0""/><corr xmlns=""http://www.tei-c.org/ns/1.0"">.</corr></choice></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<choice xmlns=""http://www.tei-c.org/ns/1.0""><sic xmlns=""http://www.tei-c.org/ns/1.0""/><corr xmlns=""http://www.tei-c.org/ns/1.0"">.</corr></choice>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" type=""verse""><surplus xmlns=""http://www.tei-c.org/ns/1.0"">„</surplus>Für die vielen Anſtrengungen</line>")
    %test:assertEquals("<milestone unit=""start-lg"" xmlns=""http://www.tei-c.org/ns/1.0""/>", "<l xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""surplus""><hi xmlns=""http://www.tei-c.org/ns/1.0"" >‹</hi>„<hi xmlns=""http://www.tei-c.org/ns/1.0"" >›</hi></seg>Für die vielen Anstrengungen</l>", "<milestone unit=""end-lg"" xmlns=""http://www.tei-c.org/ns/1.0""/>")

    function teisimple-test:analyze-interventions($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.5 Glued Pages :)
declare
    %test:name("Glued pages")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""Ir"" facs=""http://textgridrep.org/textgrid:18qcd"" type=""clipping"" subtype=""Kalenderblatt"" attachment=""partially_glued-posthumous"" ulx=""1.8"" uly=""2.3"" lrx=""8.8"" lry=""13.5""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""B03_002"" url=""http://textgridrep.org/textgrid:18qcd"" mimeType=""image/jpeg""/></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""Ir""/>")

    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""30var"" facs=""http://textgridrep.org/textgrid:167nm"" type=""clipping"" subtype=""Zeitungsausschnitt"" attachment=""formerly_partially_glued""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Test</line></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""30var""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" facs=""http://textgridrep.org/textgrid:167nm"" type=""clipping"" subtype=""Zeitungsausschnitt"" attachment=""formerly_partially_glued""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Test</div>")

    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""30var"" facs=""http://textgridrep.org/textgrid:167nm"" type=""pocket"" subtype=""angeklebte_cremefarbige_Tasche_von_Fontane_angefertigt_-_Angeklebtes_Blatt"" attachment=""formerly_partially_glued""><line xmlns=""http://www.tei-c.org/ns/1.0"">Test</line></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""30var""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" facs=""http://textgridrep.org/textgrid:167nm"" type=""pocket"" subtype=""angeklebte_cremefarbige_Tasche_von_Fontane_angefertigt_-_Angeklebtes_Blatt"" attachment=""formerly_partially_glued""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Test</div>")

    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" facs=""http://textgridrep.org/textgrid:169hc"" type=""label"" subtype=""Oblate"" ulx=""6.2"" uly=""3.7"" lrx=""7.6"" lry=""5.4""/>")
    %test:assertEmpty
    
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" subtype=""Briefmarke"" facs=""http://example.org/url/auf/textgridURI/von/Bild/der/Briefmarke""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" url=""http://example.org/url/auf/textgridURI/von/Bild/der/Briefmarke"" mimeType=""image/jpeg""/></surface>")
    %test:assertEmpty
    function teisimple-test:analyze-glued($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.6 Glue and page fragments :)
declare
    %test:name("Glue and page fragments")
    %test:args("<mod xmlns=""http://www.tei-c.org/ns/1.0"" rend=""glue_trace""/>")
    %test:assertEmpty

    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""28rar"" facs=""http://textgridrep.org/textgrid:18zt9"" type=""clipping"" subtype=""Zeitungsausschnitt_Fragment"" attachment=""glued"" ulx=""2.9"" uly=""1.9"" lrx=""8.9"" lry=""4.1""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""D05_038"" url=""http://textgridrep.org/textgrid:18zt9"" mimeType=""image/jpeg""/></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""28rar""/>")

    function teisimple-test:analyze-glue-page-fragments($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.7 Sketches :)
declare
    %test:name("Sketches")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""illustration"" ulx=""0.6"" uly=""0.0"" lry=""10.9"" lrx=""9.6""><milestone unit=""illustration""/><figure xml:id=""a1""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Jagdschloss Stern.</figDesc></figure><zone ulx=""6.1"" uly=""0.8""><line><handShift new=""#Fontane""/>Dach</line></zone></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Jagdschloss Stern.</figDesc></figure><seg type=""caption""><milestone unit=""line""/><milestone unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dach</seg></ab>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""illustration"" ulx=""1.4"" uly=""5.6"" lrx=""8.3"" lry=""15.5""><milestone unit=""illustration""/><figure xml:id=""a1""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Jagdschloss Stern.</figDesc></figure></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Jagdschloss Stern.</figDesc></figure></ab>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone><zone uly=""11.3"" lrx=""8.6"" lry=""11.8"" ><line style=""margin-left:1.2cm""><handShift new=""#Fontane""/>Eimer in dieſer Form</line></zone><zone type=""illustration""  ulx=""9.0"" uly=""11.0"" lrx=""10.6"" lry=""11.8""><figure xml:id=""a1""><figDesc><ref target=""http://vocab.getty.edu/aat/300124063"">Technische Illustration</ref></figDesc></figure></zone></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Eimer in dieser Form", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><figure xml:id=""a1""  href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc><ref target=""http://vocab.getty.edu/aat/300124063"">Technische Illustration</ref></figDesc></figure></ab>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""list"" subtype=""legend""><line style=""margin-left:1.4cm"" type=""item""><seg><ref target=""#C04_4v_3""><handShift new=""#Fontane""/>A. Kloſterkirche.</ref></seg></line><zone type=""item""><line style=""margin-left:1.4cm""><seg><ref target=""#C04_4v_2 #C04_4v_4"">B. und C. Moderniſirte Kloſter-</ref></seg></line></zone></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<list xmlns=""http://www.tei-c.org/ns/1.0"" subtype=""legend""><item><ref target=""#C04_4v_3""><milestone unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>A. Klosterkirche.</ref></item><item><milestone unit=""line""/><ref target=""#C04_4v_2 #C04_4v_4"">B. und C. Modernisirte Kloster@P</ref></item></list>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""legend""><line style=""margin-left:1.4cm""><seg><ref target=""#C04_4v_3""><handShift new=""#Fontane""/>A. Kloſterkirche.</ref></seg></line></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""legend""><milestone unit=""line""/><ref target=""#C04_4v_3""><milestone unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>A. Klosterkirche.</ref></div>")

    %test:args("<certainty xmlns=""http://www.tei-c.org/ns/1.0"" cert=""high"" target=""#C02_8v_1"" locus=""value""><desc type=""edited_text"">Unsicherer Befund. Bei den Linien könnte es sich auch um Abgrenzungslinien handeln. Da allerdings kein Text angrenzt, wurde für eine Codierung als Skizze optiert.</desc></certainty>")
    %test:assertEquals("<note xmlns=""http://www.tei-c.org/ns/1.0"" type=""editorial"" subtype=""certainty"" cert=""high"" target=""#C02_8v_1"">Unsicherer Befund. Bei den Linien könnte es sich auch um Abgrenzungslinien handeln. Da allerdings kein Text angrenzt, wurde für eine Codierung als Skizze optiert.</note>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""illustration"" style=""text-decoration:line-through"" rend=""line-through-style:zigzag"" ulx=""4.2"" uly=""9.0"" lrx=""7.1"" lry=""11.1""><del><figure/></del></zone>")
    %test:assertEmpty

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""illustration""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""illustration""/><zone type=""illustration"" ulx=""0.8"" uly=""0.0"" lrx=""5.9"" lry=""7.6""><figure xml:id=""a1""><figDesc copyOf=""#C08_16r_a""/></figure></zone><zone type=""illustration"" ulx=""6.9"" uly=""1.1"" lrx=""8.5"" lry=""3.8""><figure xml:id=""a1""><figDesc xml:id=""C08_16r_a""><ref target=""http://vocab.getty.edu/aat/300015566"">Umgebungsplan</ref> Venedig mit Markusplatz, San Giorgio Maggiore und La Giudecca</figDesc></figure></zone></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""composed-sketch""><ab rendition=""margin-left:0.8cm; margin-top:0.0cm"" type=""sketch""><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc copyOf=""#C08_16r_a""/></figure></ab><ab type=""sketch"" rendition=""margin-left:6.9cm; margin-top:1.1cm""><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc xml:id=""C08_16r_a""><ref target=""http://vocab.getty.edu/aat/300015566"">Umgebungsplan</ref> Venedig mit Markusplatz, San Giorgio Maggiore und La Giudecca</figDesc></figure></ab></ab>")    
    
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone type=""illustration"" ulx=""9.6"" uly=""11.8"" lrx=""10.2"" lry=""13.2""><seg><rs type=""direct"" ref=""plc:Gentzrode_Schloss""><figure xml:id=""a1""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Gentzrode, Speicher mit Wohnturm (Seitenfront)</figDesc></figure></rs></seg></zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><rs type=""direct"" ref=""plc:Gentzrode_Schloss""><index indexName=""plc""><term type=""main"">Gentzrode, Gentzrode. Speicher und Wohnturm</term></index><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Gentzrode, Speicher mit Wohnturm (Seitenfront)</figDesc></figure></rs></ab>")


    function teisimple-test:analyze-sketches($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.8.1 Line counting :)
declare
    %test:name("Line counting")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" rotate=""357"" ulx=""1.5"" uly=""4.7"" lrx=""8.4"" lry=""9.2""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#fremde_Hand3""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.4cm"">✓<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/> 1873.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#fremde_Hand3"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "✓", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", " 1873.")
    function teisimple-test:analyze-line-counting($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.8.2 Paragraphs :)
declare
    %test:name("Paragraphs")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.0cm"">noch nicht recht aufgehn.</line><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:2.8cm"" rend=""indent"">Unter den andern</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "noch nicht recht aufgehn.", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""indent""/>", "Unter den andern")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">horizontale einfache Absatzlinie</ref></figDesc></figure><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""short-paragraph-line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""paragraph""/>")
    function teisimple-test:analyze-paragraphs($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.8.2.2.1 Verses :)
declare
    %test:name("Verses")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" rend=""indent""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.3cm"" type=""verse"">Für König und für Vaterland,</line><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.3cm"" type=""verse"">Mit Gott, im blutgen Krieg,</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""start-lg""/>", "<l xmlns=""http://www.tei-c.org/ns/1.0"">Für König und für Vaterland,</l>", "<l xmlns=""http://www.tei-c.org/ns/1.0"">Mit Gott, im blutgen Krieg,</l>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""end-lg""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""verse"" rend=""indent""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.8cm"">Ein leiſes Wölkchen ſteigt</line><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:2.0cm"">Ein Knall rollt herüber</line></zone></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""start-lg""/>", "<l xmlns=""http://www.tei-c.org/ns/1.0"">Ein leises Wölkchen steigt<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Ein Knall rollt herüber</l>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""end-lg""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C05_52v_a"" next=""#C05_52v_b"" type=""verse"">Denkt an</line></zone><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""verse"" xml:id=""C05_52v_b"" prev=""#C05_52v_a"">Luther.</seg> Zum</line><line xmlns=""http://www.tei-c.org/ns/1.0"">Andenken</line></zone></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""start-lg""/>", "<l xmlns=""http://www.tei-c.org/ns/1.0"">Denkt an<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Luther.</l>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""end-lg""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", " Zum", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Andenken")

    function teisimple-test:analyze-verse($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.8.2.2.2 Dialogues :)
declare
    %test:name("Dialogues")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""dialogue""><line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underlinie"">Er</seg></hi>. <seg><said xmlns=""http://www.tei-c.org/ns/1.0"">Nun kommt der Däne nicht wieder.</said></seg></line><line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underlinie"">Ich</seg></hi>. <seg><said xmlns=""http://www.tei-c.org/ns/1.0"">Wer weiß.</said></seg></line></zone>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""dialogue""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi xmlns=""http://www.tei-c.org/ns/1.0"">Er</hi>. <seg xmlns=""http://www.tei-c.org/ns/1.0"" type =""said"">Nun kommt der Däne nicht wieder.</seg><lb xmlns=""http://www.tei-c.org/ns/1.0"" type=""edited_text""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi>Ich</hi>. <seg xmlns=""http://www.tei-c.org/ns/1.0"" type =""said"">Wer weiß.</seg></div>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""dialogue""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D07_69v_a"" next=""#D07_69v_1"" type=""said"">Hören Se, </seg></line><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D07_69v_1"" prev=""#D07_69v_a"" type=""said""><line xmlns=""http://www.tei-c.org/ns/1.0"">der ganze Krieg war nich</line><line xmlns=""http://www.tei-c.org/ns/1.0"">nöthig.</line></zone><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""said""><line xmlns=""http://www.tei-c.org/ns/1.0"" rend=""indent"">Nich?</line></zone></zone>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""dialogue""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" type =""said"">Hören Se, <milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>der ganze Krieg war nich<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>nöthig.</seg><lb xmlns=""http://www.tei-c.org/ns/1.0"" type=""edited_text""/><seg xmlns=""http://www.tei-c.org/ns/1.0"" type =""said""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""indent""/>Nich?</seg></div>")

    function teisimple-test:analyze-dialogues($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.8.2.3 Headings :)
declare
    %test:name("Headings")
        %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:3.1cm"" type=""heading"" subtype=""chapter"" rend=""align(center)""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:large; letter-spacing:0.2cm; text-decoration:underline"">Luther</seg></hi></line>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""large"" subtype=""chapter"" rend=""align(center)""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><hi xmlns=""http://www.tei-c.org/ns/1.0"">Luther</hi></head>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading"" subtype=""chapter""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.5cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>4. <seg xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Luetzen"">Lützen</rs></seg></line><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:3.7cm""><seg xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Gustav_Adolf"">Guſtav <retrace xmlns=""http://www.tei-c.org/ns/1.0"">Ad</retrace>olf</rs></seg>. <seg xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Weissenfels"">Weißenfels</rs></seg>.</line></zone>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default"" subtype=""chapter""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>4. <rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Luetzen""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""plc""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Lützen</term></index>Lützen</rs><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Gustav_Adolf""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Gustav II. Adolf</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1594</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1632</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">seit 1611 König von Schweden</term></index>Gustav Adolf</rs> . <rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Weissenfels""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""plc""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Weißenfels</term></index>Weißenfels</rs> .</head>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:xx-large; font-weight:bold""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""letter-spacing:0.2cm""><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when-iso=""1873""><hi xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>1<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"" rend=""underline-style:wavy"">873</seg></hi></date></seg>.</seg></seg></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""xx-large""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:xx-large; font-weight:bold""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""letter-spacing:0.2cm""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when=""1873""><hi xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>1873</hi></date>.</seg></seg></head>")

    (:examples from here on from 3.8.2.3. :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:3.1cm"" type=""heading"" rend=""align(center)""><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">Luther</seg></hi></line>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default"" rend=""align(center)""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Luther</hi></head>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading""><hi xmlns=""http://www.tei-c.org/ns/1.0"">E<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">lfter Brief.</seg></hi></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Elfter Brief.</hi></head>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading"" subtype=""section"" rend=""align(right)""><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">Riva d. 15. A</seg>ugust</hi></line>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default"" subtype=""section"" rend=""align(right)""><hi xmlns=""http://www.tei-c.org/ns/1.0"">Riva d. 15. August</hi></head>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading"" rend=""align(center)""><line xmlns=""http://www.tei-c.org/ns/1.0""><hi>O<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">ertlichkeite</seg>n</hi></line><line xmlns=""http://www.tei-c.org/ns/1.0""><hi>d<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">eutſcher Sage und Geſ</seg>chichte</hi>.</line></zone>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default"" rend=""align(center)""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi xmlns=""http://www.tei-c.org/ns/1.0"">Oertlichkeiten</hi><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi>deutscher Sage und Geschichte</hi>.</head>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""heading"" subtype=""section"" rend=""align(center)""><line><hi>O<seg style=""text-decoration:underline"">ertlichkeite</seg>n</hi></line><line><hi>d<seg style=""text-decoration:underline"">eutſcher Sage und Geſ</seg>chichte</hi>.</line></zone>")
    %test:assertEquals("<head xmlns=""http://www.tei-c.org/ns/1.0"" style=""default"" subtype=""section"" rend=""align(center)""><milestone unit=""line""/><hi>Oertlichkeiten</hi><milestone unit=""line""/><hi>deutscher Sage und Geschichte</hi>.</head>")

    function teisimple-test:analyze-headings($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.8.2.4 Lists :)
declare
    %test:name("Lists")
        %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""list"" rend=""indent""><line xmlns=""http://www.tei-c.org/ns/1.0"" type=""item"">Ludwig der Springer,</line><line xmlns=""http://www.tei-c.org/ns/1.0"" type=""item"">Ludwig der Eiſerne,</line></zone>")
    %test:assertEquals("<list xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""indent""><item xmlns=""http://www.tei-c.org/ns/1.0"">Ludwig der Springer,</item><item xmlns=""http://www.tei-c.org/ns/1.0"">Ludwig der Eiserne,</item></list>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""37v""><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C08_37v_1"" next=""#C08_38r_1"" type=""list"" rend=""indent""><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""item""><line xmlns=""http://www.tei-c.org/ns/1.0"">1. Mantegna:</line></zone></zone></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""38r""><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C08_38r_1"" prev=""#C08_37v_1"" type=""list"" rend=""indent""><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""item""><line xmlns=""http://www.tei-c.org/ns/1.0"">4. Guercino:</line> </zone></zone></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""37v""/>", "<list xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C08_37v_1"" rendition=""indent"" next=""#C08_38r_1""><item xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>1. Mantegna:</item></list>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""38r""/>", "<list xmlns=""http://www.tei-c.org/ns/1.0"" prev=""#C08_37v_1"" rendition=""indent"" xml:id=""C08_38r_1""><item xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>4. Guercino:</item></list>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""19r""><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_19r_1"" next=""#C06_20r_1"" type=""list""><line xmlns=""http://www.tei-c.org/ns/1.0"" type=""item"" rend=""indent"">c. Das Grumbach⸗Beil</line><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_19r_2"" next=""#C06_20r_2"" type=""item""><line xmlns=""http://www.tei-c.org/ns/1.0"" rend=""indent"">d. Das 101 Richtſchwert;</line></zone></zone></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""19v""/><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""20r""><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_20r_1"" prev=""#C06_19r_1"" type=""list""><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_20r_2"" prev=""C06_19r_2"" type=""item""><line xmlns=""http://www.tei-c.org/ns/1.0"">Theils weil Raum und</line></zone></zone></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""19r""/>", "<list xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_19r_1"" next=""#C06_20r_1""><item xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""indent"">c. Das Grumbach-Beil</item><item xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_19r_2"" next=""#C06_20r_2""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""indent""/>d. Das 101 Richtschwert;</item></list>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""19v""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""20r""/>", "<list xmlns=""http://www.tei-c.org/ns/1.0"" prev=""#C06_19r_1"" xml:id=""C06_20r_1""><item xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C06_20r_2"" prev=""C06_19r_2""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Theils weil Raum und</item></list>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""toc"" subtype=""Fontane""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.5cm"" type=""item"" xml:id=""C07_1r_q"" next=""#C07_1r_13""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""#xpath(//surface[@n='13r'])"">Groſsbeeren</ref></line><line xmlns=""http://www.tei-c.org/ns/1.0"" type=""item""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""#xpath(//surface[@n='25r'])"">Blankenfelde</ref></line></zone>")
    %test:assertEquals("<list xmlns=""http://www.tei-c.org/ns/1.0"" type=""toc"" subtype=""Fontane""><item xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_1r_q"" next=""#C07_1r_13""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""#xpath(//surface[@n='13r'])"">Grossbeeren</ref></item><item xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""#xpath(//surface[@n='25r'])"">Blankenfelde</ref></item></list>")
    
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""toc"" subtype=""Fontane_ungültig""> <line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.6cm"">Enthält:</line><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""item""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:3.0cm""><del>Katzbach</del></line></zone></zone>")
    %test:assertEmpty

    function teisimple-test:analyze-lists($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: BOOK COVERS :)

declare
    %test:name("Book covers: outer_front_cover")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""outer_front_cover""
        facs=""http://textgridrep.org/textgrid:164g9"" xml:id=""a5""/>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" type=""outer_front_cover""/>")
    function teisimple-test:analyze-outer-front-covers($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Book covers: inner_front_cover")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""inner_front_cover""
        facs=""http://textgridrep.org/textgrid:164g9"" xml:id=""a5""/>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" type=""inner_front_cover""/>")
    function teisimple-test:analyze-inner-front-covers($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Book covers: outer_back_cover")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""outer_back_cover""
        facs=""http://textgridrep.org/textgrid:164g9"" xml:id=""a5""/>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" type=""outer_back_cover""/>")
    function teisimple-test:analyze-outer-back-covers($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Book covers: inner_back_cover")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""inner_back_cover""
        facs=""http://textgridrep.org/textgrid:164g9"" xml:id=""a5""/>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" type=""inner_back_cover""/>")
    function teisimple-test:analyze-inner-back-covers($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Labels on Covers")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" subtype=""Signaturen-Klebchen_Archivar_alt""/>")
    %test:assertEmpty

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" subtype=""Etikett_vom_Notizbuch-Hersteller_angefertigt""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Test</surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""label""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Test</div>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" subtype=""Etikett_von_Fontane_angefertigt_-_Aufgeklebtes_Blatt""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Test</surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""label""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Test</div>")
    function teisimple-test:analyze-labels($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: PAGES :)

declare
    %test:name("Pages")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r"" facs=""http://textgridrep.org/textgrid:164h2""/>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r""/>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r"" facs=""http://textgridrep.org/textgrid:164h2""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Test</surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Test")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""52r"" facs=""http://textgridrep.org/textgrid:164j2""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C07_053"" url=""http://textgridrep.org/textgrid:164j2"" mimeType=""image/jpeg""/><zone><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">[<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/>49<handShift new=""#Archivar2"" xmlns=""http://www.tei-c.org/ns/1.0""/>]</fw></line><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">52</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/></zone></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""52v"" facs=""http://textgridrep.org/textgrid:164js""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C07_054"" url=""http://textgridrep.org/textgrid:164js"" mimeType=""image/jpeg""/></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""53r"" facs=""http://textgridrep.org/textgrid:164js"" type=""fragment"" attachment=""cut""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C07_054"" url=""http://textgridrep.org/textgrid:164js"" mimeType=""image/jpeg""/><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">53</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/></zone></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""52r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""52v""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""53r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>")
    function teisimple-test:analyze-pages($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: GRAPHICS :)
declare
    %test:name("Graphics")
    %test:args("<graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C07_001"" url=""http://textgridrep.org/textgrid:164g9"" mimeType=""image/jpeg""/>")
    %test:assertEmpty
    function teisimple-test:analyze-graphics($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: GENETIC INFORMATION :)

(: 3.21.11. DELETIONS :)
declare
    %test:name("Deletions")
    %test:args("<del xmlns=""http://www.tei-c.org/ns/1.0"">(Dann folgen wohl Reiſe</del>")
    %test:assertEmpty

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><line xmlns=""http://www.tei-c.org/ns/1.0"">herab über <seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""A02_76v_1"" next=""#A02_76v_2"" type=""cancel"">den nordiſchſten</seg></line><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""A02_76v_2"" prev=""#A02_76v_1"" type=""cancel""><line xmlns=""http://www.tei-c.org/ns/1.0"">Gardinenſchnittkleidern und</line></zone></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "herab über ")

    (: restoring text :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><restore xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C12_48r_b"" prev=""#C12_48r_a"" style=""text-decoration:line-through"" rend=""line-through-style:double"">z. B.</seg></del></restore></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C12_48r_b"" prev=""#C12_48r_a"" rendition=""text-decoration:line-through"" rend=""line-through-style:double"">z. B.</seg>")

    (: restored deletion that was again deleted :)
    %test:args("<del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"" rend=""line-through-style:single_oblique(45deg)""><restore xmlns=""http://www.tei-c.org/ns/1.0""><del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"">ſich</seg></del></restore></seg></del>")
    %test:assertEmpty

    (: partly restored deletion :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><del xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""B04_52v_d"" prev=""#B04_52v_c""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"">hand <restore xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>führen kann</restore></seg></del></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "führen kann")

    (: restored deletion :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"" rend=""line-through-medium:blue_pencil""> Herbstesbunte </seg><restore xmlns=""http://www.tei-c.org/ns/1.0""><mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""B04_53v_a"">Waldes</del></mod></restore><add xmlns=""http://www.tei-c.org/ns/1.0"">Wälder</add></del><redo xmlns=""http://www.tei-c.org/ns/1.0"" target=""#B04_53v_a"" style=""text-decoration:line-through"" rend=""line-through-medium:blue_pencil""/><add xmlns=""http://www.tei-c.org/ns/1.0"">Rot und gelbe Herbstes</add></mod>lehnen </line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Rot und gelbe Herbstes","lehnen ")

    (: marking something as complete :)
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""marked_off"" xml:id=""E04_39r_2"" uly=""0.5""><line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>3<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">. Kapitel</seg></hi>.</line></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""marked_off"" xml:id=""E04_39r_2""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>3. Kapitel</hi>.</seg>")

    function teisimple-test:analyze-del($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: PAGINATION AND STAMPS, AUCTIONS :)

declare
    %test:name("Pagination alone")
    %test:args("<fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">5</fw>")
    %test:assertEmpty
    function teisimple-test:analyze-pag-so($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Pagination with lines")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">5</fw></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>")
    function teisimple-test:analyze-pag-lines($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Pagination with handschift before")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">5</fw></line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar1"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>")
    function teisimple-test:analyze-pag-hs($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Stamp alone")
    %test:args("<stamp xmlns=""http://www.tei-c.org/ns/1.0"">STAATSBIBLIOTHEK •BERLIN•</stamp>")
    %test:assertEmpty
    function teisimple-test:analyze-stamp($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Stamp in seg")
    %test:args("<seg xmlns=""http://www.tei-c.org/ns/1.0""><stamp xmlns=""http://www.tei-c.org/ns/1.0"">STAATSBIBLIOTHEK •BERLIN•</stamp></seg>")
    %test:assertEmpty
    function teisimple-test:analyze-stamp-seg($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Stamp with handschift before")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Stempel1""/><seg xmlns=""http://www.tei-c.org/ns/1.0""><stamp xmlns=""http://www.tei-c.org/ns/1.0"">STAATSBIBLIOTHEK •BERLIN•</stamp></seg></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Stempel1"" rend=""script() medium()""/>")
    function teisimple-test:analyze-stamp-hs($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Auction numbers")
    %test:args("<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""auction_number"">5<mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""overwritten"">10</del><add place=""superimposed"">09</add></mod>-7</seg>")
    %test:assertEmpty
    function teisimple-test:analyze-auction($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: PAGE FRAGMENTS AND EMPTY PAGES :)

declare
    %test:name("Empty pages")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r"" type=""fragment"" attachment=""torn""><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">26</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm"" new=""#Fontane""/></zone></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar1"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" subtype=""#Fontane"" unit=""handshift"" rend=""script() medium()"" style=""margin-left:0.5cm""/>")

    (: adapt when further pages are implemented :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r"" type=""fragment"" attachment=""torn""><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">26</fw></line><handShift new=""#Fontane""/><line>Gewöhnliches Briefpapier.</line></zone></surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar1"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Gewöhnliches Briefpapier.")
    function teisimple-test:analyze-page-fragment($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.12 EMPTY PAGES :)
declare
    %test:name("Empty pages")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""30r""><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:5.7cm""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">[<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/>26<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/>]</fw></line><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:6.2cm""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">30</fw></line></zone></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""30r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>")

    (: adapt when further pages are implemented :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r"" type=""fragment"" attachment=""torn""><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar1""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">26</fw></line><handShift new=""#Fontane""/><line>Gewöhnliches Briefpapier.</line></zone></surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""26r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar1"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Gewöhnliches Briefpapier.")
    function teisimple-test:analyze-empty-pages($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: GEMINATION :)

declare
    %test:name("Gemination")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#mgem""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>mm</g></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "mm")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#ngem""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>nn</g></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "nn")
    function teisimple-test:analyze-gemination($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: HANDSHIFTS :)
(: Friedrich Fontane, hier noch Besonderheiten beachten! :)
declare
    %test:name("Handshifts")
    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>")

    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Friedrich_Fontane""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script() medium()""/>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" type=""label"" subtype=""Etikett_vom_Notizbuch-Hersteller_angefertigt""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Friedrich_Fontane""/>Test</surface></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""label""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Friedrich_Fontane"" rend=""script() medium()""/>Test</div>")

    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" script=""Latn clean""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn clean) medium()""/>")

    function teisimple-test:analyze-handshift($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: the tests for handshift-omissions are deprecated and therefore deleted since
 : it's easier to handle this phenomenon in the second stage of processing :)

(: FONTS :)

declare
    %test:name("Fonts")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""Latn""/></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(Latn) medium()""/></seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>LVNGiS</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>LVNGiS</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""monogram""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>LC</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""monogram""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>LC</seg>")
    function teisimple-test:analyze-fonts($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: WRITING MEDIUM :)
declare
    %test:name("Writing medium")
    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink thin_pen""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink thin_pen)""/>")
    
    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""pencil""/>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(pencil)""/>")
    function teisimple-test:analyze-medium($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: WORD, LETTER AND CHARACTER SPACING :)
declare
    %test:name("Spacing")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""letter-spacing:0.2cm;""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>1</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""letter-spacing:0.2cm;""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>1</seg>")
    function teisimple-test:analyze-spacing($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: ILLEGIBLE CHARACTERS AND WORDS :)
declare
    %test:name("Illegible characters and words")
    %test:args("<gap xmlns=""http://www.tei-c.org/ns/1.0"" reason=""illegible"" unit=""uc_chars"" quantity=""1""/>")
    %test:assertEquals("<gap xmlns=""http://www.tei-c.org/ns/1.0"" reason=""illegible"" unit=""uc_chars"" quantity=""1""/>")
    %test:args("<gap xmlns=""http://www.tei-c.org/ns/1.0"" reason=""damage"" unit=""mm"" quantity=""64""/>")
    %test:assertEquals("<gap xmlns=""http://www.tei-c.org/ns/1.0"" reason=""damage"" unit=""mm"" quantity=""64""/>")
    function teisimple-test:analyze-illeg-word($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.15 Interrupted texts :)
declare
    %test:name("Interrupted texts")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D02_42v_3"" prev=""#D02_42r_3"" next=""#D02_43r_1""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.8cm"">eigentlich alle häßlich; die</line></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D02_42v_3"" prev=""#D02_42r_3"" next=""#D02_43r_1""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>eigentlich alle häßlich; die</seg>")

    function teisimple-test:analyze-interrupted($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};



(: 3.16 Links :)
declare
    %test:name("Links")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_62v_c"" next=""#C07_62v_d"" target=""#C07_61v_1"">I.</ref></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ref xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_62v_c"" next=""#C07_62v_d"" target=""#C07_61v_1"">I.</ref>")
    
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_62v_c"" next=""#C07_62v_d"" target=""http://textgridrep.org/textgrid:XXX"">I.</ref></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ref xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_62v_c"" next=""#C07_62v_d"" target=""http://textgridrep.org/textgrid:XXX"">I.</ref>")

    function teisimple-test:analyze-links($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.18 languages :)
declare
    %test:name("Languages")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:lang=""la-Latn""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ac delecta Romae sede</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:lang=""la-Latn""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>ac delecta Romae sede</seg>")

    function teisimple-test:analyze-languages($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.19 Font sizes :)
declare
    %test:name("Font sizes")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:xx-small"">S.-A. 4 U. 5 M.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:xx-small"">S.-A. 4 U. 5 M.</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:large"">S.-A. 4 U. 5 M.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:large"">S.-A. 4 U. 5 M.</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:large"">S.-A. 4 U. 5 M.</zone></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:large"" unit=""zone"">S.-A. 4 U. 5 M.</seg>")
    function teisimple-test:analyze-font-sizes($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.20 Attachments :)
declare
    %test:name("Attachments")
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""Beilage_Ir"" facs=""http://textgridrep.org/textgrid:169ft"" type=""additional"" subtype=""Briefumschlag"" attachment=""loose""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""A11_082"" url=""http://textgridrep.org/textgrid:169ft"" mimeType=""image/jpeg""/><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><stamp xmlns=""http://www.tei-c.org/ns/1.0"">Aus Russland.</stamp></seg></line><line xmlns=""http://www.tei-c.org/ns/1.0"">An den Doctor</line></zone></surface>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" n=""Beilage_Ir"" facs=""http://textgridrep.org/textgrid:169ft"" type=""additional"" subtype=""Briefumschlag"" attachment=""loose""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><stamp xmlns=""http://www.tei-c.org/ns/1.0"">Aus Russland.</stamp><milestone unit=""line"" xmlns=""http://www.tei-c.org/ns/1.0""/>An den Doctor</div>")    
    
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""front_endpaper_IIr"" facs=""http://textgridrep.org/textgrid:166bw""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""A03_002"" url=""http://textgridrep.org/textgrid:166bw"" mimeType=""image/jpeg""/><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">II</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/></zone></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""front_endpaper_IIr""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>")   
    
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""Vr"" facs=""http://textgridrep.org/textgrid:18xgz"" type=""pocket"" subtype=""angeklebte_blaue_Tasche_von_Fontane_angefertigt_-_Angeklebtes_Blatt"" attachment=""partially_glued"" ulx=""0.0"" uly=""0.0"" lrx=""5.6"" lry=""13.9""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C09_106"" url=""http://textgridrep.org/textgrid:18xgz"" mimeType=""image/jpeg""/><zone xmlns=""http://www.tei-c.org/ns/1.0"" ><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">V</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/></zone><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""additional"" subtype=""getrocknete_Veilchen"" rend=""attachment:pocket""/><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""IVr"" facs=""http://textgridrep.org/textgrid:18xgz"" type=""clipping"" subtype=""von_Emilie_Fontane_beschriftetes_Blatt"" attachment=""glued"" ulx=""2.5"" uly=""1.9"" lrx=""3.8"" lry=""12.0""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""C09_106"" url=""http://textgridrep.org/textgrid:18xgz"" mimeType=""image/jpeg""/><zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Archivar2""/><line xmlns=""http://www.tei-c.org/ns/1.0""><fw xmlns=""http://www.tei-c.org/ns/1.0"" type=""pageNum"">IV</fw></line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/></zone><zone xmlns=""http://www.tei-c.org/ns/1.0"" rotate=""90"" ulx=""1.1"" uly=""0.7"" lrx=""13.1"" lry=""2.1""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""black_ink""/><line xmlns=""http://www.tei-c.org/ns/1.0"">Ein frohes Wiederſehen</line><line xmlns=""http://www.tei-c.org/ns/1.0"">wünſchten die deut⸗</line><line xmlns=""http://www.tei-c.org/ns/1.0"">ſchen Veilchen.</line><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/></zone></surface></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""Vr""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" facs=""http://textgridrep.org/textgrid:18xgz"" type=""pocket"" subtype=""angeklebte_blaue_Tasche_von_Fontane_angefertigt_-_Angeklebtes_Blatt"" attachment=""partially_glued""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""additional"" subtype=""getrocknete_Veilchen"" rend=""attachment:pocket""/><pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""IVr""/><div facs=""http://textgridrep.org/textgrid:18xgz"" type=""clipping"" subtype=""von_Emilie_Fontane_beschriftetes_Blatt"" attachment=""glued""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Archivar2"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/><milestone unit=""line"" xmlns=""http://www.tei-c.org/ns/1.0""/>Ein frohes Wiedersehen<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>wünschten die deut@P<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>schen Veilchen.<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(pencil)""/></div></div>")    
    
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" facs=""D07_003 D07_004"" type=""additional"" subtype=""getrocknetes_Blatt"" rend=""attachment:pocket""/>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" facs=""D07_003 D07_004"" type=""additional"" subtype=""getrocknetes_Blatt"" rend=""attachment:pocket""/>")
    
    %test:args("<surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""IIv"" facs=""http://textgridrep.org/textgrid:197d3"" type=""clipping"" subtype=""Theaterzettel"" attachment=""partially_glued"" ulx=""0.2"" uly=""0.0"" lrx=""15.6"" lry=""21.4""><graphic xmlns=""http://www.tei-c.org/ns/1.0"" n=""D08_074"" url=""http://textgridrep.org/textgrid:197d3"" mimeType=""image/jpeg"" xml:id=""a11""/> <!-- 19.4.1871 --><zone xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#roman""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Druck_z""/><zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""3.8"" uly=""1.0"" lry=""1.4""><line xmlns=""http://www.tei-c.org/ns/1.0"">Text</line></zone></zone></surface>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""IIv""/>", "<div xmlns=""http://www.tei-c.org/ns/1.0"" facs=""http://textgridrep.org/textgrid:197d3"" type=""clipping"" subtype=""Theaterzettel"" attachment=""partially_glued""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""roman"" unit=""zone""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Druck_z"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Text</seg></div>")

    function teisimple-test:analyze-attachments($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};




(: 3.21.9 TOPOGRPHY OF THE PAGE :)

declare
    %test:name("Topography of the page")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" rotate=""270"" uly=""16"" style=""border-bottom-style:solid; border-right-style:solid; padding-bottom:0.5cm; padding-right:0.5cm""><line style=""margin-left:0cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/> 4. Iron and Steel.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", " 4. Iron and Steel.")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" rotate=""326""><line style=""margin-left:0.0cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>werfen </line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "werfen ")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:x-large"" ulx=""2.1"" uly=""13.8"" lrx=""2.9"" lry=""15.0""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.0cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>(</line></zone></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" unit=""zone"" rendition=""font-size:x-large"" ulx=""2.1"" uly=""13.8"" lrx=""2.9"" lry=""15.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>(</seg>")

    function teisimple-test:analyze-topography($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.10 FRAMES ETC. :)

declare
    %test:name("Rectangular frames")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""border-style:solid"" ulx=""0.3"" uly=""6.1"" lrx=""8.3"" lry=""9.3""><line style=""margin-left:1.1cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Test</line></zone>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""frame""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Test</div>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""border-left-style:solid; border-top-style:solid; border-right-style:solid"" ulx=""0.4"" uly=""7.3"" lrx=""9.9"" lry=""16.1""><zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""0.0"" uly=""0.5"" lrx=""9.2"" lry=""5.8""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>„Es hätte ſie ſehr</line></zone></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "„Es hätte sie sehr")
    function teisimple-test:analyze-rect-frames($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Other frames")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""border-style:solid; border-radius:50%"" rend=""border-medium:red_pencil"" ulx=""2.4"" uly=""1.6"" lrx=""5.7"" lry=""9.0""><zone xmlns=""http://www.tei-c.org/ns/1.0"" rotate=""270"" ulx=""3.1"" uly=""7.6"" lrx=""7.8"" lry=""5.1""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.0cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>S. 260</line></zone></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "S. 260")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""border-style:solid"" rend=""border-style:house"" rotate=""180"" ulx=""2.4"" uly=""6.4"" lrx=""4.2"" lry=""7.3""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><line style=""margin-left:0.0cm"">Die</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Die")
    function teisimple-test:analyze-other-frames($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.10.1. BOUNDARY MARKERS :)

declare
    %test:name("Boundary markers")
    %test:args("<handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><zone xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_61r_2"" prev=""#C07_61r_1"" next=""#C07_61r_3"" rotate=""85"" ulx=""1.2"" uly=""10.8"" style=""border-left-style:solid""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.8cm"">des Feindes an und</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "des Feindes an und")
    function teisimple-test:analyze-boundary($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Boundary lines")
    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""integrate"" rend=""bracket_left"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" function=""integrate"" rend=""bracket_left"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""integrate"" rend=""bracket_right"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" function=""integrate"" rend=""bracket_right"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""authorial_note"" rend=""bracket_right"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" function=""authorial_note"" rend=""bracket_right"" corresp=""#D02_43v_2"" target=""#D02_43v_4""/>")
    function teisimple-test:analyze-boundary($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.10.2.1.2.3 GENEALOGY :)
declare
    %test:name("Genealogy")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" points=""2.8,6.7 3.0,6.6 4.7,4.8""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">Stammbaumverbindungslinie</ref></figDesc></figure></zone>")
(: genealogy lines probably shouldn't be displayed, but I still have to check that.
 : in case they should be serialized, I leave the code :)
(:    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" points=""2.8,6.7 3.0,6.6 4.7,4.8""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">Stammbaumverbindungslinie</ref></figDesc></figure></seg>"):)
    %test:assertEmpty
    function teisimple-test:analyze-genealogy($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.10.2.2. HORIZONTAL BOUNDARY MARKS :)

declare
    %test:name("Horizontal Boundary Marks")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" points=""2.8,6.7 3.0,6.6 4.7,4.8""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">horizontale einfache Abgrenzungslinie</ref></figDesc></figure></zone>")
    %test:assertEmpty

    (: end lines :)
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" points=""2.8,6.7 3.0,6.6 4.7,4.8""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">horizontale einfache Schlusslinie</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""long-end-line""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""4.0"" uly=""6.7"" lrx=""8.0"" lry=""6.7""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">Schlusslinie; horizontale Halbschleife von links oben nach rechts</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""long-end-line""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""4.0"" uly=""6.7"" lrx=""8.0"" lry=""6.7""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">horizontale einfache Schlusslinie (gewellt)</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""long-end-line-wavy""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" style=""border-bottom-style:solid"" rend=""border-bottom-style:brace""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:2.1cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Weiß und Gold</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Weiß und Gold","<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""bottom-brace""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""4.0"" uly=""6.7"" lrx=""8.0"" lry=""6.7""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">Schlusslinien; horizontale Schleife von links oben nach rechts unten</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""bottom-brace-short""/>")

    function teisimple-test:analyze-hor-boundary($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

declare
    %test:name("Horizontal paragraph lines")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""4.0"" uly=""6.7"" lrx=""8.0"" lry=""6.7""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref target=""http://vocab.getty.edu/aat/300027016"">horizontale einfache Absatzlinie</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""short-paragraph-line""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""0.7"" uly=""6.3"" lrx=""9.3"" lry=""6.0""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">oberer Teil einer doppelten Absatzlinie (nachgezogen)</ref></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""short-paragraph-line-double""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""0.7"" uly=""6.3"" lrx=""9.3"" lry=""6.0""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">unterer Teil einer doppelten Absatzlinie (nachgezogen)</ref></figDesc></figure></zone>")
    %test:assertEmpty

    (: horizontal lines with stars or plus-signs :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""paragraph""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>+&#x2003;+&#x2003;+</metamark></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""paragraph""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>+&#x2003;+&#x2003;+</ab>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""paragraph""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>*&#x2003;*&#x2003;*</metamark></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""paragraph""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>*&#x2003;*&#x2003;*</ab>")

    (: unsure cases :)
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" ulx=""0.7"" uly=""6.3"" lrx=""9.3"" lry=""6.0""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">vertikale einfache Abgrenzungslinie</ref> (unsicher)</figDesc></figure></zone>")
    %test:assertEmpty

    function teisimple-test:analyze-hor-p-lines($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.12 WRITING OVER TEXT :)
declare
    %test:name("Writing over text")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"">ſ</seg></del><del xmlns=""http://www.tei-c.org/ns/1.0"" rend=""overwritten"">eine</del><add xmlns=""http://www.tei-c.org/ns/1.0"" place=""superimposed""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ihre</add></mod></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "ihre")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><retrace xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>n</retrace></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "n")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><retrace xmlns=""http://www.tei-c.org/ns/1.0"" rend=""retrace-medium:brown_ink""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>D</retrace></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""retrace-medium:brown_ink""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>D</seg>")

    function teisimple-test:analyze-overwritten($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.13 ADDITIONS :)
declare
    %test:name("Additions")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><add xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>en</add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "en")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.3cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"">Mari</seg>a</del><add xmlns=""http://www.tei-c.org/ns/1.0"" place=""below"" style=""margin-left:0.2cm"">die büßende</add></mod> Magdalena</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "die büßende", " Magdalena")

    %test:args("<addSpan xmlns=""http://www.tei-c.org/ns/1.0"" spanTo=""#C07_1r_s"" place=""interlinear""/>")
    %test:assertEmpty

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><add xmlns=""http://www.tei-c.org/ns/1.0"" type=""edited_text"" subtype=""interlinear"" place=""above"" corresp=""#C07_62v_a""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>I.</add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<add xmlns=""http://www.tei-c.org/ns/1.0"" type=""edited_text"" subtype=""interlinear"" place=""above"" corresp=""#C07_62v_a""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>I.</add>")

    function teisimple-test:analyze-additions($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.14 TRANSPOSITIONS :)
(: TODO :)

(: 3.21.15 TEXT WRITTEN ABOVE AND ADDED :)
declare
    %test:name("Written above and added")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.8cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>junge „wenn ic<mod xmlns=""http://www.tei-c.org/ns/1.0""  type=""subst""><del xmlns=""http://www.tei-c.org/ns/1.0""  rend=""overwritten"">h</del><add xmlns=""http://www.tei-c.org/ns/1.0"">k</add></mod></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>","junge „wenn ic", "k")
    function teisimple-test:analyze-added-above($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.16 Multiphrases :)

declare
    %test:name("Multiphrases")

    (: this test results in an error:
    java:org.exist.xquery.XPathException" message="exerr:ERROR The actual return type does not match the sequence type declared in the function's signature: test:equals(item(), item()) xs:boolean. Expected cardinality: exactly one, got 0.

    reason unclear since a test with an XML yielded the desired output :)
(:    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""multiphrase"" xml:id=""B02_78v_a"" corresp=""#B02_78v_b"">an<anchor xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""B02_78v_c""/>geſehenſten</seg> <add xmlns=""http://www.tei-c.org/ns/1.0"" corresp=""#B02_78v_c"" place=""above""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""multiphrase"" xml:id=""B02_78v_b"" corresp=""#B02_78v_a"">vornehmſten</seg></add> Financiers</line>"):)
(:    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""multiphrase"" xml:id=""B02_78v_a"" corresp=""#B02_78v_b"">an<anchor xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""B02_78v_c""/>gesehensten</seg>", " ", "<add xmlns=""http://www.tei-c.org/ns/1.0"" corresp=""#B02_78v_c"" place=""above""><seg type=""multiphrase"" xml:id=""B02_78v_b"" corresp=""#B02_78v_a"">vornehmsten</seg></add>", " Financiers"):)

    %test:args("<addSpan xmlns=""http://www.tei-c.org/ns/1.0"" type=""multiphrase"" subtype=""extensive"" xml:id=""D04_10r_c"" corresp=""#D04_10r_2"" spanTo=""#D04_10r_a"" place=""margin""/>")
    %test:assertEmpty

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""multiphrase"" subtype=""extensive"" xml:id=""D04_10r_2"" next=""#D04_10v_a"" corresp=""#D04_10r_c""><zone style=""border-left-style:solid""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>mit einem ſlaviſch⸗ſtubs-</line></zone></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""D04_10r_2"" next=""#D04_10v_a"" corresp=""#D04_10r_c""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>mit einem slavisch⸗stubs@P</seg>")
    function teisimple-test:analyze-multiphrase($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.17 WRITER ABORTS TO WRITE :)
declare
    %test:name("Aborts to write")
    %test:args("<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""abort"">Nicht</seg>")
    %test:assertEmpty
    function teisimple-test:analyze-abort($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.18 INSERTION MARKS :)

declare
    %test:name("Insertion marks")
    %test:args("<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""vertical-align:super""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""caret"">╒</metamark></seg>")
    %test:assertEmpty

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1cm""><add xmlns=""http://www.tei-c.org/ns/1.0"" place=""above"" style=""margin-left:-0.7cm"" rend=""caret:slash""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>un</add> ventaglio ein</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>","un"," ventaglio ein")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><add place=""above"" style=""margin-left:-0.4cm"" rend=""caret:backslash""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>aber</add></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "aber")

    function teisimple-test:analyze-insertions($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.19 UNDERLINE :)
declare
    %test:name("Underline")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>K<mod xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"" rend=""underline-style:retrace"">aiſer</mod></hi></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<hi xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Kaiser</hi>")

    function teisimple-test:analyze-underline($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.20 VERTICAL MARKS :)
declare
    %test:name("Vertical marks")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""highlighted"" style=""border-left-style:solid""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.9cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Aber alles mehr</line></zone>")
    %test:assertEquals("<hi xmlns=""http://www.tei-c.org/ns/1.0"" type=""vertical-mark""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Aber alles mehr</hi>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""highlighted"" style=""border-left-style:double""><zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""highlighted"" style=""border-left-style:double""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.2cm""><hi xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""B11_49v_a"" next=""#B11_41v_b""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Ein<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">e koloſſa</seg>le</hi></line></zone></zone>")
    %test:assertEquals("<hi xmlns=""http://www.tei-c.org/ns/1.0"" type=""vertical-mark""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><hi xmlns=""http://www.tei-c.org/ns/1.0""  xml:id=""B11_49v_a"" next=""#B11_41v_b""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Eine kolossale</hi></hi>")

    function teisimple-test:analyze-vertical-marks($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.21 SPECIAL CASES (CHARACTERS) :)
(: hyphens :)
declare
    %test:name("Special cases (hyphens)")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Coſtümen, unter beſti<g ref=""#mgem"">mm</g>ter Um-</line><line xmlns=""http://www.tei-c.org/ns/1.0"">gebung.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Costümen, unter besti", "mm", "ter Um@P", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "gebung.")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Coſtümen, unter beſti<g ref=""#mgem"">mm</g>ter Um⸗</line><line xmlns=""http://www.tei-c.org/ns/1.0"">gebung.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Costümen, unter besti", "mm", "ter Um@P", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "gebung.")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Kunſt-Vereins-</line><lb xmlns=""http://www.tei-c.org/ns/1.0"" break=""keepHyphen""/><line xmlns=""http://www.tei-c.org/ns/1.0"">Lokal.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Kunst-Vereins-", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Lokal.")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Kunſt-Vereins⸗</line><lb xmlns=""http://www.tei-c.org/ns/1.0"" break=""keepHyphen""/><line xmlns=""http://www.tei-c.org/ns/1.0"">Lokal.</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Kunst-Vereins-", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Lokal.")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ſich durch den Baurath um<g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#vds"">⸗</g></line><line xmlns=""http://www.tei-c.org/ns/1.0"">ſtimmen; wir fahren um</line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "sich durch den Baurath um", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "stimmen; wir fahren um")

    function teisimple-test:analyze-hyphens($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: tremata :)
declare
    %test:name("Special cases (tremata)")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>eine blühende Aloë, eine Ananas, ei<retrace xmlns=""http://www.tei-c.org/ns/1.0"">n</retrace></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "eine blühende Aloë, eine Ananas, ei", "n")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Du Baste de l‘Héro&#x1e2f;ne</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Du Baste de l‘Héro&#x1e2f;ne")

    function teisimple-test:analyze-tremata($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: superscript :)
declare
    %test:name("Special cases (superscript and subscript)")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""vertical-align:super; text-decoration:underline""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>r</seg></line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""vertical-align:super;""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>r</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dinstag d. 20</seg>.</hi><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline; vertical-align:super"">ten</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<hi xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dinstag d. 20.</hi>","<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""vertical-align:super"">ten</seg>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""vertical-align:sub; text-decoration:underline""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>r</seg></line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""vertical-align:sub;""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>r</seg>")

    function teisimple-test:analyze-superscript($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: spaces :)
declare
    %test:name("Special cases (spaces)")
    %test:args("<space xmlns=""http://www.tei-c.org/ns/1.0"" type=""placeholder""/>")
    %test:assertEquals("<space xmlns=""http://www.tei-c.org/ns/1.0"" type=""placeholder""/>")

    %test:args("<space xmlns=""http://www.tei-c.org/ns/1.0"" type=""pause""/>")
    %test:assertEquals("<space xmlns=""http://www.tei-c.org/ns/1.0"" type=""pause""/>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""placeholder""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>. . .</metamark></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""placeholder""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>. . .</ab>")

    function teisimple-test:analyze-spaces($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: repetitions :)
declare
    %test:name("Special cases (repetitions)")
    %test:args("<add xmlns=""http://www.tei-c.org/ns/1.0"" cause=""catchword"">dem </add>")
    %test:assertEmpty

    %test:args("<add xmlns=""http://www.tei-c.org/ns/1.0"" place=""above"" style=""margin-left:-0.0cm"" copyOf=""#C05_31v_a"" cause=""unclear"">engſten</add>")
    %test:assertEmpty

    function teisimple-test:analyze-repetitions($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: abbreviations :)
declare
    %test:name("Special cases (abbreviations)")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>fr.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Franken</expan></choice></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>fr.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Franken</expan></choice>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>6 &#x20b0;</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "6 &#x20b0;")

    (: TODO: insert character from Fontine :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>mit Küche 60 <g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#rth"">Reichstaler</g></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "mit Küche 60 ", "<g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#rth"">Reichstaler</g>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""etc.""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>...</metamark></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""etc.""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>...</ab>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ſind bedeutend. 1 <seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-size:small""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""vertical-align:super"">1</seg><g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#hb"">/</g><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""vertical-align:sub"">2</seg></seg> Sgr</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "sind bedeutend. 1 ", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-size:small""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""vertical-align:super"">1</seg><g xmlns=""http://www.tei-c.org/ns/1.0"" ref=""#hb"">/</g><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""vertical-align:sub"">2</seg></seg>", " Sgr")

    function teisimple-test:analyze-abbreviations($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.22 REPETITION MARKS :)
declare
    %test:name("Repetition marks")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:1.0cm""><seg xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""E01_inner_front_cover_a""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>eine</seg> Paßkarte u.</line><lb xmlns=""http://www.tei-c.org/ns/1.0"" type=""keepIndent""/></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "eine", " Paßkarte u.", "<lb xmlns=""http://www.tei-c.org/ns/1.0"" type=""keepIndent""/>")

    function teisimple-test:analyze-repetition-marks($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.23 NOTES :)
declare
    %test:name("Notes")
    %test:args("<note xmlns=""http://www.tei-c.org/ns/1.0"" type=""authorial"" subtype=""text""><line style=""margin-left:0.0cm"">Dies iſt nicht richtig;</line></note>")
    %test:assertEmpty

    function teisimple-test:analyze-notes($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.24+25 CORRECTION MARKS :)
declare
    %test:name("Correction marks")
    (: TODO: find out why test doesn't work :)
(:    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.3cm"">Macchiavell. Alfieri<add xmlns=""http://www.tei-c.org/ns/1.0"" rend=""|""> </add>gefiel</line>"):)
(:    %test:assertEquals("Macchiavell. Alfieri", " ", "gefiel")  :)

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""connect"" rend=""arc""/>")
    %test:assertEmpty

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""paragraph"">z</metamark>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type =""paragraph""/>")

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""connect"" corresp=""#A19_23r_6"" target=""#A19_23r_a""><figure rend=""medium:blue_pencil""><figDesc><ref target=""http://vocab.getty.edu/aat/300200009"">Anschlusszeichen Stufe</ref></figDesc></figure></metamark>")
    %test:assertEmpty

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""transposition"" corresp=""#A19_23r_6"" target=""#A19_23r_a""/>")
    %test:assertEmpty

    function teisimple-test:analyze-correction-marks($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.26 UNCLEAR CHARACTERS :)
declare
    %test:name("Unclear characters")
    %test:args("<seg xmlns=""http://www.tei-c.org/ns/1.0"" function=""unknown"">||</seg>")
    %test:assertEmpty

    function teisimple-test:analyze-unclear($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.27 FOOTNOTE MARKS AND FOOTNOTES :)
declare
    %test:name("Footnote marks and footnotes")
    %test:args("<note xmlns=""http://www.tei-c.org/ns/1.0"" type=""authorial"" subtype=""footnote""><line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""footnote-mark""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>*</metamark> Das Bild von der Havel-Luch Entwäſſerung</line></note>")
    %test:assertEquals("<note xmlns=""http://www.tei-c.org/ns/1.0"" type=""authorial"" subtype=""footnote""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""footnote-mark""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>*</ab> Das Bild von der Havel-Luch Entwässerung</note>")

    %test:args("<metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""footnotes""><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300200009"">horizontale einfache Fußnotenlinie</ref></figDesc></figure></metamark>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""footnotes""/>")

    function teisimple-test:analyze-footnote-marks($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.21.28 ELLIPSIS MARKS :)
declare
    %test:name("Ellipsis")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""ellipsis""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>&#x2013;</metamark></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""ellipsis""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>&#x2013;</ab>")

    function teisimple-test:analyze-ellipses($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22 PRINTS :)
(: 3.22.1 print typography :)
declare
    %test:name("print typography")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#script""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Ober⸗Tribunals⸗Rath</line></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""script"" unit=""zone""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Ober-Tribunals-Rath</seg>")    
    
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#black_letter""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</line>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""black_letter""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#roman""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</line>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""roman""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#black_letter""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""black_letter""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""#roman""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</line></zone>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" unit=""zone"" rendition=""roman""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    function teisimple-test:analyze-print-typography($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22.2 spaced letters :)
declare
    %test:name("print spaced letters")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""letter-spacing:0.2cm""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""letter-spacing:0.2cm""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    function teisimple-test:analyze-print-spaced-letters($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22.3 bold letters :)
declare
    %test:name("print bold letters")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-weight:bold""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-weight:bold""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-weight:bolder""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-weight:bolder""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    function teisimple-test:analyze-print-bold-letters($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22.4 italic letters :)
declare
    %test:name("print italic letters")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-style:italic""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Dampf - Schiffahrt</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-style:italic""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Dampf - Schiffahrt</seg>")

    function teisimple-test:analyze-print-italic-letters($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22.6 Tironian notes :)
declare
    %test:name("Tironian notes")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>&#x204a;c.</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "etc.")

    function teisimple-test:analyze-print-Tironian($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.7/8 Dashes :)
declare
    %test:name("Dashes")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>„Eine Stütze &#x2014; von Gold zwar nicht</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "„Eine Stütze &#x2014; von Gold zwar nicht")
    function teisimple-test:analyze-print-dashes($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.9 Quotation marks :)
declare
    %test:name("Quotation marks")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ihren „hundert Sitzungen“ einen Sandhaufen aufge⸗</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "ihren „hundert Sitzungen“ einen Sandhaufen aufge@P")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>«Dieſe Liebe»</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "«Diese Liebe»")
    function teisimple-test:analyze-print-quote($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.10 Abbreviations: currency :)
declare
    %test:name("Abbreviations: currency")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0"">Frs.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Francs</expan></choice></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0"">Frs.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Francs</expan></choice>")

    function teisimple-test:analyze-print-abbr($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.22.11. Capital letters :)
declare
    %test:name("Capital letters")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-transform:uppercase""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""font-weight:bold""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>D</seg>as</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""text-transform:uppercase""><seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""font-weight:bold""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>D</seg>as</seg>")

    function teisimple-test:analyze-print-capital($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.12 Small caps :)
declare
    %test:name("Small caps")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:2.0cm"" rendition=""#roman #smallcaps""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Stadtrichter Lehfeldt.</line>")
    %test:assertEquals("<seg xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line"" rendition=""roman smallcaps""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Stadtrichter Lehfeldt.</seg>")

    function teisimple-test:analyze-print-small-caps($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.15 Initials :)
declare
    %test:name("Initials")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>S</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" type=""initials""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>S</seg>")

    function teisimple-test:analyze-print-intials($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.18 Print: additions in writing :)
declare
    %test:name("Print: additions in writing")
    %test:args("<mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""highlighted"" hand=""#Fontane"" spanTo=""#C04_36rfr_a"" style=""border-right-style:solid"" rend=""border-medium:pencil""/>")
    %test:assertEquals("<mod xmlns=""http://www.tei-c.org/ns/1.0"" type=""highlighted"" hand=""#Fontane"" spanTo=""#C04_36rfr_a"" style=""border-right-style:solid"" rend=""border-medium:pencil""/>")

    function teisimple-test:analyze-print-writing($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.22.18 Print: red letters :)
declare
    %test:name("Print: red letters")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""color:red""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>23</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<seg xmlns=""http://www.tei-c.org/ns/1.0"" rendition=""color:red""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>23</seg>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""legend"" style=""color:red"" rendition=""#roman"" ulx=""0.8"" uly=""12.5""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.5cm"">Wahre Abbildung des wunderthätigen</line></zone>")
    %test:assertEquals("<div xmlns=""http://www.tei-c.org/ns/1.0"" type=""legend"" rendition=""color:red roman"" ulx=""0.8"" uly=""12.5""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>Wahre Abbildung des wunderthätigen</div>")

    function teisimple-test:analyze-print-red($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(:(: 3.22.22 Printed illustrations :):)
(:declare:)
(:    %test:name("Print: red letters"):)
(:    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""printed_illustration""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""illustration""/><figure xmlns=""http://www.tei-c.org/ns/1.0""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300100181"">Gebäudeansicht</ref>; Hotel Grosse/Zähringer Hof in Karlsruhe.</figDesc></figure><zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Hotel Grosse.</line></zone></zone>") :)
(:    %test:assertEquals(""):)
(:    :)
(:    function teisimple-test:analyze-print-illustration($node as element(*)) {:)
(:        fontaneSimple:transform($node, "16b00"):)
(:};:)



(: 3.23. Overview of the principles of underlining :)
declare
    %test:name("Overview of the principles of underlining")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><hi xmlns=""http://www.tei-c.org/ns/1.0"">m<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"" rend=""underline-style:slightly_wavy"">it der gebiſſenen Wang</seg>e</hi>.</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<hi xmlns=""http://www.tei-c.org/ns/1.0"">mit der gebissenen Wange</hi>", ".")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"">Marieken die <hi xmlns=""http://www.tei-c.org/ns/1.0"">V<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">ogel</seg></hi>verſtändige</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Marieken die ", "<hi xmlns=""http://www.tei-c.org/ns/1.0"">Vogel</hi>", "verständige")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"">Damaſt überzogen<seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline"">, <hi xmlns=""http://www.tei-c.org/ns/1.0"">die er</hi></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Damast überzogen", ", ", "<hi xmlns=""http://www.tei-c.org/ns/1.0"">die er</hi>")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"">Platzes und <seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:underline""><hi xmlns=""http://www.tei-c.org/ns/1.0"">dahinter</hi>,</seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Platzes und ", "<hi xmlns=""http://www.tei-c.org/ns/1.0"">dahinter</hi>", ",")

    function teisimple-test:analyze-underlining-principles($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};



(: 3.26 ENTITIES :)


(: 3.26.1 Dates :)

declare
    %test:name("Dates")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when-iso=""1873-01-01"">1873</date></seg></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when=""1873-01-01"">1873</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from-iso=""1873"">1873</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from=""1873"">1873</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" to-iso=""-0122"">123 v. Chr.</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" to=""-0122"">123 v. Chr.</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from-iso=""1873-07"">1873</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from=""1873-07"">1873</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""fictional"" when-iso=""--09-20""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/>20. September</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""fictional"" when=""--09-20""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>20. September</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from-iso=""1521-05-04"" to-iso=""1522-03-06""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" medium=""black_ink""/>Vom 4. Mai 1521 bis 6. März 1522</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" from=""1521-05-04"" to=""1522-03-06""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(black_ink)""/>Vom 4. Mai 1521 bis 6. März 1522</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" when-iso=""0450"" precision=""low""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>um 450</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" when=""0450"" precision=""low""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>um 450</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" notAfter-iso=""1515"" cert=""low""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>1515 oder auch schon früher</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""asynchronous"" notAfter=""1515"" cert=""low""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>1515 oder auch schon früher</date>")


    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""posthumous"" when-iso=""1925-10""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Oktober 1925</date></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""posthumous"" when=""1925-10""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Oktober 1925</date>")


    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when-iso=""1871-04-23"" xml:id=""D08_21v_a""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>22. April</date></line><note xmlns=""http://www.tei-c.org/ns/1.0"" type=""editorial"" target=""#D08_21v_a"">Richtig: 23. April 1871.</note></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" type=""synchronous"" when=""1871-04-23"" xml:id=""D08_21v_a""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>22. April</date>", "<note xmlns=""http://www.tei-c.org/ns/1.0"" type=""editorial"" target=""#D08_21v_a"">Richtig: 23. April 1871.</note>")


    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" from-iso=""1796"" to-iso=""1854"" xml:id=""a"" next=""#b""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>geboren 1796</date></seg></line></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""1v""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" from-iso=""1796"" to-iso=""1854"" xml:id=""b"" prev=""#a""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>gestorben 1854</date></seg></line></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""1r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" from=""1796"" to=""1854"" xml:id=""a"" next=""#b""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>geboren 1796</date>", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""1v""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" from=""1796"" to=""1854"" xml:id=""b"" prev=""#a""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>gestorben 1854</date>")


    (: actually both tei:date should be merged into one element.for convenience we do that in the second processing step :)
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_2r_a"" next=""#C07_2r_b"" type=""asynchronous"" from-iso=""1521-05-04"" to-iso=""1522-03-06""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Vom 4. Mai 1521 bis</date></seg></line><line xmlns=""http://www.tei-c.org/ns/1.0""><seg xmlns=""http://www.tei-c.org/ns/1.0""><date xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_2r_b"" prev=""#C07_2r_a"" type=""asynchronous"" from-iso=""1521-05-04"" to-iso=""1522-03-06""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>6. März 1522</date></seg></line></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_2r_a"" next=""#C07_2r_b"" type=""asynchronous"" from=""1521-05-04"" to=""1522-03-06""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Vom 4. Mai 1521 bis</date>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<date xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_2r_b"" prev=""#C07_2r_a"" type=""asynchronous"" from=""1521-05-04"" to=""1522-03-06""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>6. März 1522</date>")

    function teisimple-test:analyze-entities-dates($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.26.2 Places :)
declare
    %test:name("Places")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Erfurt""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Erfurt</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Erfurt""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""plc""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Erfurt</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Erfurt</rs> ")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""plc:Erfurt""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Erfurt</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""plc:Erfurt""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""plc""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Erfurt</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Erfurt</rs> ")

    function teisimple-test:analyze-entity-places($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.26.3 Persons :)
declare
    %test:name("Persons")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Luther</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luther""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Luther, Martin</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1483</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1546</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">dt. Reformator</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Luther</rs> ")

(:    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luebke""><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>W.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Wilhelm</expan></choice> Lübke</rs></line>"):)
(:    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luebke""><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>W.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Wilhelm</expan></choice> Lübke</rs>")    :)
    
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luebke_Wilhelm""><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>W.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Wilhelm</expan></choice> Lübke</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""psn:Luebke_Wilhelm""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Luebke, Wilhelm</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1826</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1893</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">dt. Kunsthistoriker; 1857 Lehrer an der Berliner Bauakademie, später Professor in Zürich, Stuttgart und Karlsruhe</term></index><choice xmlns=""http://www.tei-c.org/ns/1.0""><abbr xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>W.</abbr><expan xmlns=""http://www.tei-c.org/ns/1.0"">Wilhelm</expan></choice> Lübke</rs> ")

    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""psn:Luther""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Luther</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""psn:Luther""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Luther, Martin</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1483</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1546</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">dt. Reformator</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Luther</rs> ")

    (: this is a case where actually this tei:rs and its corresponding @next should be merged into one element.for convenience we do that in the second processing step :)
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_12r_m"" next=""#C07_12r_n"" type=""indirect"" ref=""psn:Balthasar psn:Wilhelm_I_Graf""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>ſeinen</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_12r_m"" next=""#C07_12r_n"" type=""indirect"" ref=""psn:Balthasar psn:Wilhelm_I_Graf""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Balthasar von Wettin</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1336</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1406</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">Landgraf von Thüringen und Markgraf von Meißen</term></index><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""psn""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Wilhelm I. (gen. der Einäugige)</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""birth"">1343 oder 1346</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""death"">1407</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""occupation"">Markgraf von Meißen</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>seinen</rs> ")

    function teisimple-test:analyze-entity-persons($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.26.4/5 Works :)
declare
    %test:name("Works")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""wrk:Ein_feste_Burg""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Ein’ feste Burg ist unser Gott</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs  xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""wrk:Ein_feste_Burg""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""wrk""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Ein’ feste Burg ist unser Gott</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Ein’ feste Burg ist unser Gott</rs> ")

    function teisimple-test:analyze-entity-works($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.26.6 Organisations :)
declare
    %test:name("Organisations")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""org:Schmalkaldischer_Bund""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Schmalkaldischer Bund</rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs  xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""org:Schmalkaldischer_Bund""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""org""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Schmalkaldischer Bund</term></index><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>Schmalkaldischer Bund</rs> ")

    function teisimple-test:analyze-entity-organisations($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: all other entities between 3.26.6 and 3.26.14 only differ in their prefix :)

(: 3.26.14 Sketches :)
declare
    %test:name("Sketches")
    %test:args("<zone  xmlns=""http://www.tei-c.org/ns/1.0"" type=""illustration"" ulx=""0.0"" uly=""2.4"" lrx=""9.4"" lry=""16.7""><figure xml:id=""a1""><figDesc  xmlns=""http://www.tei-c.org/ns/1.0""><ref  xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300034158"">Grundriss</ref> von <rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""plc:Schloss_Cunersdorf"">Schloss Cunersdorf</rs></figDesc></figure></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><figure xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300034158"">Grundriss</ref> von <rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""indirect"" ref=""plc:Schloss_Cunersdorf""><index xmlns=""http://www.tei-c.org/ns/1.0"" indexName=""plc""><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""main"">Schloss Cunersdorf</term><term xmlns=""http://www.tei-c.org/ns/1.0"" type=""subref-of"">Kunersdorf (heute: Ortsteil der Gemeinde Bliesdorf), Friedland-Cunersdorff</term></index>Schloss Cunersdorf</rs></figDesc></figure></ab>")

    function teisimple-test:analyze-entity-sketches($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: SECTIONS :)
declare
    %test:name("Sections")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><surface n=""2v"" xmlns=""http://www.tei-c.org/ns/1.0""><line xmlns=""http://www.tei-c.org/ns/1.0""><milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""section"" type=""Text_1"" spanTo=""#C07_3r_s""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>Einiges an Text</line></surface><surface xmlns=""http://www.tei-c.org/ns/1.0"" n=""3r""><line xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/>... und noch mehr<anchor xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_3r_s""/></line></surface></zone>")
    %test:assertEquals("<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""2v""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""section"" type=""Text_1"" spanTo=""#C07_3r_s""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "Einiges an Text", "<pb xmlns=""http://www.tei-c.org/ns/1.0"" n=""3r""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "... und noch mehr", "<anchor xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""C07_3r_s""/>")

    function teisimple-test:analyze-sections($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: Enhance tei:handShift with @new :)
declare
    %test:name("Enhance tei:handShift with @new")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear"" medium=""pencil""/></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(clear) medium(pencil)""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane"" script="""" medium=""""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear""/></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(pencil)""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(clear) medium()""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fremd""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear""/></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(pencil)""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(clear) medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script() medium(pencil)""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script(clear) medium()""/>")

    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0""><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fontane""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" new=""#Fremd""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" medium=""pencil""/><handShift xmlns=""http://www.tei-c.org/ns/1.0"" script=""clear""/></zone>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script() medium(pencil)""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fontane"" rend=""script(clear) medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script() medium()""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script() medium(pencil)""/>", "<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""handshift"" subtype=""#Fremd"" rend=""script(clear) medium()""/>")

    function teisimple-test:enhance-handshifts($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};


(: 3.27 Genetical information :)
declare
    %test:name("Enhance tei:handShift with @new")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"">Dann: Friedrich der <del xmlns=""http://www.tei-c.org/ns/1.0"" instant=""true""><seg xmlns=""http://www.tei-c.org/ns/1.0"" style=""text-decoration:line-through"" rend=""line-through-style:double"">Ernſt</seg></del></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Dann: Friedrich der ")

    function teisimple-test:genetical-information($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.19.12 deleted underlining -- added 2018-10-29 :)
declare
    %test:name("3.21.19.12 deleted underlining")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:2.6cm"">etwas <seg style=""text-decoration:underline"" rend=""underline-style:jagged_cancel"">kleiner</seg> im For-</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "etwas ", "kleiner", " im For@P")

    function teisimple-test:deleted-unterlining($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.18.1.11: addition caret:funnel -- added 2018-11-19 :)

declare
    %test:name("3.21.18.1.11: addition caret:funnel")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"">der Halle <add place=""above"" style=""margin-left:-0.4cm"" rend=""caret:funnel(0.7cm,0.5cm)"">rechts</add> ein großer 15</line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "der Halle ", "rechts", " ein großer 15")

    function teisimple-test:added-funnel($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.24.2 connection line -- added 2018-11-19 :)
declare
    %test:name("3.21.24.2 connection line")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><metamark xmlns=""http://www.tei-c.org/ns/1.0"" function=""connect"" rend=""arc""/></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>")

    function teisimple-test:connection-line($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.7.13 sketches as entities -- changed 2018-11-20 :)
declare
    %test:name("3.7.13 sketches as entities")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""illustration"" ulx=""9.6"" uly=""11.8"" lrx=""10.2"" lry=""13.2""><seg xmlns=""http://www.tei-c.org/ns/1.0""><rs xmlns=""http://www.tei-c.org/ns/1.0"" type=""direct"" ref=""plc:Gentzrode_Speicher""><figure xmlns=""http://www.tei-c.org/ns/1.0"" xml:id=""a1""><figDesc xmlns=""http://www.tei-c.org/ns/1.0""><ref xmlns=""http://www.tei-c.org/ns/1.0"" target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Gentzrode, Speicher mit Wohnturm (Seitenfront)</figDesc></figure></rs></seg></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><rs type=""direct"" ref=""plc:Gentzrode_Speicher""><index indexName=""plc""><term type=""main"">Gentzrode Speicher</term><term type=""subref-of"">Gentzrode, Gentzrode. Speicher und Wohnturm</term></index><figure xml:id=""a1"" href=""https://textgridlab.org/1.0/digilib/rest/IIIF/textgrid:164hd/pct:7.468949311849614,12.023834858480528,40.56730446458543,38.518833794424346/,1000/0/default.png"" height-in-mm=""""><figDesc><ref target=""http://vocab.getty.edu/aat/300034065"">Gebäudeaufriss</ref>; Gentzrode, Speicher mit Wohnturm (Seitenfront)</figDesc></figure></rs></ab>")

    function teisimple-test:sketches-as-entities($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.7.6 sketches with rotation -- clarified 2018-11-20 :)
declare
    %test:name("3.7.13 sketches as entities")
    %test:args("<zone xmlns=""http://www.tei-c.org/ns/1.0"" type=""illustration"" ulx=""1.3"" uly=""7.6"" lrx=""5.3"" lry=""12.8""><milestone unit=""illustration""/><zone rotate=""128"" ulx=""4.2"" uly=""11.6""><line>Peristylium</line></zone><zone rotate=""158"" ulx=""4.4"" uly=""9.2""><line>Atrium</line></zone><zone rotate=""177"" ulx=""5.7"" uly=""7.5""><line>Alae</line></zone></zone>")
    %test:assertEquals("<ab xmlns=""http://www.tei-c.org/ns/1.0"" type=""sketch""><seg type=""caption""><milestone unit=""line""/>Peristylium</seg><seg type=""caption""><milestone unit=""line""/>Atrium</seg><seg type=""caption""><milestone unit=""line""/>Alae</seg></ab>")

    function teisimple-test:sketches-with-rotation($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: 3.21.18.5 additions with vague placement -- requirements changed 2018-11-23 :)
declare
    %test:name("3.21.18.5 additions with vague placement")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0"" style=""margin-left:0.0cm"">Fachwerk<add place=""above"" style=""margin-left:0.4cm"" rend=""caret:slash"">haus</add>. 1 fenſtrige Giebelſtube. 1 breites gemüthliches </line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "Fachwerk", "haus", ". 1 fenstrige Giebelstube. 1 breites gemüthliches ")

    function teisimple-test:additions-with-vague-placement($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};

(: Two sequential tei:hi that are divided by a whitespace, e.g. C6, "Schlachtfeld von Jena und Auerstädt" :)
declare
    %test:name("Two sequential tei:hi that are divided by a whitespace")
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs><hi>J<seg style=""text-decoration:underline"">ena</seg></hi> <hi><seg style=""text-decoration:underline"">und</seg></hi> <hi><seg style=""text-decoration:underline"">Auerſtäd</seg>t</hi></rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0""><hi>Jena</hi>@<hi>und</hi>@<hi>Auerstädt</hi></rs>")    
    
    %test:args("<line xmlns=""http://www.tei-c.org/ns/1.0""><rs><hi xml:id=""some-id"" next=""#another-id"">J<seg style=""text-decoration:underline"">ena</seg></hi> <hi xml:id=""another-id"" prev=""#some-id""><seg style=""text-decoration:underline"">und</seg></hi></rs></line>")
    %test:assertEquals("<milestone xmlns=""http://www.tei-c.org/ns/1.0"" unit=""line""/>", "<rs xmlns=""http://www.tei-c.org/ns/1.0""><hi xml:id=""some-id"" next=""#another-id"">Jena</hi><hi xml:id=""another-id"" prev=""#some-id"">und</hi></rs>")

    function teisimple-test:hi-with-whitespaces($node as element(*)) {
        fontaneSimple:transform($node, "16b00")
};