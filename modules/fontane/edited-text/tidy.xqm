xquery version "3.1";

(:~
 : This modules handles the conversion of the Fontante-TEI/XML into a TEI subset
 : for the edited text. The resulting TEI is the basis for the "Edierter
 : Text" (edited text) view on the website and the book. It represents the latest
 : layer of text.
 : 
 : Its main purpose is to tidy up the intermediate TEI that has been created by
 : tei2teisimple.
 :
 : @author Michelle Weidling
 : @version 0.1
 : @since TODO
 :)

module namespace tidy ="http://fontane-nb.dariah.eu/tidy";


declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";
import module namespace functx="http://www.functx.com";
import module namespace index-info="http://fontane-nb.dariah.eu/index-info" at "index-info.xqm";
import module namespace simpleHelpers="http://fontane-nb.dariah.eu/teisimplehelpers" at "teisimplehelpers.xqm";


(: only contemporary hands (and selected posthumous hands) are considered for 
 : the edited text :)
declare variable $tidy:valid-hands :=
    for $res in collection($config:data-root || "/data")
    return
        $res//tei:handNote[@script = "contemporary"]/@xml:id/string();


declare function tidy:main($tei as node()*, $uri as xs:string) {
(:    let $tidy := tidy:enhance-handshifts($tei//tei:text):)
    let $tidy := tidy:enhance-handshifts($tei)
        => tidy:sort-out-surplus-elements()
        => tidy:sort-out-invalid-hands()
        => tidy:split-headings()
        => tidy:summarize()
        => tidy:summarize-headings()
        => tidy:summarize-notes()
        => tidy:summarize-hi()
        => tidy:sort-double-imgs()
        => tidy:tidy()
    let $header :=
        tidy:get-Fontanes-sources($tei//tei:teiHeader[parent::tei:TEI])
        => tidy:get-references-in-abstract()
    (: tei:TEI/@id is always something like 'Notizbuch A1'.
    for sorting we use the shelf number :)
    let $id-parts := tokenize($tei//tei:TEI/@id, " ")
    let $key1 := substring($id-parts[2], 1, 1)
    let $key2 := substring($id-parts[2], 2)
    let $final-tei := <TEI xmlns="http://www.tei-c.org/ns/1.0" id="{$tei//tei:TEI/@id}" key1="{$key1}" key2="{$key2}">{$header}{$tidy//tei:text}</TEI>
    let $store := xmldb:store($config:data-root || "/print/xml/", $uri || ".xml", $final-tei)
    return
        $final-tei
};

(:~
 : Returns the text that has been written by contemporary (or certain posthumous)
 : hands. Up until this point, all encoded hands and their texts are still in 
 : place.
 :)
declare function tidy:sort-out-invalid-hands($nodes as node()*)
as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            if($node/preceding::tei:milestone[@unit = "handshift"][1]
            and not(simpleHelpers:is-hand-valid($tidy:valid-hands, $node/preceding::tei:milestone[@unit = "handshift"][1]))) then
                ()
            else
                $node

        (: all lines have to be preserved because of the editorial commentary
        which references the lines in the notebooks. if we omitted @unit = "line"
        referencing wouldn't work any longer :)
        case element(tei:milestone) return
            let $prev-handshift := $node/preceding::tei:milestone[@unit = "handshift"][1]
                return
                if($node/@unit = "handshift" and
                simpleHelpers:is-hand-valid($tidy:valid-hands, $node)) then
                    tidy:construct-element($node, "post")

                else if($node/@unit = "handshift") then
                    ()

                else if($prev-handshift
                and $node/@unit = "line"
                and not(simpleHelpers:is-hand-valid($tidy:valid-hands, $prev-handshift))) then
                    tidy:construct-element($node, "post")

                else if($prev-handshift
                and not(simpleHelpers:is-hand-valid($tidy:valid-hands, $prev-handshift))) then
                    ()

                else
                    tidy:construct-element($node, "post")

        case element(tei:div) return
            (: even though it's posthumous we want to keep the text written on
            calendar pages by Friedrich Fontane. Unfortunately, Friedrich's
            handshift is oftentimes not the first hand appearing on the page
            but we want to keep the page nevertheless. :)
            if($node/@type = "Kalenderblatt"
            or $node/@type = "clipping") then
                tidy:construct-element($node, "post")
            else
                tidy:invalid-hands-default-return($node)

        default return
            tidy:invalid-hands-default-return($node)
};

declare function tidy:invalid-hands-default-return($node as node()*)
as node()* {
    let $prev-handshift := $node/preceding::tei:milestone[@unit = "handshift"][1]
    let $first-child-handshift := $node/child::tei:milestone[@unit = "handshift"][1]
    let $first-child-element := $node/child::*[1]
    let $first-child-node := $node/child::node()[1]

    return
        (: in some cases the valid handshift is the first child node
        instead of a previous node. of course we want to keep the element
        then :)
        if($first-child-element = $first-child-handshift
        (: ensure there's no text before the handshift :)
        and (normalize-space($first-child-node) = ""
            or $first-child-element = $first-child-node)
        and simpleHelpers:is-hand-valid($tidy:valid-hands, $first-child-handshift)) then
            tidy:construct-element($node, "post")


        else if($prev-handshift
        and not(simpleHelpers:is-hand-valid($tidy:valid-hands, $prev-handshift))) then
            ()

        else
            tidy:construct-element($node, "post")
};


(:~
 : Some elements aren't considered in the edited text. These encompass:
 : 
 : * subsequent handshifts of the same type
 : * certain line markers
 : * empty elements that have lost their text nodes during the sorting process
 : 
 :)
declare function tidy:sort-out-surplus-elements($nodes as node()*)
as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            $node

        case element(tei:milestone) return
            if($node/@unit = "handshift") then
                if(
                    simpleHelpers:is-prev-hand-same($node)) then
                    ()
                else
                    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                        $node/@*,
                        tidy:sort-out-surplus-elements($node/node())
                    }

            else if($node/@unit = "line"
            and ($node/ancestor::tei:seg[@type = "missing-hyphen"]
                or $node/preceding-sibling::*[1][self::tei:seg[@type = "missing-hyphen"]])) then
                ()

            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:head) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:date) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:rs) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:note) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:abbr) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:list) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        case element(tei:item) return
            if(not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }


        case element(tei:div) return
            if($node/@type = "label"
            and not($node/* or $node/node())) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:sort-out-surplus-elements($node/node())
                }

        default return
            element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                $node/@*,
                tidy:sort-out-surplus-elements($node/node())
            }
};

(:~
 : As a result of the previous sorting process, certain elements may be empty
 : at this process stage. They don't have any information value anymore and are
 : therefore removed.
 :)
declare function tidy:surplus-elements-default-return($node as node())
as element() {
    if(not($node/* or $node/node())) then
        ()
    else
        tidy:construct-element($node, "surplus")
};


declare function tidy:has-hand-text($node as element(tei:milestone))
as xs:boolean {
    let $next-handshift := $node/following::tei:milestone[@unit = "handshift"][1]
    let $nodes-between := $node/following::node()[. << $next-handshift]
    let $is-text-node :=
        for $node-between in $nodes-between
        return
            if ($node-between[self::text()]
            and not(normalize-space($node-between) = "")) then
                true()
            else
                false()
    return
        if($next-handshift
        and functx:is-value-in-sequence(true(), $is-text-node)) then
            true()
        else if(not($next-handshift)) then
            true()
        else
            false()
};

(:~
 : A constructor. Creates a TEI element with the same name and jumps back into
 : the process of sorting out surplus elements.
 : 
 : @param $node The current node
 : @param $flag Indicates the function to be called from within the constructor
 :)
declare function tidy:construct-element($node as node(), $flag as xs:string)
 {
    element {QName("http://www.tei-c.org/ns/1.0", $node/name())}{
        $node/@*,
        if($flag = "post") then
            tidy:sort-out-invalid-hands($node/node())
        else if($flag = "surplus") then
            tidy:sort-out-invalid-hands($node/node())
        else if($flag = "hs-enhance") then
            tidy:enhance-handshifts($node/node())
        else if($flag = "sources") then
            tidy:get-Fontanes-sources($node/node())
        else if($flag = "summarize") then
            tidy:summarize($node/node())
        else if($flag = "summarize-headings") then
            tidy:summarize-headings($node/node())
        else if($flag = "summarize-notes") then
            tidy:summarize-notes($node/node())
        else if($flag = "summarize-hi") then
            tidy:summarize-hi($node/node())
        else if($flag = "ref") then
            tidy:get-references-in-abstract($node/node())
        else if($flag = "double-imgs") then
            tidy:sort-double-imgs($node/node())
        else if($flag = "tidy") then
            tidy:tidy($node/node())
        else
            text{"!!!Kopieren des Elements fehlgeschlagen!!!"}
    }
};

(:~
 : Purges surplus attributes from tei:milestone[@unit = "handshift"].
 :
 : @author Michelle Weidling
 : @param $node the current tei:milestone[@unit = "handshift"]
 : @return the purged tei:milestone[@unit = "handshift"]
 :  :)
declare function tidy:clear-handshift($node as element(tei:milestone))
as element(tei:milestone) {
    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
        attribute unit {"handshift"},
        $node/(@* except (@subtype, @rend)),
        if($node/@subtype = "") then
            ()
        else
            $node/@subtype,
        if($node/@rend = "") then
            ()
        else
            $node/@rend
    }
};


declare function tidy:enhance-handshifts($nodes as node()*)
as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            $node

        case element(tei:milestone) return
            if($node/@unit = "handshift") then
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                        $node/(@* except (@script, @medium)),
                        let $script := $node/@script/string()
                        let $medium := $node/@medium/string()
                        let $enhanced-script := tidy:enhance-script($node, $script)
                        let $enhanced-medium := tidy:enhance-medium($node, $medium)
                        return
                            attribute rend {$enhanced-script || " " || $enhanced-medium}
                    }
            else
                tidy:construct-element($node, "hs-enhance")

        default return
             tidy:construct-element($node, "hs-enhance")
};

declare function tidy:enhance-script($node as node(), $script as xs:string?) as xs:string {
    if(not($script) or $script = "script()") then
        let $prev-relevant-hs := $node/preceding::tei:milestone[@unit = "handshift"][@script and not(@script = "script()")][1]
        return
            if($prev-relevant-hs) then
                $prev-relevant-hs/@script/string()
            else
                "Latf"
    else
        $script
};


declare function tidy:enhance-medium($node as node(), $medium as xs:string?) as xs:string {
    if(not($medium) or $medium = "medium()") then
        let $prev-relevant-hs := $node/preceding::tei:milestone[@unit = "handshift"][@medium and not(@medium = "medium()")][1]
        return
            if($prev-relevant-hs) then
                $prev-relevant-hs/@medium/string()
            else
                "pencil"
    else
        $medium
};



declare function tidy:split-headings($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch($node)
        case text() return
            $node

        case element(tei:head) return
            if($node//tei:lb[@type = "edited_text"]) then
                let $lb := $node//tei:lb[@type = "edited_text"]
                let $split-top := $node/node()[. << $lb]
                let $split-bottom := $node/node()[. >> $lb]
                return
                    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                        $node/@*,
                        (element tei:seg{
                            attribute type {"upper-part"},
                            $split-top
                        },
                        element tei:lb{
                            attribute type {"edited_text"}
                        },
                        element tei:seg{
                            attribute type {"lower-part"},
                            $split-bottom
                        })
                    }

            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:split-headings($node/node())
                }

        default return
            element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                $node/@*,
(:                if($node/tei:add[@type = "edited_text" and @subtype = "interlinear"]) then:)
(:                    let $add := $node/tei:add[@type = "edited_text" and @subtype = "interlinear"]:)
(:                    return:)
(:                        (element tei:seg{:)
(:                            attribute type {"upper-part"},:)
(:                            $add/node():)
(:                        },:)
(:                        element tei:lb{:)
(:                            attribute type {"edited_text"}:)
(:                        },:)
(:                        element tei:seg{:)
(:                            attribute type {"lower-part"},:)
(:                            $add/following-sibling::node():)
(:                        }):)
(:                else:)
                    tidy:split-headings($node/node())
            }
};

declare function tidy:get-Fontanes-sources($header as node()*) {
    for $node in $header return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            ()

        case element(tei:derivation) return
            element tei:derivation {
                for $ref in $node//tei:link
                    let $target := substring-after($ref/@target, " ")
                    let $abbrev := index-info:get-info-about("lit", $target, "abbrev")
                    let $confer-target := $ref/@resp/string()
                    let $confer :=
                        if($confer-target) then
                            index-info:get-info-about("lit", $confer-target, "abbrev")
                        else
                            ()
                    return
                        element tei:term {
                            $abbrev,
                            if($confer) then
                                "; vgl. " || $confer
                            else
                                ()
                        }
            }
        default return
            tidy:construct-element($node, "sources")
};



declare function tidy:summarize($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            $node

        case element(tei:milestone) return
            if($node/@unit = "line") then
                if($node/preceding::*[1][self::tei:milestone[@unit = "line"]]
                and $node/preceding::node()[1][self::tei:milestone[@unit = "line"]
                    or normalize-space(.) = ""]) then
                    ()
                else
                    tidy:construct-element($node, "summarize")
            else
                tidy:construct-element($node, "summarize")

        case element(tei:rs) return
            tidy:summarize-entries($node)

        case element(tei:div) return
            tidy:summarize-entries($node)

(:        case element(tei:list) return:)
(:                tidy:summarize-entries($node):)

        case element(tei:item) return
            tidy:summarize-entries($node)

        case element(tei:seg) return
            if($node/following-sibling::*[1][self::tei:lb[@type = "edited_text"]]
            or $node/following-sibling::*[2][self::tei:lb[@type = "edited_text"]]
            or $node/preceding-sibling::*[1][self::tei:lb[@type = "edited_text"]]
            or $node/preceding-sibling::*[2][self::tei:lb[@type = "edited_text"]]) then
                tidy:construct-element($node, "summarize")
            else
                tidy:summarize-entries($node)

        case element(tei:table) return
            tidy:summarize-entries($node)

        case element(tei:note) return
            tidy:summarize-entries($node)


        default return
            if(not($node/descendant::*[self::tei:milestone[@unit = "line"]])
                or not($node/descendant::*[self::tei:rs][@prev or @next])
                or not($node/descendant::*[self::tei:list][@prev or @next])
                or not($node/descendant::*[self::tei:item][@prev or @next])
                or not($node/descendant::*[self::tei:seg][@prev or @next])
                or not($node/descendant::*[self::tei:table][@prev or @next])) then
                tidy:construct-element($node, "summarize")
            else
                $node
};



declare function tidy:summarize-entries($node as node()) as node()* {
    (: the first element of a virtual aggregation:)
    if(($node/@next and not($node/@prev))
    or ($node/@next and $node/@prev and $node/ancestor::tei:seg[@type = "integration"])
    or ($node/@next and $node/@prev and tidy:find-corresp-node($node, "prev")[self::tei:ab[@type = "sketch"] or self::tei:anchor or self::tei:head])) then
        let $next-node := tidy:find-corresp-node($node, "next")
        let $prev-node :=
            if($node/@prev) then
                tidy:find-corresp-node($node, "prev")
            else
                ()

        return
            if($next-node[self::tei:ab]
            or $next-node[self::tei:seg[@type = "integration"]]
            or $next-node[self::tei:ab[@type = "sketch"]]
            or $next-node[ancestor-or-self::tei:seg[@type = "integration"]][not(local-name(.) = ("rs", "item", "seg"))]
            or $next-node[descendant::tei:seg[@type = "integration"]][not(local-name(.) = ("rs", "item", "seg"))]
            or not(local-name($node) = local-name($next-node))
            ) then
                tidy:construct-element($node, "summarize")
            else if($prev-node/ancestor::tei:seg[@type = "integration"]) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/(@* except @next),
                    tidy:apply-all-nexts($node, "summarize")
                }
    else if($node/@prev) then
        let $prev-node := tidy:find-corresp-node($node, "prev")
        return
            (: in some cases we have elements that have been part of a virtual aggregation
            but their preceding aggregation element is lost, e.g. in case of a
            tei:line pointing to a tei:zone. since we don't keep lines, this
            link is lost, but we still want to keep that zone. :)
            if($prev-node[@type = "vertical-mark"] and not($node[@type = "vertical-mark"])
            or not(local-name($node) = local-name($prev-node))) then
                tidy:construct-element($node, "summarize")
            else if($prev-node) then
                ()
            else
                tidy:construct-element($node, "summarize")

    (: nodes that aren't part of the virtual aggregation :)
    else
        tidy:construct-element($node, "summarize")
};


declare function tidy:apply-all-nexts($node as node(), $flag as xs:string)
as node()* {
    (: entry point of virtual aggregation:)
    if($node/@next and not($node/@prev)) then
        let $next-node := tidy:find-corresp-node($node, "next")
        let $nodes-inbetween := $node/following-sibling::node()[. << $next-node]
        let $nodes-inbetween := ($nodes-inbetween, $node/following::node()[. << $next-node][self::tei:milestone])
        let $is-next-node-nested :=
            if($nodes-inbetween/descendant::* = $next-node) then
                true()
            else
                false()
        return
            if(count($next-node) = 1) then
                (
                    if($flag = "hi") then
                        tidy:summarize-hi($node/node())
                    else
                        tidy:summarize($node/node()),
                    $nodes-inbetween,
                    if($is-next-node-nested) then
                        ()
                    else
                        tidy:apply-all-nexts($next-node, $flag)
                )
            else
                if($flag = "hi") then
                    tidy:summarize-hi($node/node())
                else
                    tidy:summarize($node/node())

    (: last of a virtual aggregation: exit point :)
    else if(not($node/@next)) then
        if($flag = "hi") then
            tidy:summarize-hi($node/node())
        else
            tidy:summarize($node/node())

    (: element in the middle of a virtual aggregation:)
    else
        let $next-node := tidy:find-corresp-node($node, "next")
        let $nodes-inbetween := $node/following-sibling::node()[. << $next-node]
        let $nodes-inbetween := ($nodes-inbetween, $node/following::node()[. << $next-node][self::tei:milestone])
        let $is-next-node-nested :=
            if($nodes-inbetween/descendant::* = $next-node) then
                true()
            else
                ()
        return
            if(count($next-node) = 1) then
                (
                    if($flag = "hi") then
                        tidy:summarize-hi($node/node())
                    else
                        tidy:summarize($node/node()),
                    $nodes-inbetween,
                    if($is-next-node-nested) then
                        ()
                    else
                        tidy:apply-all-nexts($next-node, $flag)
                )
            else
                if($flag = "hi") then
                    tidy:summarize-hi($node/node())
                else
                    tidy:summarize($node/node())
};


declare function tidy:exclude-nested-hi($nodes as node()*, $hi-to-exclude as element(tei:hi)) as node()* {
    for $node in $nodes return
        typeswitch ($node)
        case text() return
            $node

        case comment() return
            $node

        default return
            if($node = $hi-to-exclude) then
                ()
            else
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    tidy:exclude-nested-hi($node/node(), $hi-to-exclude)
                }
};

declare function tidy:find-corresp-node($node as node(), $flag as xs:string) {
    let $target-id :=
        if($flag = "next") then
            tidy:get-id($node/@next)
        else if($flag = "prev") then
            tidy:get-id($node/@prev)
        else
            error(QName("FONTANE", "tidySimple001"), "Invalid flag: " || $flag || "." )
    return
        $node/root()//*[@xml:id = $target-id][1]
};


declare function tidy:get-id($target as xs:string) as xs:string {
    $target => replace("#", "")
};



(:
 : Resolves all tei:ptr in a tei:abstract (which is displayed in the Ueberblicks-
 : kommentar) and places the resp. abbreviated bibliographical reference instead.
 :
 : @param $nodes Nodes within the tei:teiHeader
 : @return processed nodes
 : @author Michelle Weidling
 :)
declare function tidy:get-references-in-abstract($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            ()

        case element(tei:ptr) return
            if($node/ancestor::tei:abstract) then
                let $target-id := replace($node/@target, "#", "")
                let $abbrev := index-info:get-info-about("lit", $target-id, "abbrev")
                return
                    text{$abbrev || " "}
            else
                tidy:construct-element($node, "ref")

        default return
            tidy:construct-element($node, "ref")
};


declare function tidy:sort-double-imgs($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            $node

        case element(tei:ab) return
            if($node//tei:figure[@type = "double"]) then
                if($node/@next) then
                    let $next-id := $node/@next
                        => replace("#", "")
                    let $corresp-node := $node/root()//*[@xml:id= $next-id]
                    return
                        ($node, $corresp-node)
                else
                    ()
            else
                tidy:construct-element($node, "double-imgs")

        default return
            tidy:construct-element($node, "double-imgs")
};


declare function tidy:summarize-headings($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            $node

        case element(tei:head) return
            if($node/@next) then
                let $next-id := tidy:get-id($node/@next)
                let $corresp-node := tidy:find-corresp-node($node, "next")

                return
                    if($corresp-node[self::tei:seg]) then
                        element tei:head {
                            $node/@*,
                            $node/node()
                        }
                    else
                        $node
            else if($node/@prev) then
                ()
            else
                tidy:construct-element($node, "summarize-headings")

        default return
            tidy:construct-element($node, "summarize-headings")
};


(:~ A function for summarizing tei:note, especially for authorial notes  :)
declare function tidy:summarize-notes($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case comment() return
            $node

        case element(tei:note) return
            if($node/@next) then
                let $next-id := tidy:get-id($node/@next)
                let $corresp-node := tidy:find-corresp-node($node, "next")

                return
                    element tei:note {
                        $node/(@* except @next),
                        $node/node(),
                        $corresp-node/node()
                    }
            else if($node/@prev) then
                ()
            else
                tidy:construct-element($node, "summarize-notes")

        default return
            tidy:construct-element($node, "summarize-notes")
};


(:~ A function for summarizing tei:hi  :)
declare function tidy:summarize-hi($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            (: this should avoid duplicates :)
            if($node/ancestor::tei:rs
            and $node/preceding-sibling::*[self::tei:hi[@next]]
            and $node/preceding-sibling::*[self::tei:hi[@next]]/following-sibling::text()[1] = $node) then
                ()
            else
                $node

        case comment() return
            $node

        case element(tei:hi) return
            (: first element of virtual aggregation :)
            if($node/@next and not($node/@prev)) then
                let $next := tidy:find-corresp-node($node, "next")
                let $nodes-inbetween := $node/following::*[. << $next]
                return
                    if($nodes-inbetween[self::tei:milestone[@unit = "paragraph"]]
                    or $next/ancestor::* = $node
                    or not(local-name($next) = "hi")) then
                        tidy:construct-element($node, "summarize-hi")
                    else
                        element tei:hi {
                            $node/(@* except @next),
                            tidy:apply-all-nexts($node, "hi")
                        }
            (: middle or last element o virutal aggregation:)
            else if($node/@prev) then
                if(tidy:find-corresp-node($node, "prev") = $node/ancestor::*) then
                    ()
                else
                    let $prev := tidy:find-corresp-node($node, "prev")
                    let $nodes-inbetween := $node/preceding::*[. >> $prev]
                    return
                        if($nodes-inbetween[self::tei:milestone[@unit = "paragraph"]]
                        or not(local-name($node) = local-name($prev))) then
                            tidy:construct-element($node, "summarize-hi")
                        else
                            ()
            (: not part of virtual aggregation at all :)
            else
                tidy:construct-element($node, "summarize-hi")

        default return
            tidy:construct-element($node, "summarize-hi")
};


declare function tidy:tidy($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            if(normalize-space($node) = "") then
                if(
                    ($node/preceding::*[1][self::tei:milestone]
                    and $node/following::*[1][self::tei:milestone])
                ) then
                    ()
                else
                    tidy:fix-text($node)

            else if(ends-with($node, "- ")
            and $node/following::*[1][self::tei:milestone[@unit = "line"]]) then
                let $fixed :=
                    tidy:fix-text($node)
                    => functx:right-trim()
                return
                    text{$fixed}

            else if(ends-with($node, "@P ")
            and $node/following::*[1][self::tei:milestone[@unit = "line"]]) then
                let $fixed :=
                    tidy:fix-text($node)
                    => functx:right-trim()
                return
                    text{$fixed}

            else
                tidy:fix-text($node)

        case element(tei:milestone) return
            if($node[@unit = "line"]) then
                if(($node/following::node()[1][self::tei:milestone[@unit = "line"]]
                    or normalize-space($node/following::node()[1]) = "")
                and $node/following::*[1][self::tei:milestone[@unit = "line"]]) then
                    ()
                else
                    $node
            else if($node[@unit = "line"]
            and $node/following::node()[1][self::tei:milestone[@unit = "handshift"]]) then
                ()
            else
                $node

        case element(tei:figure) return
            if($node/ancestor::tei:ab) then
                tidy:construct-element($node, "tidy")
            else
                ()

        case element(tei:seg) return
            if($node[@type = "verse"] and not($node//text())) then
                ()
            else if($node[@type = "integration"] and count($node/*) = 2) then
                ()
            else if($node//* or $node//node()) then
                tidy:construct-element($node, "tidy")
            else
                ()

        case element(tei:div) return
            if($node//text()[not(normalize-space(.) = "")]) then
                tidy:construct-element($node, "tidy")
            else
                ()

        case element(tei:list) return
            if($node//text()[not(normalize-space(.) = "")]) then
                tidy:construct-element($node, "tidy")
            else
                ()


        default return
            tidy:construct-element($node, "tidy")
};

declare function tidy:fix-text($node as text()) as text() {
    let $special-chars := "\.,\)\?\-!"
    let $string :=
        replace($node, " \-", "-")
        => replace("@P ", "@P")
        => replace("\[@@", "[")
        => replace(" \)", ")")
        => replace(" ;", ";")
    let $string :=
        if(ends-with($string, "@@")) then
            let $next-char := substring($node/following::text()[1], 1, 1)
            return if(matches($next-char, $special-chars)) then
                substring($string, 1, string-length($string) - 2)
            else
                $string
        else
            $string
    let $string :=
        if(normalize-space($string) = "") then
            let $next-char := substring($node/following::text()[1], 1, 1)
            return if(matches($next-char, $special-chars)) then
                substring($string, 1, string-length($string) - 2)
            else
                $string
        else
            $string

    return
        text{$string}
};
