xquery version "3.1";

(:~
 : This modules handles the conversion of the Fontante-TEI/XML into TEI simplePrint
 : for the edited text. The resulting TEI simplePrint is the basis for the "Editerter
 : Text" (edited text) view on the website and the book. It represents the latest
 : layer of text.
 :
 : @author Michelle Weidling
 : @version 0.0.2
 : @since TODO
 :)

module namespace fontaneSimple="http://fontane-nb.dariah.eu/teisimple";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";
import module namespace functx="http://www.functx.com";
import module namespace simpleHelpers="http://fontane-nb.dariah.eu/teisimplehelpers" at "teisimplehelpers.xqm";
import module namespace tbleapi="https://fontane-nb.dariah.eu/tble" at "../tble-api.xqm";
import module namespace index-info="http://fontane-nb.dariah.eu/index-info" at "index-info.xqm";

(:~
 : The main function initiates the transformation of a given notebook.
 :
 : :)
declare function fontaneSimple:main($doc as node()*, $uri as xs:string, $log as
xs:string) as element(tei:teiCorpus) {
    let $front-covers := $doc//tei:sourceDoc/tei:surface[contains(@n, "front_cover")]
    let $back-covers := $doc//tei:sourceDoc/tei:surface[contains(@n, "back_cover")]
    let $content := ($doc//tei:sourceDoc/tei:surface[not(contains(@n, "cover")
        or matches(@n, "spine"))], $doc//tei:sourceDoc/tei:note[@type = "editorial"])

    let $tei :=
        <teiCorpus xmlns="http://www.tei-c.org/ns/1.0">
          <TEI id="{$doc//tei:teiHeader//tei:titleStmt/tei:title/string()}">
            {$doc//tei:teiHeader}
            <text>
                <front>{fontaneSimple:transform($front-covers, $uri, $log)}</front>
                <body>{fontaneSimple:transform($content, $uri, $log)}</body>
                <back>{fontaneSimple:transform($back-covers, $uri, $log)}</back>
            </text>
          </TEI>
        </teiCorpus>

    let $store := xmldb:store($config:data-root || "/print/xml/", $uri || "-tmp.xml", $tei)
    return $tei
};

(:~
 : Recursivly iterates the passed nodes and converts them according to the
 : requirements for the "Edierter Text". While it convers almost all of the
 : requirements stated in the encoding documentation (c.f.
 : https://fontane-nb.dariah.eu/doku.html), some parts of it are handled in a
 : second step (c.f. TODO) - especially the removal of tei:handShift duplicates
 : and the tei:milestone expansion to tei:div[@type = "section"] resp. tei:p -
 : because it is easier to perform these steps after the XML hierarchy has been
 : flattened a bit.
 :
 : @author Michelle Weidling
 : @param $nodes the elements of the book covers and the book content
 : @return $node()* a TEI simplePrint element
 :)
declare function fontaneSimple:transform($nodes as node()*, $uri as xs:string,
$log as xs:string) as node()* {
    for $node in $nodes
      return
        typeswitch ($node)
        case text() return
            if($node/ancestor::tei:line
            or $node/ancestor::tei:figDesc
            or $node/ancestor::tei:desc[@type = "edited_text"]
            or $node/ancestor::tei:note[@type = "editorial"]
            or $node/ancestor::tei:note[@type = "authorial" and @subtype = ("footnote", "revision", "text")]
            or $node/ancestor::tei:seg[@type = ("editorial-label", "integration")]
            or $node/ancestor::tei:ref[not(matches(@target, "getty") or matches(@target, "xpath"))]
            or $node/ancestor::tei:seg[@type = "heading"]) then
(:                if($node/parent::tei:rs and starts-with($node, " "):)
(:                and not($node/preceding-sibling::*[1][self::tei:handShift or self::tei:hi]):)
(:                and simpleHelpers:is-trimming-necessary($node)) then:)
(:                    simpleHelpers:prepare-text(text{substring-after($node, " ")}):)
(:                else:)
                    simpleHelpers:prepare-text($node)
            else
                ()

        case element(tei:lb) return
            if($node[@break = "keepHyphen"]) then
                ()

            else if($node/ancestor::tei:seg[@type = "editorial-label"]) then
                text{" "}

            else if($node[@break = "no"]
            and not($node/preceding-sibling::tei:line[1]/child::*[last()][self::tei:choice])) then
                fontaneSimple:mark-intervention($node, $uri, $log)

            else if($node[@break = "no"]
            and $node/preceding-sibling::tei:line[1]/child::*[last()][self::tei:choice]) then
                ()

            else
                fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:g) return
            if($node[@ref = "#vds"]) then
                ()
            else if($node/@ref ="#rth" or $node/@ref ="#hb") then
                fontaneSimple:copy-element($node, $uri, $log)
            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:del) return
            if($node/parent::tei:restore) then
                fontaneSimple:transform($node/node(), $uri, $log)
            else if($node/descendant::tei:restore) then
                fontaneSimple:transform($node/descendant::tei:restore, $uri, $log)
            else
                ()

        case element(tei:restore) return
            if(count($node/child::*) = 1 and $node/child::tei:del
            and $node/ancestor::tei:del) then
                ()
            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:retrace) return
            if($node/@rend) then
                element tei:seg {
                    attribute rendition {$node/@rend},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }
            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:add) return
            if($node/@type = "edited_text"
            or $node/child::tei:seg[@type = "multiphrase"]) then
                fontaneSimple:copy-element($node, $uri, $log)

            else if($node/@cause = "unclear") then
                ()

            else if($node/@cause ="catchword") then
                ()

            else if($node[replace($node/@copyOf, "#", "") = $node/root()//tei:seg/@xml:id]) then
                fontaneSimple:mark-intervention($node, $uri, $log)

            else if($node/@rend ="|") then
                fontaneSimple:transform($node/node(), $uri, $log)

            else if($node/@place = ("above", "below")
(:            and :)
(:            ( :)
(:                $node/preceding::node()[1][normalize-space(.) = ""]:)
(:                or $node/ancestor::tei:seg[@type = "transposed"]):)
(:                or ends-with($node/preceding-sibling::*[1][self::tei:del], ","):)
            )
            then
                (
                    if(not($node/preceding-sibling::node()[1][self::text()][matches(substring(., string-length(.), 1), "[a-z]")])
                    and not($node/preceding-sibling::*[1][self::tei:del])) then
                        text{" "}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log),
                    if($node/following::node()[1][normalize-space(.) = ""]
                    or $node/ancestor::tei:seg[@type = "transposed"]
                    or matches(substring($node/following::text()[1],1 ,1), "[A-Z]")) then
                        text{" "}
                    else
                        ()
                )

            else if($node[@place = "superimposed"]) then
                (
                    if(ends-with($node/preceding-sibling::*[1][self::tei:del], ",")) then
                        text{" "}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                )


            else if(not($node/@xml:id)) then
                fontaneSimple:transform($node/node(), $uri, $log)

            else if(simpleHelpers:is-transposed($node)) then
                let $corresp := $node/root()//tei:metamark[matches(@target, $node/@xml:id)]
                return
                    (fontaneSimple:transform($corresp/node(), $uri, $log),
                    fontaneSimple:transform($node/node(), $uri, $log))

            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:addSpan) return
            if($node/@type = "edited_text") then
                fontaneSimple:copy-element($node, $uri, $log)
            else
                ()

        case element(tei:fw) return
            ()

        case element(tei:line) return
            if($node[ancestor::tei:seg[@type = "editorial-label"]]) then
                fontaneSimple:transform($node/node(), $uri, $log)

            else if($node/@type = "heading") then
                fontaneSimple:make-head($node, $uri, $log)

            else if(simpleHelpers:has-valid-style($node)
            or matches($node/@rendition, "black_letter")
            or matches($node/@rendition, "roman")) then
                fontaneSimple:make-seg-with-rendition($node, $uri, $log)

            else if($node/following::*[1]/local-name() = "lb" and $node/following::*[1]/@break = "no"
            and not($node/child::*[last()][self::tei:choice])) then
                (simpleHelpers:start-line($node),
                simpleHelpers:trim-last-char($node))

            else if($node/preceding::*[1]/local-name() = "lb" and $node/preceding::*[1]/@break = "no"
            and not($node/child::*[1][self::tei:choice])) then
                (simpleHelpers:start-line($node),
                simpleHelpers:trim-first-char($node))

            else if($node/@type = "verse"
            or $node/ancestor::tei:zone[@type = "verse"]) then
                if($node/@prev) then
                    ()

                else if(not($node/@next)) then
                    element tei:l {
                            if($node/@rend) then
                                attribute subtype {$node/@rend}
                            else
                                (),
                            fontaneSimple:transform($node/node(), $uri, $log)
                        }

                (: 3.8.2.2.1.3 Vers mit anderer Beschriftung in einer Zeile :)
                else
                    let $corresp := $node/following::*[@type = "verse"
                        and replace($node/@next, "#", "") = @xml:id]
                    return
                        element tei:l {
                            fontaneSimple:transform($node/node(), $uri, $log),
                            simpleHelpers:start-line($node),
                            fontaneSimple:transform($corresp/node(), $uri, $log)
                        }

            else if($node/parent::tei:zone[@type = "verse"]/child::*[1] = $node) then
                fontaneSimple:transform($node/node(), $uri, $log)

            (: with opting for a tei:milestone we try to meet the project's
            requirements on the one hand while avoiding to run into hierarchical
            problems in the further processing on the other hand:)
            else if(not($node/@type = "item")) then
                (simpleHelpers:start-line($node),
                fontaneSimple:transform($node/node(), $uri, $log))

            else if($node/@type = "item") then
                element tei:item {
                    $node/(@* except (@rend, @type, @style)),
                    if($node/@rend) then
                        attribute rendition {$node/@rend}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else
                (simpleHelpers:start-line($node),
                fontaneSimple:transform($node/node(), $uri, $log))

        case element(tei:handShift) return
            if($node/ancestor::tei:seg[@type = "editorial-label"]) then
                ()
            else
                fontaneSimple:enhance-handshift($node)

        case element(tei:stamp) return
            if($node/ancestor::tei:surface[matches(@n, "Beilage")]) then
                fontaneSimple:copy-element($node, $uri, $log)
            else
                ()

        case element(tei:seg) return
            if(count($node/*) = 1 and
            (($node/tei:stamp
                and not($node/ancestor::tei:surface[matches(@n, "Beilage")]))
            or $node/tei:metamark[@function = "caret"]))
                then
                    ()

            else if($node/@type = "heading") then
                fontaneSimple:make-head($node, $uri, $log)

            else if($node[@style = "text-decoration:underline" and @rend = "underline-medium:blue_pencil"]) then
                element tei:seg {
                    attribute type {"blue-underlined"},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }


            else if(matches($node/@style, "underline")
            and not(simpleHelpers:has-valid-style($node))) then
                fontaneSimple:transform($node/node(), $uri, $log)

            else if(simpleHelpers:has-valid-style($node)
            or matches($node/@rendition, "black_letter")
            or matches($node/@rendition, "roman")) then
                fontaneSimple:make-seg-with-rendition($node, $uri, $log)

            else if($node/@type = "initials"
            or $node/@type = "monogram"
            or $node/@type = "multiphrase"
            or $node/@type = "highlighted-area"
            or $node/@xml:lang)
                then
                    (fontaneSimple:copy-element($node, $uri, $log),
                    if($node/@type = "multiphrase") then
                        text{" "}
                    else
                        ())

            else if($node/@type = "auction_number"
            or $node/@type = "cancel"
            or $node/@type = "abort"
            or $node/@function ="unknown")
                then
                    ()

            else if($node/ancestor::tei:add and $node/@copyOf) then
                fontaneSimple:mark-intervention($node, $uri, $log)


            else if($node/@type = "said") then
                if($node/@next) then
                    let $next := replace($node/@next, "#", "")
                    let $corresp := $node/following::*[@xml:id = $next]
                    return
                        (element tei:seg {
                            $node/@type,
                            fontaneSimple:transform($node/node(), $uri, $log),
                            fontaneSimple:transform($corresp/node(), $uri, $log)
                        },
                        element tei:lb {
                            attribute type {"edited_text"}
                        })
                else
                    element tei:seg {
                        $node/@type,
                        fontaneSimple:transform($node/node(), $uri, $log)
                    }

            else if($node/@type = "item") then
                element {QName("http://www.tei-c.org/ns/1.0", $node/@type)} {
                    $node/(@xml:id, @subtype, @rendition, @prev, @next),
                    if($node/@rend) then
                        attribute rendition {$node/@rend}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = ("integration", "editorial-label", "col", "marked_off", "transposed")) then
                element tei:seg {
                    $node/@*,
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@xml:id) then
                element tei:seg {
                    $node/@*,
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:hi) return
            if($node/ancestor::tei:seg[@style = "text-decoration:underline" and @rend = "underline-medium:blue_pencil"]
            or $node/descendant::tei:seg[@style = "text-decoration:underline" and @rend = "underline-medium:blue_pencil"]) then
                element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    attribute type {"blue-underlined"},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else
                (element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                    $node/@*,
                    fontaneSimple:transform($node/node(), $uri, $log)
                },
                if(not($node/@prev or $node/@next)
                and $node/following::node()[1][self::text()][normalize-space(.) = ""]
                and $node/following::*[1][self::tei:hi]) then
                    text{"@"}
                else
                    ()
                )

        (: TODO if $node/@type = "highlighted" then make
        a hi[@type = "vertical-mark"] in the second stage of creating the
        simple format. use simpleHelpers:get-xml-chunk($node) for this.:)
        case element(tei:mod) return
            if($node/@type = "highlighted"
            and simpleHelpers:is-hand-contemporary($node/@hand)) then
                $node
(:            else if($node/following::node()[1][self::text()]) then:)
(:                fontaneSimple:preserve-whitespace($node, $uri, $log):)
            else
                fontaneSimple:transform($node/node(), $uri, $log)

        case element(tei:anchor) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:surface) return
            if(matches($node/@n, "cover")) then
                (fontaneSimple:make-pb-with-type($node/@n),
                fontaneSimple:transform($node/node(), $uri, $log))

            else if(matches($node/@n, "Beilage")) then
                element tei:div {
                    $node/@*,
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "pocket") then
                element tei:div{
                    $node/(@* except (@n, @ulx, @uly, @lry, @lrx)),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if(simpleHelpers:is-page($node)
            and $node/@type = "clipping") then
                if($node/@subtype = "Kalenderblatt"
                and contains($node//tei:handShift/@new, "Friedrich_Fontane")) then
                    (fontaneSimple:make-pb($node),
                    element tei:div {
                        $node/@n,
                        attribute type {$node/@subtype},
                        fontaneSimple:transform($node/node(), $uri, $log)
                    })

                else if(not($node/@subtype = "Kalenderblatt")) then
                    element tei:div {
                        $node/(@* except (@n, @ulx, @uly, @lry, @lrx, @facs)),
                        fontaneSimple:transform($node/node(), $uri, $log)
                    }

                else
                    ()

            else if(simpleHelpers:is-page($node)) then
                (
                    if(ends-with($node//text()[matches(., "[\w]")][last()], "-")
                    or ends-with($node//text()[matches(., "[\w]")][last()], "⸗")) then
                        ()
                    else
                        text{" "},
                    fontaneSimple:transform($node/node(), $uri, $log)
                )

            else if($node/@type = "label" and
            (contains($node/@subtype, "Fontane")
            or contains($node/@subtype, "Hersteller")
            or contains($node/@subtype, "Firmen"))
            and not($node/@attachment = "glued-posthumous")
            ) then
                fontaneSimple:make-div($node, $uri, $log)

            else
                ()

        case element(tei:milestone) return
            if($node/@unit = "illustration") then
                ()
            (: during sort.xqm we already create handshifts, but they lack
            information about who if the author. :)
            else if($node/@unit = "handshift") then
                fontaneSimple:enhance-handshift($node)
            else
                fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:gap) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:supplied) return
            fontaneSimple:mark-supplied($node, $uri, $log)

        case element(tei:metamark) return
            if($node/@function = ("integrate", "authorial_note")) then
                element tei:ab {
                    $node/@*
                }
            else if($node/@function = ("placeholder", "etc.",
            "footnote-mark", "footnotes", "ellipsis", "paragraph", "verse_marker")) then
                element tei:ab {
                    attribute type {$node/@function},
                    if($node/@function = "verse_marker") then
                        attribute rend {$node/@rend}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }
            else
                ()

        case element(tei:surplus) return
            fontaneSimple:mark-intervention($node, $uri, $log)

        case element(tei:zone) return
            (
            if($node/@subtype = "referenced") then
                element tei:ab {
                    $node/@xml:id,
                    attribute type {"referenced"}
                }
            else
                (),
            if(matches($node/@style, "border-style:solid")
            and not(matches($node/@style, "border-radius"))
            and not($node/@rend = "border-style:house")) then
                element tei:seg {
                    attribute type {"framed"},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if(matches($node/@rend, "border-bottom-style:brace")) then
                (fontaneSimple:transform($node/node(), $uri, $log),
                element tei:ab {
                    attribute type {"bottom-brace"}
                })

            else if($node/@type = "cancel") then
                ()

            else if($node/@type = "marked_off") then
                element tei:div {
                    $node/(@type, @xml:id, @next, @prev),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "highlighted") then
                if($node/child::tei:zone[@type = "highlighted"]) then
                    fontaneSimple:transform($node/node(), $uri, $log)
                else
                    element tei:hi {
                        $node/(@xml:id, @prev, @next),
                        attribute type {"vertical-mark"},
                        fontaneSimple:transform($node/node(), $uri, $log)
                    }

            else if($node/@type = ("illustration", "printed_illustration")) then
                if(not($node//tei:figure/parent::tei:del)) then
                    element tei:ab {
                        (if($node/child::tei:zone[@type = "illustration"]) then
                            attribute type {"composed-sketch"}
                        else
                            (attribute type {"sketch"},
                            if($node/parent::tei:zone[@type = "illustration"]) then
                                attribute rendition {"margin-left:" || $node/@ulx
                                || "cm; " || "margin-top:" || $node/@uly || "cm"}
                            else
                                (),
                            if($node/ancestor::tei:surface/@next and not($node/@xml:id)) then
                                (attribute xml:id {$node/ancestor::tei:surface/@xml:id},
                                attribute next {$node/ancestor::tei:surface/@next})
                            else if($node/ancestor::tei:surface/@prev and not($node/@xml:id)) then
                                (attribute xml:id {$node/ancestor::tei:surface/@xml:id},
                                attribute prev {$node/ancestor::tei:surface/@prev})
                            else
                                ()
                            )
                        ),
                        attribute width {$node/@lrx - $node/@ulx || "cm"},
                        attribute height {$node/@lry - $node/@uly || "cm"},
                        $node/(@xml:id),
                        fontaneSimple:transform($node/node(), $uri, $log)
                    }
                else
                    ()

            else if($node/parent::tei:zone/@type = ("illustration", "printed_illustration")) then
                element tei:seg {
                    attribute type {"caption"},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "heading") then
                fontaneSimple:make-head($node, $uri, $log)

            else if($node/@type = "list"
            and $node//tei:seg[@type = "col"]) then
                element tei:table {
                    $node/@*,
                    for $line in $node/tei:line[descendant::tei:seg[@type = "col"]] return
                        element tei:row {
                            for $seg in $line/descendant::tei:seg[@type = "col"] return
                                element tei:cell {
                                    fontaneSimple:transform($seg/node(), $uri, $log)
                                }
                        }
                }

            else if($node/@type = ("list", "item")) then
                element {QName("http://www.tei-c.org/ns/1.0", $node/@type)} {
                    $node/(@xml:id, @subtype, @rendition, @prev, @next),
                    if($node/@rend) then
                        attribute rendition {$node/@rend}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = ("dialogue", "edited_text", "multiphrase")) then
                element tei:div {
                    $node/@*,
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = ("verse", "said")) then
                element tei:seg {
                    attribute type {$node/@type},
                    if($node/@rend) then
                        attribute subtype {$node/@rend}
                    else
                        (),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "toc"
            and matches($node/@subtype, "ungültig")) then
                ()

            else if($node/@type = "toc") then
                element tei:div {
                    $node/(@type, @subtype, @xml:id),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "legend") then
                element tei:div {
                    (if($node/@style
                    or $node/@rendition) then
                        attribute rendition {simpleHelpers:filter-rendition($node)}
                    else
                        ()),
                    $node/(@* except (@rendition, @style)),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@type = "additional") then
                element tei:ab {
                    $node/(@* except (@ulx, @uly, @lry, @lrx)),
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if(simpleHelpers:has-valid-style($node)
            or matches($node/@rendition, "black_letter")
            or matches($node/@rendition, "script")
            or matches($node/@rendition, "roman")) then
                fontaneSimple:make-seg-with-rendition($node, $uri, $log)

            else if($node/@rend = "indent") then
                element tei:div {
                    attribute type {$node/@rend},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if(not($node/@xml:id)) then
                fontaneSimple:transform($node/node(), $uri, $log)

            else if($node/@xml:id) then
                element tei:seg {
                    $node/@xml:id,
                    $node/@prev,
                    $node/@next,
                    $node/@corresp,
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else if($node/@rend) then
                element tei:seg {
                    attribute subtype {$node/@rend},
                    fontaneSimple:transform($node/node(), $uri, $log)
                }

            else
                fontaneSimple:transform($node/node(), $uri, $log)
            )

        case element(tei:said) return
            (element tei:seg {
                attribute type {"said"},
                fontaneSimple:transform($node/node(), $uri, $log)
            },
            if($node/ancestor::tei:zone[@type = "dialogue"]/descendant::tei:said[position() != last()] = $node) then
                element tei:lb {
                        attribute type {"edited_text"}
                }
            else
                ())

        case element(tei:figure) return
            if(matches($node/descendant::tei:ref, "Schlusslinie")
            and not(matches($node/descendant::tei:ref, "ehemalig")))
                then
                    element tei:ab {
                        switch ($node/descendant::tei:ref)
                            case "horizontale einfache Schlusslinie" return
                                attribute type {"long-end-line"}
                            case "Schlusslinie; horizontale Halbschleife von links oben nach rechts" return
                                attribute type {"long-end-line"}
                            case "horizontale einfache Schlusslinie (gewellt)" return
                                attribute type {"long-end-line"}
                            default return
                                attribute type {"end-line"}
                    }

                else if(matches($node/descendant::tei:ref, "Absatzlinie")
                (: in case of double paragraph lines the single lines are
                encoded with "oberer" resp. "unterer Teil", but we only
                serialize the encoding for the upper line :)
                and not(matches($node/descendant::tei:ref, "unterer Teil"))
                and not(matches($node/descendant::tei:figDesc, "unsicher"))) then
                    element tei:ab {
                        if(matches($node/descendant::tei:ref, "doppelt"))
                            then
                                attribute type {"short-paragraph-line-double"}
                            else
                                attribute type {"short-paragraph-line"}
                    }

                else if(matches($node/descendant::tei:ref, "doppelter Summenstrich")) then
                    element tei:ab {
                        attribute type {"double-sum"}
                    }

                else if(matches($node/descendant::tei:ref, "Summenstrich")) then
                    element tei:ab {
                        attribute type {"sum"}
                    }

                else if(matches($node/descendant::tei:ref, "Differenzstrich")) then
                    element tei:ab {
                        attribute type {"difference"}
                    }


                (: 3.7.13.2 Nicht ermittelte Skizzen :)
                else if(not($node/@xml:id)) then
                    ()

                else if($node/ancestor::tei:surface[@next or @prev]) then
                    fontaneSimple:make-img($node, $uri, $log,"double")

                else if($node/ancestor::tei:zone[@type = ("illustration", "printed_illustration")]) then
                    fontaneSimple:make-img($node, $uri, $log, "")

                else
                    ()

        case element(tei:note) return
            if($node/@type = "authorial"
            and not($node/@subtype = ("footnote", "text"))) then
                ()
            else
                fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:certainty) return
            element tei:note {
                attribute type {"editorial"},
                attribute subtype {"certainty"},
                $node/@cert,
                $node/@target,
                fontaneSimple:transform($node/node(), $uri, $log)
            }

        case element(tei:figDesc) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:ref) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:space) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:choice) return
            if($node[ancestor::tei:seg[@type = "editorial-label"]]) then
                $node/tei:abbr
            else if($node/tei:corr) then
                fontaneSimple:mark-missing-syllable($node, $uri, $log)
            else
                fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:abbr) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:expan) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:sic) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:corr) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:rs) return
            let $index-type := substring-before($node/@ref, ":")
            return
                (: ignore tei:rs elements in labels for the editorial commentary :)
                if($node[ancestor::tei:seg[@type = "editorial-label"]]) then
                    fontaneSimple:transform($node/node(), $uri, $log)

                (: ignore tei:rs elements that are empty after presorting. this
                is important because they would cause a lot of whitespace trouble
                otherwise :)
                else if($node//text()[matches(., "[\w]")]) then
                    (
                    element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                        $node/@*,
                        if($node/@prev) then
                            ()
                        else
                            fontaneSimple:make-index-infos($node, $index-type),
                        fontaneSimple:transform($node/node(), $uri, $log)
                    },
                    if(not($node//tei:abbr/text()[ends-with(., ":")]
                    or ($node//text()[last()][ends-with(., "-")] or $node/following-sibling::*[1][self::tei:rs])
                    or $node/following::text()[1][starts-with(., "-") or starts-with(., ".") or starts-with(., ")")]
                    or $node/following::text()[1][matches(substring(., 1, 1), "[\w]")]
                    or $node/following::*[1][self::tei:handShift])
(:                    or $node/following-sibling::node()[1][starts-with(., "-") or starts-with(., ".")]):)
                    ) then
                        text{" "}
                    else
                        ()
                    )
                else
                    ()

        case element(tei:date) return
            element {QName("http://www.tei-c.org/ns/1.0", $node/name())} {
                (if($node/@when-iso) then
                    attribute when {$node/@when-iso}
                else
                    ()),
                (if($node/@from-iso) then
                    attribute from {$node/@from-iso}
                else
                    ()),
                (if($node/@to-iso) then
                    attribute to {$node/@to-iso}
                else
                    ()),
                (if($node/@notAfter-iso) then
                    attribute notAfter {$node/@notAfter-iso}
                else
                    ()),
                $node/(@* except (@when-iso, @to-iso, @from-iso, @notAfter-iso)),
                fontaneSimple:transform($node/node(), $uri, $log)
            }

        case element(tei:ptr) return
            if($node[@type = 'editorial-commentary']) then
                $node
            else if($node[ancestor::tei:bibl]) then
                if(fontaneSimple:has-only-ptr-as-child($node/ancestor::tei:bibl)) then
                    fontaneSimple:get-sec-lit-reference($node)
                else
                    fontaneSimple:copy-element($node, $uri, $log)
            else
                ()

        case element (tei:bibl) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:unclear) return
            fontaneSimple:copy-element($node, $uri, $log)

        case element(tei:div) return
            fontaneSimple:copy-element($node, $uri, $log)

        default return
            fontaneSimple:transform($node/node(), $uri, $log)
};

(:~
 : Takes a given element over as is since it is compliant to TEI simplePrint.
 :
 : @author Michelle Weidling
 : @param $node the current text node
 : @return node() a copy of the current node
 :)
declare function fontaneSimple:copy-element($node as node(), $uri as xs:string,
$log as xs:string) as node() {
    element {QName("http://www.tei-c.org/ns/1.0", $node/name())}{
        $node/(@* except @rend),
        if($node/@rend) then
            attribute rendition {$node/@rend}
        else
            (),
        fontaneSimple:transform($node/node(), $uri, $log)
    }
};

(:~
 : Creates a tei:head with an @rendition and a font-size, if available.
 :
 : @author Michelle Weidling
 : @param $node the current tei:line, tei:zone or tei:seg node
 : @return element(tei:head)
 : :)
declare function fontaneSimple:make-head($node as node(), $uri as xs:string,
$log as xs:string) as element(tei:head) {
    element tei:head {
        (if($node/descendant::tei:seg[matches(@style, "font-size")]) then
            attribute style {simpleHelpers:get-font-size($node)}
        else
            attribute style {"default"}
        ),

        $node/@subtype,
        $node/@xml:id,
        (if($node//tei:hi) then
            attribute underlined {"true"}
        else
            ()),

        (if($node/@rendition) then
            $node/@rendition
        else
            ()),

        if($node/@rend) then
            $node/@rend
        else
            (),
        if($node/@next) then
            $node/@next
        else
            (),
        if($node/@prev) then
            $node/@prev
        else
            (),
        fontaneSimple:transform($node/node(), $uri, $log)
    }
};

(:~
 : Creates a tei:div.
 :
 : @author Michelle Weidling
 : @param $node the current tei:surface node
 : @return element(tei:div)
 : :)
declare function fontaneSimple:make-div($node as element(tei:surface), $uri as
xs:string, $log as xs:string) as element(tei:div) {
    element tei:div {
        $node/(@* except (@facs, @n, @attachment, @subtype, @ulx, @uly, @lrx, @lry, @points)),
        fontaneSimple:transform($node/node(), $uri, $log)
    }
};

(:~
 : Creates a new tei:seg with all relevant rendition information.
 :
 : @author Michelle Weidling
 : @param $node the current tei:zone, tei:line, or tei:seg element
 : @return element(tei:seg)
 :)
declare function fontaneSimple:make-seg-with-rendition($node as element(*),
$uri as xs:string, $log as xs:string) as element(tei:seg) {
    element tei:seg {
        attribute rendition {simpleHelpers:filter-rendition($node)},
        $node/(@* except (@style, @rendition)),
        (if($node[self::tei:line or self::tei:zone]) then
            attribute unit {$node/name()}
        else
            ()),
        fontaneSimple:transform($node/node(), $uri, $log)
    }
};


(:~
 : Creates a tei:pb with a type instead of a @n representing the pagination.
 : This function is mainly used to separate the inner and outer book covers from
 : each other.
 :
 : @author Michelle Weidling
 : @param $node the current tei:surface node
 : @return element(tei:pb)
 :)
declare function fontaneSimple:make-pb-with-type($type as xs:string)
as element(tei:pb) {
    element tei:pb {
        attribute type {$type}
    }
};


declare function fontaneSimple:enhance-handshift($node as element(*))
as element(tei:milestone) {
    let $prev-hand := $node/preceding::tei:handShift[@new][1]

    return
        element tei:milestone {
            attribute unit {"handshift"},
            attribute subtype {if($node/@new) then $node/@new else $prev-hand/@new},

            $node/(@* except (@new, @unit))
        }
};


declare function fontaneSimple:mark-intervention($node as element(*), $uri as
xs:string, $log as xs:string)
as element(tei:seg) {
    let $type :=
        if($node[self::tei:lb]) then
            "missing-hyphen"
        else if($node[self::tei:surplus]) then
            "surplus"
        else
            "reduplication"
    return
    element tei:seg {
        attribute type {$type},
        <tei:hi type="{$type}" xmlns="http://www.tei-c.org/ns/1.0">‹</tei:hi>,

        if($node[self::tei:lb]) then
            simpleHelpers:find-chars($node)
        else
            fontaneSimple:transform($node/node(), $uri, $log),

        <tei:hi type="{$type}" xmlns="http://www.tei-c.org/ns/1.0">›</tei:hi>
    }
};

declare function fontaneSimple:mark-missing-syllable($node as element(*), $uri as
xs:string, $log as xs:string) as element(tei:seg) {
    let $sic := $node/tei:sic/string()
    let $sic-length := string-length($sic)
    let $corr := $node/tei:corr/string()
    let $corr-length := string-length($corr)

    let $diff := $corr-length - $sic-length
    let $missing := substring($corr, 1, $diff)
    let $existent := $sic

    let $type := "missing-syllable"


    return
        element tei:seg {
            attribute type {$type},
            <tei:hi type="{$type}" xmlns="http://www.tei-c.org/ns/1.0">‹</tei:hi>,
            $missing,
            <tei:hi type="{$type}" xmlns="http://www.tei-c.org/ns/1.0">›</tei:hi>,
            $existent
        }
};


declare function fontaneSimple:mark-missing-hyphen($node as element(tei:lb)) as
element(tei:seg) {
    element tei:seg {
        attribute type {"missing-hyphen"},
        <tei:hi xmlns="http://www.tei-c.org/ns/1.0">‹</tei:hi>,
        simpleHelpers:find-chars($node),
        <tei:hi xmlns="http://www.tei-c.org/ns/1.0">›</tei:hi>
    }
};

declare function fontaneSimple:mark-supplied($node as element(tei:supplied), $uri as xs:string, $log as xs:string) as
element(tei:seg) {
    element tei:seg {
        attribute type {"supplied"},
        <tei:hi xmlns="http://www.tei-c.org/ns/1.0">‹</tei:hi>,
        fontaneSimple:transform($node/node(), $uri, $log),
        <tei:hi xmlns="http://www.tei-c.org/ns/1.0">›</tei:hi>
    }
};


(:~ In this first serialization step the beginning and end of line groups are
 : simply marked with milestones, which are expanded to a full tei:lg in the
 : second serialization step. :)
declare function fontaneSimple:mark-linegroup-beginning() as
element(tei:milestone) {
    element tei:milestone {
        attribute unit {"start-lg"}
    }
};

declare function fontaneSimple:mark-linegroup-end() as
element(tei:milestone) {
    element tei:milestone {
        attribute unit {"end-lg"}
    }
};

declare function fontaneSimple:has-pb-inbetween($node as node(),
$corresp as node()*) as xs:boolean {
    if($node/following::tei:surface[. << $corresp]) then
        true()
    else
        false()
};

declare function fontaneSimple:separates-corresp-nodes($node as element(tei:surface))
as xs:boolean {
    some $prev in $node/preceding::*[@next]
    satisfies contains($prev/@next, $node/descendant::*/@xml:id)
        or contains($prev/@next, $node/following::*/@xml:id)
};

declare function fontaneSimple:make-index-infos($node as element(tei:rs),
$index-type as xs:string) as element()* {
    let $refs := tokenize($node/@ref, " ")
    let $no-of-refs := count($refs)

    return
        for $iii in 1 to $no-of-refs
        let $ref := $refs[$iii]
        return
            element tei:index {
                attribute indexName {$index-type},

                try {
                    element tei:term {
                        attribute type {"main"},
                        index-info:get-info-about($index-type, $ref, "regular-name")
                    },
                    element tei:term {
                        attribute type {"key"},
                        fontaneSimple:make-key($index-type, $ref, "regular-name")
                    },
                    switch ($index-type)
                        case "eve" return
                            let $category := index-info:get-info-about($index-type, $ref, "category")
                            return
                                fontaneSimple:make-term("subref-of", $category)
                        case "org" return
                            ()

                        case "plc" return
                            let $subref := index-info:get-info-about($index-type, $ref, "subref-of")
                            return
                                fontaneSimple:make-term("subref-of", $subref)

                        case "psn" return
                            let $birth := index-info:get-info-about($index-type, $ref, "birth")
                            let $death := index-info:get-info-about($index-type, $ref, "death")
                            let $occupation := index-info:get-info-about($index-type, $ref, "occupation")
                            return
                                (fontaneSimple:make-term("birth", $birth),
                                fontaneSimple:make-term("death", $death),
                                fontaneSimple:make-term("occupation", $occupation),
                                if(index-info:get-info-about($index-type, $ref, "relation-to-fontane")) then
                                    let $relation := index-info:get-info-about($index-type, $ref, "relation-to-fontane")
                                    return
                                        fontaneSimple:make-term("relation-to-fontane", $relation)
                                else
                                    (),
                                if(index-info:get-info-about($index-type, $ref, "same-as")) then
                                    let $same-as := index-info:get-info-about($index-type, $ref, "same-as")
                                    return
                                        fontaneSimple:make-term("same-as", $same-as)
                                else
                                    ())

                        case "wrk" return
                            if(index-info:get-info-about($index-type, $ref, "type") = "work") then
                                fontaneSimple:make-infos-about-work($ref, $index-type)

                            (: the index "wrk" not only encompasses works but
                            also fictional characters or places that occur in
                            them. @part-of-id denotes this relationship. to put
                            the character into the right place we also have to
                            provide the whole information about the work. :)
                            else if(index-info:get-info-about($index-type, $ref, "type") = ("person", "place")) then
                                let $work-id :=
                                    try {
                                        index-info:get-info-about($index-type, $ref, "part-of-id")
                                    } catch * {
                                        error(QName("error", "TEI2SIMPLE001"), "No work found for " || $ref || ".")
                                    }
                                let $work-name := index-info:get-info-about($index-type, $work-id, "regular-name")

                                return
                                    (
                                        fontaneSimple:make-infos-about-work($work-id, $index-type),
                                        fontaneSimple:make-infos-about-fictional-character($ref, $index-type, $work-name)
                                    )

                            else
                                ()

                        default return
                            ()
                }
                catch * {
                        fontaneSimple:make-term("key", $ref),
                        fontaneSimple:make-term("main", $ref),
                        element tei:term {
                            attribute type {"subref"},
                            fontaneSimple:make-term("key", "!!FEHLERHAFTERDATENSATZ"),
                            fontaneSimple:make-term("main", "!!FEHLERHAFTER DATENSATZ")
                        }
                }
            }
};

declare function fontaneSimple:make-term($type as xs:string, $info as xs:string)
as element(tei:term)* {
    if($info != "") then
        element tei:term {
            attribute type {$type},
            $info
        }
    else
        ()
};

declare function fontaneSimple:make-key($index-type as xs:string, $ref as xs:string, $term as xs:string) {
    let $main := index-info:get-info-about($index-type, $ref, $term)
    return
        fontaneSimple:make-key($main)
};

declare function fontaneSimple:make-key($regular-name as xs:string?) {
    let $name :=
        if(contains($regular-name, ".")) then
            substring-before($regular-name, ".")
        else
            $regular-name
    let $affix := substring-after($regular-name, ".")
    let $key :=
        replace($name, "Ä", "A")
        => replace("ä", "ae")
        => replace("Ö", "Oe")
        => replace("ö", "oe")
        => replace("Ü", "Ue")
        => replace("ü", "ue")
        => replace(" ", "")
        => replace("\.", "")
        => replace(",", "")
    return
        (: sorts terms with a trailer, e.g. 'Friederich II. von Sachsen' after the ones
            without, e.g. 'Friederich II.' :)
        if(matches($affix, "[\w]")) then
            $key || "A"
        else
            $key
};

(:~
 : Creates all information needed about a tei:rs[matches(@ref, 'wrk')] which includes:
 :  * the creator of the work
 :  * information about the work is a periodical or not
 :
 : @param $ref the string of tei:rs/@ref
 : @param $index-type "wrk"
 :   :)
declare function fontaneSimple:make-infos-about-work($ref as xs:string, $index-type
as xs:string) as element(tei:term)+ {
    let $subref-ids := index-info:get-info-about($index-type, $ref, "creator-ids")
    let $creator-ids :=tokenize($subref-ids, " ")
    (: Fontane or anonymus :)
    let $creator := index-info:get-info-about($index-type, $ref, "creator")
    let $creators :=
        if($creator) then
            $creator
        else
            for $creator-id in $creator-ids
            return
                if(matches($creator-id, "#")) then
                    ()
                else
                    $creator-id

    let $type := index-info:get-info-about($index-type, $ref, "periodical")
    let $same-as := index-info:get-info-about($index-type, $ref, "same-as")

    let $no-of-ids := count($creators)
    return
        (
        fontaneSimple:make-term("periodical", $type),

        if($same-as) then
            fontaneSimple:make-term("same-as", $same-as)
        else
            (),

        if($subref-ids) then
            for $iii in 1 to $no-of-ids return
                let $subref := index-info:get-info-about("psn", $creators[$iii], "regular-name")
                let $subref-key := fontaneSimple:make-key($subref)
                let $birth := index-info:get-info-about("psn", $creators[$iii], "birth")
                let $death := index-info:get-info-about("psn", $creators[$iii], "death")
                let $occupation := index-info:get-info-about("psn", $creators[$iii], "occupation")
                return
                    element tei:term {
                        attribute type {"subref"},
                        fontaneSimple:make-term("birth", $birth),
                        fontaneSimple:make-term("death", $death),
                        fontaneSimple:make-term("occupation", $occupation),
                        fontaneSimple:make-term("key", $subref-key),
                        fontaneSimple:make-term("main", $subref),
                        if(index-info:get-info-about("psn", $creators[$iii], "relation-to-fontane")) then
                            let $relation := index-info:get-info-about("psn", $creators[$iii], "relation-to-fontane")
                            return
                                fontaneSimple:make-term("relation-to-fontane", $relation)

                         else
                            ()
                    }
        else if(matches($creator, "Fontane")) then
            fontaneSimple:make-term("creator", $creator)

        else if(matches($creator, "Anonym")) then
            element tei:term {
                attribute type {"subref"},
                fontaneSimple:make-term("main", $creator)
            }

        else
            ())
};

declare function fontaneSimple:make-infos-about-fictional-character($ref as
xs:string, $index-type as xs:string, $work-name as xs:string) as element(tei:term) {
    let $regular-name := index-info:get-info-about($index-type, $ref, "regular-name")
    let $note := index-info:get-info-about($index-type, $ref, "note")

    return
        element tei:term {
            attribute type {"person-mentioned"},
            fontaneSimple:make-term("part-of-work", $work-name),
            fontaneSimple:make-term("regular-name", $regular-name),
            if($note) then
                fontaneSimple:make-term("note", $note)
             else
                ()
        }
};

declare function fontaneSimple:preserve-whitespace($node as element(*), $uri as
xs:string, $log as xs:string) as node()* {
    let $first-char := $node/following::node()[1][self::text()]
        => substring(1, 1)

    return
        (fontaneSimple:transform($node/node(), $uri, $log),
        if(matches($first-char, "[\s]") and matches($node/following::node()[1][self::text()], "[\w]")) then
             text{" "}
        else
            ())
};

declare function fontaneSimple:has-only-ptr-as-child($bibl as element(tei:bibl))
as xs:boolean {
    let $text-nodes := $bibl//text()
    let $have-nodes-content :=
        for $text in $text-nodes return
            if(normalize-space($text) != "") then
                true()
            else
                false()

    return
        if($have-nodes-content = true()) then
            false()
        else
            true()
};

declare function fontaneSimple:get-sec-lit-reference($ptr as element(tei:ptr))
as text() {
    let $reference := $ptr/@target
    return
        text {
            index-info:get-info-about("lit", $reference, "abbrev")
        }
};


declare function fontaneSimple:add-log-entry($log-file as xs:string,
$message as xs:string) as empty-sequence() {
  let $entry := <LogEntry timestamp="{util:system-time()}">{$message}</LogEntry>
  return update insert $entry into doc($log-file)/*
};

declare function fontaneSimple:make-img($node as element(tei:figure), $uri as xs:string,
$log as xs:string, $flag as xs:string?) as element(tei:figure) {
    let $display :=
        if($node/ancestor::tei:zone[@type = "illustration"]/tei:milestone[@unit = "illustration"][. << $node]) then
            "block"
        else
            "inline"
    let $img-url := try {tbleapi:get-url($uri, $node/@xml:id, "png") }
        catch * { fontaneSimple:add-log-entry($log, "No TBLE-file found for " || $node/@xml:id) }
    let $img-url :=
        if($display = "inline") then
            replace($img-url, ",1000", ",500")
        else
            $img-url
    let $rotation :=
        (: comment right after element:)
        if($node/following-sibling::node()[1][self::comment()][matches(./string(), "rotate")]) then
            substring-after($node/following::comment()[1], "rotate(")
            => substring-before("deg")
        (: comment as first descendant :)
        else if($node/child::node()[1][self::comment()][matches(./string(), "rotate")]) then
            substring-after($node/child::comment()[1], "rotate(")
            => substring-before("deg")
        else
            ()

    return
        element tei:figure {
            $node/@*,
            attribute href {$img-url},
            if($flag = "double") then
                (attribute type {"double"},
                attribute id {util:hash(generate-id($node), "md5")})
            else
                (),
            if($rotation) then
                attribute rotate {$rotation}
            else
                (),
            fontaneSimple:transform($node/node(), $uri, $log)
        }
};


declare function fontaneSimple:make-double-img($node as element(tei:figure),
$uri as xs:string, $log as xs:string) as element(tei:figure)+ {
    let $left-img-part := fontaneSimple:make-img($node, $uri, $log, "double")
    let $right-img-part := fontaneSimple:make-right-img-part($node, $uri, $log)

    return
        ($left-img-part,
        $right-img-part)
};

declare function fontaneSimple:make-right-img-part($node as element(tei:figure),
$uri as xs:string, $log as xs:string) as element(tei:figure) {
    let $next-id := $node/ancestor::tei:surface/@next
        => replace("#", "")
    let $corresp-node := $node/root()//tei:surface[@xml:id = $next-id]//tei:figure

    return
        fontaneSimple:make-img($corresp-node, $uri, $log, "double")
};

declare function fontaneSimple:make-pb($node as element(tei:surface)) as element(tei:pb)? {
    let $editorial-list-entries := $node/root()//tei:msContents/tei:ab/tei:list[@type="editorial"]//tei:ref
    let $refs := for $entry in $editorial-list-entries return
        $entry/@target/string()
        => substring-after("@n='")
        => substring-before("']")
    let $refs := distinct-values($refs)
    return
        if($node/@n = $refs) then
            element tei:pb {
                attribute n {$node/@n}
            }
        else
            ()
};
