xquery version "3.1";

(:~
 : This module handles the creation of the XHTML serialization of the edited text.
 : On the Fontane website each notebook is provided as a so called "Lesefassung"
 : that contains only the contemporary, definitive text as well as some styling
 : information such as pencil colours and different fonts.
 :
 :)

module namespace simple2xhtml="http://fontane-nb.dariah.eu/simple2xhtml";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";
import module namespace functx="http://www.functx.com";

declare function simple2xhtml:main($nodes as node()*, $uri as xs:string) {
    let $xhtml := element xhtml:div {simple2xhtml:recursion($nodes//tei:text)}
    let $tidy := element xhtml:div {simple2xhtml:tidy($xhtml)}
    return
        xmldb:store($config:data-root || "/print/xhtml/", $uri || ".html", $tidy)
};

(:~
 : Tidy up all that messy white spaces.
 :)
declare function simple2xhtml:fix-whitespaces($node as xs:string) as xs:string {
    let $general :=
        replace($node, " ,", ",")
        => replace("\?@\?", "&#x2003;")
        => replace("\?@@\?", "&#12291;")
        => replace(" \?", "?")
        => replace(" \.", ".")
        => replace(" ;", ";")
        => replace("@@", " ")
        => replace(" &#x2003;", "&#x2003;")
        => replace(":", ": ")
        => replace("\s+“", "“")
(:    let $general :=:)
(:        if(normalize-space($node) = "":)
(:        and matches(substring($node/following::text()[1], 1, 1), "[,\.“\-\)]")) then:)
(:            ():)
(:        else:)
(:            $node:)
    let $hyphen :=
        if(matches($general, "@P[A-Z]")) then
            replace($general, "@P", "-")
            => replace("@", "")
        else
            replace($general, "@P", "")
            => replace("@", "")
    return $hyphen
};


(:~
 : The main transformation for XML to XHTML.
 :
 : @author Simon Sendler
 : @author Michelle Weidling
 : @param $node The current TEI node
 : @return The respective XHTML node
 :)
declare function simple2xhtml:recursion($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            if (normalize-space($node) = "") then
                ()
            else
                (
                    if($node/preceding-sibling::*[1][self::tei:milestone[@rendition = "indent"]]) then
                        element xhtml:br {}
                    else
                        (),
                    element xhtml:span {
                        simple2xhtml:set-hs-info($node, "text"),
                        if($node/preceding-sibling::*[1][self::tei:milestone[@rendition = "indent"]]) then
                            attribute style {"margin-left: 20px;"}
                        else if($node/preceding-sibling::*[1][self::tei:milestone[@rendition = "align(center)"]]) then
                            attribute style {"text-align: center;"}
                        else
                            (),
                        simple2xhtml:fix-whitespaces($node)
                    }
                )

        case element(tei:body) return
            (
                element xhtml:div {
                    simple2xhtml:set-hs-info($node, ()),
                    simple2xhtml:recursion($node/node())
                },
                for $note in $node/root()//tei:note[@type = "authorial" and @subtype ="footnote"] return
                        element xhtml:div {
                            attribute id {$note/../@xml:id},
                            attribute class {"nb-footnote"},
                            simple2xhtml:recursion($note/node())
                        }
            )

        case element(tei:abbr) return
            (
            if($node/@prev) then
                ()
            else
                text{" "},
            element xhtml:span {
                simple2xhtml:set-hs-info($node, "abbr"),
                simple2xhtml:recursion($node/node())
            })

        case element(tei:add) return
            if($node/@place = "above"
            and $node/preceding-sibling::*[1][self::tei:seg[@type = "multiphrase"]]) then
                let $corresp-id := replace($node/@corresp, "#", "")
                let $corresp-anchor := $node/root()//tei:anchor[@xml:id = $corresp-id]
                let $corresp-seg := $corresp-anchor/ancestor::tei:seg[@type = 'multiphrase']
                let $super := $node/root()//tei:seg[@type = "multiphrase"][@xml:id = replace($corresp-seg/@corresp, "#", "")]
                let $sub := $corresp-seg
                return
                    element xhtml:span {
                        simple2xhtml:set-hs-info($node, "multiphrase"),
                        element xhtml:span {
                            attribute class {"multiphrase-top"},
                            simple2xhtml:recursion($super/node())
                        },
                        element xhtml:span {
                            attribute class {"multiphrase-bottom"},
                            simple2xhtml:recursion($sub/node())
                        }
                    }
            else
                simple2xhtml:recursion($node/node())

        case element(tei:div) return
            if($node/@type = "toc") then
                if($node/@subtype = "Friedrich_Fontane") then
                    (element xhtml:span {
                        attribute class {"handshift"},
                        text{"<Schreiberhand Friedrich Fontane>"}
                    },
                    element xhtml:ul {
                        simple2xhtml:set-hs-info($node, "nb-toc-list"),
                        simple2xhtml:recursion($node/node())
                    },
                    element xhtml:span {
                        attribute class {"handshift"},
                        text{"<Schreiberhand Theodor Fontane>"}
                    })
                else
                    element xhtml:ul {
                        simple2xhtml:set-hs-info($node, "nb-toc-list"),
                        simple2xhtml:recursion($node/node())
                    }
            else
                (if($node/@type = "marked_off") then
                        element xhtml:div {
                            attribute class {"nb-editorial"},
                            text{"<Beginn Erledigung>"}
                        }
                else if($node/@type = "multiphrase") then
                    element xhtml:div {
                        attribute class {"nb-editorial"},
                        text{"<Beginn Textschicht " || count($node/preceding-sibling::tei:div[@type = "multiphrase"]) + 1|| ">"}
                    }
                else
                    (),
                element xhtml:div {
                    if($node/@type = ("label", "additional", "marked_off", "indent")) then
                        simple2xhtml:set-hs-info($node, "nb-" || $node/@type)

                    else
                        simple2xhtml:set-hs-info($node, ()),
                    simple2xhtml:recursion($node/node())

                },
                if($node/@type = "marked_off") then
                    element xhtml:div {
                        attribute class {"nb-editorial"},
                        text{"<Ende Erledigung>"}
                    }
                else if($node/@type = "multiphrase") then
                    element xhtml:div {
                        attribute class {"nb-editorial"},
                        text{"<Ende Textschicht " || count($node/preceding-sibling::tei:div[@type = "multiphrase"]) + 1 || ">"}
                    }
                else
                    ()
                )

        case element(tei:expan) return
            element xhtml:span {
                simple2xhtml:set-hs-info($node, "expan"),
                simple2xhtml:recursion($node/node())
            }

        case element(tei:front) return
            element xhtml:div {
                simple2xhtml:set-hs-info($node, "front-area"),
                simple2xhtml:recursion($node/node())
            }

        case element(tei:hi) return
            if($node/ancestor::tei:seg[@type = ("reduplication", "missing-syllable", "missing-hyphen", "supplied")]
            or $node[@type = "blue-underlined"]) then
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-" || $node/@type),
                    simple2xhtml:recursion($node/node())
                }

            else if($node/@type = "vertical-mark") then
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-" || $node/@type),
                    simple2xhtml:recursion($node/node())
                }

            else
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-underline"),
                    simple2xhtml:recursion($node/node())
                }

        case element(tei:lb) return
            if ($node/@type="edited_text") then
                element xhtml:br {}
            else if ($node/@type="keepIndent") then
                element xhtml:div {
                    attribute class {"nb-keepIndent"}
                }
            else
                ()

        case element(tei:list) return
            element xhtml:ul {
                attribute class {"nb-list"},
                simple2xhtml:recursion($node/node())
            }

        case element(tei:item) return
            element xhtml:li {
                simple2xhtml:set-hs-info($node, ()),
                simple2xhtml:recursion($node/node())
            }

        case element(tei:rs) return
            let $id := $node/@ref => substring-after(":")
            let $index-link := "register.html?e=" || $id
            return
                element xhtml:a {
                    attribute href {$index-link},
                    attribute target {"_blank"},
                    simple2xhtml:set-hs-info($node, "nb-index-entry"),
                    simple2xhtml:recursion($node/node()),
                    if($node/@prev or $node[not(@prev or @next)]) then
                        (switch(substring-before($node/@ref, ":"))
                            case "psn" return
                                <i class="fa fa-user icon-invisible" aria-hidden="true"/>
                            case "plc" return
                                <i class="fa fa-map-marker icon-invisible" aria-hidden="true"/>
                            case "eve" return
                                <i class="fa fa-flag icon-invisible" aria-hidden="true"/>
                            case "org" return
                                <i class="fa fa-university icon-invisible" aria-hidden="true"/>
                            case "wrk" return
                                <i class="fa fa-book icon-invisible" aria-hidden="true"/>
                            default return ()

    (:                        let $first-char := $node/following::text()[1]/substring(., 1, 1):)
    (:                        return:)
    (:                        if(matches($first-char, "[\.,)(\?!]")) then:)
    (:                            ():)
    (:                        else:)
    (:                            text{" "}:)
                        )
                    else
                        ()
                }

        case element(tei:head) return
            switch($node/@subtype)
                case "sub" return
                    element xhtml:h5 {
                        simple2xhtml:set-classes($node),
                        simple2xhtml:recursion($node/node())
                    }

                case "chapter" return
                    element xhtml:h4 {
                        simple2xhtml:set-classes($node),
                        simple2xhtml:recursion($node/node())
                    }

                case "section" return
                    element xhtml:h3 {
                        simple2xhtml:set-classes($node),
                        simple2xhtml:recursion($node/node())
                    }

                default return
                    element xhtml:h2 {
                        simple2xhtml:set-classes($node),
                        simple2xhtml:recursion($node/node())
                    }

        case element(tei:milestone) return
            if($node/@unit = "line"
            and ($node/ancestor::tei:seg[@type = "said"][not(preceding-sibling::*[1][self::*[@type = "said"]] or preceding-sibling::*[1]//*[last()][self::*[@type = "said"]])]
            or $node/ancestor::tei:div[@type = "edited_text"])) then
                element xhtml:br {}

            else if($node/@unit = "line") then
                let $next-char := substring($node/following::text()[1], 1, 1)
                let $prev-char := substring($node/preceding::text()[1], 1, 1)
                return
                    if((matches($next-char, "[\.\)\?,;!]") or $next-char = "&#8220;")
                    and not(ends-with($node/preceding::text()[1], "@P"))) then
                        ()

                    else if(ends-with($node/preceding::text()[1], "@P")
                    or ends-with($node/preceding::text()[1], "-")) then
                        ()

                    else if(matches($prev-char, "\(")) then
                        ()

                    else if($node/preceding-sibling::*[1][self::tei:rs[concat('#', @xml:id) = $node/following-sibling::*[2][self::tei:rs]/@prev]]) then
                        ()

                    else if($node/preceding-sibling::*[1][self::tei:abbr[@next]]) then
                        ()

                    else
                        text{" "}

            else if($node/@unit = "start-lg") then
                let $lines := $node/following-sibling::tei:l[. << $node/following-sibling::tei:milestone[@unit = "end-lg"][1]]
                return
                    element xhtml:div {
                        attribute class {"nb-verses"},
                        for $line in $lines return
                            element xhtml:div {
                                attribute class {"nb-verse"},
                                simple2xhtml:recursion($line/node())
                            }
                    }

            else if($node/@unit = "paragraph") then
                (element xhtml:br {},
                element xhtml:div {
                    attribute class {"nb-paragraph"}
                })

            else
                ()


        case element(tei:l) return
            (element xhtml:div {
                attribute class {"nb-verse"},
                simple2xhtml:recursion($node/node())
            },
            if($node/following-sibling::*[1][not(self::tei:seg[@type = "verse"] or self::tei:l)]) then
                element xhtml:span {
                    attribute class {"nb-verses-end"}
                }
            else
                ()
            )

        case element(tei:ab) return
            if($node/@type = "sketch"
            and $node/descendant::tei:figure) then
                (
                    if($node//tei:ptr[@type = "editorial-commentary"]) then
                        simple2xhtml:recursion($node//tei:ptr[@type = "editorial-commentary"])
                    else
                        (),
                    element xhtml:table {
                        attribute class {"nb-sketch"},
                        element xhtml:tr {
                            simple2xhtml:recursion($node/node())
                        }
                    }
                )

            else if($node/@type = "footnote-mark") then
                element xhtml:seg {
                    attribute class {"nb-" || $node/@type},
                    simple2xhtml:recursion($node/node())
                }

            else if($node/@type = "caret") then
                ()

            else if($node/@type = "verse_marker") then
                element xhtml:seg {
                    attribute class {"nb-" || $node/@type},
                    if($node/@rend = "pipe") then
                        text{"|"}
                    else
                        text{"/"}
                }

            else if($node/@type = ("short-paragraph-line", "sum", "double-sum",
            "difference", "long-end-line", "end-line", "short-paragraph-line-double",
            "footnotes")) then
                element xhtml:div {
                    attribute class {$node/@type}
                }

            else if($node/@type = "bottom-brace") then
                (<br/>,
                element xhtml:span {
                    attribute class {$node/@type},
                    text{"⏟"}
                },
                <br/>)

            else if($node/@type = "paragraph") then
                    element xhtml:div {
                    attribute class {"nb-paragraph-line-special-characters"},
                    if($node/string() = "z") then
                        ()
                    else
                        simple2xhtml:recursion($node/node())
                }

            else if($node/@type = "authorial_note") then
                text{"authorial note"}

            else
                simple2xhtml:recursion($node/node())

        case element(tei:figure) return
            if($node/ancestor::tei:div/@type = "genealogy") then
                let $coordinates := tokenize( $node/ancestor::tei:div[1]/@points, " ")
                return
                    (<svg class="{tokenize($node//tei:figDesc/tei:ref/text(), ' ')}"
                    width="{$node/root()//tei:dimensions[@type="leaf"]/tei:width/string(@quantity)}mm"
                    height="{$node/root()//tei:dimensions[@type="leaf"]/tei:height/string(@quantity)}mm"
                    style="position:absolute; top:0;">
                        {$node/@xml:id ! attribute id { string(.) },
                        for $pair in 1 to (count($coordinates) - 1)
                        let $x1 := tokenize($coordinates[$pair], ',')[1]
                        let $y1 := tokenize($coordinates[$pair], ',')[2]
                        let $x2 := tokenize($coordinates[$pair + 1], ',')[1]
                        let $y2 := tokenize($coordinates[$pair + 1], ',')[2]
                            return
                                <line style="stroke-width:1;stroke:#000" x1="{$x1}cm" y1="{$y1}cm" x2="{$x2}cm" y2="{$y2}cm" />
                        }
                    </svg>,
                    element xhtml:div {
                        attribute class {'hrHover'},
                        tokenize($node//tei:figDesc/tei:ref/text(), ' ')[2] => replace(";", "")
                        }
                    )

            else if(not($node/ancestor::tei:ab/preceding-sibling::*[1][descendant::tei:figure[@type = "double"]])) then
                let $captions-before :=
                    $node/preceding-sibling::tei:seg[@type = "caption"][ancestor::tei:ab = $node/ancestor::tei:ab]
                let $captions-after :=
                    $node/following-sibling::tei:seg[@type = "caption"][ancestor::tei:ab = $node/ancestor::tei:ab]
                return
                    (:  make captions left of sketch:)
                    (if(exists($captions-before)) then
                        simple2xhtml:make-img-captions($captions-before, "right-aligned")
                    else
                        (),

                    (: for an ordinary img or the left part of a double img :)
                    element xhtml:td {
                        if($node/@type = "double") then
                            attribute class {"nb-double-img"}
                        else
                            attribute class {"nb-img"},
                        (: in case of a 180deg rotation the original width/height
                        preserved while we have to switch width and height in
                        case of 90deg/270deg :)
                        if($node/@rotate = "180") then
                            attribute style {concat("width: ", $node/parent::*/@width, "; height: ", $node/parent::*/@height, ";")}
                        else if($node/@rotate) then
                            attribute style {concat("width: ", $node/parent::*/@height, "; height: ", $node/parent::*/@width, ";")}
                        else
                            (),
                        if($node/@type = "double") then
                            simple2xhtml:make-img($node, "double")
                        else
                            simple2xhtml:make-img($node, "block")
                    },

                    if($node/@type = "double"
                    and $node/ancestor::tei:ab/following-sibling::*[1][descendant::tei:figure[@type = "double"]]) then
                        let $next-node := $node/ancestor::tei:ab/following-sibling::*[1]/descendant::tei:figure[@type = "double"]
                        return
                            element xhtml:td {
                                attribute class {"nb-double-img"},
                                if($next-node/@rotate) then
                                    attribute style {concat("width: ", $next-node/parent::*/@height, "; height: ", $next-node/parent::*/@width, ";")}
                                else
                                    (),
                                simple2xhtml:make-img($next-node, "double")
                            }

                    else
                        (),

                    (: make captions right of sketch:)
                    if(exists($captions-after)) then
                        simple2xhtml:make-img-captions($captions-after, "left-aligned")
                    else
                        ())

                (: right parts of sketches on double pages are processed when processing the left part :)
                else
                    ()

        case element(tei:seg) return
            if($node/@type = ("caption", "editorial-label", "multiphrase")) then
                ()

            else if($node/@type = "verse") then
                (element xhtml:div {
                    attribute class {"nb-verse"},
                    simple2xhtml:recursion($node/node())
                },
                if($node/following-sibling::*[1][not(self::tei:seg[@type = "verse"] or self::tei:l)]) then
                    element xhtml:span {
                        attribute class {"nb-verses-end"}
                    }
                else
                    ()
                )

            else if($node/@type = "framed") then
                element xhtmlspan {
                    simple2xhtml:set-hs-info($node, "nb-framed"),
                    simple2xhtml:recursion($node/node())
                }

            else if($node[@rendition = ("vertical-align:super", "vertical-align:sub")
            and
                (following-sibling::*[1][self::tei:g[@ref ="#hb"]]
                or preceding-sibling::*[1][self::tei:g[@ref ="#hb"]])
            ]) then
                ()

            else if($node/@type = "integration") then
                simple2xhtml:make-integration($node)

            else if($node/@type = ("missing-hyphen", "supplied", "blue-underlined", "highlighted-area")) then
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-" || $node/@type),
                    simple2xhtml:recursion($node/node())
                }

            else if($node/@type = "said") then
                element xhtml:div {
                    attribute class {"nb-said"},
                    simple2xhtml:recursion($node/node())
                }

            else if(matches($node/@rendition, "smallcaps")) then
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-smallcaps"),
                    attribute style {$node/@rendition},
                    simple2xhtml:recursion($node/node())
                }

            else if($node/@type = "initials") then
                element xhtml:span {
                    simple2xhtml:set-hs-info($node, "nb-initials"),
                    attribute style {$node/@rendition},
                    simple2xhtml:recursion($node/node())
                }

            else
                (
                    element xhtml:span {
                        simple2xhtml:set-hs-info($node, "nb-seg"),
                        attribute style {$node/@rendition},
                        simple2xhtml:recursion($node/node())
                    },
                    if(matches($node/@rendition, "letter\-spacing")
                    and not(ends-with($node, "-"))) then
                        let $next-char := substring($node/following::text()[1], 1, 1)
                        return if(matches($next-char, "\.,\)")) then
                            ()
                        else
                            text{" "}
                    else
                        (),
                    if($node/ancestor::*[@type ="said"]) then
                        element xhtml:br {}
                    else
                        ()
                )

        case element(tei:unclear) return
            (
                element xhtml:span {
                    attribute class {"unclear"},
                    simple2xhtml:recursion($node/node())
                },
                text{" "}
            )

        case element(tei:date) return
            (simple2xhtml:set-whitespace-before($node),
            element xhtml:span {
                simple2xhtml:set-hs-info($node, "nb-date"),
                simple2xhtml:recursion($node/node()),
            simple2xhtml:set-whitespace-after($node)
            })

(:little helper, not yet specified for transformation:)

        case element(tei:index) return
            ()

        case element(tei:note) return
            if($node/@type = ("editorial")
            and not($node/@subtype = "footnote")) then
                element xhtml:span {
                    attribute class {$node/@type || "-note"},
                    simple2xhtml:recursion($node/node())
                }
            else if($node/@type = "authorial") then
                simple2xhtml:make-modal($node)
            else if($node/@subtype = ("revision")) then
                element xhtml:div {
                    attribute class {"nb-" || $node/@subtype},
                    simple2xhtml:recursion($node/node())
                }
            else
                ()

        case element(tei:g) return
            if($node/@ref = "#hb") then
                let $super := $node/preceding-sibling::tei:seg[@rendition = "vertical-align:super"][1]
                let $sub := $node/following-sibling::tei:seg[@rendition = "vertical-align:sub"][1]
                return
                    element xhtml:span {
                        simple2xhtml:set-hs-info($node, "fraction"),
                        element xhtml:span {
                            simple2xhtml:set-hs-info($node, "fraction-top"),
                            simple2xhtml:recursion($super/node())
                        },
                        element xhtml:span {
                            simple2xhtml:set-hs-info($node, "fraction-bottom"),
                            simple2xhtml:recursion($sub/node())
                        }
                    }

            else if($node/@ref = "#rth") then
                element xhtml:span {
                    attribute class {'g rth'},
                    <xhtml:div class="hover">Reichstaler</xhtml:div>,
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 82 107">
                        {element path {
                        attribute fill {"none"},
                        attribute stroke {"darkslategrey"},
                        attribute stroke-width {"5"},
                        attribute d {
                        "M 24.73,35.82" ||
                        " C 24.73,35.82 30.36,30.73 32.00,26.55" ||
                        " 46.73,45.27 80.55,37.45 70.00,38.18" ||
                        " 16.73,50.73 -2.55,99.09 19.09,101.82" ||
                        " 52.00,103.45 57.09,80.00 42.91,80.91" ||
                        " 11.82,83.64 35.45,105.45 52.00,99.64" }}}
                        </svg>
                }

            else
                ()

        case element(tei:table) return
            (element xhtml:table {
                attribute class {"nb-computation-table"},
                simple2xhtml:recursion($node/tei:row)
            },
            simple2xhtml:recursion($node/node()[not(ancestor-or-self::tei:row)]))

        case element(tei:row) return
            element xhtml:tr {
                simple2xhtml:recursion($node/node())
            }

        case element(tei:cell) return
            element xhtml:td {
                simple2xhtml:recursion($node/node())
            }

        case element(tei:gap) return
            if($node/following-sibling::*[1][self::tei:seg[@type = "supplied"]]) then
                ()
            else
                element xhtml:span {
                    if($node/@unit = "mm") then
                        attribute class {"gap-" || $node/@quantity}
                    else
                        attribute class {"gap"},
                    switch ($node/@unit)
                        case "words" return
                            text{"X---x"}
                        case "cap_words" return
                            text{"X---x"}
                        case "uncap_words" return
                            text{"x---x"}
                        case "cap_word_chars" return
                            text{"X---x"}
                        case "uncap_word_chars" return
                            text{"x---x"}
                        case "lc_chars" return
                            text{
                                for $iii in 1 to $node/@quantity return
                                    "x"
                            }
                        case "uc_chars" return
                            text{
                                for $iii in 1 to $node/@quantity return
                                    "X"
                            }
                        case "chars" return
                            text{
                                for $iii in 1 to $node/@quantity return
                                    "X"
                            }
                        default return ()
            }

        case element(tei:space) return
            if($node/@type = ("pause", "placeholder")) then
                element xhtml:span {
                    attribute class {"nb-" || $node/@type}
                }
            else
                ()

        case element(tei:ref) return
            if($node/tei:ab[@type = "footnote-mark"]) then
                let $target-id := replace($node/@target, "#", "")
                let $target := $node/root()//*[@xml:id = $target-id]

                return
                    element xhtml:a {
                        attribute href {$node/@target},
                        simple2xhtml:recursion($node/tei:ab)
                    }
            else
                simple2xhtml:recursion($node/node())

        case element(tei:choice) return
            if($node/tei:expan) then
                element xhtml:span {
                    attribute class {"choice"},
                    element xhtml:div {
                        attribute class {"expan italic"},
                        simple2xhtml:recursion($node/tei:expan/node())
                    },
                    simple2xhtml:recursion($node/tei:abbr),
                    let $next-char := substring($node/following::text()[1], 1, 1)
                    return if(matches($next-char, "\.,\)")) then
                        ()
                    else
                        text{" "}
                }
            else
                simple2xhtml:recursion($node/node())

        case element(tei:abbr) return
            element xhtml:span {
                attribute id {$node/@xml:id},
                attribute class {"abbr"},
                simple2xhtml:recursion($node/node())
            }

        case element(tei:expan) return
            ()

        case element(tei:ptr) return
            if($node[@type = "editorial-commentary"]) then
                simple2xhtml:make-modal($node)
            else
                ()

        default return
            simple2xhtml:recursion($node/node())
};

(:~
 : Sets classes necessary for CSS. This encompassing mainly information about
 : the writing medium as well as heading sizes.
 :
 : @author Michelle Weidling
 : @param $node The current element
 : @return
 :)
declare function simple2xhtml:set-classes($node as element(*)) as attribute() {
    if($node/@rend = "align(center)"
    and $node/ancestor::tei:div[@type = "label"]) then
        simple2xhtml:set-hs-info($node, "nb-head nb-sub-head nb-centered nb-label-head")

    else if($node/@rend = "align(center)") then
        simple2xhtml:set-hs-info($node, "nb-head nb-sub-head nb-centered")

    else if($node/ancestor::tei:div[@type = "label"]) then
        simple2xhtml:set-hs-info($node, "nb-head nb-sub-head nb-label-head")

    else
        simple2xhtml:set-hs-info($node, "nb-head nb-sub-head")
};


(:~
 : Creates a xhtml:img element with all information necessary for getting the
 : right TBLE snippet and displaying it correctly.
 :
 : @author Michelle Weidling
 : @param $node The current tei:figure
 : @param $mode "right-aligned", "block" or "inline"
 : @return A complete xhtml:img element
 :)
declare function simple2xhtml:make-img($node as element(tei:figure),
$mode as xs:string) as element(xhtml:img) {
    let $width := "width:" || $node/ancestor::tei:ab[@type = "sketch"]/@width || "; "
    let $height := "height:" || $node/ancestor::tei:ab[@type = "sketch"]/@height || ";"
    let $rotation :=
        if($node/@rotate) then
            "transform: rotate(" || $node/@rotate || "deg);"
        else
            ""

    return
        element xhtml:img {
            if($node/@id) then
                attribute id {$node/@id}
            else
                (),
            attribute src {$node/@href},
            attribute alt {$node//tei:figDesc/string() => replace("@@", " ")},
            if($mode = "double") then
                attribute class {"nb-double-img"}
            else
                attribute class {"nb-block-img"},
            attribute style {$width || $height || $rotation}
        }
};


(:~
 : Creates the captions for a tei:figure.
 :
 : @author Michelle Weidling
 : @param The given captions as tei:seg[@type = 'caption']
 : @param $align "right-aligned" or "left-aligned"
 : @return An element entry containing the caption contents
 :)
declare function simple2xhtml:make-img-captions($nodes as element(tei:seg)+,
$align as xs:string) as element(xhtml:td) {
    element xhtml:td {
        attribute class {"nb-caption-data"},
        for $node in $nodes return
            element xhtml:span {
                if($align = "right-aligned") then
                    simple2xhtml:set-hs-info($node, "nb-caption nb-caption-ra")
                else
                    simple2xhtml:set-hs-info($node, "nb-caption nb-caption-la"),
                simple2xhtml:recursion($node/node())
            }
    }
};


(:~
 : Sets a white space before a node.
 :
 : @author Michelle Weidling
 : @param The current node
 : @return One or zero white space(s)
 :)
declare function simple2xhtml:set-whitespace-before($node as element(*)) as text()? {
    let $prev-text := $node/preceding::text()[1]
    let $last-char := substring($prev-text, string-length($prev-text), 1)
    return
        if(matches($last-char, "[(†]")) then
            ()
        else
            text{" "}
};


(:~
 : Sets a white space after a node.
 :
 : @author Michelle Weidling
 : @param The current node
 : @return One or zero white space(s)
 :)
declare function simple2xhtml:set-whitespace-after($node as element(*)) as text()? {
    let $next-text := $node/following::text()[1]
    let $first-char := substring($next-text, 1, 1)
    return
        if(matches($first-char, "[“\.,\?!;\)]")) then
            ()
        else
            text{" "}
};


(:~
 : Transforms all element of a virtual aggregation to XHTML.
 :
 : @author Michelle Weidling
 : @param The current node
 : @return The processed node
 :)
declare function simple2xhtml:apply-next-element($node as node()*)
as node()* {
    (: exit point. applies the contents of the current element. :)
    if(not($node/@next)) then
        simple2xhtml:recursion($node/node())

    (: nodes that have a @prev and/or a @next:)
    else
        (: because sort.xqm has been successful we already know that the
        next sibling is the one referenced in @next :)
        let $next-node := $node/following-sibling::*[1]
        return
            (simple2xhtml:recursion($node/node()),
            simple2xhtml:apply-next-element($next-node))
};


(:~
 : Creates a class attribute for the resulting XHTML element containing information
 : about the current writing medium as well as other CSS classes.
 :
 : @author Michelle Weidling
 : @param $node The current node
 : @param $attributes Further class attributes that the resulting XHTML element should have
 : @return A class attribute with all relevant entries
 :)
declare function simple2xhtml:set-hs-info($node as node(), $attributes as xs:string*)
as attribute() {
    let $prev-hs := $node/preceding::tei:milestone[@unit = "handshift"][1]
    let $classes :=
        if($attributes) then
            $prev-hs/@rend || " " || $attributes
        else
            $prev-hs/@rend

    return
        attribute class {$classes}
};


(:~
 : Handles the creation of an integration, i.e. authorial comments to some part
 : of a text. An integration is rendered as a table with 3 columns in which the
 : middle part contains the bracket and the outer ones the text referred to resp.
 : the comment if the bracket faces left or right.
 : In some cases we have horizontal brackets which we render as a table with
 : 3 rows.
 :
 : @author Michelle Weidling
 :)
declare function simple2xhtml:make-integration($node as element(tei:seg)) {
    let $bracket := $node//tei:ab[@function = ("integrate", "authorial_note")][1]
    let $orientation := substring-after($bracket/@rend, "bracket_")

    (:
        target is the side where the pointy end of the bracket goes.
        corresp is the side where bracket faces to.
        example: target { corresp
    :)
    let $target-id := replace($bracket/@target, "#", "")
    let $target := $node//*[@xml:id = $target-id]
    let $corresp-id := replace($bracket/@corresp, "#", "")
    let $corresp := $node//*[@xml:id = $corresp-id]

    let $lines-target := count($target//tei:milestone[@unit = "line"])
    let $lines-corresp := count($corresp//tei:milestone[@unit = "line"])
    let $max-number-of-lines :=
        if($lines-target gt $lines-corresp) then
            $lines-target
        else
            $lines-corresp
    let $bracket-size := 12 * ($max-number-of-lines + 1)

    return
        if($orientation = "bottom") then
            simple2xhtml:make-bottom-bracket-integration-table($corresp, $target)
        else
            simple2xhtml:make-lr-bracket-integration-table($orientation, $corresp, $target, $bracket-size)



};


(:~
 : Renders the table for integrations with left or right facing brackets.
 :
 : @author Michelle Weidling
 :)
declare function simple2xhtml:make-lr-bracket-integration-table($orientation as xs:string,
$corresp as node()*, $target as node()*, $bracket-size as xs:integer) {
    element xhtml:table {
        attribute class {"integration"},
        element xhtml:tr {
            element xhtml:td {
                attribute class {"integration-left"},
                if($orientation = "right") then
                    simple2xhtml:recursion($corresp/node())
                else if($orientation = "left") then
                    simple2xhtml:recursion($target/node())
                else
                    ()
            },
            element xhtml:td {
                attribute class {"integration-bracket"},
                attribute style {"font-size: " || $bracket-size || "pt;"},
                if($orientation = "right") then
                    text{"}"}
                else if($orientation = "left") then
                    text{"{"}
                else
                    ()
            },
            element xhtml:td {
                attribute class {"integration-right"},
                if($orientation = "right") then
                    simple2xhtml:recursion($target/node())
                else if($orientation = "left") then
                    simple2xhtml:recursion($corresp/node())
                else
                    ()
            }
        }
    }
};


(:~
 : Renders the table for integrations with brackets facing down.
 :
 : @author Michelle Weidling
 :)
declare function simple2xhtml:make-bottom-bracket-integration-table($corresp as node()*,
$target as node()*) {
    element xhtml:table {
        attribute class {"integration integration-bottom"},
        element xhtml:tr {
            element xhtml:td {
                simple2xhtml:recursion($corresp/node())
            }
        },
        element xhtml:tr {
            element xhtml:td {
                attribute class {"integration-bottom-bracket"},
                text{"⏟"}
            }
        },
        element xhtml:tr {
            element xhtml:td {
                simple2xhtml:recursion($target/node())
            }
        }
    }
};


(:~
 : A final decluttering of broken white spaces.
 : :)
declare function simple2xhtml:tidy($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            if(string-length($node) = 1 and normalize-space($node) = ""
            and matches(substring($node/following::text()[1], 1, 1), "[,\.\?]")) then
                ()
            else
                text{ replace($node, "[\s]+", " ")
                => replace(" ,", ",")
                => replace(" \.", ".")
                => replace(" :", ":") }

        default return
            element {QName("http://www.w3.org/1999/xhtml", $node/name())} {
                $node/@*,
                simple2xhtml:tidy($node/node())
            }
};


(:~ Creates an icon and a modal window for an editorial or authorial comment.
 :
 : @author Michelle Weidling
 : @param $node a tei:ptr or tei:note[@type = 'authorial']
 : @return an icon as xhtml:i and a modal window as xhtml:div
 :)
declare function simple2xhtml:make-modal($node) as element(*)* {
    let $content :=
        if($node[ancestor::tei:ab[@type = "sketch"]]) then
            $node/ancestor::tei:ab[@type = "sketch"]//tei:figDesc//text()[not(./ancestor::tei:index)]
            => string-join(" ")
            => simple2xhtml:fix-whitespaces()

        else if($node[self::tei:note]) then
            simple2xhtml:recursion($node/node())

        else if($node[self::tei:ptr]) then
            let $corresp-node := $node/root()//*[@target = "#" || $node/@reference]
            return
                simple2xhtml:recursion($corresp-node/node())

        else
            ()

    let $id := util:hash(generate-id($node), "md5")
    let $target := "#" || $id
    return
        if($content) then
        (
            <i class="fa fa-comment" data-toggle="modal" data-target="{$target}"></i>,
            <div class="modal fade" id="{$id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">⨉</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        {if($node[self::tei:note]) then
                            "Autoranmerkung"
                        else
                            "Stellenkommentar"
                        }
                    </h4>
                  </div>
                  <div class="modal-body">
                    {$content}
                  </div>
                </div>
              </div>
            </div>
        )
        else
            ()
};
