xquery version "3.1";

(:~
 : This module is responsible for getting an XML version of the abbreviations
 : index which can be converted to TeX. The output XML version is appended to
 : fontane-full.xml during the creation of the intermediate format in etTransfo.xqm.
 :
 : @author Michelle Weidling
 : @version 0.1
 : @since v4.0.
 :)

module namespace abbrev-index="http://fontane-nb.dariah.eu/abbrev-index";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../../config/config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";


declare function abbrev-index:main() as element(tei:div) {
    let $docs-coll := collection($config:data-root || "/doku/")
    let $file := $docs-coll[matches(base-uri(.), "verzeichnis_der_abkuerzungen")]/*
    let $transformed :=
        abbrev-index:transform($file)
        => abbrev-index:tidy()
        => abbrev-index:sort()

    return
        element tei:div {
            attribute type {"abbrev-index"},
            $transformed
        }
};

(:~
 : Handles the transformation from XHTML to XML.
 :
 : @param $nodes The nodes currently processed
 : @return The transformed nodes
 :)
declare function abbrev-index:transform($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            if(matches($node, "important")
            or matches($node, "Gesamtdokumentation")) then
                ()
            else
                text {
                    replace($node, "ſ", "s")
                    => replace("m̄", "mm")
                    => replace("n̄", "nn")
                }

        case element(xhtml:h2) return
            ()

        case element(xhtml:h3) return
            element tei:head {
                attribute type {"table-head"},
                abbrev-index:transform($node/node())
            }

        case element(xhtml:tr) return
            element tei:row {
                if($node/ancestor::xhtml:thead
                or $node/child::xhtml:th) then
                    attribute role {"head"}
                else
                    (),
                abbrev-index:transform($node/node())
            }

        case element(xhtml:td) return
            element tei:cell {
                abbrev-index:transform($node/node())
            }

        case element(xhtml:th) return
            element tei:cell {
                abbrev-index:transform($node/node())
            }

        case element(xhtml:thead) return
            abbrev-index:transform($node/node())

        case element(xhtml:br) return
            element tei:lb {
                abbrev-index:transform($node/node())
            }

        case element(xhtml:i) return
            ()

        case element(xhtml:a) return
            if($node/text()) then
                abbrev-index:transform($node/node())
            else
                ()

        case element(xhtml:span) return
            abbrev-index:transform($node/node())

        default return
            element {QName("http://www.tei-c.org/ns/1.0", $node/local-name())} {
                abbrev-index:transform($node/node())
            }
};

(:~
 : Removes empty elements.
 :
 : @param $nodes The nodes currently processed
 : @return The transformed nodes
 :)
declare function abbrev-index:tidy($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        default return
            if($node//text()[matches(., "[\w\d]") and not(normalize-space(.) = "")]
            or $node[self::tei:lb]) then
                element {QName("http://www.tei-c.org/ns/1.0", $node/local-name())} {
                    $node/@*,
                    abbrev-index:tidy($node/node())
                }
            else
                ()
};


declare function abbrev-index:sort($nodes as node()*) as node()* {
    for $node in $nodes return
        typeswitch ($node)

        case text() return
            $node

        case element(tei:row) return
            let $first-char :=
                normalize-space($node/tei:cell[1]/text())
                => substring(1, 1)
                => upper-case()
            let $prev-first-char :=
                normalize-space($node/preceding-sibling::tei:row[1]/tei:cell[1]/text())
                => substring(1, 1)
                => upper-case()
            let $current-row :=
                element {QName("http://www.tei-c.org/ns/1.0", $node/local-name())} {
                    $node/@*,
                    abbrev-index:sort($node/node())
                }
            return
                if($node[@role = "head"]) then
                    $current-row
                else if(not($first-char = $prev-first-char)
                or not($node/preceding-sibling::tei:row[1])
                or $node/preceding-sibling::tei:row[1][@role = "head"]) then
                    (
                        element {QName("http://www.tei-c.org/ns/1.0", $node/local-name())} {
                            element tei:cell {
                                attribute type {"lemma"},
                                $first-char
                            },
                            element tei:cell {}
                        },
                        $current-row
                    )
                else
                    $current-row

        default return
                element {QName("http://www.tei-c.org/ns/1.0", $node/local-name())} {
                    $node/@*,
                    abbrev-index:sort($node/node())
                }
};
