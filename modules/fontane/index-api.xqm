xquery version "3.1";
(:~
 : This module provides an API to retrieve index entries as a sequence for a
 : given page.
 :
 : @author Mathias Göbel
 : @author Michelle Weidling
 : @version 1.1
 : @since 2.5.6
 : :)

module namespace index="https://fontane-nb.dariah.eu/indexapi";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace functx = "http://www.functx.com";
import module namespace ixp="http://fontane-nb.dariah.eu/index-processor" at "index-processor.xqm";
import module namespace rest="http://exquery.org/ns/restxq";

(:~
 : resolves and lists all references on a given page based on their index data
 :
 : @param $uri A valid TextGrid Base-URI without prefix
 : @param $surface A string to match a tei:surface/@n in the document
 : (use “-” as delimiter)
 : @return xhtml list with all entries
 : @see http://localhost:8080/exist/restxq/fontane/index/16b00/62v
 : @see https://fontane-nb.dariah.eu/api/index/16b00/62v
 :  :)
declare
  %rest:GET
  %rest:path("/api/index/{$uri}/{$surface}")
  %output:method("html")

  %test:name("entities per page")

  %test:args("16b00", "2r") (: return 4 groups :)
  %test:assertXPath("count($result) eq 4")

  %test:arg("uri", "16b00")
  %test:arg("surface", "2v")
  (: return empty sequence on interleaf :)
  %test:assertEmpty

  %test:args("", "") (: return custom error INDEX-API1 :)
  %test:assertError("INDEX-API1")

  %test:args("16b00", "foo") (: return custom error INDEX-API2 :)
  %test:assertError("INDEX-API2")

  %test:args("16b00", "1r") (: return more then 10 entities :)
  %test:assertXPath("count($result//xhtml:li) gt 10")

function index:entites-per-page($uri as xs:string, $surface as xs:string)
as element(xhtml:ul)* {
let $docpath := $ixp:dataPath || $uri || ".xml"
let $surface := tokenize($surface, "-")

(: test for document available :)
return
  if(not(doc-available($docpath)))
  then
    let $docsInDb := $ixp:dataCollection//tei:sourceDoc
        /replace(base-uri(), "\.xml|/|db|sade-projects|textgrid|data|xml", "")
        => string-join(", ")
    return
      error(QName("FONTANE", "INDEX-API1"), "Unable to resolve URI. Try one of " || $docsInDb || "." )
  else

let $doc := doc($docpath)
let $page := $doc//tei:surface[@n = $surface]

(: test for surface available :)
return if(not(exists($page)))
  then
    let $surfacesInDocument := $doc//tei:surface[@n]/string(@n)
        => string-join(", ")
    return
      error(QName("FONTANE", "INDEX-API2"), "tei:surface " || $surface || " not found. Try one of " || $surfacesInDocument || "." )
else

(: test for entries before further processing :)
if(not(exists(($page/tei:zone//tei:rs, $page/tei:surface[not(@n)]//tei:rs)))) then () else

let $entities := ($page/tei:zone//tei:rs/tokenize(@ref, " "), $page/tei:surface[not(@n)]//tei:rs/tokenize(@ref, " ")) => distinct-values()
let $groups := $entities ! substring-before(., ":") => distinct-values()

for $group in $groups
let $translate := local:translate-prefix($group, (count($entities[substring-before(., ":") = $group]) gt 1) )
order by $translate
return
  element xhtml:ul {
    element xhtml:li {
      attribute class {"group", $group},
      $translate,
      element xhtml:ul {
        for $entity in $entities[substring-before(., ":") = $group]
        let $ref := substring-after($entity, ":")
        let $index := ($ixp:dataCollection/id($ref))[1]
        let $name := string($index/*[1])
        let $list := index:get-list-by-id($ref)
        let $href := "register.html?e=" || $ref
        let $attributeTitle := if($group = "plc" and count($index/ancestor::tei:place) gt 1) then "in: " || string-join(reverse($index/ancestor::tei:place/tei:placeName), ", ") else ()
        order by $name
        return
          element xhtml:li {
            attribute data-ref { $entity },
            attribute class { $index/local-name() },
            $attributeTitle ! attribute title {$attributeTitle},
            element xhtml:a {
              attribute href { $href },
              attribute target { "_blank" },
              $name,
              if(($group = "plc") and $index/ancestor::tei:place)
              then element xhtml:span {
                  attribute class { "nestedPlaces" },
                  string($index/ancestor::tei:place[1]/*[1])
              }
              else ()
            }
          }
      }
    }
  }
};

(:~
 : Translate an index prefix into German.
 : @param $prefix a three letter code to resolve
 : @param $plural true when plural translation is required
 : @response the translation
 :   :)
declare function local:translate-prefix($prefix as xs:string, $plural as xs:boolean)
as xs:string {
if ($plural)
then
  switch ($prefix)
    case "eve" return "Ereignisse"
    case "psn" return "Personen"
    case "plc" return "Orte"
    case "org" return "Institutionen"
    case "wrk" return "Werke"
    default return "The prefix " || $prefix || " is not in the dictonary."
else
  switch ($prefix)
    case "eve" return "Ereignis"
    case "psn" return "Person"
    case "plc" return "Ort"
    case "org" return "Institution"
    case "wrk" return "Werk"
    default return "The prefix " || $prefix || " is not in the dictonary."
};

declare
  %test:name("entity list relation")

  %test:args("Dom_Karlos_Figur") (: person in wrk :)
  %test:assertEquals("list-works")

  %test:args("Purschian") (: person :)
  %test:assertEquals("listPerson")

function index:get-list-by-id($id as xs:string)
as xs:string {
let $entity := $ixp:dataCollection/id($id)[ not(. instance of element(tei:handNote) ) ]
let $test := if($entity[2]) then error(QName("FONTANE", "INDEX-API3"), "Entity »" || $id || "« has more then one occurence." ) else ()
let $test := if($entity) then () else error(QName("FONTANE", "INDEX-API3"), "Entity »" || $id || "« was not found." )
return
    switch ( $entity/local-name() )
        case "event" return "listEvent"
        case "org" return "listOrg"
        case "place" return "listPlace"
        case "person" return
            if($entity/ancestor::tei:item)
            then index:get-list-by-id( ($entity/ancestor::tei:item)[last()]/string(@xml:id) )
            else "listPerson"
        case "personGrp" return
            if($entity/ancestor::tei:item)
            then index:get-list-by-id( ($entity/ancestor::tei:item)[last()]/string(@xml:id) )
            else "listPerson"
        default return
            let $list := $entity/ancestor::tei:list[@type]
            return
                $list/local-name() || "-" || string($list/@type)
};

declare function index:get-list-by-node($node as element(*))
as xs:string {
    switch ( $node/local-name() )
        case "event" return "listEvent"
        case "org" return "listOrg"
        case "place" return "listPlace"
        case "person" return
            if($node/ancestor::tei:item)
            then index:get-list-by-node($node/ancestor::tei:item[@xml:id][1])
            else "listPerson"
        case "personGrp" return
            if($node/ancestor::tei:item)
            then index:get-list-by-node( ($node/ancestor::tei:item[@xml:id])[1] )
            else "listPerson"
        default return
            let $list := $node/ancestor::tei:list[@type]
            return
                $list/local-name() || "-" || string($list/@type)
};

(:~
 : Returns the rendered Information on a specific entity
 : @param $id the xml:id of an entity in the database
 : @return HTML serialization of the index entry
 :)
declare
  %rest:GET
  %rest:path("/api/index/entity/{$id}")
  %output:method("html")

  %test:arg("id", "Storm")
  %test:assertTrue
function index:rendered-entity($id as xs:string)
as item()* {
    let $index-uris := ("253sx.xml", "253t0.xml", "253t1.xml", "253t2.xml", "253t3.xml")
    let $indices := $ixp:dataCollection[functx:substring-after-last(base-uri(.), "/") = $index-uris]
    let $node := $indices/id($id)
    let $type := $node/root()//tei:body/*[1]/local-name()
                (: listEvent, listPlace, listOrg, listPerson or list :)
    return
        switch ($type)
            case "listEvent" return
                ixp:prepare-event($node)
            case "listOrg" return
                ixp:prepare-org($node)
            case "listPerson" return
                ixp:prepare-person($node)
            case "listPlace" return
                ixp:prepare-place($node)
            case "list" return
                switch ($node/local-name())
                    case "person" return ixp:prepare-person($node)
                    case "place" return ixp:prepare-place($node)
                    default return
                        ixp:prepare-work($node)
            default return
                error(QName("FONTANE", "INDEX-API4"), "unknown node-name: " || $type)
};

(:~
 : Provides a simple API for searching the index entries. Used for autocompletion.
 : Usually the query should be a simple string. No wildcards, nothing special,
 : always case-insensitive. Only performed for strings larger than one char.
 : @param $query A string to search all index entries (names, label, etc.)
 : @return $index A list of indices to query, defaults to all, oneOf: listEvent,listOrg,listPerson,listPlace,list-works,list-fontane,list-periodicals.
 :)
declare
  %rest:GET
  %rest:path("/api/index/search/{$query}")
  %rest:query-param("index", "{$index}", "listEvent,listOrg,listPerson,listPlace,list-works,list-Fontane,list-periodicals")
  %output:method("json")

  %test:name("simple search")
  %test:arg("query", "Dom Karlos")
  %test:arg("index", "listEvent,listOrg,listPerson,listPlace,list-works,list-Fontane,list-periodicals")
  %test:assertXPath("array:size($result) = 2")

function index:search($query as xs:string, $index)
as array(*) {
let $query := $query => xmldb:decode-uri()
return
  if(string-length($query) lt 2)
  then []
  (: an empty array. explicit array constructor fails in eXist returning somewhat
     like a xs:string "Index: 0, Size: 0" from
         java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
  :)
  else (: continue :)
let $prepare := tokenize($index, ",")
let $hits :=
(
  $ixp:dataCollection//tei:person[contains(tei:persName, $query)],
  $ixp:dataCollection//tei:personGrp[contains(tei:persName, $query)],
  $ixp:dataCollection//tei:place[contains(tei:placeName, $query)],
  $ixp:dataCollection//tei:org[contains(tei:orgName, $query)],
  $ixp:dataCollection//tei:event[contains(tei:label, $query)],
  $ixp:dataCollection//tei:name[contains(., $query)]/parent::tei:item
)
return
    array{
        for $hit in $hits
        let $id := string($hit/@xml:id)
        let $target := if($id) then $id else
                        $hit/tei:note/tei:ptr/substring-after(@target, "#")
        let $list := index:get-list-by-node($hit)
        where $list = $prepare
        where $id != ""
        return
            map{
                "name": string($hit/*[1]),
                "id": $target,
                "list": $list,
                "type": $hit/local-name()
            }
    }
};

(:~
 : Returns available image from TextGrid Repository
 : @param $type the index to look up, one of: plc, wrk, psn, eve, org
 : @param $id the xml:id of an entity in the database
 : @return an array of URIs or an empty array
 :)
declare
  %rest:GET
  %rest:path("/api/index/image/{$type}/{$id}")
  %output:method("json")

  %test:arg("id", "Arnoldi-Denkmal")
  %test:arg("type", "plc")
  %test:assertXPath("$result?1 => exists()")
function index:rendered-entity($type as xs:string, $id as xs:string)
as array(*) {
    let $request :=
      <hc:request
        method="get"
         href="https://textgridlab.org/1.0/tgsearch-public/search/?q=(title:%22{$type}:{$id}%22)" />
    let $tgsearch := hc:send-request($request)[2]
    return
        [ $tgsearch//tgmd:textgridUri/string() ]
};
