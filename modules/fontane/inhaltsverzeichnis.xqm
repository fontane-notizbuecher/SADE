xquery version "3.1";

(:~
 : This module parses all TEI documents and creates an over-all table of contents.
 : @author Mathias Göbel
 : @version 0.1
 : @since v4.7.0
 :)


module namespace f-content="http://fontane-nb.dariah.eu/ns/SADE/content";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

(:~
 : The main function returns a complete over-all index.
 : @param $node The current node prepared by the templating engine
 : @param $model Stores any arbitrary information to pass-through templating functions
 :)
declare function f-content:main($node as node(), $model as map(*)) {
let $items := collection($config:data-root || "/data")//tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:ab/tei:list[@type="editorial"]/tei:item
let $sortPattern := "^\W+"
return
<xhtml:div>
  <xhtml:ul class="paginationAlphabet">
    {let $groups := $items ! (upper-case(string(.)) => replace($sortPattern, "") => substring(1, 1))
    return sort(distinct-values($groups)) ! <xhtml:li><xhtml:a href="#id{.}">{.}</xhtml:a></xhtml:li>}
  </xhtml:ul>
  <xhtml:ul class="gesamtinhaltsverzeichnis">
    {for $item in $items
    let $order := upper-case(string($item)) => replace($sortPattern, "")
    let $group := $order => substring(1, 1)
    stable order by $order
    group by $group
    return
        <xhtml:li id="id{$group}">{$group}
            <xhtml:ul>
                {
                    for $i in $item
                    let $nbname := $i/root()/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]/substring-after(., " ")
                    return
                    <xhtml:li class="tocItem"><xhtml:span class="nbname">[{$nbname}]</xhtml:span>&#160;{local:RenderTOCitem($i)}</xhtml:li>
                }
            </xhtml:ul>
        </xhtml:li>
    }
  </xhtml:ul>
</xhtml:div>
};

(:~
 : Prepares all content of tei:item from within tei:msContent. Recursion.
 : @param $item The tei:item or any content from within via recursion
 :)
declare %private function local:RenderTOCitem($item) {
    let $uri := try{$item/base-uri() => substring-after("data/")}catch*{()}
    return
    for $n in $item/node() return
        typeswitch($n)
            case element(tei:item) return
                local:RenderTOCitem($n)
            case element(tei:ref) return
                element xhtml:a {
                    attribute href {
                      if(starts-with($n/@target, "#xpath"))
                      then
                        'edition.html?id=/'|| $uri ||'&amp;page='||substring-before(substring-after($n/@target, "@n='"), "'])")
                      else
                        'edition.html?id=/'|| $uri ||'&amp;page='||substring-before(substring-after($n/@target, "_"), "_")||'&amp;target='||substring-after($n/@target, "#")
                      },
                    $n/text()
                }
            case element(tei:rs) return
                let $ref := $n/string(@ref)
                let $type := substring-before($ref, ':')
                let $link := switch($type)
                                case 'lit' return 'literaturvz.html?id='||substring-after($ref, ':')
                                default return 'TODOvz.html'
                return <xhtml:a href="{$link}">{$n/text()}</xhtml:a>
            case text() return
                $n
            default return
                local:RenderTOCitem($n)
};
