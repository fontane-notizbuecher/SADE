xquery version "3.1";
(:~
: This module prepares the index html.
: @author Mathias Göbel
: @version 1.0
: @see https://fontane-nb.dariah.eu/register.html
:)
module namespace ixv="http://fontane-nb.dariah.eu/index-viewer";

import module namespace ixp="http://fontane-nb.dariah.eu/index-processor" at "index-processor.xqm";
import module namespace index="https://fontane-nb.dariah.eu/indexapi" at "index-api.xqm";

import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace functx="http://www.functx.com";

declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

(: Main cache function
 : @return path to the cached document :)
declare
  %test:name("cache")
(:  %test:pending (: throws NPE on non-initialized db. needs some data from the grid. :):)
function local:cache()
 {
let $test4lock := if (doc-available("/db/sade-projects/textgrid/data/xml/xhtml/index.lock"))
                    then error(QName("FONTANE", "INDEXVIEW2"), "unable to lock resource. index is in preparation or preparation failed.")
                    else true()

let $set-option := util:declare-option("exist:output-size-limit", "2000000")
let $collection-uri := "/db/sade-projects/textgrid/data/xml/xhtml",
    $resource := "index.xml"
let $agg := '253st.xml' (: TextGrid Aggregation containing all index files :)
let $uris := (doc('/db/sade-projects/textgrid/data/xml/agg/' || $agg)
          //ore:aggregates/substring-after(@rdf:resource, 'textgrid:')
        )[. != "25547"] (: omit the bibliography here :)
let $lastMod := $uris ! xmldb:last-modified($ixp:dataPath, . || '.xml')

return
    if(not(local:login($collection-uri)))
    then error(QName("FONTANE", "INDEXVIEW1"), "unable to authenticate.")
    else
        if( xmldb:last-modified($collection-uri, $resource) gt max($lastMod) )
        then
            (: prerendered index younger than index files: do nothing :)
            $collection-uri || "/" || $resource
        else
            (: create the cache :)
            let $lock := xmldb:store("/db/sade-projects/textgrid/data/xml/xhtml", "index.lock", <lock/>),
                $cache := xmldb:store($collection-uri, $resource, <xhtml:div>{util:eval('ixp:register()')}</xhtml:div>),
                $removeLock := xmldb:remove("/db/sade-projects/textgrid/data/xml/xhtml", "index.lock")
            return
                $cache
};

(: forces the creation of a new cache file
 : @return document-node() of the index cache file :)
declare function ixv:prepare-new-cache()
as document-node() {
let $path := "/db/sade-projects/textgrid/data/xml/xhtml/index.xml"
let $cleanup :=
    if( doc-available($path) )
    then xmldb:remove("/db/sade-projects/textgrid/data/xml/xhtml", "index.xml")
    else true()
let $prepare := local:cache()
return
    doc( $prepare )
};

(:~
 : Recursive transformation of the index document, if a specific item is requested.
 : @param $nodes Node to be parsed
 : @param $id A single id prepared with generate-id()
 : :)
declare function local:transform-entity($nodes as node()+, $id as xs:string)
as node()* {
for $node in $nodes
let $thisId := generate-id($node)
return
    if ($thisId = $id)
    then
      (: set 'active' class :)
        element { node-name($node) } {
            $node/@*[local-name() != "class"],
            attribute class {
                string($node/@class),
                "fhighlighted"
            },
            local:transform-entity($node/node(), $id)
        }
    else if(starts-with($id, $thisId))
    then
        typeswitch ($node)
        case element(*) return
            element { node-name($node) } {
                if(contains($node/@class, "tab-pane"))
                then
                    ($node/@*[local-name() != "class"],
                    attribute class {
                        string($node/@class),
                        "active"
                    })
                else
                    $node/@*,
                local:transform-entity($node/node(), $id)
            }
        default return
            $node
    else $node
};

declare function ixv:main($model as map()*, $node as node(), $index as xs:string) {
if ($ixp:notebookParam)
then
    ixp:register($index)[./@id = $index]
    => functx:change-element-ns-deep("http://www.w3.org/1999/xhtml", "")
else
    let $doc := doc( local:cache() )
    return
        if ($ixp:getEntity)
        then
            let $id := generate-id($doc//xhtml:li[@id = $ixp:getEntity])
            return
                local:transform-entity($doc/xhtml:div/xhtml:div[@id = $index], $id)
        else
            $doc/*/xhtml:div[@id = $index]
};

declare function ixv:tabs($nodes as node(), $model as map(*), $index as xs:string) {
    local:transfrom-tabs($nodes/*, $index)
};

(:~
 : Recursive transformation of a given node from the template to a
 : represent a list of tabs with current index view marked `active`.
 : @param $nodes Nodes to transform
 : @param $index The index that should be marked `active`
 :   :)
declare
  %test:name("mark active tab")

  %test:arg("nodes", "<li xmlns=""http://www.w3.org/1999/xhtml""><a href=""register-listEvent.html"">Ereignisse</a></li>")
  %test:arg("index", "listEvent")
  %test:assertXPath("$result/@class = 'active'")

  %test:arg("nodes", "<div xmlns=""http://www.w3.org/1999/xhtml""/>")
  %test:arg("index", "listEvent")
  %test:assertEmpty

function local:transfrom-tabs($nodes as element()+, $index as xs:string) {
for $node in $nodes
return
    typeswitch ( $node )
    case element(xhtml:div) return
        $node/node() ! local:transfrom-tabs(., $index)
    case element(xhtml:ul) return
        element xhtml:ul {
            $node/@*,
            $node/* ! local:transfrom-tabs(., $index)
        }
    case element(xhtml:li) return
        element xhtml:li {
            $node/@*[not(@class)],
            if($node/xhtml:a/@href = "register-" || $index || ".html")
            then
                attribute class { $node/@class ! string(.), "active"}
            else $node/@class,
            $node/* ! local:transfrom-tabs(., $index)
        }
    case element(xhtml:a) return
        element xhtml:a {
            if($ixp:notebookParam)
            then
                ($node/@*[local-name() != "href"],
                attribute href { string($node/@href) || "?nb=" || $ixp:notebookParam })
            else $node/@*,
            $node/(*,text())
        }
    default return
        $node
};

declare function local:login($col) {
(: check for db availability :)
if(hc:send-request(<hc:request method="get"/>, "http://localhost:8080")[1]/@status = 200)
then
    if(false() = xmldb:login($col, config:get('sade.user'), config:get("sade.password")))
    then (xmldb:login($col, "admin", ""))
    else true()
else true()
};

declare function ixv:item($node, $map, $e as xs:string?, $nb as xs:string?) {
    if(string-length($e) gt 1)
    then
        index:rendered-entity($e)
    else ()
};

declare function ixv:items($node, $map, $nb as xs:string, $index as xs:string) {
let $sourceDoc := doc("/db/sade-projects/textgrid/data/xml/data/" || $nb || ".xml")/tei:TEI/tei:sourceDoc
return
switch ($index)
    case "listPerson"
    return
        let $prefix := "psn:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $persDoc := doc("/db/sade-projects/textgrid/data/xml/data/253sx.xml")//tei:body/tei:listPerson
        return
            ixp:persons( $persDoc, $list)
    case "listPlace" return
        let $prefix := "plc:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $placeDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t2.xml")//tei:body/tei:listPlace
        return
            ixp:places( $placeDoc, $list )
    case "listOrg" return
        let $prefix := "org:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $orgDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t1.xml")//tei:body/tei:listOrg
        return
            ixp:orgs( $orgDoc, $list )
    case "listEvent" return
        let $prefix := "eve:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $eventDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t0.xml")//tei:body/tei:listEvent
        return
            ixp:events( $eventDoc, $list )
    case "list-works" return
        let $prefix := "wrk:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $itemDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t3.xml")//tei:list[@type="works"]
        return
            ixp:works( $itemDoc, $list )
    case "list-Fontane" return
        let $prefix := "wrk:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $itemDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t3.xml")//tei:list[@type="Fontane"]
        return
            ixp:works( $itemDoc, $list )
    case "list-periodicals" return
        let $prefix := "wrk:"
        let $references := ($sourceDoc//tei:rs[contains(@ref, $prefix)]/string(@ref) ! tokenize(., " "))[starts-with(., $prefix)]
        let $list := $references ! substring-after(., $prefix)
        let $itemDoc := doc("/db/sade-projects/textgrid/data/xml/data/253t3.xml")//tei:list[@type="periodicals"]
        return
            ixp:works( $itemDoc, $list )
default return ()
};
