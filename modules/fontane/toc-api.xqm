xquery version "3.1";
(:~
 : Provides a REST-API to get table of contents
 :
 : @author Mathias Göbel
 : @version 1.2
 : @since 2.6
 : :)

module namespace toc="https://fontane-nb.dariah.eu/tocapi";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace test="http://exist-db.org/xquery/xqsuite";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace rest="http://exquery.org/ns/restxq";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";

declare variable $toc:dataPath := "/db/sade-projects/textgrid/data/xml/data/";
declare variable $toc:xhtmlPath := "/db/sade-projects/textgrid/data/xml/xhtml/";

(:~
 : Prepares the complete table of contents used at "Inhaltsübersicht" tab.
 : @param $uri A valid textgrid URI without prefix pointing to a TEI document (notebook)
 : @return HTML serilaization of the tabel of contents, pushed into bootstrap panels.
 :)
declare
  %rest:GET
  %rest:path("/api/toc/{$uri}")
  %output:method("html")

  %test:name("entities per page")
  %test:arg("uri", "16b00")
function toc:table-of-contents($uri as xs:string)
{
  let $doc := doc($toc:dataPath || $uri || ".xml")
  let $toc := $doc//tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:ab/tei:list[@type="editorial"]
  return(
    element div {
      element div {
        attribute class {"panel-group"},
        for $item at $pos in $toc/tei:item
        let $heading := tokenize(string($item), "\s?\(")[1]
        let $id := replace($heading, "\W", "-")
        return
          element div {
            attribute class {"panel panel-default"},
            element div { (: heading :)
              attribute class {"panel-heading"},
              element h4 {
                attribute class {"panel-title"},
                element a {
                  attribute href { "#collapse" || $pos },
                  attribute data-parent {"#accordion"},
                  attribute data-toggle { "collapse" },
                  text { $heading }
                }
              },
              element div {
                attribute class {"toc-checker"},
                element input {
                  attribute type {"checkbox"},
                  attribute value {"1"},
                  attribute id {$id},
                  attribute name {$id}
                },
                element label {
                  attribute for {$id}
                }
              }
            }, (: end heading :)
            element div {
              attribute class { "panel-collapse collapse" },
              attribute id { "collapse" || $pos },
              element div {
                attribute class {"panel-body tocview"},
                element p {
                  substring-before($item, ", Blatt")
                },
                element ul {
                  for $ref in $item//tei:ref
                  let $target := string($ref/@target)
                  let $targetZone := matches($target, "^#[A-E]\d\d_.+_[a-z0-9]+$")
                  let $targetPage :=
                    if(starts-with($target, "#xpath(//surface"))
                    then $target => substring-after("[@n='") => substring-before("']")
                    else if ($targetZone)
                    then $target => substring-after("_") => substring-before("_")
                    else error(QName("FONTANE", "TOC-API1"), "invalid target: «" || $target || "»")
                  let $targetParam := if($targetZone) then "&amp;target=" || substring-after($target, "#") else ""
                  let $targetXhtml := doc("/sade-projects/textgrid/data/xml/xhtml/" || $uri || "/" || $targetPage || ".xml")
                  return
                    element li {
                      attribute class { "target" || $targetPage },
                      element a {
                        attribute href { "edition.html?id=/xml/data/" || $uri || ".xml&amp;page=" || $targetPage || $targetParam },
                        element div {
                            attribute class {"upper"},
                            element img {
                                attribute src {($targetXhtml//xhtml:img/@src)[1] => replace("/,1000/0/default.jpg", "/,150/0/default.jpg")}
                            }
                        },
                        element div {
                            attribute class {"lower"},
                            text { string($ref) => replace("\s*Blatt\s+", "") }
                        }
                      }
                    }
                }
              }
            }
          }
      }
    },
    doc( $toc:xhtmlPath || $uri || "/toc.xml" )
  )
};
