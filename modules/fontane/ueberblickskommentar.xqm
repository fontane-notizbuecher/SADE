xquery version "3.1";
module namespace f-ueberblickskommentar="http://fontane-nb.dariah.eu/Ueberblickskommentar";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare function f-ueberblickskommentar:render($docpath as xs:string) {
let $tei := doc($docpath)//tei:teiHeader,
    $items :=  map{
      "Signatur":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/msIdentifier",
          "content": f-ueberblickskommentar:signatur($tei)
        },
      "Druckgeschichte":
        map{
          "path": "//fileDesc/publicationStmt/ab/listBibl/listBibl[1]",
          "content": f-ueberblickskommentar:druckgeschichte($tei)
        },
      "Inhalt":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/msContents/ab/list[@type='editorial']/item",
          "content": f-ueberblickskommentar:inhalt($tei)
        },
      "Textsorten/Gattungen":
        map{
          "path": "//profileDesc/textClass/keywords",
          "content": f-ueberblickskommentar:textsorten($tei)
        },
      "Fontanes Quellen":
        map{
          "path": "//profileDesc/textDesc/derivation/linkGrp/link/",
          "content": f-ueberblickskommentar:fontanes-quellen($tei)
        },
      "Beilagen":
        map{
          "path": "//msPart",
          "content": f-ueberblickskommentar:beilagen($tei)
        },
      "Entstehungszeit/Benutzungszeitraum":
        map{
          "path": "//profileDesc/creation/date",
          "content": f-ueberblickskommentar:entstehungszeit($tei)
        },
      "Sprachen":
        map{
          "path": "//profileDesc/langUsage/language",
          "content": f-ueberblickskommentar:sprachen($tei)
        },
      "Schreiberhände":
        map{
          "path": "//profileDesc/handNotes[ number(@n) lt 7 ]",
          "content": f-ueberblickskommentar:schreiberhaende($tei)
        },
      "Schreibstoffe und -geräte":
        map{
          "path": "//profileDesc/handNotes[ number(@n) = (7,8) ]",
          "content": f-ueberblickskommentar:schreibstoffe($tei)
        },
      "Duktus":
        map{
          "path": "//profileDesc/handNotes[ number(@n) = 9 ]",
          "content": f-ueberblickskommentar:duktus($tei)
        },
      "Druckschriften":
        map{
          "path": "//sourceDesc/msDesc/physDesc/typeDesc/ab",
          "content": f-ueberblickskommentar:druckschriften($tei)
        },
      "Layout":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/layoutDesc/layout",
          "content": f-ueberblickskommentar:layout($tei)
        },
      "Umfang mit Vorsatzblatt":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/",
          "content": f-ueberblickskommentar:umfang($tei)
        },
      "Format":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/dimensions",
          "content": f-ueberblickskommentar:format($tei)
        },
      "Einband":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/bindingDesc",
          "content": f-ueberblickskommentar:einband($tei)
        },
      "An- und Aufklebungen":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) != 'Vakat-Seiten:'][hi/string(.) != 'Blattfragmente:']",
          "content": f-ueberblickskommentar:anklebungen($tei)
        },
      "Blattfragmente":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Blattfragmente:']",
          "content": f-ueberblickskommentar:blattfragmente($tei)
        },
      "Vakat-Seiten":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Vakat-Seiten:']",
          "content": f-ueberblickskommentar:vakat-seiten($tei)
        },
      "Skizzen":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/decoDesc/ab[hi/string(.) = 'Skizzen:']",
          "content": f-ueberblickskommentar:skizzen($tei)
        },
      "Stempel":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/additions/ab[hi/string(.) = 'Stempel:']",
          "content": f-ueberblickskommentar:stempel($tei)
        },
      "Foliierung":
        map{
          "path": "//fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/foliation",
          "content": f-ueberblickskommentar:foliierung($tei)
        },
      "Besonderheiten":
        map{
          "path": "(//fileDesc/sourceDesc/msDesc/physDesc/handDesc, //fileDesc/sourceDesc/msDesc/physDesc/typeDesc)",
          "content": f-ueberblickskommentar:besonderheiten($tei)
        }
    },

    $items := for $title at $pos in map:keys($items)
              let $path := $items($title)("path")
              let $content := $items($title)("content")
              return
                f-ueberblickskommentar:row($pos, $title, $path, $content)

return
    <div class="container">
    {   for $ab in $tei//abstract/ab
        return
            <xhtml:p class="intro">{
                for $node in $ab/node()
                return
                    typeswitch ( $node )
                    case element( tei:bibl ) return
                        let $target := $node/tei:ptr/@target => substring-after("lit:")
                        let $bibl := collection("/db/sade-projects")//@xml:id[.=$target]/parent::node()
                        let $citRange := $node/tei:citedRange/text()
                        return
                            (<xhtml:a href="literaturvz.html?id={$target}">{ $bibl//tei:abbr/text() }</xhtml:a>,
                              if( $citRange ) then " "||$citRange else ()
                            )

                    case element( tei:hi ) return
                        <xhtml:em>{$node/text()}</xhtml:em>
                    default return
                        $node


            }</xhtml:p>
    }
    {if(doc-available($docpath)) then
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Information</th>
                    <th>Beschreibung</th>
                </tr>
            </thead>
            <tbody>
                {$items}
            </tbody>
        </table>
    else <xhtml:div>The requested document is not available.</xhtml:div> }
    </div>
};


declare function local:textref($text as node()*) as node()*{
for $item in $text return
    if($item/parent::tei:ref)
    then
        let $target := string($item/parent::tei:ref/@target),
            $id := if(matches($target, '^#[A-E]\d\d_'))
                    then substring-after($target, '#')
                    else (),
            $page :=    if($id)
                        then $text/ancestor::tei:TEI//tei:surface[.//@xml:id = $id]/string(@n)
                        else tokenize($target, "'")[2],
            $doc := request:get-parameter('id', ''),
            $hl := if($id) then '#'||$id else (),
            $link := 'edition.html?id='||$doc||'&amp;page='||$page||$hl
    return
        <xhtml:a href="{$link}">{$item}</xhtml:a>
    else if($item/parent::tei:rs) then
            let $ref := $item/parent::tei:rs/string(@ref)
            let $type := substring-before($ref, ':')
            let $link := switch($type)
                            case 'lit' return 'literaturvz.html?id='||substring-after($ref, ':')
                            default return 'TODOvz.html'
            return <xhtml:a href="{$link}">{$item}</xhtml:a>
    else $item
};


declare function f-ueberblickskommentar:row($pos, $title, $path, $content){
  <tr class="teiHeaderRow{if(replace(string-join($content), "\s", "") = 'Keine') then ' OutOfOrder' else ''}">
    <th scope="row">
      <span class="rowNum">
        <a class="headerLink" href="ueberblickskommentar.html?{request:get-query-string()}#r{$pos}">
          <i class="fa fa-link"></i>
        </a>{$pos}
      </span>
      <span class="anchor" id="r{$pos}"/>
    </th>
    <td><span title="{$path[$pos]}">{$title}</span></td>
    <td>{$content}</td>
  </tr>
};


declare function f-ueberblickskommentar:signatur($tei as element(tei:teiHeader)){
  <ul>
    <li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/repository/text()}
      <ul>
        <li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/collection/text()}
          <ul>
            <li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/idno/string(@type)}: {$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/idno/text()}</li>
            <li>{$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/altIdentifier/idno/string(@type)}: {$tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/altIdentifier/idno/text()}</li>
            <li>Titel: {if(exists($tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/msName/text())) then $tei/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/msName/text() else 'ohne'}</li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
};


declare function f-ueberblickskommentar:druckgeschichte($tei as element(tei:teiHeader)){
  <ul>{
      for $listBibl in $tei/tei:fileDesc/publicationStmt/ab/listBibl/listBibl[1]
      return <li>{$listBibl/head/text()}<ul>{for $item in $listBibl/bibl return <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li>}</ul></li>
  }</ul>
};


declare function f-ueberblickskommentar:inhalt($tei as element(tei:teiHeader)){
  <ul>
      <li>Herausgeber-Inhaltsverzeichnis
          <ul>{for $item in $tei//msContents/ab/list[@type="editorial"]/item return
              <li>{for $text in $item//text() where not($text/parent::tei:hi) return local:textref($text)}</li> }
          </ul>
      </li>
  </ul>
};


declare function f-ueberblickskommentar:textsorten($tei as element(tei:teiHeader)){
  let $keywordsCat := $tei/profileDesc/textClass/keywords[@scheme]
  return
      <xhtml:ul>
      {for $keyword at $pos in $keywordsCat
      return
          (if($pos = 2) then <xhtml:hr/> else (),
          <xhtml:li class="textClass_cat">
              <xhtml:span title="{string($keyword/@scheme)}">
                  {if( $keyword//term[. = 'fiction'] ) then "Fiktional" else ()}
                  {if( $keyword//term[. = 'non-fiction'] ) then "Nicht-Fiktional" else ()}
              </xhtml:span>
              <xhtml:ul>
                  {for $term in $keyword/following-sibling::tei:keywords[1][not(@scheme)]//tei:term
                  return
                      <xhtml:li>{string($term)}</xhtml:li>
                  }
              </xhtml:ul>
          </xhtml:li>)
      }
      </xhtml:ul>
};


declare function f-ueberblickskommentar:fontanes-quellen($tei as element(tei:teiHeader)){
  if( not($tei//profileDesc/textDesc/derivation/*) ) then "Keine" else
  <ul>{for $type in distinct-values($tei//profileDesc/textDesc/derivation/linkGrp/link/string(@type))
      return
          <li>
            {
                switch($type)
                  case 'mentioned' return 'Im Notizbuch von Fontane genannte Quellen'
                  case 'ascertained' return 'Ermittelte Quellen'
                  case 'ascertained' return 'Weitere ermittelte Quellen'
                  default return 'TODO @type'
            }
            <ul> {for $link in $tei//profileDesc/textDesc/derivation/linkGrp/link[@type = $type]
              let $target := substring-after(substring-after($link/@target, ' '), ':')
              let $litVZ := doc('/db/sade-projects/textgrid/data/xml/data/25547.xml')/tei:TEI
              let $page := substring-after($link/@target, "_") => substring-before("_")
              return
                  if(starts-with($link/@target, "http://textgridrep.org/")) then
                      <xhtml:li>
                        <xhtml:a href="literaturvz.html?id={$target}">
                            {
                                if(exists($litVZ//bibl[@xml:id = $target]//bibl)) then
                                    $litVZ//bibl[@xml:id = $target]/descendant::abbr[1]/text() => replace("\.$", "")
                                else
                                    $litVZ//bibl[@xml:id = $target]//abbr/text() => replace("\.$", "")
                            }
                        </xhtml:a>;
                        {
                            if($link/@resp) then
                                (' vgl. ',
                                <xhtml:a href="literaturvz.html?id={substring-after($link/@resp, ':')}">
                                    {$litVZ//bibl[@xml:id = substring-after($link/@resp, ':')]//abbr/text()}
                                </xhtml:a>, '.')
                            else
                                ()
                        }
                      </xhtml:li>
                  else
                  <xhtml:li>
                        <xhtml:a href="literaturvz.html?id={$target}">
                            {
                                if(exists($litVZ//bibl[@xml:id = $target]//bibl)) then
                                    $litVZ//bibl[@xml:id = $target]/descendant::abbr[1]/text() => replace("\.$", "")
                                else
                                    $litVZ//bibl[@xml:id = $target]//abbr/text() => replace("\.$", "")

                            }
                        </xhtml:a>;
                        <xhtml:a href="edition.html?id={request:get-parameter("id", "")}&amp;page={$page}&amp;target={substring-before($link/@target, " ") => substring-after("#")}">
                            Blatt {$page}
                        </xhtml:a>.
                        {
                            if($link/@resp) then
                                (' [Vgl. ',
                                <xhtml:a href="literaturvz.html?id={substring-after($link/@resp, ':')}">
                                    {
                                        $litVZ//bibl[@xml:id = substring-after($link/@resp, ':')]//abbr/text()
                                    }
                                </xhtml:a>, '.]')
                            else
                                ()
                        }
                  </xhtml:li>
          } </ul></li>}</ul>
};


declare function f-ueberblickskommentar:beilagen($tei as element(tei:teiHeader)){
  if(not($tei//msPart/*)) then "Keine" else
    <div>
      <div>{
        switch(count($tei//msPart))
            case 1 return "Eine"
            case 2 return "Zwei"
            case 3 return "Drei"
            case 4 return "Vier"
            case 5 return "Fünf"
            case 6 return "Sechs"
            case 7 return "Sieben"
            case 8 return "Acht"
            case 9 return "Neun"
            default return "Übersetzung von Zahl nur bis 9."}
      </div>
      <ul>{
         for $msPart in $tei//tei:msPart[tei:msIdentifier][tei:ab]
          return
              <li>{local:textref($msPart/ab//text())}</li>
      }</ul>

    </div>
};


declare function f-ueberblickskommentar:entstehungszeit($tei as element(tei:teiHeader)){
  let $lis := (<li>{if (exists($tei/profileDesc/creation/date[@type = 'authorial']//text()))then () else attribute class {'OutOfOrder'} }Fontanes Angabe: {if(exists($tei/profileDesc/creation/date[@type = 'authorial']//text())) then $tei/profileDesc/creation/date[@type = 'authorial']//text() else 'Keine'}</li>,
              <li>{if (exists($tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text()))then () else attribute class {'OutOfOrder'} }Friedrich Fontanes Angabe: {if(exists($tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text())) then $tei/profileDesc/creation/date[@type = 'Friedrich_Fontane']//text() else 'Keine'}</li>,
              <li>{if (exists($tei/profileDesc/creation/date[@type = 'editorial']//text()))then () else attribute class {'OutOfOrder'} }Ermittelt: {if(exists($tei/profileDesc/creation/date[@type = 'editorial']//text())) then $tei/profileDesc/creation/date[@type = 'editorial']//text() else 'Keine'}</li>)
  return
  <ul>{$lis}</ul>
};


declare function f-ueberblickskommentar:sprachen($tei as element(tei:teiHeader)){
  <ul>{for $lang in $tei/profileDesc/langUsage/language return <li>{$lang/text()}</li>}</ul>
};


declare function f-ueberblickskommentar:schreiberhaende($tei as element(tei:teiHeader)){
  <ul>{for $hand in $tei/profileDesc/handNotes return
      switch($hand/number(@n))
          case 1 return <li>Zeitgenössische Schreiberhände &#x2013; Autor <ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
          case 2 return <li>Zeitgenössische Schreiberhände &#x2013; Schreiber<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
          case 3 return <li>Postume Schreiberhände &#x2013; Schreiber<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
          case 4 return <li>Postume Schreiberhände &#x2013; Archivare<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
          case 5 return <li>Zeitgenössiche und postume Schreiberhände &#x2013; Stempel<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
          case 6 return <li>Zeitgenössische und postume Schreiberhände &#x2013; Drucke<ul>{for $h in $hand/handNote return <li>{$h//text()}</li>}</ul></li>
      default return ()
   }</ul>
};


declare function f-ueberblickskommentar:schreibstoffe($tei as element(tei:teiHeader)){
  <ul>{
    for $hand in $tei/profileDesc/handNotes[@n = ("7", "8")]
    return (
      if($hand/@n = "8") then <hr/> else (),
      for $h in $hand/handNote
      return
        <li>{$h//text()}</li>
   )}</ul>
};


declare function f-ueberblickskommentar:duktus($tei as element(tei:teiHeader)){
  <ul>{
    for $h in $tei/profileDesc/handNotes[@n = "9"]/handNote
    return
      <li>
          {switch($h/string(@script))
              case 'clean' return 'Reinschriftlich'
              case 'standard' return 'Standard'
              case 'hasty' return "Unruhig"
              case 'angular' return "Gezackt"
              default return 'TODO'}
          <ul>
            <li>{$h//text()}</li>
          </ul>
      </li>
  }</ul>
};


declare function f-ueberblickskommentar:druckschriften($tei as element(tei:teiHeader)){
  <ul>{
    for $item in $tei//sourceDesc/msDesc/physDesc/typeDesc/ab
    return
      <li>{local:textref($item//text()[not(parent::tei:hi)])}</li>
  }</ul>
};


declare function f-ueberblickskommentar:layout($tei as element(tei:teiHeader)){
  <ul>{
      for $layout in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/layoutDesc/layout
      return
        <li>{$layout//text() ! local:textref(.)}</li>
  }</ul>
};


declare function f-ueberblickskommentar:umfang($tei as element(tei:teiHeader)){
  let $unit := string($tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/@unit)
  let $quantity := $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/measure/@quantity
  return
    $quantity
    || ' '
    || (switch($unit) case 'leaf' return 'Blatt' default return 'TODO: Einheit übersetzen!!!')
};


declare function f-ueberblickskommentar:format($tei as element(tei:teiHeader)){
  <ul>{
    for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/extent/dimensions
    return
      <li>{
        switch(string($item/@type)) case 'leaf' return 'Blatt' case 'binding' return 'Einband' default return 'TODO: @type übersetzen!!!',
        <ul>{
          for $i in $item/*
          return
            <li>{
              switch($i/local-name()) case 'width' return 'Breite' case 'height' return 'Höhe' case 'depth' return 'Tiefe'
              default return 'TODO: ' || $i/local-name() || ' übersetzen!!!'}
              &#x20;
              {string($i/@quantity)}
              {string($i/@unit)
            }</li>
        }</ul>
      }</li>
  }</ul>
};

declare function f-ueberblickskommentar:einband($tei as element(tei:teiHeader)){
  <p>{$tei/fileDesc/sourceDesc/msDesc/physDesc/bindingDesc//text()}</p>
};

declare function f-ueberblickskommentar:anklebungen($tei as element(tei:teiHeader)){
  let $types := distinct-values( $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab/hi/string(.) )
  let $out :=
      <ul>
          {for $item in $types
          where $item != 'Vakat-Seiten:' and $item != 'Blattfragmente:'
          return
              <li>{replace($item, ":", "")}
                  <ul>{local:textref(
                          $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/text() = $item]//text()[not(parent::tei:hi)]
                          )}
                  </ul>
              </li> }

      </ul>
  return if(not($out//li)) then 'Keine' else $out
};

declare function f-ueberblickskommentar:blattfragmente($tei as element(tei:teiHeader)){
  <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Blattfragmente:']
                      let $empty := starts-with(($item//text())[2], ' Keine')
                      return
                          <li class="{if( $empty = true() ) then 'OutOfOrder' else '' }">
                              {if( $empty = true() )
                              then substring-after(($item//text())[2], 'Keine ')
                              else tokenize(($item//text())[2], ';\s')[2]  }
                              <ul>
                                  { if( $empty = true() )
                                  then <li>Keine</li>
                                  else
                                      <li>{tokenize(string-join($item//text()[not(parent::tei:hi)][not(parent::tei:ref)]), ';\s')[1]};
                                          {for $text in ($item//text()[not(parent::tei:hi)])[position() != 1]
                                          return local:textref($text)}</li>
                                  }</ul></li>
  }</ul>
};

declare function f-ueberblickskommentar:vakat-seiten($tei as element(tei:teiHeader)){
  <ul>{
    for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/support/ab[hi/string(.) = 'Vakat-Seiten:']
    return
      <li>{$item//text()[not(parent::tei:hi)]}</li>
  }</ul>

};

declare function f-ueberblickskommentar:skizzen($tei as element(tei:teiHeader)){
  <ul>{
    for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/decoDesc/ab[hi/string(.) = 'Skizzen:']
    return
      <li>{
        for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)
      }</li>
  }</ul>
};

declare function f-ueberblickskommentar:stempel($tei as element(tei:teiHeader)){
  <ul>{for $item in $tei/fileDesc/sourceDesc/msDesc/physDesc/additions/ab[hi/string(.) = 'Stempel:'] return <li>{for $text in $item//text()[not(parent::tei:hi)] return local:textref($text)}</li>}</ul>
};

declare function f-ueberblickskommentar:foliierung($tei as element(tei:teiHeader)){
  $tei/fileDesc/sourceDesc/msDesc/physDesc/objectDesc/supportDesc/foliation//text()
};

declare function f-ueberblickskommentar:besonderheiten($tei as element(tei:teiHeader)){
  let $items := $tei//sourceDesc/msDesc/physDesc/handDesc/ab
  (: Do we find only entries with "Keine" in our sequence of Elements? :)
  let $count := count($items//ab[contains(., "Keine")]) eq count($items//ab)
  return
  <xhtml:ul>
      {if(not($count))
      then
          for $item in $items//ab
          where not($item[contains(., "Keine")])
          return
              <xhtml:li>{local:textref($item//text()[not( exists(parent::tei:hi) )])}</xhtml:li>
      else
          <xhtml:li>Keine</xhtml:li>
      }
  </xhtml:ul>
};
