xquery version "3.1";
module namespace transformTest="http://fontane-nb.dariah.eu/TransfoTest";

declare namespace test="http://exist-db.org/xquery/xqsuite";

import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "transform.xqm";

declare
  %test:name("transform get script")
  %test:args("")
  %test:assertTrue
function transformTest:script($n){
  let $n :=
      <node xmlns="http://www.tei-c.org/ns/1.0">
          Latf standard
          <handShift script="Latn clean"/>
          <seg>Latn clean</seg>
          <handShift script="Latf"/>
          <seg>Latf clean</seg>
          <seg xml:lang="en-Latn">Latn clean</seg>
          <seg xml:lang="en-Latn">Latn clean<handShift script="Latf"/>Latf clean<handShift script="Latn angular"/>Latn angular</seg>
      </node>

  let $return :=
      for $text in $n//text()
      let $script := fontaneTransfo:script($text)
      return
        if(normalize-space($text) != $script) then false() else true()
  return
      $return = true()
};
