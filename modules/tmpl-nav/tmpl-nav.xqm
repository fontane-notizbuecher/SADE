xquery version "3.1";
module namespace nav = "http://sade/tmpl-nav" ;

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";

(: TODO: Nav ohne Link :)

declare variable $nav:module-key := "tmpl-nav";

declare
    %templates:wrap
function nav:navitems($node as node(), $model as map(*)) as map(*) {

    let $confLocation := "/db/sade-projects/textgrid/navigation.xml"
    let $navitems :=
        if($confLocation != "") then
            doc( $confLocation)//navigation/*
        else
            $model("config")//module[string(@key)=$nav:module-key]//navigation/*
    return map { "navitems" : $navitems }

};

declare function nav:head($node as node(), $model as map(*)) {

    switch(local-name($model("item")))
        case "submenu" return
            element { node-name($node) } {
                    $node/@*, string($model("item")/@label), for $child in $node/node() return $child
            }
        case "item" return
            element { node-name($node) } {
                    attribute href {$model("item")/@link}, string($model("item")/@label)
            }
        default return
            <b>not defined: {node-name($model("item"))}</b>
};

declare
    %templates:wrap
function nav:subitems($node as node(), $model as map(*)) as map(*) {
    map{ "subitems" : $model("item")/*}
};

declare
function nav:subitem($node as node(), $model as map(*)) {
    if($model("subitem")/@class) then
        <span class="{$model("subitem")/@class}">&#160;</span>
    else if ($model("subitem")/name() != 'divider') then
        element a {
            if(string($model("subitem")/@link)) then attribute href { string($model("subitem")/@link)} else (),
            string($model("subitem")/@label)
        }
        else <span>&#160;</span>
};

declare function nav:edit($node as node(), $model as map(*)) {
let $new := request:get-parameter("new", "0")
return
    if (string($new) eq '0')
    then
        <div>
            <form class="form-horizontal" action="edit.html" method="GET">
                <textarea class="form-control" rows="10" name="new" />
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
            <pre>{serialize(doc('/db/sade-projects/' || config:get("project-id") || '/navigation.xml'))}</pre>
        </div>
    else
        let $item := xmldb:login('/db/sade-projects/' || config:get("project-id"), config:get("sade.user"), config:get("sade.password")),
            $egal := xmldb:store('/db/sade-projects/' || config:get("project-id"), 'navigation.xml', $new, "text/xml")
        return
        <div><pre>{serialize(doc('/db/sade-projects/' || config:get("project-id") || '/navigation.xml'))}</pre></div>
};

declare function nav:textgrid($node as node(), $model as map(*)) {
doc('/sade-projects/' || config:get("project-id") || '/navigation-'|| config:get("template") ||'.xml')
};
