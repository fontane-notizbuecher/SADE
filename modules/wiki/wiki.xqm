xquery version "3.1";
(:~
 : This module provides an interface to DokuWiki, transforms and stores exported
 : data including images from a configured wikispace. see config.xml.
 :
 : @author: Mathias Göbel
 : @version: 2.0
 : @see: https://fontane-nb.dariah.eu/doku.html
 :)

module namespace wiki="http://textgrid.de/ns/SADE/wiki";
import module namespace code="http://bdn-edition.de/ns/code-view";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace functx="http://www.functx.com";
import module namespace rest="http://exquery.org/ns/restxq";

declare namespace http="http://expath.org/ns/http-client";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

(:~
 : returns the transformed wiki-text without a superior element (e.g. <div class="dokuwiki"/>)
 : @param $id A pagename in the wikispace according to config.xml
 : @param $node The current node form the html-template with context
 : @param $model The current execution model
 :)
declare function wiki:include($node as node(), $model as map(*), $id as xs:string)
as node()* {
  wiki:dokuwiki($node, $model, $id)/*
};

(:~
 : returns the transformed wiki-text
 : @param $id A pagename in the wikispace according to config.xml
 : @param $node The current node form the html-template with context
 : @param $model The current execution model
 :)
declare function wiki:dokuwiki($node as node(), $model as map(*), $id as xs:string)
 as node()* {
(: check for valid id and replace id not in whitelist :)
let $id:= if($id = doc('/db/sade-projects/textgrid/data/xml/data/2shfj.xml')//text()) then $id else 'gesamtdokumentation'

let $doc :=
        doc(
            '/sade-projects/textgrid/data/xml/doku/' || $id || '.rev' || wiki:revision-local($id) || '.xml' )/div/*

let $reload :=
    if( wiki:resource-is-locked($id) )
    then
        element xhtml:div {
            attribute class {"alert alert-info alert-dismissible"},
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&#215;</a>,
            "Update in progress…"
        }
    else if(wiki:revision-remote($id) gt wiki:revision-local($id))
    then
        element xhtml:div {
                attribute class {"alert alert-info alert-dismissible"},
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&#215;</a>,
                "Update in progress…",
                element xhtml:script {
                    "$.get('api/doku/update/" || $id || "').done(function() {location.reload();});"
                }
            }
    else if(not($doc))
    then
         error(QName("FONTANE", "DOKU06"), "Unable to get a local or remote document. Is the remote up and are there valid credentials available to the processor?" )
    else ()

return
  element xhtml:div {
    attribute class {
      "dokuwiki",
      $id
      },
    $reload,
    $doc
  }
};

(:~
 : Gets the last revision number from the wikis feed and removes an old version
 : in the database.
 : @param $id The name of page in the wiki space
 : @param $lastRevinDb Revision number of the currently stored resource
 : @return the last revsion number from the remote wiki
 :)
declare function wiki:revision-remote($id as xs:string)
as xs:int {

let $url := 
    config:get("dokuwiki.url")
    || '/doku.php?do=revisions'
    || '&amp;u=' || config:get("dokuwiki.user")
    || '&amp;p=' || config:get("dokuwiki.password")
    || '&amp;id=fontane:' || $id

let $request := <hc:request method="get" href="{ $url }" />
let $wikiList :=
    try {
        hc:send-request($request)[2]
    } catch * {
        error(QName("FONTANE", "DOKU05"), "Unable to connect wiki page history. Are there valid credentials provided?" )
    }

let $lastRevinWiki :=
    $wikiList//xhtml:form[@id='page__revisions']/xhtml:div/xhtml:ul/xhtml:li[1]/xhtml:div/xhtml:input/@value

return
    if( $lastRevinWiki )
    then $lastRevinWiki => xs:int()
    else xs:int(-1)
};

(:~
 : Gets the currently stored revision number in the database for a given id
 : @param $id A pagename in the wikispace according to config.xml
 : @return the revision number stored in the database
 :)
declare function wiki:revision-local($id as xs:string)
as xs:int {
let $document-name :=
    xmldb:get-child-resources('/sade-projects/textgrid/data/xml/doku/')
        [substring-before(., '.rev') = $id]
        [not(ends-with(., ".lock.xml"))]

return
    if(not($document-name[1]))
    then xs:int(-1)
    else
        $document-name ! (substring-after(., '.rev') 
            => substring-before('.xml')
            => xs:int())
        => max()
};

declare function wiki:lastModProcessor() {
    xmldb:last-modified("/db/apps/SADE/modules/wiki", "wiki.xqm")
};

declare function wiki:lock-resource($id as xs:string) {
    xmldb:store('/sade-projects/textgrid/data/xml/doku/', $id || ".lock.xml", <lock/>)
};

declare function wiki:unlock-resource($id as xs:string) {
    xmldb:remove('/sade-projects/textgrid/data/xml/doku/', $id || ".lock.xml")
};

declare %private function wiki:resource-is-locked($id as xs:string)
as xs:boolean {
    xmldb:get-child-resources('/sade-projects/textgrid/data/xml/doku/') = ($id || ".lock.xml")
};

(:~
 : Compares local and remote version number and performs an update if necessary.
 : Usually via AJAX call provided by the templating function: <code>wiki:dokuwiki()</code>
 : @param $id The name of page in the wiki space
 : @return <true/>
 :)
declare
  %rest:GET
  %rest:path("/api/doku/update/{$id}")
function wiki:reload($id as xs:string) {
let $remote := wiki:revision-remote($id)
let $local := wiki:revision-local($id)

return
    if( $remote le wiki:revision-local($id) )
    then
        error(QName("FONTANE", "DOKU01"), "Remote revision (" || $remote || ") is lower or equal local revision (" || $local || ")." )
    else (: proceed :)
    if ( wiki:resource-is-locked($id) )
    then
        error(QName("FONTANE", "DOKU02"), "Resource is locked." )
    else (: proceed :)
let $lock := wiki:lock-resource($id)
let $url :=
        config:get("dokuwiki.url")
        || '/doku.php?id=fontane:' || $id
        || '&amp;rev=' || $remote
        || '&amp;u='|| config:get("dokuwiki.user")
        || '&amp;p='|| config:get("dokuwiki.password")
        || '&amp;do=export_xhtml',
    $export :=
        try {
            hc:send-request(<hc:request method="get" href="{ $url }" />)[2]//*[@class="dokuwiki export"]
        } catch * {
            error(QName("FONTANE", "DOKU04"), "unable to load resource." )
        },
    $result :=  $export,
    $result := <div>{wiki:dokuwikiparser( $result/*[not(@id='dw__toc')] )}</div>,
    $remove := if($local = -1) then true() else
                xmldb:remove('/sade-projects/textgrid/data/xml/doku/', $id || '.rev' || $local || '.xml'),
    $store := xmldb:store( '/sade-projects/textgrid/data/xml/doku/', $id || '.rev' || $remote || '.xml' , $result )

    let $unlock := wiki:unlock-resource($id)
return
    <true/>
};

declare function wiki:dokuwikiparser($nodes as node()*){
    wiki:dokuwikiparser($nodes, false())
};

declare function wiki:dokuwikiparser($nodes as node()*, $char as xs:boolean){
let $char := false()
for $node in $nodes return
    typeswitch($node)
        case element(xhtml:h1)
        return
            element xhtml:h2  {
                <div class="block-header">
                    <h2><span class="title">{wiki:dokuwikiparser($node/text(), $char)}</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span></h2>
                    <div id="toggleToc"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    <div id="dokuToc">
                        {  $node/root()//xhtml:div[@id="dw__toc"]/xhtml:div/xhtml:ul/xhtml:li/xhtml:ul ! wiki:dokuwikiTOC(.) }
                    </div>
                </div>
            }
        case element (xhtml:h2)
        return
            element xhtml:h3  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fa fa-link" aria-hidden="true"></i></a></span>,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:h3)
        return
            element xhtml:h4  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fa fa-link" aria-hidden="true"></i></a></span>,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:h4)
        return
            element xhtml:h5  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fa fa-link" aria-hidden="true"></i></a></span>,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:h5)
        return
            element xhtml:h6  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fa fa-link" aria-hidden="true"></i></a></span>,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element(xhtml:h6)
        return
            element xhtml:h7  {
                $node/@id,
                <span class="dokuLink"><a href="#{$node/@id}"> <i class="fa fa-link" aria-hidden="true"></i></a></span>,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case attribute(class) return ()
        case element (xhtml:div)
        return
            element xhtml:div {(
                if(contains($node/@class, "noteimportant"))
                then
                    attribute class { 'note' }
                else (),
                wiki:dokuwikiparser($node/node(), $char)
            )}
        case element(xhtml:br) return
            element xhtml:br {}
        case element (xhtml:table)
        return
            element xhtml:table {
                attribute class {'table'},
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:thead)
        return
            element xhtml:thead {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:th)
        return
            element xhtml:th {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:tr)
        return
            element xhtml:tr {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:tbody)
        return
            element xhtml:tbody {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:td)
        return
            element xhtml:td {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:a)
        return
            if(($node/@class eq 'media') or exists($node/xhtml:img)) then wiki:dokuwikiparser($node/node(), $char) else
            element xhtml:a {
                attribute href {
                    if($node/@class = "urlextern") then
                    (: wiki external links, should not link to test instance :)
                        attribute href {replace($node/@href, 'fontane-nb.dariah.eu/test/', 'fontane-nb.dariah.eu/')}
                    (: internal links :)
                    else if(starts-with($node/@href, '#'))
                        then string($node/@href)
                        else 
                            if($node/@href eq 'datenschutz.html')
                            then string($node/@href)
                            else
                        '?id='||substring-after($node/@href, 'fontane:')},
                $node/@rel,
                $node/@id,
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:p)
        return
            element xhtml:p {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:ul)
        return
            element xhtml:ul {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:li)
        return
            element xhtml:li {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:pre)
        return
            $node

        case element (xhtml:em)
        return
            element xhtml:em {
                if($node/@class = 'u') then attribute class {'underline'} else (),
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:strong)
        return
            element xhtml:strong {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:del)
        return
            element xhtml:del {
                wiki:dokuwikiparser($node/node(), $char)
            }
        case element (xhtml:img)
        return
            (element xhtml:img {
                $node/@alt,
                $node/@width,
                $node/@title,
                attribute class {'imgLazy'},
                attribute data-original { 'public/doku/' || $node/substring-after(@data-original, 'fontane:') },
                attribute src {"public/img/loader.svg"}
            }, () )
        case text() return $node
        default return wiki:dokuwikiparser($node/node(), $char)

};

declare function wiki:dokuwikiTOC($nodes as node()+) {
for $node in $nodes return
    typeswitch($node)
    case element (xhtml:div) return
        wiki:dokuwikiTOC($node/node())
    case element () return
        element { $node/local-name() } { $node/@*[local-name() != 'class'], wiki:dokuwikiTOC($node/node()) }
    case text() return $node
    default return wiki:dokuwikiTOC($node/node())
};