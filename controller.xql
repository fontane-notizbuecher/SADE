xquery version "3.1";

import module namespace config="http://textgrid.de/ns/SADE/config" at "modules/config/config.xqm";
import module namespace digilib="http://textgrid.info/namespaces/xquery/digilib" at "../textgrid-connect/digilibProxy.xqm";
import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo" at "modules/fontane/transform.xqm";

declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

declare function local:everything($project) {
    let $path := "/../sade-projects/"|| $project || "/" || config:get("data-dir") || "/" || substring-after($exist:path, $project)
    let $path := if( doc-available( $path ) ) then $path else "/../sade-projects/"|| $project || config:get("template") || substring-after($exist:path, $project)
    return
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
        <forward url="{$path}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>
};

let $project := tokenize( $exist:path, "/" )[. != ""][not(ends-with(., "html"))][1]
let $listproject := config:list-projects()[1]
return

    (: forward requests without resources to index.html :)
if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

    (: forward requests without project to the first project listed :)
else if (empty($project)) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{config:list-projects()[1]}/index.html"/>
    </dispatch>

else if($exist:resource eq "robots.txt") then local:everything($project)

    (: forward requests without resources to index.html :)
else if (ends-with($exist:path, "/") or empty($exist:resource)) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>

    (: forward REST requests to data collection of the project:)
else if (tokenize($exist:path, '/') = "rest") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/../sade-projects/{$project}/data/xml/{substring-after($exist:path, "/rest/")}"/>
    </dispatch>

    (: forward DIGILIB requests to the proxy function :)
else if (tokenize($exist:path, '/') = "digilib") then
    let $path := tokenize($exist:path, '/'),
        $id := $path[last()],
        $if-modified-since := request:get-header("if-modified-since")
    return
        digilib:proxy($project, $id, $if-modified-since)

    (: REST interface of the interface! :)
else if (tokenize($exist:path, '/') = "get") then
    let $reqid := request:get-parameter("id", "/xml/data/16b00.xml"),
        $id := $reqid => substring-after("/xml/data/") => substring-before(".xml"),
        $pagereq := request:get-parameter("page", "1r"),
        $alternative := contains($pagereq, "-alt"),
        $double := contains($pagereq, "-"),
        $page := if( $pagereq = "" )
                 then "outer_front_cover"
                 else
                    if($alternative or $double)
                    then $pagereq => substring-before("-")
                 else $pagereq,
        $docCollection := $config:projects-dir || $project ||"/data/xml/xhtml/"|| $id ||"/",
        $docpath := $docCollection || $page ||".xml",
        $doc := (doc( $docpath ), if($double) then doc( replace($docpath, $page || "\.xml" , $pagereq => substring-after("-") || ".xml")) else ()),
        $error := $doc//error,
        $content :=
            switch ($exist:resource)
                case "code.html" return (
                    replace(serialize($doc//xhtml:div[@class="teixml"]), "xhtml:", "")
                        (: replace the second heading. useful for double-page spread. :)
                        => replace("(<b>[\s\S]+?</b>[\s\S]*?)(<b>[\s\S]+?</b>)", "$1"),
                    serialize($error)
                )

                case "trans.html" return
                    if($alternative)
                    then serialize($doc/xhtml:body/xhtml:div/xhtml:div[not(@class="teixml")][not(@class="facs")])
                        => replace("xhtml:", "") => replace('class="surface', 'class="surface no-nesting')
                    else
                        serialize($doc/xhtml:body/xhtml:div/xhtml:div[not(@class="teixml")][not(@class="facs")])
                        => replace("xhtml:", "")
                case "facs.html" return
                    if ($alternative and $id="2128f")
                    (: using one transformation for two renderings :)
                    (: TODO: use this for 18vm7 1r as well
                       TODO: we are replacing instead of traversing the images :)
                    then replace(serialize($doc//xhtml:div[@class="facs"]), "xhtml:", "")
                        => replace("1671z", "16725")
                    else
                        replace(serialize($doc//xhtml:div[@class="facs"]), "xhtml:", "")

                case "toc.html" return
                    replace(serialize(doc( $docCollection || "toc.xml" )), "xhtml:", "")

                case "comm.html" return
                    replace(serialize($doc//xhtml:div[@class="notes"]), "xhtml:", "")

                default return "supported requests are: facs, trans, code"
        return
            response:stream($content, "method=text media-type=text/plain")

    (: the important stuff :)
else if(
        $exist:resource = "edition.html"
    and config:get("sade.develop") = "true"
    and string(request:get-cookie-value('fontaneAuth')) != config:get("secret")
    ) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <redirect url="login.html?redirect={$exist:path => substring-after("textgrid/")}?{request:get-query-string()}"/>
        </dispatch>
else if ( $exist:path = "/" || $project || "/" || $exist:resource ) then
    let $doc-path := $config:projects-dir || $project || config:get("template") || $exist:resource
    return
    (: the html page is run through view.xql to expand templates :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <!--view-->
            <forward url="{$exist:controller}/modules/view.xql" method="get">
                <add-parameter name="doc-path" value="{$doc-path}"/>
            </forward>
        <!--/view-->
		<error-handler>
			<forward url="{$exist:controller}/error-page.html" method="get"/>
			<forward url="{$exist:controller}/modules/view.xql"/>
		</error-handler>
    </dispatch>

    (: resource paths starting with $shared are loaded from the shared-resources app :)
else if (contains($exist:path, "/$shared/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/shared-resources/{substring-after($exist:path, '/$shared/')}">
            <set-header name="Cache-Control" value="max-age=3600, must-revalidate"/>
        </forward>
    </dispatch>

else if (contains($exist:path, "/api/")) then
    let $api-call := substring-after($exist:path, '/api/')
    let $query-string := request:get-query-string()
    let $path := if($query-string) then $api-call || "?" || $query-string else $api-call
    return
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <redirect url="/exist/restxq/api/{$path}"/>
        </dispatch>

else if (contains($exist:path, "/doc/api/") or contains($exist:path, "/openapi/")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="/openapi/{ $exist:path => substring-after("/openapi/") => replace("json", "xq") }" method="get">
            <add-parameter name="target" value="{ substring-after($exist:root, "://") || $exist:controller }"/>
            <add-parameter name="register" value="false"/>
        </forward>
    </dispatch>
else
    (:  check if the resource comes from the template or the data
        we try to leave the template manipulation as minimal as
        possible
    :)
    local:everything($project)
