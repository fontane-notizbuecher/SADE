# About SADE
The Scalable Architecture for Digital Editions (SADE) tries to meet the requirement for an easy to use publication system for electronic resources and Digital Editions. It is an attempt to provide a modular concept for publishing scholarly editions in a digital medium, based on open standards. Furthermore it is a distribution of open source software tools for the easy publication of Digital Editions and digitized texts in general out of the box. SADE is a scalable system that can be adapted by different projects which follow the TEI guidelines or other XML based formats.

This is the modified version for Fontane Notizbücher. It is used together with several other packages (see dependencies) in addition to the TextGrid system.

# Distribution
This package is available via the [DARIAH-DE eXist-repo](https://ci.de.dariah.eu/exist-repo/index.html). There are two flavors available: «SADE-fontane» (for production) and «SADE-fontane-develop».

# Development
We follow the git-flow.

Find the git repo at [GWDG\`s GitLab](https://gitlab.gwdg.de/fontane-notizbuecher).

Use the issue tracker at [GWDG\`s GitLab](https://gitlab.gwdg.de/fontane-notizbuecher).

Go for local development with the ant tasks. In addition to the `*.xar` file containing this package all dependencies are loaded via `ant test`. This will place the version of eXist declared in `build.properties` and comes with all needed packages.
For a new up and running instance two commands are needed:
```bash
ant test &&
bash test/eXist-db-*/bin/startup.sh
```
This will result in a local instance at [localhost:8080](http://localhost:8080/exist/).

CAUTION: A new build will remove the instance!

# Build from source
`ant` is used as build tool. The standard target will place a `*.xar` file in the build directory.
